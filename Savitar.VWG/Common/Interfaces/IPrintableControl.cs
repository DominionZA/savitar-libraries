﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.VWG.Common.Interfaces
{
  public interface IPrintableControl
  {
    void Print(System.Web.UI.HtmlTextWriter objHtmlWriter);
  }
}
