﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Gizmox.WebGUI.Common.Resources;
using Gizmox.WebGUI.Common.Gateways;

namespace Savitar.VWG.Common.Resources
{
  public class DataResourceHandle : GatewayResourceHandle
  {
    public DataResourceHandle(System.IO.Stream stream, string contentType)
      : base(new Gizmox.WebGUI.Common.Gateways.GatewayReference(new DataComponent(stream, contentType), "SendData"))
    {
    }

    public DataResourceHandle(byte[] imageData, string contentType)
      : base(new Gizmox.WebGUI.Common.Gateways.GatewayReference(new DataComponent(imageData, contentType), "SendData"))
    {
    }

    private class DataComponent : Gizmox.WebGUI.Forms.Component
    {      
      private byte[] data;
      private string contentType;

      public DataComponent(byte[] data, string contentType)
      {        
        if (data == null)
          throw new HttpException(this.GetType().ToString() + ".DataComponent cannot be passed a null data arguement");
        if (data.Length == 0)
          throw new HttpException(this.GetType().ToString() + ".DataComponent cannot be passed a zero length data arguement");

        this.data = data;
        this.contentType = contentType;
        this.RegisterSelf();
      }

      public DataComponent(System.IO.Stream stream, string contentType)
      {
        data = new byte[stream.Length];
        stream.Position = 0;
        int i = 0;
        while (i < stream.Length)
          data[i++] = Convert.ToByte(stream.ReadByte());

        this.contentType = contentType;
        this.RegisterSelf();
      }

      protected override Gizmox.WebGUI.Common.Interfaces.IGatewayHandler ProcessGatewayRequest(HttpContext objHttpContext, string strAction)
      {
        if (strAction == "SendData")
        {
          if ((data == null) || (data.Length == 0))
            throw new Exception(this.GetType().ToString() + ".ProcessGatewayRequest is unable to send image data as the data property is null or empty");

          // Set ContentType
          if (!String.IsNullOrEmpty(contentType))
            objHttpContext.Response.ContentType = contentType;

          // Disable Caching
          objHttpContext.Response.Expires = -1;

          // Send Data to Browser          
          objHttpContext.Response.BinaryWrite(data);

          // Close the HTTP Response Stream
          // Used instead of objResponse.End()
          // http://support.microsoft.com/kb/312629/
          objHttpContext.Response.Flush();
          objHttpContext.ApplicationInstance.CompleteRequest();

          // Clean up
          //data = null;
        }

        return null;
      }
     
      protected override void Dispose(bool disposing)
      {
        // Remove Myself from the Current VWG Application
        this.UnRegisterSelf();

        base.Dispose(disposing);
      }
    }
  }
}
