﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.VWG.Common.Resources
{
  public class JPegDataResourceHandle : DataResourceHandle
  {
    public JPegDataResourceHandle(System.IO.Stream stream) : base(stream, "image/jpeg") { }
    public JPegDataResourceHandle(byte[] jpegData) : base(jpegData, "image/jpeg") { }
  }

  public class PNGDataResourceHandle : DataResourceHandle
  {
    public PNGDataResourceHandle(System.IO.Stream stream) : base(stream, "") { }
    public PNGDataResourceHandle(byte[] jpegData) : base(jpegData, "") { }
  }
}
