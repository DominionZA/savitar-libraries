﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savitar.VWG.Images
{
  public static class Buttons
  {
    public static Gizmox.WebGUI.Common.Resources.ResourceHandle Cancel()
    {
      return Savitar.VWG.Imaging.GetResourceHandle(Properties.Resources.Cancel__Red, System.Drawing.Imaging.ImageFormat.Jpeg);
    }

    public static Gizmox.WebGUI.Common.Resources.ResourceHandle Text()
    {
      return Savitar.VWG.Imaging.GetResourceHandle(Properties.Resources.Text, System.Drawing.Imaging.ImageFormat.Png);
    }

    public static Gizmox.WebGUI.Common.Resources.ResourceHandle Graph()
    {
      return Savitar.VWG.Imaging.GetResourceHandle(Properties.Resources.Graph, System.Drawing.Imaging.ImageFormat.Png);
    }
  }
}
