﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;
using Savitar.Data.Interfaces;
using Savitar.Model;

namespace Savitar.VWG.Extensions
{
    public static class ControlBindingExtensions
    {
        public static void Bind<TEntity>(this ComboBox comboBox, IList<TEntity> entities)
            where TEntity : class, IEntity
        {
            comboBox.DataSource = null;            
            comboBox.ValueMember = "Id";
            comboBox.DisplayMember = "Text";
            comboBox.DataSource = entities.OrderBy(x => x.Text).ToList();
        }

        public static void BindWithSelect<TEntity>(this ComboBox comboBox, IList<TEntity> entities)
            where TEntity : class, IEntity
        {
            Bind(comboBox, entities);

            if (comboBox.Items.Count > 0)
                comboBox.SelectedIndex = 0;
        }

        public static void Bind<TEntity>(this CheckedListBox checkedListBox, IList<TEntity> entities, bool isChecked)
            where TEntity : class, IEntity
        {
            checkedListBox.DataSource = null;

            checkedListBox.ValueMember = "Id";
            checkedListBox.DataSource = entities;

            if (isChecked)
            {
                for (int index = 0; index < checkedListBox.Items.Count; index++)
                    checkedListBox.SetItemChecked(index, true);
            }
        }

        public static void Bind<TEntity>(CheckedListBox checkedListBox, IList<TEntity> entities)
            where TEntity : class, IEntity
        {
            checkedListBox.Bind(entities, false);
        }

        public static void Bind<TEntity>(this ListBox control, IList<TEntity> entities)
            where TEntity : class, IEntity
        {
            control.DataSource = null;

            control.ValueMember = "Id";
            control.DataSource = entities;
        }

        public static void SelectFirst(ComboBox comboBox)
        {
            if (comboBox.Items.Count == 0)
                return;

            comboBox.SelectedIndex = 0;
        }

        public static void BindSort(ComboBox comboBox)
        {
            foreach (Types.Sort item in Enum.GetValues(typeof(Types.Sort)))
                comboBox.Items.Add(item);

            comboBox.Sorted = true;
        }

        public static void Bind<TEntity>(this DataGridView @this, IList<TEntity> entities)
            where TEntity : class, IEntity
        {
            
        }
    }
}
