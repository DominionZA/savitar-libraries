﻿using System;
using System.Collections.Generic;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
    public class Data
    {
        public static void Bind(ComboBox comboBox, Savitar.Data.Interfaces.ISavItemList list)
        {
            comboBox.DataSource = null;

            comboBox.DisplayMember = list.GetDisplayMember();
            comboBox.ValueMember = list.GetValueMember();
            comboBox.DataSource = list;
        }        

        public static void BindWithSelect(ComboBox comboBox, Savitar.Data.Interfaces.ISavItemList list)
        {
            Bind(comboBox, list);
            if (comboBox.Items.Count > 0)
                comboBox.SelectedIndex = 0;
        }

        public static void Bind(CheckedListBox checkedListBox, Savitar.Data.Interfaces.ISavItemList items, bool isChecked)
        {
            checkedListBox.DataSource = null;

            checkedListBox.DisplayMember = items.GetDisplayMember();
            checkedListBox.ValueMember = items.GetValueMember();
            checkedListBox.DataSource = items;

            if (isChecked)
            {
                for (int index = 0; index < checkedListBox.Items.Count; index++)
                    checkedListBox.SetItemChecked(index, true);
            }
        }

        public static void Bind(CheckedListBox checkedListBox, Savitar.Data.Interfaces.ISavItemList items)
        {
            Bind(checkedListBox, items, false);
        }                

        public static void Bind(ListBox listBox, Savitar.Data.Interfaces.ISavItemList items)
        {
            listBox.DataSource = null;

            listBox.DisplayMember = items.GetDisplayMember();
            listBox.ValueMember = items.GetValueMember();
            listBox.DataSource = items;
        }

        public static void Bind(ComboBox comboBox, List<Savitar.Data.Interfaces.ISavItem> items)
        {
            comboBox.DataSource = null;

            if (items.Count != 0)
            {
                int index = 0;
                if ((items[0] is Savitar.Data.SimpleSavItem) && (items.Count > 1))
                    index = 1; // Get the second item rather. Sometimes the first item is Savitar.Data.SimpleSavItem while the rest of the collection.

                string displayMember = items[index].GetDisplayMember();
                if (String.IsNullOrEmpty(displayMember))
                    throw new Exception(items[index].GetType() + ".GetDisplayMember() returned an empty string");

                string valueMember = items[index].GetValueMember();
                if (String.IsNullOrEmpty(valueMember))
                    throw new Exception(items[index].GetType() + ".GetValueMember() returned an empty string");

                comboBox.DisplayMember = displayMember;
                comboBox.ValueMember = valueMember;
                comboBox.DataSource = items;
            }
        }

        public static void SelectFirst(ComboBox comboBox)
        {
            if (comboBox.Items.Count == 0)
                return;

            comboBox.SelectedIndex = 0;
        }

        public static void BindSort(ComboBox comboBox)
        {
            foreach (Types.Sort item in Enum.GetValues(typeof(Types.Sort)))
                comboBox.Items.Add(item);

            comboBox.Sorted = true;
        }
    }
}
