﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savitar.VWG
{
  public class Image
  {
    public static Gizmox.WebGUI.Common.Resources.ResourceHandle Get(byte[] imageData)
    {
      if ((imageData == null) || (imageData.Length == 0))
        return Savitar.VWG.Imaging.GetResourceHandle(Properties.Resources.Cancel__Red, System.Drawing.Imaging.ImageFormat.Jpeg);

      return new Savitar.VWG.Common.Resources.JPegDataResourceHandle(imageData);
    }

    public static Gizmox.WebGUI.Common.Resources.ResourceHandle Get(string imageName)
    {
      Gizmox.WebGUI.Common.Resources.ResourceHandle rh = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      rh.File = imageName;
      return rh;
    }
  }
}
