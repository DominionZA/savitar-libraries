﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
  public class MsgBox
  {
    public static void ShowMessage(string message, string title)
    {
      MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    public static void ShowError(Exception ex, string title)
    {
      string msg = ex.Message;
      if ((ex.InnerException != null) && (!String.IsNullOrEmpty(ex.InnerException.Message)))
        msg += "\r\n\r\n" + ex.InnerException.Message;

      MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    public static void ShowError(Exception ex)
    {
      MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    public static void ShowError(string message)
    {
      MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    public static void ShowError(string message, string title)
    {
      MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
    }
  }
}
