using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Forms
{
	public partial class SavPropertyGrid : Gizmox.WebGUI.Forms.PropertyGrid
	{
    public bool FilterByIsPropertyGrid
    {
      get { return _FilterByIsPropertyGrid; }
      set
      {
        if (_FilterByIsPropertyGrid == value)
          return;
        _FilterByIsPropertyGrid = value;

        if (_FilterByIsPropertyGrid)
        {
          Savitar.IsPropertyGridAttribute att = new Savitar.IsPropertyGridAttribute();
          this.BrowsableAttributes = new AttributeCollection(new Attribute[] { att });
        }
        else
          this.BrowsableAttributes = null;
      }
    } bool _FilterByIsPropertyGrid;
    
    public SavPropertyGrid() : base()
		{
			InitializeComponent();
      this.FilterByIsPropertyGrid = true;
		}    

    public Gizmox.WebGUI.Forms.ToolBar GetToolBar()
    {
      return (ToolBar)typeof(Gizmox.WebGUI.Forms.PropertyGrid).InvokeMember("mobjToolBar",
                                                        System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.NonPublic |
                                                        System.Reflection.BindingFlags.Instance,
                                                        null,
                                                        this,
                                                        null);
    }

    public void AddButton(System.Drawing.Bitmap image, System.Drawing.Imaging.ImageFormat imageFormat, string toolTip, EventHandler clickEventHandler)
    {
      this.AddButton(Savitar.VWG.Imaging.GetResourceHandle(image, imageFormat), toolTip, clickEventHandler);
    }

    public void AddButton(Gizmox.WebGUI.Common.Resources.ResourceHandle image, string toolTip, EventHandler clickEventHandler)
    {
      ToolBarButton button = new ToolBarButton();
      button.Image = image;
      button.ToolTipText = toolTip;
      if (clickEventHandler != null)
        button.Click += clickEventHandler;

      this.AddButton(button);
    }

    public void AddButton(Gizmox.WebGUI.Forms.ToolBarButton button)
    {
      GetToolBar().Buttons.Add(button);
    }    
	}
}
