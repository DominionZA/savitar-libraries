var VideoRate = 1;
var WMP;
var ID;

function MediaPlayer_Init(id) {  
  WMP = Web_GetTargetElementByDataId(id, window)
  if (!WMP) {
    alert("[MediaPlayer_Init] Cannot find MediaPlayer with ID " + id);
    return;
  }

  ID = id;
  WMP.attachEvent("playstatechange", handlePlayerStateChange);
  WMP.attachEvent("error", handlePlayerErrors);
  WMP.attachEvent("statuschange", handleStatusChange);
  WMP.attachEvent("scriptcommand", handleScriptCommand); 
}

function IncRate(direction) {
  if ((direction > 0) && (VideoRate < 0))
    VideoRate = 1;
  else if ((direction < 0) && (VideoRate > 0))
    VideoRate = -1;

  VideoRate *= 2;
  WMP.Settings.Rate = VideoRate;

  var objEvent = Events_CreateEvent(ID, "OnRateChanged");
  Events_SetEventAttribute(objEvent, "VideoRate", VideoRate);
  Events_SetEventAttribute(objEvent, "CallbackID", ID);
  Events_RaiseEvents();
}

function Stop() {
  WMP.controls.stop();
}

function Pause() {
  WMP.controls.pause();
}

function Play() {
  WMP.controls.play();
}

function GotoPosition(value) 
{
  WMP.Controls.currentPosition = value;
}

function SetRate(value) {
  WMP.Settings.rate = value;
}

function SetURL(value) {
  WMP.Settings.baseURL = value;
}

function handlePlayerStateChange(value) {
  var pos = WMP.Controls.CurrentPosition;
  var rate = WMP.Settings.rate;
  
  if (value == 3) // Playing
    VideoRate = 1;
    
  var objEvent = Events_CreateEvent(ID, "OnPlayerStateChanged");
  Events_SetEventAttribute(objEvent, "PlayerState", value);
  Events_SetEventAttribute(objEvent, "Position", pos);
  Events_SetEventAttribute(objEvent, "Rate", rate);
  Events_SetEventAttribute(objEvent, "CallbackID", ID);
  Events_RaiseEvents();
}

function handlePlayerErrors(error_object) { 
  alert("An error occurred"); 
} 

function handleStatusChange(new_status_value) {
  //alert("The status changed to " + new_status_value);
} 

function handleScriptCommand(commandType, commandText) { 
  alert("Script command sent of type " + commandType + " with value " + commandText); 
} 
