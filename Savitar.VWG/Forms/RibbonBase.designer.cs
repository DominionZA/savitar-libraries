namespace Savitar.VWG.Forms
{
    partial class RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Visaul WebGui Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof (RibbonBase));
            this.HeaderPanel = new Gizmox.WebGUI.Forms.Panel();
            this.MainRibbonBar = new Gizmox.WebGUI.Forms.RibbonBar();
            this.HomePage = new Gizmox.WebGUI.Forms.RibbonBarPage();
            this.MyAccountGroup = new Gizmox.WebGUI.Forms.RibbonBarGroup();
            this.MyDetailsButton = new Gizmox.WebGUI.Forms.RibbonBarButtonItem();
            this.LogoutButton = new Gizmox.WebGUI.Forms.RibbonBarButtonItem();
            this.ModuleInfoPanel = new Gizmox.WebGUI.Forms.Panel();
            this.UserLabel = new Gizmox.WebGUI.Forms.Label();
            this.ModuleNameLabel = new Gizmox.WebGUI.Forms.Label();
            this.FooterPanel = new Gizmox.WebGUI.Forms.Panel();
            this.SaveButton = new Gizmox.WebGUI.Forms.Button();
            this.AddButton = new Gizmox.WebGUI.Forms.Button();
            this.EditButton = new Gizmox.WebGUI.Forms.Button();
            this.DeleteButton = new Gizmox.WebGUI.Forms.Button();
            this.errorProvider = new Gizmox.WebGUI.Forms.ErrorProvider();
            this.tabControl = new Gizmox.WebGUI.Forms.WorkspaceTabs();
            this.panel1 = new Gizmox.WebGUI.Forms.Panel();
            this.FooterLabel = new Gizmox.WebGUI.Forms.Label();
            this.panel2 = new Gizmox.WebGUI.Forms.Panel();
            this.HeaderPanel.SuspendLayout();
            this.ModuleInfoPanel.SuspendLayout();
            this.FooterPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.Clear;
            this.HeaderPanel.Controls.Add(this.MainRibbonBar);
            this.HeaderPanel.Controls.Add(this.ModuleInfoPanel);
            this.HeaderPanel.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.HeaderPanel.Location = new System.Drawing.Point(0, 0);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Size = new System.Drawing.Size(800, 141);
            this.HeaderPanel.TabIndex = 2;
            // 
            // MainRibbonBar
            // 
            this.MainRibbonBar.AutoScroll = true;
            this.MainRibbonBar.AutoSize = true;
            this.MainRibbonBar.AutoValidate = Gizmox.WebGUI.Forms.AutoValidate.EnablePreventFocusChange;
            this.MainRibbonBar.Location = new System.Drawing.Point(0, 0);
            this.MainRibbonBar.Name = "MainRibbonBar";
            this.MainRibbonBar.Pages.Add(this.HomePage);
            this.MainRibbonBar.SelectedIndex = 0;
            this.MainRibbonBar.TabIndex = 0;
            // 
            // HomePage
            // 
            this.HomePage.Groups.Add(this.MyAccountGroup);
            this.HomePage.Text = "Home";
            // 
            // MyAccountGroup
            // 
            this.MyAccountGroup.Items.Add(this.MyDetailsButton);
            this.MyAccountGroup.Items.Add(this.LogoutButton);
            this.MyAccountGroup.Text = "My Account";
            // 
            // MyDetailsButton
            // 
            this.MyDetailsButton.ClientAction = null;
            this.MyDetailsButton.Image =
                new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("MyDetailsButton.Image"));
            this.MyDetailsButton.Text = "My Details";
            this.MyDetailsButton.Click += new System.EventHandler(this.MyDetailsButton_Click);
            // 
            // LogoutButton
            // 
            this.LogoutButton.ClientAction = null;
            this.LogoutButton.Image =
                new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("LogoutButton.Image"));
            this.LogoutButton.Text = "Logout";
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // ModuleInfoPanel
            // 
            this.ModuleInfoPanel.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.Clear;
            this.ModuleInfoPanel.Controls.Add(this.UserLabel);
            this.ModuleInfoPanel.Controls.Add(this.ModuleNameLabel);
            this.ModuleInfoPanel.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.ModuleInfoPanel.Location = new System.Drawing.Point(0, 112);
            this.ModuleInfoPanel.Name = "ModuleInfoPanel";
            this.ModuleInfoPanel.Size = new System.Drawing.Size(800, 29);
            this.ModuleInfoPanel.TabIndex = 2;
            // 
            // UserLabel
            // 
            this.UserLabel.BackColor = System.Drawing.Color.MidnightBlue;
            this.UserLabel.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.UserLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.UserLabel.ForeColor = System.Drawing.Color.White;
            this.UserLabel.Location = new System.Drawing.Point(512, 0);
            this.UserLabel.Name = "UserLabel";
            this.UserLabel.Size = new System.Drawing.Size(288, 29);
            this.UserLabel.TabIndex = 1;
            this.UserLabel.Text = "User Info Here";
            this.UserLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ModuleNameLabel
            // 
            this.ModuleNameLabel.BackColor = System.Drawing.Color.MidnightBlue;
            this.ModuleNameLabel.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.ModuleNameLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.ModuleNameLabel.ForeColor = System.Drawing.Color.White;
            this.ModuleNameLabel.Location = new System.Drawing.Point(0, 0);
            this.ModuleNameLabel.Name = "ModuleNameLabel";
            this.ModuleNameLabel.Size = new System.Drawing.Size(800, 29);
            this.ModuleNameLabel.TabIndex = 1;
            this.ModuleNameLabel.Text = "Module Info Here";
            this.ModuleNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FooterPanel
            // 
            this.FooterPanel.BorderWidth = new Gizmox.WebGUI.Forms.BorderWidth(2);
            this.FooterPanel.Controls.Add(this.SaveButton);
            this.FooterPanel.Controls.Add(this.AddButton);
            this.FooterPanel.Controls.Add(this.EditButton);
            this.FooterPanel.Controls.Add(this.DeleteButton);
            this.FooterPanel.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
            this.FooterPanel.Location = new System.Drawing.Point(725, 0);
            this.FooterPanel.Name = "FooterPanel";
            this.FooterPanel.Size = new System.Drawing.Size(60, 424);
            this.FooterPanel.TabIndex = 4;
            // 
            // SaveButton
            // 
            this.SaveButton.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.SaveButton.Enabled = false;
            this.SaveButton.Image =
                new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("SaveButton.Image"));
            this.SaveButton.Location = new System.Drawing.Point(0, 135);
            this.SaveButton.Margin = new Gizmox.WebGUI.Forms.Padding(5, 0, 0, 10);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(57, 60);
            this.SaveButton.TabIndex = 0;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.AddButton.Enabled = false;
            this.AddButton.Image =
                new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("AddButton.Image"));
            this.AddButton.Location = new System.Drawing.Point(0, 90);
            this.AddButton.Margin = new Gizmox.WebGUI.Forms.Padding(5, 0, 0, 10);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(57, 60);
            this.AddButton.TabIndex = 0;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.EditButton.Enabled = false;
            this.EditButton.Image =
                new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("EditButton.Image"));
            this.EditButton.Location = new System.Drawing.Point(0, 45);
            this.EditButton.Margin = new Gizmox.WebGUI.Forms.Padding(5, 0, 0, 10);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(57, 60);
            this.EditButton.TabIndex = 0;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
            this.DeleteButton.Enabled = false;
            this.DeleteButton.Image =
                new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("DeleteButton.Image"));
            this.DeleteButton.Location = new System.Drawing.Point(0, 0);
            this.DeleteButton.Margin = new Gizmox.WebGUI.Forms.Padding(5, 0, 0, 10);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(57, 60);
            this.DeleteButton.TabIndex = 0;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkRate = 3;
            this.errorProvider.BlinkStyle = Gizmox.WebGUI.Forms.ErrorBlinkStyle.BlinkIfDifferentError;
            // 
            // tabControl
            // 
            this.tabControl.Alignment = Gizmox.WebGUI.Forms.TabAlignment.Top;
            this.tabControl.ClientBehavior = Gizmox.WebGUI.Forms.TabControlClientBehavior.DrawingOptimized;
            this.tabControl.CustomStyle = "Workspace";
            this.tabControl.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.Size = new System.Drawing.Size(725, 424);
            this.tabControl.TabIndex = 7;
            this.tabControl.CloseClick += new System.EventHandler(this.tabControl_CloseClick);
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.FooterLabel);
            this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 577);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 23);
            this.panel1.TabIndex = 8;
            // 
            // FooterLabel
            // 
            this.FooterLabel.BackColor = System.Drawing.Color.MidnightBlue;
            this.FooterLabel.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
            this.FooterLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.FooterLabel.ForeColor = System.Drawing.Color.White;
            this.FooterLabel.Location = new System.Drawing.Point(0, 0);
            this.FooterLabel.Name = "FooterLabel";
            this.FooterLabel.Size = new System.Drawing.Size(800, 23);
            this.FooterLabel.TabIndex = 0;
            this.FooterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Anchor =
                ((Gizmox.WebGUI.Forms.AnchorStyles)
                 ((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                    | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                   | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tabControl);
            this.panel2.Controls.Add(this.FooterPanel);
            this.panel2.Location = new System.Drawing.Point(12, 147);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(782, 424);
            this.panel2.TabIndex = 9;
            // 
            // RibbonBase
            // 
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderPanel);
            this.Icon = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("$this.Icon"));
            this.Size = new System.Drawing.Size(800, 600);
            this.Load += new System.EventHandler(this.Main_Load);
            this.HeaderPanel.ResumeLayout(false);
            this.ModuleInfoPanel.ResumeLayout(false);
            this.FooterPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Gizmox.WebGUI.Forms.Panel HeaderPanel;
        private Gizmox.WebGUI.Forms.Panel ModuleInfoPanel;
        private Gizmox.WebGUI.Forms.Label UserLabel;
        private Gizmox.WebGUI.Forms.Label ModuleNameLabel;
        private Gizmox.WebGUI.Forms.RibbonBar MainRibbonBar;
        private Gizmox.WebGUI.Forms.RibbonBarPage HomePage;
        private Gizmox.WebGUI.Forms.RibbonBarGroup MyAccountGroup;
        private Gizmox.WebGUI.Forms.RibbonBarButtonItem MyDetailsButton;
        private Gizmox.WebGUI.Forms.RibbonBarButtonItem LogoutButton;
        private Gizmox.WebGUI.Forms.Panel FooterPanel;
        private Gizmox.WebGUI.Forms.ErrorProvider errorProvider;
        private Gizmox.WebGUI.Forms.WorkspaceTabs tabControl;
        private Gizmox.WebGUI.Forms.Panel panel1;
        private Gizmox.WebGUI.Forms.Label FooterLabel;
        private Gizmox.WebGUI.Forms.Panel panel2;
        public Gizmox.WebGUI.Forms.Button AddButton;
        public Gizmox.WebGUI.Forms.Button EditButton;
        public Gizmox.WebGUI.Forms.Button DeleteButton;
        public Gizmox.WebGUI.Forms.Button SaveButton;
    }
}