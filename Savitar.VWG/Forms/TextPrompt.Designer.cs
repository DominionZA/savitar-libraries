namespace Savitar.VWG.Forms
{
  partial class TextPrompt
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      Gizmox.WebGUI.Common.Resources.ImageResourceHandle imageResourceHandle1 = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      Gizmox.WebGUI.Common.Resources.ImageResourceHandle imageResourceHandle2 = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      this.PromptLabel = new Gizmox.WebGUI.Forms.Label();
      this.PromptTextBox = new Gizmox.WebGUI.Forms.TextBox();
      this.CancelButtonX = new Gizmox.WebGUI.Forms.Button();
      this.OKButtonX = new Gizmox.WebGUI.Forms.Button();
      this.SuspendLayout();
      // 
      // PromptLabel
      // 
      this.PromptLabel.Location = new System.Drawing.Point(12, 9);
      this.PromptLabel.Name = "PromptLabel";
      this.PromptLabel.Size = new System.Drawing.Size(354, 23);
      this.PromptLabel.TabIndex = 0;
      this.PromptLabel.Text = "label1";
      this.PromptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // PromptTextBox
      // 
      this.PromptTextBox.Location = new System.Drawing.Point(15, 36);
      this.PromptTextBox.Name = "PromptTextBox";
      this.PromptTextBox.Size = new System.Drawing.Size(351, 20);
      this.PromptTextBox.TabIndex = 1;
      // 
      // CancelButtonX
      // 
      imageResourceHandle1.File = "Cancel.png";
      this.CancelButtonX.Image = imageResourceHandle1;
      this.CancelButtonX.Location = new System.Drawing.Point(321, 62);
      this.CancelButtonX.Name = "CancelButtonX";
      this.CancelButtonX.Size = new System.Drawing.Size(45, 45);
      this.CancelButtonX.TabIndex = 2;
      this.CancelButtonX.Click += new System.EventHandler(this.CancelButtonX_Click);
      // 
      // OKButtonX
      // 
      imageResourceHandle2.File = "Ok.png";
      this.OKButtonX.Image = imageResourceHandle2;
      this.OKButtonX.Location = new System.Drawing.Point(270, 62);
      this.OKButtonX.Name = "OKButtonX";
      this.OKButtonX.Size = new System.Drawing.Size(45, 45);
      this.OKButtonX.TabIndex = 2;
      this.OKButtonX.Click += new System.EventHandler(this.OKButtonX_Click);
      // 
      // TextPrompt
      // 
      this.AcceptButton = this.OKButtonX;
      this.CancelButton = this.CancelButtonX;
      this.Controls.Add(this.OKButtonX);
      this.Controls.Add(this.CancelButtonX);
      this.Controls.Add(this.PromptTextBox);
      this.Controls.Add(this.PromptLabel);
      this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.FixedDialog;
      this.Size = new System.Drawing.Size(378, 113);
      this.Text = "TextPrompt";
      this.Load += new System.EventHandler(this.TextPrompt_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.Label PromptLabel;
    private Gizmox.WebGUI.Forms.TextBox PromptTextBox;
    private Gizmox.WebGUI.Forms.Button CancelButtonX;
    private Gizmox.WebGUI.Forms.Button OKButtonX;


  }
}