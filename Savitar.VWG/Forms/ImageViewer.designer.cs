using System.Drawing;

namespace Savitar.VWG.Forms
{
	partial class ImageViewer
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.PrintButton = new Gizmox.WebGUI.Forms.Button();
      this.pictureBox = new Savitar.VWG.Forms.PictureBox();
      this.SuspendLayout();
      // 
      // PrintButton
      // 
      this.PrintButton.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.PrintButton.Location = new System.Drawing.Point(437, 12);
      this.PrintButton.Name = "PrintButton";
      this.PrintButton.Size = new System.Drawing.Size(40, 40);
      this.PrintButton.TabIndex = 1;
      this.PrintButton.UseVisualStyleBackColor = true;
      this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
      // 
      // pictureBox
      // 
      this.pictureBox.AllowFullScreenView = false;
      this.pictureBox.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.None;
      this.pictureBox.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
      this.pictureBox.Image = null;
      this.pictureBox.Location = new System.Drawing.Point(0, 0);
      this.pictureBox.Name = "pictureBox";
      this.pictureBox.Size = new System.Drawing.Size(489, 420);
      this.pictureBox.SizeMode = Gizmox.WebGUI.Forms.PictureBoxSizeMode.CenterImage;
      this.pictureBox.TabIndex = 0;
      this.pictureBox.TabStop = false;
      // 
      // ImageViewer
      // 
      this.Controls.Add(this.PrintButton);
      this.Controls.Add(this.pictureBox);
      this.Size = new System.Drawing.Size(489, 420);
      this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterScreen;
      this.WindowState = Gizmox.WebGUI.Forms.FormWindowState.Maximized;
      this.ResumeLayout(false);

		}

		#endregion

    private Savitar.VWG.Forms.PictureBox pictureBox;
    private Gizmox.WebGUI.Forms.Button PrintButton;

  }
}
