#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Microsoft.Reporting.WebForms;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.Forms
{
    public partial class ReportViewer : Form
    {
        private bool isReportLoaded = false;

        public class SubReport
        {
            public string Name { get; set; }
            public System.IO.MemoryStream Definition { get; set; }
        }

        public delegate System.IO.Stream GetRDLCDefinitionDelegate();

        public event GetRDLCDefinitionDelegate GetRDLCDefinition;

        public delegate void SubreportProcessingDelegate(object sender, SubreportProcessingEventArgs e);

        public event SubreportProcessingDelegate SubreportProcessing;

        public delegate List<SubReport> GetSubReportDefinitionsDelegate();

        public event GetSubReportDefinitionsDelegate GetSubReportDefinitions;

        public delegate List<ReportParameter> GetReportParametersDelegate();

        public event GetReportParametersDelegate GetReportParameters;

        public delegate List<ReportDataSource> GetReportDataSourcesDelegate();
        public event GetReportDataSourcesDelegate GetReportDataSources;        

        public ReportViewer()
        {
            InitializeComponent();
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            //rv.LocalReport.AddTrustedCodeModuleInCurrentAppDomain("System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
            //rv.LocalReport.ExecuteReportInCurrentAppDomain(AppDomain.CurrentDomain.Evidence);

            this.WindowState = FormWindowState.Maximized;
        }

        private void rv_HostedControlPreRender(object sender, Gizmox.WebGUI.Forms.Hosts.AspControlEventArgs e)
        {
            rv.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            if (SubreportProcessing == null)
                return;

            SubreportProcessing(sender, e);
        }        

        private void rv_HostedPageLoad(object sender, Gizmox.WebGUI.Forms.Hosts.AspPageEventArgs e)
        {
            if (isReportLoaded)
                return;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                rv.Reset();
                rv.ZoomMode = ZoomMode.FullPage;
                rv.ProcessingMode = ProcessingMode.Local;
                rv.LocalReport.EnableExternalImages = true;
                rv.LocalReport.EnableHyperlinks = true;
                rv.HostedControlPreRender += rv_HostedControlPreRender;

                if (GetRDLCDefinition != null)
                {
                    System.IO.Stream rdlcDefinition = GetRDLCDefinition();
                    rv.LocalReport.LoadReportDefinition(rdlcDefinition);
                }

                // Now load sub report definitions if any
                if (GetSubReportDefinitions != null)
                {
                    List<SubReport> subReports = GetSubReportDefinitions();
                    if ((subReports != null) && (subReports.Count > 0))
                    {
                        foreach (SubReport subReport in subReports)
                            rv.LocalReport.LoadSubreportDefinition(subReport.Name, subReport.Definition);
                    }
                }

                if (GetReportParameters != null)
                {
                    List<ReportParameter> reportParameters = GetReportParameters();
                    if (reportParameters == null)
                        reportParameters = new List<ReportParameter>();

                    rv.LocalReport.SetParameters(reportParameters);
                }

                if (GetReportDataSources != null)
                {
                    List<ReportDataSource> reportDataSources = GetReportDataSources();
                    if ((reportDataSources != null) && (reportDataSources.Count > 0))
                    {
                        foreach (ReportDataSource reportDataSource in reportDataSources)
                            rv.LocalReport.DataSources.Add(reportDataSource);
                    }
                }
            }
            finally
            {
                isReportLoaded = true;
                this.Cursor = Cursors.Default;
            }
        }
    }
}