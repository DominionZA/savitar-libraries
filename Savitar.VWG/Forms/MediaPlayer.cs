﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI;
using Gizmox.WebGUI.Forms;
using Gizmox.WebGUI.Forms.Skins;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms.Design;
using Gizmox.WebGUI.Common.Interfaces;
using Gizmox.WebGUI.Common.Extensibility;

namespace Savitar.VWG.Forms
{
  [ToolboxItem(true)]
  [ToolboxBitmapAttribute(typeof(MediaPlayer), "Savitar.VWG.Forms.MediaPlayer.bmp")]
  [Serializable()]
  //[MetadataTag("Savitar.VWG.Forms.MediaPlayer")]
  [Skin(typeof(MediaPlayerSkin))]
  public class MediaPlayer : Gizmox.WebGUI.Forms.Hosts.ObjectBox, ISkinable
  {
    public enum InterfaceMode { Invisible, None, Mini, Full, Custom }
    public enum PlayerState { Stopped = 1, Paused = 2, Playing = 3, FFwd = 4, RWnd = 5, Buffering = 6, Waiting = 7, Finished = 8, Transitioning = 9, Ready = 10 }
    
    public delegate void OnRateChangedDelegate(object sender, int newValue);
    public event OnRateChangedDelegate OnRateChanged;

    public delegate void OnPlayerStateChangedDelegate(object sender, PlayerState playerState, double position, int rate);
    public event OnPlayerStateChangedDelegate OnPlayerStateChanged;

    public MediaPlayer()
      : base()
    {
      this.ObjectClassId = "clsid:6BF52A52-394A-11d3-B153-00C04F79FAA6";
      this.ObjectType = "application/x-oleobject";
      
      this.RegisterSelf();
    }

    protected override void OnCreateControl()
    {      
      base.OnCreateControl();
      this.InvokeMethod("MediaPlayer_Init", this.ID);
    }

    public void GoForward()
    {
      this.InvokeMethod("IncRate", 1);
    }

    public void GoBack()
    {
      this.InvokeMethod("IncRate", -1);
    }

    public void GotoPosition(string time)
    {
      string[] timeElements = time.Split(':');
      int hours, minutes, seconds = 0;

      if (timeElements.Length < 2)
        throw new Exception(this.GetType().ToString() + ".GotoPosition(timeValue) must be passed a value in the format HH:MM or HH:MM:SS. Current value is " + time);

      Int32.TryParse(timeElements[0], out hours);
      Int32.TryParse(timeElements[1], out minutes);
      if (timeElements.Length > 2)
        Int32.TryParse(timeElements[2], out seconds);
      
      GotoPosition(new TimeSpan(hours, minutes, seconds));
    }

    public void GotoPosition(TimeSpan value)
    {
      GotoPosition(value.TotalSeconds);      
    }

    public void GotoPosition(double seconds)
    {
      this.InvokeMethod("GotoPosition", seconds);
    }

    public void Stop()
    {
      this.InvokeMethod("Stop");
    }

    public void Pause()
    {
      this.InvokeMethod("Pause");
    }

    public void Play()
    {
      this.SetRate(1);
      this.InvokeMethod("Play");
    }

    public void SetURL(string value)
    {
      this.InvokeMethod("SetURL", value);
    }

    public void SetRate(int rate)
    {
      this.InvokeMethod("SetRate", rate);
    }

    #region JavaScript Callback Handling
    // Callback type that will be called with info retrieved from the client
    public delegate void ReturnInformationSub(string strInfo);
    Dictionary<string, ReturnInformationSub> callbackList = new Dictionary<string, ReturnInformationSub>();

    int _NextID = 0;
    private string NextId
    {
      get
      {
        _NextID++;
        return _NextID.ToString();
      }
    }

    private string AddCallback(ReturnInformationSub dlg)
    {
      string strID = this.NextId;
      if (dlg != null)
      {
        callbackList.Add(strID, dlg);
      }
      return strID;
    }

    private void CallCallback(string callbackID, string strRetvalue)
    {
      if (callbackID != null & callbackID.Length > 0)
      {
        ReturnInformationSub dlg = this.callbackList[callbackID];
        callbackList.Remove(callbackID);
        if (dlg != null)
        {
          dlg(strRetvalue);
        }
      }
    }
    #endregion

    protected override void FireEvent(IEvent objEvent)
    {
      base.FireEvent(objEvent);

      switch (objEvent.Type)
      {
        case "OnRateChanged":
          if (OnRateChanged != null)
            OnRateChanged(this, Convert.ToInt32(objEvent["VideoRate"]));
          break;
        case "OnPlayerStateChanged":
          if (OnPlayerStateChanged != null)
          {
            object state = objEvent["PlayerState"];
            object position = objEvent["Position"];
            object rate = objEvent["Rate"];
            OnPlayerStateChanged(this, (PlayerState)Convert.ToInt32(objEvent["PlayerState"]), Convert.ToDouble(objEvent["Position"]), Convert.ToInt32(rate));
          }
          break;
        case "GetPosition":
        case "":
          CallCallback(objEvent["CallbackID"], objEvent["ClientReturns"]);
          break;
      }
    }    

    #region WMP Specific Properties
    public string URL
    {
      get
      {
        if (this.Parameters.Contains("URL"))
          return this.Parameters["URL"].ToString();
        else
          return "";
      }
      set { this.Parameters["URL"] = value; }
    }

    public bool AutoStart
    {
      get
      {
        if (this.Parameters.Contains("AutoStart"))
          return Convert.ToBoolean(this.Parameters["AutoStart"].ToString());
        else
          return true;
      }
      set { this.Parameters["AutoStart"] = value; }
    }

    public int Balance
    {
      get
      {
        if (this.Parameters.Contains("Balance"))
          return Convert.ToInt32(this.Parameters["Balance"].ToString());
        else
          return 0;
      }
      set { this.Parameters["Balance"] = value; }
    }

    public bool CurrentMarker
    {
      get
      {
        if (this.Parameters.Contains("CurrentMarker"))
          return Convert.ToBoolean(this.Parameters["CurrentMarker"].ToString());
        else
          return true;
      }
      set { this.Parameters["CurrentMarker"] = value; }
    }

    public double CurrentPosition
    {
      get
      {
        if (this.Parameters.Contains("CurrentPosition"))
          return Convert.ToDouble(this.Parameters["CurrentPosition"]);
        else
          return 0;
      }
      set { this.Parameters["CurrentPosition"] = value; }
    }

    public bool EnableContextMenu
    {
      get
      {
        if (this.Parameters.Contains("EnableContextMenu"))
          return Convert.ToBoolean(this.Parameters["EnableContextMenu"].ToString());
        else
          return true;
      }
      set { this.Parameters["EnableContextMenu"] = value; }
    }

    public bool EnablePositionControls
    {
      get
      {
        if (this.Parameters.Contains("EnablePositionControls"))
          return Convert.ToBoolean(this.Parameters["EnablePositionControls"].ToString());
        else
          return true;
      }
      set { this.Parameters["EnablePositionControls"] = value; }
    }

    public bool EnableTracker
    {
      get
      {
        if (this.Parameters.Contains("EnableTracker"))
          return Convert.ToBoolean(this.Parameters["EnableTracker"].ToString());
        else
          return true;
      }
      set { this.Parameters["EnableTracker"] = value; }
    }

    public bool FullScreen
    {
      get
      {
        if (this.Parameters.Contains("FullScreen"))
          return Convert.ToBoolean(this.Parameters["FullScreen"].ToString());
        else
          return false;
      }
      set { this.Parameters["FullScreen"] = value; }
    }

    public bool Mute
    {
      get
      {
        if (this.Parameters.Contains("Mute"))
          return Convert.ToBoolean(this.Parameters["Mute"].ToString());
        else
          return false;
      }
      set { this.Parameters["Mute"] = value; }
    }

    public int PlayCount
    {
      get
      {
        if (this.Parameters.Contains("PlayCount"))
          return Convert.ToInt32(this.Parameters["PlayCount"]);
        else
          return 1;
      }
      set { this.Parameters["PlayCount"] = value; }
    }

    public int Rate
    {
      get
      {
        if (this.Parameters.Contains("Rate"))
          return Convert.ToInt32(this.Parameters["Rate"]);
        else
          return 1;
      }
      set { this.Parameters["Rate"] = value; }
    }

    public bool SelectionStart
    {
      get
      {
        if (this.Parameters.Contains("SelectionStart"))
          return Convert.ToBoolean(this.Parameters["SelectionStart"]);
        else
          return true;
      }
      set { this.Parameters["SelectionStart"] = value; }
    }

    public bool SelectionEnd
    {
      get
      {
        if (this.Parameters.Contains("SelectionEnd"))
          return Convert.ToBoolean(this.Parameters["SelectionEnd"]);
        else
          return true;
      }
      set { this.Parameters["SelectionEnd"] = value; }
    }

    public bool SendPlayStateChangeEvents
    {
      get
      {
        if (this.Parameters.Contains("SendPlayStateChangeEvents"))
          return Convert.ToBoolean(this.Parameters["SendPlayStateChangeEvents"]);
        else
          return false;
      }
      set { this.Parameters["SendPlayStateChangeEvents"] = value; }
    }

    public bool ShowGotoBar
    {
      get
      {
        if (this.Parameters.Contains("ShowGotoBar"))
          return Convert.ToBoolean(this.Parameters["ShowGotoBar"]);
        else
          return true;
      }
      set { this.Parameters["ShowGotoBar"] = value; }
    }

    public bool ShowStatusBar
    {
      get
      {
        if (this.Parameters.Contains("ShowStatusBar"))
          return Convert.ToBoolean(this.Parameters["ShowStatusBar"]);
        else
          return true;
      }
      set { this.Parameters["ShowStatusBar"] = value; }
    }

    public bool StretchToFit
    {
      get
      {
        if (this.Parameters.Contains("StretchToFit"))
          return Convert.ToBoolean(this.Parameters["StretchToFit"]);
        else
          return false;
      }
      set { this.Parameters["StretchToFit"] = value; }
    }

    public InterfaceMode UIMode
    {
      get
      {
        if (this.Parameters.Contains("uiMode"))
        {
          return (InterfaceMode)Enum.Parse(typeof(InterfaceMode), this.Parameters["uiMode"].ToString());
        }
        else
          return InterfaceMode.Full;
      }
      set { this.Parameters["uiMode"] = value.ToString(); }
    }
    #endregion    
  }
}
