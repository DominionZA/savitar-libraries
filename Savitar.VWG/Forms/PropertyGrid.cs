using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Forms;
using Savitar.Model;

namespace Savitar.VWG.Forms
{
	public partial class PropertyGrid : Gizmox.WebGUI.Forms.PropertyGrid
	{
		public PropertyGrid() : base()
		{
			InitializeComponent();      
		}

    public Gizmox.WebGUI.Forms.ToolBar GetToolBar()
    {
      return (ToolBar)typeof(Gizmox.WebGUI.Forms.PropertyGrid).InvokeMember("mobjToolBar",
                                                        System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.NonPublic |
                                                        System.Reflection.BindingFlags.Instance,
                                                        null,
                                                        this,
                                                        null);
    }

    public void AddButton(System.Drawing.Bitmap image, System.Drawing.Imaging.ImageFormat imageFormat, string toolTip, EventHandler clickEventHandler)
    {
      this.AddButton(Savitar.VWG.Imaging.GetResourceHandle(image, imageFormat), toolTip, clickEventHandler);
    }

    public void AddButton(Gizmox.WebGUI.Common.Resources.ResourceHandle image, string toolTip, EventHandler clickEventHandler)
    {
      ToolBarButton button = new ToolBarButton();
      button.Image = image;
      button.ToolTipText = toolTip;
      if (clickEventHandler != null)
        button.Click += clickEventHandler;

      this.AddButton(button);
    }

    public void AddButton(Gizmox.WebGUI.Forms.ToolBarButton button)
    {
      GetToolBar().Buttons.Add(button);
    }

    public new IEntity SelectedObject
    {
      get
      {
        if (base.SelectedObject != null)
          return (IEntity)base.SelectedObject;
        else
          return null;
      }
      set
      {
        if (!(value is Savitar.Data.Interfaces.ISavItem))
          throw new Exception(this.GetType().ToString() + "SelectedObject can only accept objects of type Savitar.Data.Interfaces.ISavItem");

        base.SelectedObject = value;
      }
    }

    //public void Save()
    //{
    //  if (this.SelectedObject == null)
    //    return;

    //  this.SelectedObject.Save();
    //}
	}
}
