using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;
using Savitar.Model;

namespace Savitar.VWG.Forms
{
	public partial class DataGridViewEx : Savitar.VWG.Forms.DataGridView
  {
    public enum GridEditMode { Row, PropertyGrid }
        
    public delegate void SavItemDoubleClickedDelegate(object sender, Savitar.Data.Interfaces.ISavItem picItem);
    

    /*
    public bool ShowCheckBoxes
    {
      get { return _ShowCheckboxes; }
      set 
      {
        if (_ShowCheckboxes == value)
          return;

        _ShowCheckboxes = value; 
      }
    } bool _ShowCheckboxes = false;
    */

    public override bool BeginEdit(bool blnSelectAll)
    {
      if (this.EditModeEx == GridEditMode.PropertyGrid)
      {
        ShowPropGrid();
        return true;
      }
      else 
        return base.BeginEdit(blnSelectAll);
    }

    public new bool ReadOnly
    {
      get { return base.ReadOnly; }
      set
      {
        if (this.EditModeEx == GridEditMode.PropertyGrid)
          base.ReadOnly = true;
        else
          base.ReadOnly = value;
      }
    }

    public GridEditMode EditModeEx
    {
      get { return _EditMode; }
      set
      {
        if (_EditMode == value)
          return;

        _EditMode = value;
        if (_EditMode == GridEditMode.PropertyGrid)
        {
          this.ReadOnly = true;
          this.DoubleClick += new EventHandler(DataGridView_DoubleClick);
          this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        else if (_EditMode == GridEditMode.Row)
        {
          this.DoubleClick -= new EventHandler(DataGridView_DoubleClick);
          this.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }
      }
    } GridEditMode _EditMode = GridEditMode.Row;

    /*
    public PDPropertyGrid PropGrid
    {
      get { return _PropGrid; }
      set 
      { 
        _PropGrid = value;

        if (_PropGrid != null)
        {
          this.SelectionChanged += new EventHandler(DataGridView_SelectionChanged);

          foreach (DataGridViewColumn col in this.Columns)
          {
            if (col.Name == "CheckBoxCol")
              col.ReadOnly = false;
            else
              col.ReadOnly = true;
          }
        }
        else
          this.SelectionChanged -= new EventHandler(DataGridView_SelectionChanged);
      }
    } PDPropertyGrid _PropGrid;
    */
    public DataGridViewEx()
		{
			InitializeComponent();      
		}

    protected override void DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
    {
      // If we have a property grid linked then no need to do editing in the grid view, so make all the columns read only.
      if (EditModeEx == GridEditMode.PropertyGrid)
      {
        foreach (DataGridViewColumn col in this.Columns)
        {
          if (col.Name == "CheckBoxCol")
            col.ReadOnly = false;
          else
            col.ReadOnly = true;
        }
      }

      /*
      // If check boxes are to be shown, then slot them in as the first column. Ensure they are not read only otherwise they cannot be checked.
      if (_ShowCheckboxes)
      {
        // Check that we don't already have check boxes in the first column.
        if ((this.Columns.Count != 0) && (this.Columns[0] is DataGridViewCheckBoxColumn))
          return;

        DataGridViewColumn col = new DataGridViewCheckBoxColumn(false);
        col.Width = 35;
        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        col.ReadOnly = false;
        col.Name = "CheckBoxCol";
        col.HeaderText = " ";
        this.Columns.Insert(0, col);        
      }
      */

      base.DataGridView_DataBindingComplete(sender, e);
    }

    void DataGridView_DoubleClick(object sender, EventArgs e)
    {
      if (_EditMode == GridEditMode.PropertyGrid)
        ShowPropGrid();      
    }

    void ShowPropGrid()
    {
      IEntity selectedItem = this.GetSelectedPICItem();
      if (selectedItem == null)
        return;

      Forms.PropGrid frm = new Forms.PropGrid(selectedItem);
      frm.FormClosed += new Form.FormClosedEventHandler(frm_FormClosed);
      frm.ShowDialog();
    }

    void frm_FormClosed(object sender, FormClosedEventArgs e)
    {
      try
      {
        Forms.PropGrid frm = (Forms.PropGrid)sender;

        SubSonic.Schema.IActiveRecord ar = (SubSonic.Schema.IActiveRecord)frm.SelectedObject;

        if (frm.DialogResult != DialogResult.OK)
        {
          if (ar.IsNew())
          {
            Savitar.Data.Interfaces.ISavItemList ds = (Savitar.Data.Interfaces.ISavItemList)this.DataSource;
            ds.RemoveAt(0);
            this.DataSource = null;
            this.DataSource = ds;
          }

          return;
        }

        if (ar.IsDirty() || ar.IsNew())
        {
          //frm.SelectedObject.Save();
          //frm.SelectedObject.ResetForeignKeys();
          RefreshDataSource(); // Only refreshes the data. Does not query the database.      
        }
      }
      catch (Exception ex)
      {
        Savitar.VWG.MsgBox.ShowError(ex);
      }
    }

    public override void AddItem(IEntity item)
    {
      base.AddItem(item);
      this.ShowPropGrid();
    }    
	}
}
