using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using Gizmox.WebGUI.Forms;
using System.Web;
using Savitar.VWG.Views;

namespace Savitar.VWG.Forms
{
    public partial class RibbonBase : Form        
    {
        protected string FooterText
        {
            get { return FooterLabel.Text; }
            set
            {
                FooterLabel.Text = value;
                //bool isVisible = !String.IsNullOrEmpty(FooterLabel.Text);

                //if (isVisible != FooterLabel.Visible)
                //  FooterLabel.Visible = isVisible;
            }
        }

        protected RibbonBar Ribbon
        {
            get { return MainRibbonBar; }
        }

        protected RibbonBarPage Ribbon_Homepage
        {
            get { return HomePage; }
        }

        public RibbonBase()
        {
            InitializeComponent();

        }

        private void Main_Load(object sender, EventArgs e)
        {
            ShowSplashView();
            UpdateUI();
        }

        protected virtual BaseView GetSplashView()
        {
            throw new NotImplementedException();
        }

        protected virtual BaseView GetMyDetailsView()
        {
            throw new NotImplementedException();
        }

        protected virtual string GetAppName()
        {
            throw new NotImplementedException();
        }

        protected virtual Version GetAppVersion()
        {
            throw new NotImplementedException();
        }

        public virtual void LoadView(BaseView view)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                BaseTabPage tabPage = new BaseTabPage(view);
                view.RibbonBase = this;
                view.Dock = DockStyle.Fill;
                view.Margin = new Padding(0);

                tabPage.TextChanged += page_TextChanged;
                tabPage.Text = view.Text;
                tabPage.OnShowView += page_OnShowView;
                tabPage.OnCloseTab += page_OnCloseTab;
                tabControl.TabPages.Add(tabPage);

                ModuleNameLabel.Text = GetAppName() + " - " + view.Text;

                tabControl.SelectedIndex = tabControl.TabPages.Count - 1;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex, "Error Showing View " + view);
            }
            finally
            {
                Cursor = Cursors.Default;

                UpdateUI();
            }
        }

        private void page_TextChanged(object sender, EventArgs e)
        {
            BaseTabPage page = (BaseTabPage) sender;
            ModuleNameLabel.Text = GetAppName() + " - " + page.Text;
        }

        private void page_OnCloseTab(BaseTabPage sender)
        {
            tabControl_CloseClick(null, null);
        }

        private void page_OnShowView(BaseView sender, BaseView view)
        {
            LoadView(view);
        }

        private void page_OnControlError(Control owner, string message)
        {
            errorProvider.SetError(owner, message);
        }

        protected void EnableButton(Button button, bool value)
        {
            if (button.Enabled == value)
                return;

            button.Enabled = value;
            Utils.SetButtonImage(button);
            button.Invalidate();

            FooterPanel.Visible = AddButton.Enabled || DeleteButton.Enabled || EditButton.Enabled || SaveButton.Enabled;
            //FooterPanel.Visible = true;
        }

        protected void ShowSplashView()
        {
            BaseView splashView = GetSplashView();
            splashView.Text = "Version " + GetAppVersion();
            LoadView(splashView);
        }

        private void UpdateUI()
        {
            MainRibbonBar.Enabled = Context.Session.IsLoggedOn;

            if (Context.Session.IsLoggedOn)
                UserLabel.Text = HttpContext.Current.User.Identity.Name; // Membership.GetUser().UserName;
            else
                UserLabel.Text = "Not Logged In";
        }

        private void MyDetailsButton_Click(object sender, EventArgs e)
        {
            BaseView view = GetMyDetailsView();
            view.Text = "My Details";
            LoadView(view);
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            Context.Cookies["UserGUID"] = null;
            Context.Session["User"] = null;
            Context.Session.IsLoggedOn = false;

            UpdateUI();
        }

        private BaseTabPage CurrentPage
        {
            get { return (BaseTabPage) tabControl.SelectedItem; }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            CurrentPage.View.Add();
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            CurrentPage.View.Edit();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            CurrentPage.View.Delete();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {            
            CurrentPage.View.ClearErrorProviderErrors(CurrentPage.View);

            if (!CurrentPage.View.PreCommit())
                return;
            
            try
            {
                Cursor = Cursors.WaitCursor;
                CurrentPage.View.Save();
            }
            finally
            {
                Cursor = Cursors.Default;
            }            

            CurrentPage.View.PostCommit();
        }

        protected virtual RibbonBarPage AddPage(string tabName)
        {
            RibbonBarPage page = new RibbonBarPage {Text = tabName};
            MainRibbonBar.Pages.Add(page);
            return page;
        }

        protected virtual RibbonBarGroup AddGroup(RibbonBarPage page, string groupName)
        {
            RibbonBarGroup group = new RibbonBarGroup();
            group.Text = groupName;
            group.Tag = page;
            page.Groups.Add(group);
            return group;
        }

        protected virtual RibbonBarStackItem AddStackItem(RibbonBarGroup group)
        {
            RibbonBarStackItem item = new RibbonBarStackItem();
            item.Tag = group;
            group.Items.Add(item);
            return item;
        }

        protected virtual RibbonBarButtonItem AddButtonItem(RibbonBarGroup group, string text, string imageName,
                                                            Type module)
        {
            if (!module.IsSubclassOf(typeof (BaseView)))
                throw new Exception("All modules must inherit from Views.BaseView.");

            RibbonBarButtonItem button = new RibbonBarButtonItem();
            button.Text = text;
            button.Image = Image.Get(imageName);
            button.Tag = module;
            button.Click += button_Click;
            group.Items.Add(button);
            return button;
        }

        protected virtual RibbonBarButtonItem AddButtonItem(RibbonBarStackItem stackItem, string text, string imageName,
                                                            Type module)
        {
            if (!module.IsSubclassOf(typeof (BaseView)))
                throw new Exception("All modules must inherit from Views.BaseView.");

            RibbonBarButtonItem button = new RibbonBarButtonItem();
            button.Text = text;
            button.Image = Image.Get(imageName);
            button.Tag = module;
            button.Click += button_Click;
            stackItem.Items.Add(button);
            return button;
        }

        protected virtual RibbonBarFlowItem AddFlowItem(RibbonBarGroup group, string text)
        {
            RibbonBarFlowItem item = new RibbonBarFlowItem();
            item.Text = text;
            group.Items.Add(item);
            return item;
        }

        protected virtual RibbonBarComboBoxItem AddComboBox(RibbonBarFlowItem flowItem)
        {
            RibbonBarComboBoxItem item = new RibbonBarComboBoxItem();
            flowItem.Items.Add(item);
            return item;
        }

        protected virtual RibbonBarDropDownButtonItem AddDropDownButtonItem(RibbonBarGroup group, string buttonText,
                                                                            object tag, string imageName)
        {
            RibbonBarDropDownButtonItem item = new RibbonBarDropDownButtonItem();
            item.Text = buttonText;
            item.Tag = tag;

            if (!String.IsNullOrEmpty(imageName))
                item.Image = Image.Get(imageName);

            group.Items.Add(item);

            item.DropDownMenu = new ContextMenu();

            return item;
        }

        private void button_Click(object sender, EventArgs e)
        {
            RibbonBarButtonItem button = (RibbonBarButtonItem) sender;
            Type viewType = (Type) button.Tag;
            BaseView view = (BaseView) Activator.CreateInstance(viewType);

            LoadView(view);
        }

        private void tabControl_CloseClick(object sender, EventArgs e)
        {
            foreach (WorkspaceTab tab in tabControl.TabPages)
            {
                if (tab == CurrentPage)
                {
                    CloseTab(tab);
                    break;
                }
            }
        }

        protected void CloseTab(WorkspaceTab tab)
        {
            BaseTabPage page = (BaseTabPage) tab;

            if (page.IsDirty)
            {
                MessageBox.Show("Do you want to save changes before closing this tab?", "Confirm Save",
                                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, SaveBeforeClose);
                return;
            }

            page.Dispose();
            if (tabControl.TabPages.Count == 0)
                ShowSplashView();

            tabControl.SelectedIndex = tabControl.TabPages.Count - 1;
        }

        protected void SaveBeforeClose(object sender, EventArgs e)
        {
            Form frm = (Form) sender;

            switch (frm.DialogResult)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.Yes:
                    if (!CurrentPage.Save())
                        return;

                    break;
                default:
                    CurrentPage.IsDirty = false;
                    break;
            }

            CloseTab(CurrentPage);
        }

        protected void CleanRibbon()
        {
            // Remove stack items from groups that don't have any buttons.
            IList<RibbonBarStackItem> emptyStackItems = (
                                                            from page in MainRibbonBar.Pages
                                                            from grp in page.Groups
                                                            from stackItem in grp.Items
                                                            where
                                                                stackItem is RibbonBarStackItem &&
                                                                ((RibbonBarStackItem) stackItem).Items.Count == 0
                                                            select (RibbonBarStackItem) stackItem
                                                        ).ToList();

            foreach (RibbonBarStackItem emptyStackItem in emptyStackItems)
            {
                RibbonBarGroup group = emptyStackItem.Tag as RibbonBarGroup;
                if (group != null)
                    group.Items.Remove(emptyStackItem);
            }

            // Now get the empty groups and remove them from the pags (tabs)
            IList<RibbonBarGroup> emptyGroups = (
                                                    from page in MainRibbonBar.Pages
                                                    from grp in page.Groups
                                                    where grp.Items.Count == 0
                                                    select grp
                                                ).ToList();

            foreach (RibbonBarGroup emptyGroup in emptyGroups)
            {
                RibbonBarPage page = emptyGroup.Tag as RibbonBarPage;
                if (page != null)
                    page.Groups.Remove(emptyGroup);
            }

            // And now remove any empty pages that don't have groups or anything.
            IList<RibbonBarPage> emptyPages = (
                                                  from page in MainRibbonBar.Pages
                                                  where page.Groups.Count == 0
                                                  select page
                                              ).ToList();

            for (int i = emptyPages.Count - 1; i >= 0; i--)
                MainRibbonBar.Pages.Remove(emptyPages[i]);
        }

        private void tabControl_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (CurrentPage == null)
                return;

            ModuleNameLabel.Text = GetAppName() + " - " + CurrentPage.Text;
            //EnableButton(AddButton, CurrentPage.IsAddEnabled);
            //EnableButton(DeleteButton, CurrentPage.IsDeleteEnabled);
            //EnableButton(EditButton, CurrentPage.IsEditEnabled);
            //EnableButton(SaveButton, CurrentPage.IsAddEnabled || CurrentPage.IsEditEnabled || CurrentPage.IsDeleteEnabled);
        }
    }
}