using System.Drawing;

namespace Savitar.VWG.Forms
{
	partial class PropGrid
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.propertyGrid1 = new Savitar.VWG.Forms.PropertyGrid();
      this.CancelButtonX = new Gizmox.WebGUI.Forms.Button();
      this.OKButtonX = new Gizmox.WebGUI.Forms.Button();
      this.SuspendLayout();
      // 
      // propertyGrid1
      // 
      this.propertyGrid1.AutoValidate = Gizmox.WebGUI.Forms.AutoValidate.EnablePreventFocusChange;
      this.propertyGrid1.CategoryForeColor = System.Drawing.Color.Empty;
      this.propertyGrid1.CommandsVisibleIfAvailable = false;
      this.propertyGrid1.HelpBackColor = System.Drawing.Color.Transparent;
      this.propertyGrid1.HelpForeColor = System.Drawing.Color.Black;
      this.propertyGrid1.HelpVisible = false;
      this.propertyGrid1.LineColor = System.Drawing.Color.Empty;
      this.propertyGrid1.Location = new System.Drawing.Point(12, 12);
      this.propertyGrid1.Name = "propertyGrid1";
      this.propertyGrid1.PropertySort = Gizmox.WebGUI.Forms.PropertySort.CategorizedAlphabetical;
      this.propertyGrid1.Size = new System.Drawing.Size(387, 385);
      this.propertyGrid1.TabIndex = 0;
      this.propertyGrid1.ViewBackColor = System.Drawing.Color.White;
      this.propertyGrid1.ViewForeColor = System.Drawing.Color.Black;
      // 
      // CancelButtonX
      // 
      this.CancelButtonX.Location = new System.Drawing.Point(324, 415);
      this.CancelButtonX.Name = "CancelButtonX";
      this.CancelButtonX.Size = new System.Drawing.Size(75, 23);
      this.CancelButtonX.TabIndex = 1;
      this.CancelButtonX.Text = "Cancel";
      this.CancelButtonX.UseVisualStyleBackColor = true;
      this.CancelButtonX.Click += new System.EventHandler(this.CancelButtonX_Click);
      // 
      // OKButtonX
      // 
      this.OKButtonX.Location = new System.Drawing.Point(243, 415);
      this.OKButtonX.Name = "OKButtonX";
      this.OKButtonX.Size = new System.Drawing.Size(75, 23);
      this.OKButtonX.TabIndex = 1;
      this.OKButtonX.Text = "OK";
      this.OKButtonX.Click += new System.EventHandler(this.OKButtonX_Click);
      // 
      // PropGrid
      // 
      this.AcceptButton = this.OKButtonX;
      this.CancelButton = this.CancelButtonX;
      this.Controls.Add(this.OKButtonX);
      this.Controls.Add(this.CancelButtonX);
      this.Controls.Add(this.propertyGrid1);
      this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.FixedSingle;
      this.Size = new System.Drawing.Size(411, 450);
      this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterScreen;
      this.ResumeLayout(false);

		}

		#endregion

    private Savitar.VWG.Forms.PropertyGrid propertyGrid1;
    private Gizmox.WebGUI.Forms.Button CancelButtonX;
    private Gizmox.WebGUI.Forms.Button OKButtonX;
	}
}
