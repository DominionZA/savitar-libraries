﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savitar.VWG.Forms.Design
{
  public class SavItemCollectionEditor : Gizmox.WebGUI.Forms.Design.CollectionEditor
  {
    //private object[] _Items;
    public delegate void MyFormClosedEventHandler(object sender, Gizmox.WebGUI.Forms.FormClosedEventArgs e);
    public static event MyFormClosedEventHandler MyFormClosed;

    public SavItemCollectionEditor(Type type) : base(type) { }
    
    protected override CollectionForm CreateCollectionForm()
    {
      CollectionForm collectionForm = base.CreateCollectionForm();    
      collectionForm.FormClosed += new Gizmox.WebGUI.Forms.Form.FormClosedEventHandler(collectionForm_FormClosed);
      return collectionForm;
    }

    protected override void DestroyInstance(object instance)
    {
      // The instance being destroyed here must be deleted from the database too.
      if (instance is Savitar.Data.Interfaces.ISavItem)
      {
        Savitar.Data.Interfaces.ISavItem item = (Savitar.Data.Interfaces.ISavItem)instance;

        //if (item.ID == null)
          item.Delete();
      }

      base.DestroyInstance(instance);
    }
    
    void collectionForm_FormClosed(object sender, Gizmox.WebGUI.Forms.FormClosedEventArgs e)
    {
      if (MyFormClosed != null)
        MyFormClosed(this, e);
    }
  }
}
