using System.Drawing;

namespace Savitar.VWG.Forms
{
	partial class BaseLookupForm
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.OKButtonX = new Gizmox.WebGUI.Forms.Button();
      this.CancelButtonX = new Gizmox.WebGUI.Forms.Button();
      this.SuspendLayout();
      // 
      // OKButtonX
      // 
      this.OKButtonX.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.OKButtonX.Location = new System.Drawing.Point(176, 73);
      this.OKButtonX.Name = "button1";
      this.OKButtonX.Size = new System.Drawing.Size(75, 23);
      this.OKButtonX.TabIndex = 3;
      this.OKButtonX.Text = "OK";
      this.OKButtonX.Click += new System.EventHandler(this.OKButtonX_Click);
      // 
      // CancelButtonX
      // 
      this.CancelButtonX.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.CancelButtonX.Location = new System.Drawing.Point(257, 73);
      this.CancelButtonX.Name = "CancelButtonX";
      this.CancelButtonX.Size = new System.Drawing.Size(75, 23);
      this.CancelButtonX.TabIndex = 3;
      this.CancelButtonX.Text = "Cancel";
      this.CancelButtonX.Click += new System.EventHandler(this.CancelButtonX_Click);
      // 
      // BaseLookupForm
      // 
      this.AcceptButton = this.OKButtonX;
      this.CancelButton = this.CancelButtonX;
      this.Controls.Add(this.CancelButtonX);
      this.Controls.Add(this.OKButtonX);
      this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.FixedSingle;
      this.Size = new System.Drawing.Size(344, 108);
      this.ResumeLayout(false);

		}

		#endregion

    private Gizmox.WebGUI.Forms.Button OKButtonX;
    private Gizmox.WebGUI.Forms.Button CancelButtonX;
	}
}
