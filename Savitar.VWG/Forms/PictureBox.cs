using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Forms
{
  [Serializable]
  [ToolboxBitmap(typeof(PictureBox), "Gizmox.WebGUI.Forms.PictureBox.bmp")]
	public partial class PictureBox : Gizmox.WebGUI.Forms.PictureBox, Savitar.VWG.Common.Interfaces.IPrintableControl
	{
    private byte[] imageData;

    public bool AllowFullScreenView
    {
      get { return _AllowFullScreenView; }
      set
      {
        if (_AllowFullScreenView == value)
          return;
        _AllowFullScreenView = value;

        if (_AllowFullScreenView)
          this.DoubleClick += new EventHandler(PictureBox_DoubleClick);
        else
          this.DoubleClick -= new EventHandler(PictureBox_DoubleClick);
      }
    } bool _AllowFullScreenView = false;

		public PictureBox()
		{
			InitializeComponent();
      this.AllowFullScreenView = true;      
		}

    public new byte[] Image
    {
      get { return imageData; }
      set
      {
        imageData = value;
        base.Image = Savitar.VWG.Image.Get(imageData);
      }
    }

    void PictureBox_DoubleClick(object sender, EventArgs e)
    {      
      Forms.ImageViewer frm = new Savitar.VWG.Forms.ImageViewer();
      frm.Image = this.Image;
      frm.ShowDialog();
    }

    #region IPrintableControl Members

    void Savitar.VWG.Common.Interfaces.IPrintableControl.Print(System.Web.UI.HtmlTextWriter objHtmlWriter)
    {
      if (base.Image != null)
      {
        string image = base.Image.ToString();
        objHtmlWriter.WriteBeginTag("IMG");
        objHtmlWriter.WriteAttribute("src", base.Image.ToString());
        objHtmlWriter.Write("/>");
      }
    }

    #endregion
  }
}
