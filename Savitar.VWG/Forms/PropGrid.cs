using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Forms;
using Savitar.Model;

namespace Savitar.VWG.Forms
{
    public partial class PropGrid : Form
    {
        public IEntity SelectedObject
        {
            get { return (IEntity) propertyGrid1.SelectedObject; }
            set
            {
                if (!(value is Savitar.Data.SavItem))
                    throw new Exception(this.GetType().ToString() +
                                        ".SelectedObject can only take objects of type Savitar.Data.Interfaces.ISavItem");

                propertyGrid1.SelectedObject = value;
            }
        }

        public PropGrid(IEntity savItem)
        {
            InitializeComponent();

            this.Text = "Edit Item";
            this.SelectedObject = savItem;
        }

        private void OKButtonX_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelButtonX_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}