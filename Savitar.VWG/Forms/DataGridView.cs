using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;
using Savitar.Data.Interfaces;
using Savitar.Model;

namespace Savitar.VWG.Forms
{
    public partial class DataGridView : Gizmox.WebGUI.Forms.DataGridView
    {
        public delegate void SavItemSelectedDelegate(object sender, IEntity picItem);
        public event SavItemSelectedDelegate SavItemSelected
        {
            add
            {
                _SavItemSelected += value;
                SelectionChanged += DataGridView_SelectionChanged;
            }
            remove
            {
                _SavItemSelected -= value;
                SelectionChanged -= DataGridView_SelectionChanged;
            }
        } SavItemSelectedDelegate _SavItemSelected;

        public DataGridView()
        {
            InitializeComponent();

            DataSourceChanged += DataGridView_DataSourceChanged;
            DataBindingComplete += DataGridView_DataBindingComplete;
            DataError += DataGridView_DataError;
            RowHeadersVisible = false;
            RowTemplate.Height = 20;
        }

        protected virtual void DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            // Now select the first row in the data grid.
            if (((IList<IEntity>)DataSource).Count != 0)
                Rows[0].Selected = true;
        }

        void DataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception == null)
                return;

            MsgBox.ShowError(e.Exception, "Data Error");
        }

        void DataGridView_DataSourceChanged(object sender, EventArgs e)
        {
            if (DataSource == null)
                return;

            // Iterage through all columns of the datasource object and set column properties (if they exist)
            foreach (Type intType in DataSource.GetType().GetInterfaces())
            {
                if (intType.IsGenericType && intType.GetGenericTypeDefinition() == typeof(IList<>))
                {
                    Type elementType = intType.GetGenericArguments()[0];
                    System.Reflection.PropertyInfo[] properties = elementType.GetProperties();
                    foreach (System.Reflection.PropertyInfo property in properties)
                    {
                        DataGridViewColumn col = Columns[property.Name];

                        if (col != null)
                        {
                            Attribute att = Attribute.GetCustomAttribute(property, typeof(Savitar.Attributes.DataGridViewColAttribute));
                            Savitar.Attributes.DataGridViewColAttribute dgvAttribute = null;

                            if (att != null)
                                dgvAttribute = (Savitar.Attributes.DataGridViewColAttribute)att;

                            col.Visible = GetVisible(property, col, dgvAttribute);
                            col.Width = GetWidth(property, col, dgvAttribute);
                            col.HeaderCell.Style.Alignment = GetAlignment(property, col, dgvAttribute);
                            col.DefaultCellStyle.Alignment = col.HeaderCell.Style.Alignment;
                            if (!col.ReadOnly) // Only set ReadOnly if the col is NOT already ReadOnly (set via the property its-self).
                                col.ReadOnly = GetReadOnly(property, col, dgvAttribute);
                        }
                    }
                }
            }
        }

        protected int GetWidth(System.Reflection.PropertyInfo prop, DataGridViewColumn col, Savitar.Attributes.DataGridViewColAttribute att)
        {
            if ((att != null) && (att.IsWidthSet))
                return att.Width;
            else
                return 100;
        }

        protected bool GetReadOnly(System.Reflection.PropertyInfo prop, DataGridViewColumn col, Savitar.Attributes.DataGridViewColAttribute att)
        {
            if ((att != null) && (att.IsReadOnlySet))
                return att.ReadOnly;
            else
                return false;
        }

        protected bool GetVisible(System.Reflection.PropertyInfo prop, DataGridViewColumn col, Savitar.Attributes.DataGridViewColAttribute att)
        {
            if ((att != null) && (att.IsVisibleSet))
                return att.IsVisible;
            else
                return true;
        }

        protected DataGridViewContentAlignment GetAlignment(System.Reflection.PropertyInfo prop, DataGridViewColumn col, Savitar.Attributes.DataGridViewColAttribute att)
        {
            if ((att != null) && (att.IsAlignmentSet))
            {
                switch (att.Alignment)
                {
                    case Attributes.PDContentAlignment.Left: return DataGridViewContentAlignment.MiddleLeft;
                    case Attributes.PDContentAlignment.Center: return DataGridViewContentAlignment.MiddleCenter;
                    case Attributes.PDContentAlignment.Right: return DataGridViewContentAlignment.MiddleRight;
                }
            }

            // Work out defaults if here.
            if ((prop.PropertyType == typeof(Nullable<Double>))
              || (prop.PropertyType == typeof(Double))
              || (prop.PropertyType == typeof(Nullable<Decimal>))
              || (prop.PropertyType == typeof(Decimal)))
                return DataGridViewContentAlignment.MiddleRight;
            if ((prop.PropertyType == typeof(Nullable<Int32>))
                || (prop.PropertyType == typeof(Int32))
                || (prop.PropertyType == typeof(Nullable<Boolean>))
                || (prop.PropertyType == typeof(Boolean)))
                return DataGridViewContentAlignment.MiddleCenter;
            else
                return DataGridViewContentAlignment.NotSet;
        }

        void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            //if (_PropGrid != null)
            //  _PropGrid.SelectedObject = GetSelectedPICItem();

            if (_SavItemSelected != null)
                _SavItemSelected(this, GetSelectedPICItem());
        }

        public void RefreshDataSource()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                IList<IEntity> items = (IList<IEntity>)DataSource;
                DataSource = null;
                DataSource = items;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        protected List<IEntity> GetSelectedPICItems()
        {
            List<IEntity> items = new List<IEntity>();

            if (SelectedRows.Count != 0)
            {
                foreach (DataGridViewRow row in SelectedRows)
                    items.Add((IEntity)row.DataBoundItem);
            }
            else
            {
                IEntity item = GetSelectedPICItem();
                if (item != null)
                    items.Add(item);
            }

            return items;
        }

        public IEntity GetSelectedPICItem()
        {
            DataGridViewRow row = null;
            if (SelectedRows.Count != 0)
                row = SelectedRows[0];
            else if (SelectedCells.Count != 0)
                row = Rows[SelectedCells[0].RowIndex];
            else
                return null;

            if (row.DataBoundItem is Savitar.Data.Interfaces.ISavItem)
                return (IEntity)row.DataBoundItem;

            return null;
        }

        public IList<IEntity> GetCheckedItems()
        {
            IList<IEntity> items = new List<IEntity>();

            foreach (DataGridViewRow row in SelectedRows)
            {
                items.Add((IEntity)row.DataBoundItem);
            }

            /*
            if (!_ShowCheckboxes)
              return items;

            foreach (DataGridViewRow row in Rows)
            {
              if ((row.Cells[0].Value != null) && Convert.ToBoolean(row.Cells[0].Value))
                items.Add((Savitar.Data.Interfaces.ISavItem)row.DataBoundItem);
            }
            */
            return items;
        }

        public void DeleteSelected(string recordSingleName)
        {
            //if (!ShowCheckBoxes)
            //  throw new Exception("ShowCheckBoxes is set to false, so checked items cannot be deleted");

            if (SelectedRows.Count == 0)
                throw new Exception("No records have been selected to delete.");

            string s = "";
            if (SelectedRows.Count > 1)
                s = "s";

            string recordName = recordSingleName + s;

            MessageBox.Show(String.Format("Are you sure you want to delete the selected {0}?", recordName), "Confirm Delete", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, ConfirmDeleteDialog);
        }

        public void DeleteChecked()
        {
            DeleteSelected("record");
        }

        void ConfirmDeleteDialog(object sender, EventArgs e)
        {
            IList<IEntity> items = GetCheckedItems();

            Gizmox.WebGUI.Forms.Form frm = (Gizmox.WebGUI.Forms.Form)sender;
            if (frm.DialogResult != DialogResult.Yes)
                return;

            try
            {
                foreach (IEntity item in items)
                {                                        
                    ((IList<IEntity>)DataSource).Remove(item);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex, "Delete Error");
            }

            IList<IEntity> tempItems = (IList<IEntity>)DataSource;
            DataSource = null;
            DataSource = tempItems;
            ClearSelection();
        }

        public virtual void AddItem(IEntity item)
        {
            IList<IEntity> ds = (IList<IEntity>)DataSource;
            ds.Insert(0, item);

            DataSource = null;
            DataSource = ds;

            ClearSelection();
            Rows[0].Selected = true;
        }

        public void Save()
        {
            if (DataSource == null)
                return;

            throw new Exception("DataGridView.Save must be handled");
            //foreach (IEntity item in DataSource)
            //    item.Save();
        }

        public IList<TEntity> GetDataSource<TEntity>()
            where TEntity : IEntity
        {
            if (DataSource == null)
                return null;

            return (IList<TEntity>) DataSource;
        }

        public void SetDataSource<TEntity>(IList<TEntity> items)
            where TEntity : IEntity
        {
            DataSource = items;
        }
    }
}
