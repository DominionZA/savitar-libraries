#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.Forms
{
  public partial class TextPrompt : Form
  {
    public delegate void OKDelegate(string text);
    private OKDelegate OKMethod { get; set; }

    public TextPrompt()
    {
      InitializeComponent();
    }

    public static void Show(string caption, string text, OKDelegate okMethod)
    {
      Show(caption, text, "", okMethod);
    }

    public static void Show(string caption, string text, string defaultText, OKDelegate okMethod)
    {      
      TextPrompt frm = new TextPrompt();
      frm.Text = caption;
      frm.PromptLabel.Text = text;
      frm.PromptTextBox.Text = defaultText;
      frm.OKMethod = okMethod;
      frm.ShowDialog();
    }

    private void TextPrompt_Load(object sender, EventArgs e)
    {
      PromptTextBox.Focus();
      PromptTextBox.SelectAll();
    }

    private void OKButtonX_Click(object sender, EventArgs e)
    {
      try
      {
        if (String.IsNullOrEmpty(PromptTextBox.Text))
        {
          MsgBox.ShowError("Please enter a value", "Invalid Value");
          PromptTextBox.Focus();
          return;
        }

        this.DialogResult = DialogResult.OK;
        this.Close();

        if (OKMethod != null)
          OKMethod(PromptTextBox.Text);
      }
      catch (Exception ex)
      {
        MsgBox.ShowError(ex);
      }
    }

    private void CancelButtonX_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }
  }
}