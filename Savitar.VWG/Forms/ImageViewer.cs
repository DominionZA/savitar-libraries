using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Forms
{
	public partial class ImageViewer : Form
	{
    public byte[] Image
    {
      set { pictureBox.Image = value; }
    }

		public ImageViewer()
		{
			InitializeComponent();
            PrintButton.Image = Savitar.VWG.Image.Get("Print32.png");
		}
    
    protected override Gizmox.WebGUI.Common.Interfaces.IGatewayHandler ProcessGatewayRequest(System.Web.HttpContext objHttpContext, string strAction)
    {
      if (strAction == "PRINT")
      {
        Savitar.VWG.Common.Interfaces.IPrintableControl objControl = pictureBox;

        System.Web.UI.HtmlTextWriter objHtmlWriter = new System.Web.UI.HtmlTextWriter(objHttpContext.Response.Output);
        objHtmlWriter.WriteBeginTag("HTML");
        objHtmlWriter.Write("><title>PICData.NET</title>");
        objHtmlWriter.WriteBeginTag("BODY");
        objHtmlWriter.Write(" onload='window.print();window.close();' >");
        objControl.Print(objHtmlWriter);
        objHtmlWriter.WriteEndTag("BODY");
        objHtmlWriter.WriteEndTag("HTML");

        objHtmlWriter.Flush();
      }

      return null;
    }    

    private void PrintButton_Click(object sender, EventArgs e)
    {
      LinkParameters p = new LinkParameters();
      p.Size = new Size(400, 400);
      p.Location = new Point(0, 0);

      Link.Open(new Gizmox.WebGUI.Common.Gateways.GatewayReference(this, "PRINT"), p); 
    }
    
  }
}
