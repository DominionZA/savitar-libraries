using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;
using Savitar.VWG;

namespace Savitar.VWG.Forms
{
	public partial class BaseLookupForm : Form
	{
    public Savitar.VWG.Controls.Lookups.BaseLookup Lookup
    {
      get { return _Lookup; }
      set 
      { 
        _Lookup = value; 
        if (_Lookup != null)
          _Lookup.SelectedItemChanged += new Savitar.VWG.Controls.Lookups.BaseLookup.SelectedItemChangedDelegate(_Lookup_SelectedItemChanged);
      }
    } Savitar.VWG.Controls.Lookups.BaseLookup _Lookup;

    public virtual Savitar.Data.Interfaces.ISavItem SelectedItem
    {
      get
      {
        if (_Lookup.SelectedItem != null)
          return (Savitar.Data.Interfaces.ISavItem)_Lookup.SelectedItem;
        else
          return null;
      }
      set
      {
        SetItem(value);
      }
    }

    protected virtual void SetItem(Savitar.Data.Interfaces.ISavItem item)
    {
    }

    void _Lookup_SelectedItemChanged(object sender, Savitar.Data.Interfaces.ISavItem item)
    {
      OKButtonX.Enabled = item != null;  
    } 
   
		public BaseLookupForm()
		{
			InitializeComponent();
		}

    private void OKButtonX_Click(object sender, EventArgs e)
    {
      if (this.SelectedItem == null)
        throw new Exception("Please select an item");

      DialogResult = DialogResult.OK;
      this.Close();
    }

    private void CancelButtonX_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
      this.Close();
    }		
	}
}
