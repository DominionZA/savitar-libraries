namespace Savitar.VWG.Forms
{
	partial class ReportViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportViewer));
      this.rv = new Gizmox.WebGUI.Reporting.ReportViewer();
      this.SuspendLayout();
      // 
      // rv
      // 
      this.rv.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.rv.AutoScroll = false;
      this.rv.ControlCode = resources.GetString("rv.ControlCode");
      this.rv.Location = new System.Drawing.Point(9, 9);
      this.rv.Name = "rv";
      this.rv.Size = new System.Drawing.Size(762, 542);
      this.rv.TabIndex = 0;
      this.rv.HostedPageLoad += new Gizmox.WebGUI.Forms.Hosts.AspPageEventHandler(this.rv_HostedPageLoad);
      // 
      // ReportViewer
      // 
      this.Controls.Add(this.rv);
      this.Size = new System.Drawing.Size(780, 560);
      this.StartPosition = Gizmox.WebGUI.Forms.FormStartPosition.CenterScreen;
      this.Text = "ReportViewer";
      this.Load += new System.EventHandler(this.ReportViewer_Load);
      this.ResumeLayout(false);

		}

		#endregion

    private Gizmox.WebGUI.Reporting.ReportViewer rv;





  }
}