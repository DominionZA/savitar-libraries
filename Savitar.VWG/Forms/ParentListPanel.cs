using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Forms;
using Savitar.Model;

namespace Savitar.VWG.Forms
{
    public partial class ParentListPanel : Panel
    {
        public event UserControls.ParentList.MasterChangedDelegate OnMasterChanged;

        public ParentListPanel()
        {
            InitializeComponent();
        }

        public void Add<TEntity>(int index, string labelText, IList<TEntity> items)
            where TEntity : IEntity
        {
            UserControls.ParentList parentListUC = new UserControls.ParentList();
            this.Controls.Add(parentListUC);
            this.Height = this.Controls.Count*parentListUC.Height;
            parentListUC.Dock = DockStyle.Bottom;

            parentListUC.OnMasterChanged +=
                new UserControls.ParentList.MasterChangedDelegate(parentListUC_OnMasterChanged);
            parentListUC.Refresh(index, labelText, items);
        }

        private void parentListUC_OnMasterChanged(UserControls.ParentList sender)
        {
            if (OnMasterChanged != null)
                OnMasterChanged(sender);
        }

        /// <summary>
        /// Returns the selected item from the LAST ParentList control in the list of controls.
        /// </summary>
        public IEntity SelectedItem
        {
            get
            {
                if (this.Controls.Count == 0)
                    return null;

                return ((UserControls.ParentList) this.Controls[this.Controls.Count - 1]).SelectedItem;
            }
        }

        public List<IEntity> GetSelectedItems()
        {
            List<IEntity> items = new List<IEntity>();
            foreach (UserControls.ParentList parentList in this.Controls)
                items.Add(parentList.SelectedItem);

            return items;
        }

        public IEntity GetSelectedItem(int index)
        {
            if (index <= Controls.Count - 1)
                return ((UserControls.ParentList) Controls[index]).SelectedItem;
            else
                return null;
        }
    }
}