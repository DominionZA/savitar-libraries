﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.VWG
{
  public class Imaging
  {
    public static Gizmox.WebGUI.Common.Resources.ResourceHandle GetResourceHandle(System.Drawing.Bitmap bitmap, System.Drawing.Imaging.ImageFormat imageFormat)
    {
      System.IO.MemoryStream memStream = new System.IO.MemoryStream();
      bitmap.Save(memStream, imageFormat);

      if (imageFormat == System.Drawing.Imaging.ImageFormat.Jpeg)
        return new Savitar.VWG.Common.Resources.JPegDataResourceHandle(memStream);
      else if (imageFormat == System.Drawing.Imaging.ImageFormat.Png)
        return new Savitar.VWG.Common.Resources.PNGDataResourceHandle(memStream);
      else
        throw new Exception("Savitar.VWG.Imaging.GetResourceHandle() does not know how to handle image format " + imageFormat.ToString());
    }
  }
}
