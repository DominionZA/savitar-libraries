namespace Savitar.VWG.Views
{
  partial class BaseView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Visual WebGui UserControl Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.errorProvider = new Gizmox.WebGUI.Forms.ErrorProvider();
      this.headerPanel = new Gizmox.WebGUI.Forms.Panel();
      this.SuspendLayout();
      // 
      // errorProvider
      // 
      this.errorProvider.BlinkRate = 3;
      this.errorProvider.BlinkStyle = Gizmox.WebGUI.Forms.ErrorBlinkStyle.BlinkIfDifferentError;
      // 
      // headerPanel
      // 
      this.headerPanel.Dock = Gizmox.WebGUI.Forms.DockStyle.Top;
      this.headerPanel.Location = new System.Drawing.Point(0, 0);
      this.headerPanel.Name = "headerPanel";
      this.headerPanel.Size = new System.Drawing.Size(391, 32);
      this.headerPanel.TabIndex = 1;
      // 
      // BaseView
      // 
      this.Controls.Add(this.headerPanel);
      this.Size = new System.Drawing.Size(391, 306);
      this.Text = "BaseUserControl";
      this.Load += new System.EventHandler(this.BaseView_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.ErrorProvider errorProvider;
    public Gizmox.WebGUI.Forms.Panel headerPanel;


  }
}