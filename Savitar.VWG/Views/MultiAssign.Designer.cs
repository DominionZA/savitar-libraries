namespace Savitar.VWG.Views
{
  partial class MultiAssign
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Visual WebGui UserControl Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiAssign));
      this.AvailableListBox = new Gizmox.WebGUI.Forms.ListBox();
      this.panel1 = new Gizmox.WebGUI.Forms.Panel();
      this.MoveLeftButton = new Gizmox.WebGUI.Forms.Button();
      this.MoveRightButton = new Gizmox.WebGUI.Forms.Button();
      this.AssignedListBox = new Gizmox.WebGUI.Forms.ListBox();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // AvailableListBox
      // 
      this.AvailableListBox.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.None;
      this.AvailableListBox.Dock = Gizmox.WebGUI.Forms.DockStyle.Left;
      this.AvailableListBox.Location = new System.Drawing.Point(0, 0);
      this.AvailableListBox.Name = "AvailableListBox";
      this.AvailableListBox.SelectionMode = Gizmox.WebGUI.Forms.SelectionMode.MultiExtended;
      this.AvailableListBox.Size = new System.Drawing.Size(200, 329);
      this.AvailableListBox.Sorted = true;
      this.AvailableListBox.TabIndex = 0;
      this.AvailableListBox.DoubleClick += new System.EventHandler(this.AvailableListBox_DoubleClick);
      // 
      // panel1
      // 
      this.panel1.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.panel1.Controls.Add(this.MoveLeftButton);
      this.panel1.Controls.Add(this.MoveRightButton);
      this.panel1.Location = new System.Drawing.Point(202, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(50, 329);
      this.panel1.TabIndex = 1;
      // 
      // MoveLeftButton
      // 
      this.MoveLeftButton.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.Top;
      this.MoveLeftButton.Image = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("MoveLeftButton.Image"));
      this.MoveLeftButton.Location = new System.Drawing.Point(5, 53);
      this.MoveLeftButton.Name = "MoveLeftButton";
      this.MoveLeftButton.Size = new System.Drawing.Size(40, 40);
      this.MoveLeftButton.TabIndex = 0;
      this.MoveLeftButton.Click += new System.EventHandler(this.MoveLeftButton_Click);
      // 
      // MoveRightButton
      // 
      this.MoveRightButton.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.Top;
      this.MoveRightButton.Image = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle(resources.GetString("MoveRightButton.Image"));
      this.MoveRightButton.Location = new System.Drawing.Point(5, 7);
      this.MoveRightButton.Name = "MoveRightButton";
      this.MoveRightButton.Size = new System.Drawing.Size(40, 40);
      this.MoveRightButton.TabIndex = 0;
      this.MoveRightButton.Click += new System.EventHandler(this.MoveRightButton_Click);
      // 
      // AssignedListBox
      // 
      this.AssignedListBox.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.None;
      this.AssignedListBox.Dock = Gizmox.WebGUI.Forms.DockStyle.Right;
      this.AssignedListBox.Location = new System.Drawing.Point(254, 0);
      this.AssignedListBox.Name = "AssignedListBox";
      this.AssignedListBox.SelectionMode = Gizmox.WebGUI.Forms.SelectionMode.MultiExtended;
      this.AssignedListBox.Size = new System.Drawing.Size(200, 329);
      this.AssignedListBox.Sorted = true;
      this.AssignedListBox.TabIndex = 0;
      this.AssignedListBox.DoubleClick += new System.EventHandler(this.AssignedListBox_DoubleClick);
      // 
      // MultiAssign
      // 
      this.Controls.Add(this.AssignedListBox);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.AvailableListBox);
      this.Size = new System.Drawing.Size(454, 339);
      this.Text = "MultiAssign";
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.ListBox AvailableListBox;
    private Gizmox.WebGUI.Forms.Panel panel1;
    private Gizmox.WebGUI.Forms.ListBox AssignedListBox;
    private Gizmox.WebGUI.Forms.Button MoveRightButton;
    private Gizmox.WebGUI.Forms.Button MoveLeftButton;


  }
}