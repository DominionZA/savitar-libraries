namespace Savitar.VWG.Views
{
  partial class MessageBox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Visual WebGui UserControl Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      Gizmox.WebGUI.Common.Resources.ImageResourceHandle imageResourceHandle1 = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      Gizmox.WebGUI.Common.Resources.ImageResourceHandle imageResourceHandle2 = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      Gizmox.WebGUI.Common.Resources.ImageResourceHandle imageResourceHandle3 = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      this.ButtonThree = new Gizmox.WebGUI.Forms.Button();
      this.ButtonTwo = new Gizmox.WebGUI.Forms.Button();
      this.ButtonOne = new Gizmox.WebGUI.Forms.Button();
      this.MessageLabel = new Gizmox.WebGUI.Forms.Label();
      this.panel1 = new Gizmox.WebGUI.Forms.Panel();
      this.SuspendLayout();
      // 
      // ButtonThree
      // 
      this.ButtonThree.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      imageResourceHandle1.File = "Cancel.png";
      this.ButtonThree.Image = imageResourceHandle1;
      this.ButtonThree.Location = new System.Drawing.Point(279, 77);
      this.ButtonThree.Name = "ButtonThree";
      this.ButtonThree.Size = new System.Drawing.Size(45, 45);
      this.ButtonThree.TabIndex = 0;
      this.ButtonThree.Click += new System.EventHandler(this.OnAnyButtonClick);
      // 
      // ButtonTwo
      // 
      this.ButtonTwo.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      imageResourceHandle2.File = "No.png";
      this.ButtonTwo.Image = imageResourceHandle2;
      this.ButtonTwo.Location = new System.Drawing.Point(228, 77);
      this.ButtonTwo.Name = "ButtonTwo";
      this.ButtonTwo.Size = new System.Drawing.Size(45, 45);
      this.ButtonTwo.TabIndex = 0;
      this.ButtonTwo.Click += new System.EventHandler(this.OnAnyButtonClick);
      // 
      // ButtonOne
      // 
      this.ButtonOne.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      imageResourceHandle3.File = "Common.Yes.png";
      this.ButtonOne.Image = imageResourceHandle3;
      this.ButtonOne.Location = new System.Drawing.Point(177, 77);
      this.ButtonOne.Name = "ButtonOne";
      this.ButtonOne.Size = new System.Drawing.Size(45, 45);
      this.ButtonOne.TabIndex = 0;
      this.ButtonOne.Click += new System.EventHandler(this.OnAnyButtonClick);
      // 
      // MessageLabel
      // 
      this.MessageLabel.Location = new System.Drawing.Point(3, 8);
      this.MessageLabel.Name = "MessageLabel";
      this.MessageLabel.Size = new System.Drawing.Size(321, 66);
      this.MessageLabel.TabIndex = 1;
      this.MessageLabel.Text = "label1";
      // 
      // panel1
      // 
      this.panel1.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.None;
      this.panel1.BackColor = System.Drawing.Color.LightSteelBlue;
      this.panel1.BorderColor = System.Drawing.Color.MidnightBlue;
      this.panel1.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.FixedSingle;
      this.panel1.Controls.Add(this.ButtonThree);
      this.panel1.Controls.Add(this.MessageLabel);
      this.panel1.Controls.Add(this.ButtonTwo);
      this.panel1.Controls.Add(this.ButtonOne);
      this.panel1.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(329, 127);
      this.panel1.TabIndex = 2;
      // 
      // MessageBox
      // 
      this.Controls.Add(this.panel1);
      this.Size = new System.Drawing.Size(329, 127);
      this.Text = "Prompt";
      this.Load += new System.EventHandler(this.MessageDialog_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.Button ButtonThree;
    private Gizmox.WebGUI.Forms.Button ButtonTwo;
    private Gizmox.WebGUI.Forms.Button ButtonOne;
    private Gizmox.WebGUI.Forms.Label MessageLabel;
    private Gizmox.WebGUI.Forms.Panel panel1;


  }
}