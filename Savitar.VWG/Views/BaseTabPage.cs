using System;
using System.Data.Objects;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Views
{
    public partial class BaseTabPage : WorkspaceTab        
    {
        public delegate void CloseTabDelegate(BaseTabPage sender);

        public event BaseView.ShowViewDelegate OnShowView;
        public event CloseTabDelegate OnCloseTab;

        public BaseView View
        {
            get
            {
                if (_View == null)
                    throw new Exception(GetType() + ".View has not been set and is currently NULL");

                return _View;
            }
            set
            {
                if (_View == value)
                    return;

                _View = value;
            }
        }

        private BaseView _View;


        public new bool IsDirty
        {
            get { return View.IsDirty; }
            set { View.IsDirty = value; }
        }

        public bool Save()
        {
            return View.Save();
        }

        public BaseTabPage()
        {
            InitializeComponent();
        }

        public BaseTabPage(BaseView view)
            : this()
        {
            View = view;

            Controls.Add(view);
            //view.AutoScroll = true;
            view.TextChanged += view_TextChanged;
            view.Dock = DockStyle.Fill;
            view.OnShowView += view_OnShowView;
            view.OnCloseView += view_OnCloseView;
            view.OnShowMessage += view_OnShowMessage;
            view.OnShowException += view_OnShowException;
        }

        private void view_TextChanged(object sender, EventArgs e)
        {
            Text = ((BaseView) sender).Text;
        }

        private void view_OnShowView(BaseView sender, BaseView view)
        {
            if (OnShowView != null)
                OnShowView(sender, view);
        }

        private void view_OnCloseView(BaseView sender)
        {
            if (OnCloseTab != null)
                OnCloseTab(this);
        }

        private void view_OnShowException(BaseView sender, Exception ex, string caption)
        {
            MsgBox.ShowError(ex, caption);
        }

        private void view_OnShowMessage(BaseView sender, string message, string caption)
        {
            MsgBox.ShowMessage(message, caption);
        }

        public void UpdateUI()
        {
            View.UpdateUI();
        }
    }
}