using System;
using System.Data;
using System.Data.Objects;
using System.Drawing;
using Gizmox.WebGUI.Forms;
using System.Linq;
using Savitar.VWG.Forms;

namespace Savitar.VWG.Views
{
    public partial class BaseView : UserControl        
    {
        private bool haveFormErrors;
        public AccessControl.Permission Permission { get; private set; }
        public bool IsLoading;

        public RibbonBase RibbonBase { get; set; }

        protected virtual ObjectContext DBContext
        {
            get { return _DBContext ?? (_DBContext = GetNewContext()); }
        } ObjectContext _DBContext;

        protected virtual ObjectContext GetNewContext()
        {
            throw new Exception(GetType() + ".GetNewContext() has not been implemented");
        }

        public new bool IsDirty
        {
            get
            {
                if (_DBContext == null)
                    return false;

                var stateManager = _DBContext.ObjectStateManager;
                var inserted = stateManager.GetObjectStateEntries(EntityState.Added);
                var modified = stateManager.GetObjectStateEntries(EntityState.Modified);
                var deleted = stateManager.GetObjectStateEntries(EntityState.Deleted);

                return (inserted.Any() || modified.Any() || deleted.Any());
            }
            set
            {
                if (_DBContext != null)
                    _DBContext.AcceptAllChanges();
            }
        }

        public delegate void ShowViewDelegate(BaseView sender, BaseView view);
        public delegate void CloseViewDelegate(BaseView sender);
        public delegate void ClosedDelegate(BaseView sender, BaseView nextView, System.Object tag);
        public delegate void ShowMessageDelegate(BaseView sender, string message, string caption);
        public delegate void ShowExceptionDelegate(BaseView sender, Exception ex, string caption);

        public event ShowViewDelegate OnShowView;
        public event CloseViewDelegate OnCloseView;
        public event ClosedDelegate OnViewClosed;
        public event ShowMessageDelegate OnShowMessage;
        public event ShowExceptionDelegate OnShowException;        

        public BaseView()
        {
            InitializeComponent();

            AutoScroll = true;
            errorProvider.ContainerControl = this;
        }

        public new bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                if (base.Enabled == value)
                    return;

                base.Enabled = value;
                if (Enabled)
                    BackColor = Color.White;
                else
                    BackColor = Color.Silver;
            }
        }

        /// <summary>
        /// Gets the permission attributes from the child views.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        /// <returns></returns>
        protected virtual AccessControl.Permission GetPermissionAttributes()
        {
            throw new NotImplementedException(GetType() + ".GetPermissionAttributes");
        }

        protected void Close(Object tag)
        {
            Close(null, tag);
        }

        public void ShowView(BaseView view)
        {
            if (OnShowView != null)
                OnShowView(this, view);
        }

        protected void CloseView(BaseView view)
        {
            if (OnCloseView != null)
                OnCloseView(this);
        }

        protected void Close(BaseView nextView, System.Object tag)
        {
            if (OnViewClosed != null)
                OnViewClosed(this, nextView, tag);

            if (_DBContext != null)
                _DBContext.Dispose();
        }

        protected void ShowMessage(string message, string caption)
        {
            if (OnShowMessage != null)
                OnShowMessage(this, message, caption);
        }

        protected void ShowException(Exception ex, string caption)
        {
            if (OnShowException != null)
                OnShowException(this, ex, caption);
        }

        protected void AddControlError(Control owner, string message)
        {
            haveFormErrors = true;
            errorProvider.SetError(owner, message);
        }

        public virtual void Add() { }
        public virtual void Edit() { }
        public virtual void Delete() { }

        public void ClearErrorProviderErrors(Control control)
        {
            Utils.ClearErrorProviderErrors(errorProvider, control);
            haveFormErrors = false;
        }

        protected virtual void ValidateViewData()
        {
            //throw new NotImplementedException("ValidateViewData must be implemented in base classes");
        }

        public virtual bool PreCommit()
        {
            ClearErrorProviderErrors(this);
            ValidateViewData();

            if (haveFormErrors)
                return false;

            return true;
        }

        public virtual void PostCommit()
        {
        }

        public virtual bool Save()
        {
            ClearErrorProviderErrors(this);

            try
            {
                if (_DBContext != null)
                    _DBContext.SaveChanges();

                ShowMessage("Data successfully saved", "Success");

                return true;
            }
            catch (Exception ex)
            {
                ShowException(ex, "Error Saving");
                return false;
            }
        }

        protected virtual void InitView()
        {
            if (headerPanel.Controls.Count == 0)
                HideHeaderPanel();
        }

        protected virtual void HideHeaderPanel()
        {
            if (!headerPanel.Visible)
                return;

            headerPanel.Visible = false;
        }

        private void BaseView_Load(object sender, EventArgs e)
        {
            if (ComponentModel.Component.IsInDesignMode)
                return;

            InitView();

            IsLoading = true;
            try
            {
                Permission = GetPermissionAttributes();

                BindData();
                UpdateUI();
            }
            finally
            {
                if (Permission != null)
                    Enabled = Permission.CanView;

                IsLoading = false;
            }
        }

        public virtual void BindData()
        {
        }

        protected virtual bool CanAdd
        {
            get
            {
                if (Permission == null)
                    return false;
                return Permission.CanAdd;
            }
        }

        protected virtual bool CanEdit
        {
            get
            {
                if (Permission == null)
                    return false;
                return Permission.CanEdit;
            }
        }

        protected virtual bool CanDelete
        {
            get
            {
                if (Permission == null)
                    return false;
                return Permission.CanDelete;
            }
        }

        protected virtual bool CanSave
        {
            get { return CanAdd || CanEdit || CanDelete; }
        }

        public virtual void UpdateUI()
        {
            if (RibbonBase == null)
                return;

            RibbonBase.AddButton.Visible = CanAdd;
            RibbonBase.EditButton.Visible = CanEdit;
            RibbonBase.DeleteButton.Visible = CanDelete;
            RibbonBase.SaveButton.Enabled = CanSave;
        }
    }
}