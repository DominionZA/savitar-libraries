using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Drawing;
using System.Text;

using Savitar.VWG.Views;

namespace Savitar.VWG.Views
{
	public partial class Database : BaseView        
	{
    public static string DBConnectionStringName = "DB";

		public Database()
		{
			InitializeComponent();
		}

    protected override Savitar.AccessControl.Permission GetPermissionAttributes()
    {
      return new Savitar.AccessControl.Permission()
      {
        CanAdd = true,
        CanDelete = true,
        CanEdit = true,
        CanView = true,
        PermissionCode = -99,
        PermissionID = -1,
        PermissionName = "Update Database"
      };
    }

    public static bool HaveUpdate
    {
      get
      {
        return Savitar.Data.Upgrade.HaveUpdate(Gizmox.WebGUI.Forms.VWGContext.Current.Server.MapPath("~/DBScripts"));
      }
    }

    private void UpgradeLinkLabel_LinkClicked(object sender, Gizmox.WebGUI.Forms.LinkLabelLinkClickedEventArgs e)
    {
      try
      {
        Savitar.Data.Upgrade.Execute(Gizmox.WebGUI.Forms.VWGContext.Current.Server.MapPath("~/DBScripts"));
        LoadProperties();
        VWG.MsgBox.ShowMessage("Database successfully upgraded", "Success");
      }
      catch (Exception ex)
      {
        Savitar.VWG.MsgBox.ShowError(ex);
      }
    }

    private void Database_Load(object sender, EventArgs e)
    {
      LoadProperties();
    }

    private void LoadProperties()
    {
      int version = Savitar.Data.Upgrade.GetDBVersion();
      CurrentDBVersionLabel.Text = version.ToString();

      int newVersion = Savitar.Data.Upgrade.GetNewDBVersion(Gizmox.WebGUI.Forms.VWGContext.Current.Server.MapPath("~/DBScripts"));
      if (newVersion > 0)
      {
        UpgradeLinkLabel.Visible = true;
        NewDBVersionLabel.Text = newVersion.ToString();
      }
      else
      {
        UpgradeLinkLabel.Visible = false;
        NewDBVersionLabel.Text = "N/A";
      }
    }
	}
}
