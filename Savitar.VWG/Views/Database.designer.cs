using System.Drawing;

namespace Savitar.VWG.Views
{
    partial class Database
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.label1 = new Gizmox.WebGUI.Forms.Label();
      this.CurrentDBVersionLabel = new Gizmox.WebGUI.Forms.Label();
      this.label2 = new Gizmox.WebGUI.Forms.Label();
      this.NewDBVersionLabel = new Gizmox.WebGUI.Forms.Label();
      this.UpgradeLinkLabel = new Gizmox.WebGUI.Forms.LinkLabel();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 14);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Current Database Version";
      // 
      // CurrentDBVersionLabel
      // 
      this.CurrentDBVersionLabel.AutoSize = true;
      this.CurrentDBVersionLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.CurrentDBVersionLabel.Location = new System.Drawing.Point(140, 14);
      this.CurrentDBVersionLabel.Name = "CurrentDBVersionLabel";
      this.CurrentDBVersionLabel.Size = new System.Drawing.Size(35, 13);
      this.CurrentDBVersionLabel.TabIndex = 0;
      this.CurrentDBVersionLabel.Text = "0";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 36);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(35, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "New Version";
      // 
      // NewDBVersionLabel
      // 
      this.NewDBVersionLabel.AutoSize = true;
      this.NewDBVersionLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.NewDBVersionLabel.Location = new System.Drawing.Point(140, 36);
      this.NewDBVersionLabel.Name = "NewDBVersionLabel";
      this.NewDBVersionLabel.Size = new System.Drawing.Size(35, 13);
      this.NewDBVersionLabel.TabIndex = 0;
      this.NewDBVersionLabel.Text = "0";
      // 
      // UpgradeLinkLabel
      // 
      this.UpgradeLinkLabel.AutoSize = true;
      this.UpgradeLinkLabel.ClientMode = false;
      this.UpgradeLinkLabel.LinkColor = System.Drawing.Color.Blue;
      this.UpgradeLinkLabel.Location = new System.Drawing.Point(192, 36);
      this.UpgradeLinkLabel.Name = "UpgradeLinkLabel";
      this.UpgradeLinkLabel.Size = new System.Drawing.Size(53, 13);
      this.UpgradeLinkLabel.TabIndex = 1;
      this.UpgradeLinkLabel.TabStop = true;
      this.UpgradeLinkLabel.Text = "Upgrade";
      this.UpgradeLinkLabel.LinkClicked += new Gizmox.WebGUI.Forms.LinkLabelLinkClickedEventHandler(this.UpgradeLinkLabel_LinkClicked);
      // 
      // Database
      // 
      this.Controls.Add(this.UpgradeLinkLabel);
      this.Controls.Add(this.NewDBVersionLabel);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.CurrentDBVersionLabel);
      this.Controls.Add(this.label1);
      this.Text = "Database";
      this.Load += new System.EventHandler(this.Database_Load);
      this.ResumeLayout(false);

		}

		#endregion

    private Gizmox.WebGUI.Forms.Label label1;
    private Gizmox.WebGUI.Forms.Label CurrentDBVersionLabel;
    private Gizmox.WebGUI.Forms.Label label2;
    private Gizmox.WebGUI.Forms.Label NewDBVersionLabel;
    private Gizmox.WebGUI.Forms.LinkLabel UpgradeLinkLabel;
	}
}
