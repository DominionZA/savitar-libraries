#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Savitar.Data;
using Savitar.Data.Interfaces;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Savitar.Model;

#endregion

namespace Savitar.VWG.Views
{
    public partial class MultiAssign : UserControl
    {
        public delegate void OnAssignDelegate(System.Object sender, IList<ILookupEntity> items);

        public delegate void OnRemoveDelegate(System.Object sender, IList<ILookupEntity> items);

        public event OnAssignDelegate OnAssign;
        public event OnRemoveDelegate OnRemove;

        public MultiAssign()
        {
            InitializeComponent();
        }

        public IList<ILookupEntity> Available
        {
            get { return (IList<ILookupEntity>) AvailableListBox.DataSource; }
            set
            {
                //if (value != null)
                //{
                //    //AvailableListBox.DisplayMember = "Text";
                //    AvailableListBox.ValueMember = "Id";
                //}
                AvailableListBox.DataSource = null;
                AvailableListBox.DataSource = value;
            }
        }

        public IList<ILookupEntity> Assigned
        {
            get { return (IList<ILookupEntity>) AssignedListBox.DataSource; }
            set
            {
                //if (value != null)
                //{
                //    AssignedListBox.DisplayMember = "Text";
                //    AssignedListBox.ValueMember = "Id";
                //}
                AssignedListBox.DataSource = null;
                AssignedListBox.DataSource = value;
            }
        }

        protected void MoveItems(ListBox fromListBox, ListBox toListBox)
        {
            IList<ILookupEntity> fromSource = (IList<ILookupEntity>) fromListBox.DataSource;
            IList<ILookupEntity> toSource = (IList<ILookupEntity>) toListBox.DataSource;

            List<ILookupEntity> selected = new List<ILookupEntity>();
            foreach (ILookupEntity item in fromListBox.SelectedItems)
                selected.Add(item);

            if (fromListBox == AvailableListBox)
            {
                if (OnAssign != null)
                    OnAssign(this, selected);
            }
            else
            {
                if (OnRemove != null)
                    OnRemove(this, selected);
            }

            foreach (ILookupEntity selectedItem in selected)
            {
                toSource.Add(selectedItem);
                for (int i = fromSource.Count - 1; i >= 0; i--)
                {
                    if (fromSource[i].Id == selectedItem.Id)
                        fromSource.RemoveAt(i);
                }
            }

            IList<ILookupEntity> tempAssigned = this.Assigned;
            this.Assigned = null;
            this.Assigned = tempAssigned;

            IList<ILookupEntity> tempAvailable = this.Available;
            this.Available = null;
            this.Available = tempAvailable;
        }

        protected void HandleAssign()
        {
            MoveItems(AvailableListBox, AssignedListBox);
        }

        protected void HandleRemove()
        {
            MoveItems(AssignedListBox, AvailableListBox);
        }

        private void MoveRightButton_Click(System.Object sender, EventArgs e)
        {
            HandleAssign();
        }

        private void MoveLeftButton_Click(System.Object sender, EventArgs e)
        {
            HandleRemove();
        }

        private void AvailableListBox_DoubleClick(System.Object sender, EventArgs e)
        {
            HandleAssign();
        }

        private void AssignedListBox_DoubleClick(System.Object sender, EventArgs e)
        {
            HandleRemove();
        }
    }
}