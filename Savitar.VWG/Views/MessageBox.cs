#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.Views
{
  public partial class MessageBox : UserControl
  {
    public enum ParentState { Visible, Enabled }

    /// <summary>
    /// Static property to determine how to display the MessageBox. Visible will toggle the visibility state of all controls on the form, and Enabled will simple disable/enable all controls.
    /// </summary>
    public static ParentState ToggleParentState = ParentState.Enabled;
    public delegate void MethodDelegate();
    public delegate void ButtonClickDelegate(MessageBox sender, DialogResult dialogResult);
    public event ButtonClickDelegate OnButtonClick;
    
    private MethodDelegate AbortMethod;
    private MethodDelegate CancelMethod;
    private MethodDelegate IgnoreMethod;
    private MethodDelegate NoMethod;
    private MethodDelegate OKMethod;
    private MethodDelegate RetryMethod;
    private MethodDelegate YesMethod;

    protected string Message
    {
      get { return MessageLabel.Text; }
      set { MessageLabel.Text = value; }
    }

    public MessageBox()
    {
      InitializeComponent();    
    }

    private void ResetMethodPointers()
    {
      AbortMethod = CancelMethod = IgnoreMethod = NoMethod = OKMethod = RetryMethod = YesMethod = null;
    }

    private static MessageBox Show(Control parent, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
    {
      MessageBox msgBox = new MessageBox();
      msgBox.ResetMethodPointers();

      msgBox.Message = message;
      msgBox.OnButtonClick += new ButtonClickDelegate(msgBox_OnButtonClick);
      msgBox.SetupButtons(buttons);      
      parent.Controls.Add(msgBox);
      msgBox.EnableParent(false);

      return msgBox;
    }

    public static void ShowAbortRetryIgnore(Control parent, string message, string caption, MethodDelegate abort, MethodDelegate retry, MethodDelegate cancel)
    {
      MessageBox msgBox = Show(parent, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
      msgBox.AbortMethod = abort;
      msgBox.RetryMethod = retry;
      msgBox.CancelMethod = cancel;
    }

    public static MessageBox ShowOK(Control parent, string message, string caption, MessageBoxIcon icon)
    {      
      return Show(parent, message, caption, MessageBoxButtons.OK, icon);
    }

    public static void ShowOKCancel(Control parent, string message, string caption, MethodDelegate ok, MethodDelegate cancel)
    {      
      MessageBox msgBox = Show(parent, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
      msgBox.OKMethod = ok;
      msgBox.CancelMethod = cancel;
    }

    public static void ShowRetryCancel(Control parent, string message, string caption, MethodDelegate retry, MethodDelegate cancel)
    {
      MessageBox msgBox = Show(parent, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
      msgBox.RetryMethod = retry;
      msgBox.CancelMethod = cancel;      
    }

    public static void ShowYesNo(Control parent, string message, string caption, MethodDelegate yes, MethodDelegate no)
    {
      MessageBox msgBox = Show(parent, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
      msgBox.YesMethod = yes;
      msgBox.NoMethod = no;      
    }

    public static void ShowYesNoCancel(Control parent, string message, string caption, MethodDelegate yes, MethodDelegate no, MethodDelegate cancel)
    {
      MessageBox msgBox = Show(parent, message, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
      msgBox.YesMethod = yes;
      msgBox.NoMethod = no;
      msgBox.CancelMethod = cancel;
    }

    private void OnAnyButtonClick(object sender, EventArgs e)
    {
      Button button = (Button)sender;
      DialogResult dialogResult = (DialogResult)button.Tag;

      if (OnButtonClick != null)
        OnButtonClick(this, dialogResult);

      InvokeMethod(DialogResultToMethod(dialogResult));
    }

    protected MethodDelegate DialogResultToMethod(DialogResult dialogResult)
    {
      switch (dialogResult)
      {
        case DialogResult.Abort:
          return AbortMethod;
        case DialogResult.Cancel:
          return CancelMethod;
        case DialogResult.Ignore:
          return IgnoreMethod;
        case DialogResult.No:
          return NoMethod;
        case DialogResult.OK:
          return OKMethod;
        case DialogResult.Retry:
          return RetryMethod;
        case DialogResult.Yes:
          return YesMethod;
      }

      return null;
    }

    protected void SetupButton(Button button, DialogResult dialogResult)
    {
      if (dialogResult == DialogResult.None)
      {
        button.Visible = false;
        return;
      }

      button.Visible = true;
      button.Tag = dialogResult;
      button.Image = Utils.GetImage(dialogResult.ToString() + ".png");
    }

    protected void SetupButtons(MessageBoxButtons buttons)
    {
      switch (buttons)
      {
        case MessageBoxButtons.AbortRetryIgnore:
          SetupButton(ButtonOne, DialogResult.Abort);
          SetupButton(ButtonTwo, DialogResult.Retry);
          SetupButton(ButtonThree, DialogResult.Ignore);
          break;
        case MessageBoxButtons.OK:
          SetupButton(ButtonOne, DialogResult.None);
          SetupButton(ButtonTwo, DialogResult.None);
          SetupButton(ButtonThree, DialogResult.OK);
          break;
        case MessageBoxButtons.OKCancel:
          SetupButton(ButtonOne, DialogResult.None);
          SetupButton(ButtonTwo, DialogResult.OK);
          SetupButton(ButtonThree, DialogResult.Cancel);
          break;
        case MessageBoxButtons.RetryCancel:
          SetupButton(ButtonOne, DialogResult.None);
          SetupButton(ButtonTwo, DialogResult.Retry);
          SetupButton(ButtonThree, DialogResult.Cancel);
          break;
        case MessageBoxButtons.YesNo:
          SetupButton(ButtonOne, DialogResult.None);
          SetupButton(ButtonTwo, DialogResult.Yes);
          SetupButton(ButtonThree, DialogResult.No);
          break;
        case MessageBoxButtons.YesNoCancel:
          SetupButton(ButtonOne, DialogResult.Yes);
          SetupButton(ButtonTwo, DialogResult.No);
          SetupButton(ButtonThree, DialogResult.Cancel);
          break;
      }
    }

    protected void EnableParent(bool value)
    {
      Control prevParent = null;
      Control parent = this.Parent;
      while (parent != null)
      {
        foreach (Control control in parent.Controls)
        {
          if (control != this) 
          {
            if ((!value) && (control != prevParent))
            {
              if (MessageBox.ToggleParentState == ParentState.Visible)
                control.Visible = false;
              else
                control.Enabled = false;
            }
            else
            {
              if (MessageBox.ToggleParentState == ParentState.Visible)
                control.Visible = true;
              else
                control.Enabled = true;
            }
          }          
        }

        prevParent = parent;
        parent = parent.Parent;
      }
    }

    static void msgBox_OnButtonClick(MessageBox sender, DialogResult dialogResult)
    {
      sender.EnableParent(true);     
      sender.Parent.Controls.Remove(sender);      
    }

    private void MessageDialog_Load(object sender, EventArgs e)
    {
      this.Left = ((int)this.Parent.Width / 2) - ((int)this.Width / 2);
      this.Top = ((int)this.Parent.Height / 2) - ((int)this.Height / 2);
      
    }    

    protected void InvokeMethod(MethodDelegate method)
    {
      if (method != null)
        method();
    }
  }
}