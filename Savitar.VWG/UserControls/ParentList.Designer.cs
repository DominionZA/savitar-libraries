namespace Savitar.VWG.UserControls
{
  partial class ParentList
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Visual WebGui UserControl Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ParentListComboBox = new Gizmox.WebGUI.Forms.ComboBox();
      this.ParentLabel = new Gizmox.WebGUI.Forms.Label();
      this.SuspendLayout();
      // 
      // ParentListComboBox
      // 
      this.ParentListComboBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)(((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.ParentListComboBox.BorderStyle = Gizmox.WebGUI.Forms.BorderStyle.Fixed3D;
      this.ParentListComboBox.DropDownStyle = Gizmox.WebGUI.Forms.ComboBoxStyle.DropDownList;
      this.ParentListComboBox.Location = new System.Drawing.Point(82, 2);
      this.ParentListComboBox.MaxDropDownItems = 8;
      this.ParentListComboBox.Name = "ParentListComboBox";
      this.ParentListComboBox.Size = new System.Drawing.Size(248, 21);
      this.ParentListComboBox.TabIndex = 0;
      this.ParentListComboBox.SelectedIndexChanged += new System.EventHandler(this.ParentListComboBox_SelectedIndexChanged);
      // 
      // ParentLabel
      // 
      this.ParentLabel.Location = new System.Drawing.Point(3, 5);
      this.ParentLabel.Name = "ParentLabel";
      this.ParentLabel.Size = new System.Drawing.Size(73, 13);
      this.ParentLabel.TabIndex = 0;
      this.ParentLabel.Text = "Parent Label";
      this.ParentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // ParentList
      // 
      this.Controls.Add(this.ParentLabel);
      this.Controls.Add(this.ParentListComboBox);
      this.Size = new System.Drawing.Size(333, 27);
      this.Text = "ParentList";
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.ComboBox ParentListComboBox;
    private Gizmox.WebGUI.Forms.Label ParentLabel;


  }
}