#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.UserControls
{
  public partial class CheckedListBox : UserControl
  {
    public object DataSource
    {
      get { return ItemsCheckedListBox.DataSource; }
      set { ItemsCheckedListBox.DataSource = value; }
    }

    public Gizmox.WebGUI.Forms.CheckedListBox ListBox
    {
      get { return ItemsCheckedListBox; }
    }

    public void CheckAll(bool value)
    {
      if (ToggleAllCheckBox.Checked == value)
        return;

      ToggleAllCheckBox.Checked = value;
    }

    public CheckedListBox()
    {      
      InitializeComponent();
    }

    private void ToggleAllCheckBox_CheckedChanged(object sender, EventArgs e)
    {      
      for (int i = 0; i < ItemsCheckedListBox.Items.Count; i++)
        ItemsCheckedListBox.SetItemChecked(i, ToggleAllCheckBox.Checked);
    }    
  }
}