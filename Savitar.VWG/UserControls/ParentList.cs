#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;
using Savitar.Model;

#endregion

namespace Savitar.VWG.UserControls
{
  public partial class ParentList : UserControl
  {
    public delegate void MasterChangedDelegate(ParentList sender);
    public event MasterChangedDelegate OnMasterChanged;

    public IEntity SelectedItem
    {
      get
      {
        if (ParentListComboBox.SelectedItem == null)
          return null;

        return (IEntity)ParentListComboBox.SelectedItem;
      }
    }

    public int Index
    {
      get { return _Index; }
    } int _Index = -1;

    public ParentList()
    {
      InitializeComponent();
    }

    public void Refresh<TEntity>(int parentIndex, string parentLabel, IList<TEntity> items)
        where TEntity : IEntity
    {
      _Index = parentIndex;
      ParentLabel.Text = parentLabel;

      ParentListComboBox.DisplayMember = "Text";
      ParentListComboBox.ValueMember = "ID";
      ParentListComboBox.DataSource = items;
      if (ParentListComboBox.Items.Count > 0)
      {
        if (ParentListComboBox.SelectedIndex != 0)
          ParentListComboBox.SelectedIndex = 0;
        else
          ParentListComboBox_SelectedIndexChanged(ParentListComboBox, null);
      }
    }

    private void ParentListComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (OnMasterChanged != null)
        OnMasterChanged(this);
    }
  }
}