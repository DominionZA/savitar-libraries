namespace Savitar.VWG.UserControls
{
  partial class CheckedListBox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Visual WebGui UserControl Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ItemsCheckedListBox = new Gizmox.WebGUI.Forms.CheckedListBox();
      this.ToggleAllCheckBox = new Gizmox.WebGUI.Forms.CheckBox();
      this.groupBox1 = new Gizmox.WebGUI.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // ItemsCheckedListBox
      // 
      this.ItemsCheckedListBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.ItemsCheckedListBox.Location = new System.Drawing.Point(0, 0);
      this.ItemsCheckedListBox.Name = "ItemsCheckedListBox";
      this.ItemsCheckedListBox.SelectionMode = Gizmox.WebGUI.Forms.SelectionMode.One;
      this.ItemsCheckedListBox.Size = new System.Drawing.Size(319, 260);
      this.ItemsCheckedListBox.TabIndex = 0;
      // 
      // ToggleAllCheckBox
      // 
      this.ToggleAllCheckBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Left)));
      this.ToggleAllCheckBox.AutoSize = true;
      this.ToggleAllCheckBox.CheckState = Gizmox.WebGUI.Forms.CheckState.Unchecked;
      this.ToggleAllCheckBox.Location = new System.Drawing.Point(6, 17);
      this.ToggleAllCheckBox.Name = "ToggleAllCheckBox";
      this.ToggleAllCheckBox.Size = new System.Drawing.Size(72, 17);
      this.ToggleAllCheckBox.TabIndex = 1;
      this.ToggleAllCheckBox.Text = "Toggle All";
      this.ToggleAllCheckBox.UseVisualStyleBackColor = true;
      this.ToggleAllCheckBox.CheckedChanged += new System.EventHandler(this.ToggleAllCheckBox_CheckedChanged);
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)(((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this.ToggleAllCheckBox);
      this.groupBox1.FlatStyle = Gizmox.WebGUI.Forms.FlatStyle.Flat;
      this.groupBox1.Location = new System.Drawing.Point(0, 269);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(319, 40);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      // 
      // CheckedListBox
      // 
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.ItemsCheckedListBox);
      this.Size = new System.Drawing.Size(319, 312);
      this.Text = "CheckedListBox";
      this.groupBox1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.CheckedListBox ItemsCheckedListBox;
    private Gizmox.WebGUI.Forms.CheckBox ToggleAllCheckBox;
    private Gizmox.WebGUI.Forms.GroupBox groupBox1;


  }
}