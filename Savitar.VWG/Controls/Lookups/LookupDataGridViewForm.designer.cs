using System.Drawing;

namespace Savitar.VWG.Controls.Lookups
{
  partial class LookupDataGridViewForm
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.OKButtonX = new Gizmox.WebGUI.Forms.Button();
      this.CancelButtonX = new Gizmox.WebGUI.Forms.Button();
      this.label1 = new Gizmox.WebGUI.Forms.Label();
      this.SearchTextBox = new Gizmox.WebGUI.Forms.TextBox();
      this.SearchButton = new Gizmox.WebGUI.Forms.Button();
      this.dataGridViewTextBoxColumn1 = new Gizmox.WebGUI.Forms.DataGridViewTextBoxColumn();
      this.listView = new Gizmox.WebGUI.Forms.ListView();
      this.dataGridViewTextBoxColumn2 = new Gizmox.WebGUI.Forms.DataGridViewTextBoxColumn();
      this.AllCheckBox = new Gizmox.WebGUI.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // OKButtonX
      // 
      this.OKButtonX.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.OKButtonX.Enabled = false;
      this.OKButtonX.Location = new System.Drawing.Point(182, 430);
      this.OKButtonX.Name = "OKButtonX";
      this.OKButtonX.Size = new System.Drawing.Size(75, 23);
      this.OKButtonX.TabIndex = 1;
      this.OKButtonX.Text = "OK";
      this.OKButtonX.UseVisualStyleBackColor = true;
      this.OKButtonX.Click += new System.EventHandler(this.OKButtonX_Click);
      // 
      // CancelButtonX
      // 
      this.CancelButtonX.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.CancelButtonX.Location = new System.Drawing.Point(263, 430);
      this.CancelButtonX.Name = "CancelButtonX";
      this.CancelButtonX.Size = new System.Drawing.Size(75, 23);
      this.CancelButtonX.TabIndex = 2;
      this.CancelButtonX.Text = "Cancel";
      this.CancelButtonX.UseVisualStyleBackColor = true;
      this.CancelButtonX.Click += new System.EventHandler(this.CancelButtonX_Click);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(75, 23);
      this.label1.TabIndex = 4;
      this.label1.Text = "Search";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // SearchTextBox
      // 
      this.SearchTextBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)(((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.SearchTextBox.Location = new System.Drawing.Point(93, 12);
      this.SearchTextBox.Name = "SearchTextBox";
      this.SearchTextBox.Size = new System.Drawing.Size(210, 20);
      this.SearchTextBox.TabIndex = 5;
      // 
      // SearchButton
      // 
      this.SearchButton.Location = new System.Drawing.Point(309, 12);
      this.SearchButton.Name = "SearchButton";
      this.SearchButton.Size = new System.Drawing.Size(29, 20);
      this.SearchButton.TabIndex = 7;
      this.SearchButton.Text = "...";
      this.SearchButton.UseVisualStyleBackColor = true;
      this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.DataPropertyName = "Text";
      this.dataGridViewTextBoxColumn1.HeaderText = "Description";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Resizable = Gizmox.WebGUI.Forms.DataGridViewTriState.False;
      this.dataGridViewTextBoxColumn1.Width = 260;
      // 
      // listView
      // 
      this.listView.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.listView.AutoGenerateColumns = true;
      this.listView.DataMember = null;
      this.listView.ItemsPerPage = 20;
      this.listView.Location = new System.Drawing.Point(12, 38);
      this.listView.Name = "listView";
      this.listView.ShowItemToolTips = false;
      this.listView.Size = new System.Drawing.Size(326, 386);
      this.listView.TabIndex = 9;
      this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
      this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.DataPropertyName = "Text";
      this.dataGridViewTextBoxColumn2.HeaderText = "Description";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.Width = 280;
      // 
      // AllCheckBox
      // 
      this.AllCheckBox.AutoSize = true;
      this.AllCheckBox.CheckState = Gizmox.WebGUI.Forms.CheckState.Unchecked;
      this.AllCheckBox.Location = new System.Drawing.Point(12, 433);
      this.AllCheckBox.Name = "AllCheckBox";
      this.AllCheckBox.Size = new System.Drawing.Size(75, 17);
      this.AllCheckBox.TabIndex = 10;
      this.AllCheckBox.Text = ">> All <<";
      this.AllCheckBox.UseVisualStyleBackColor = true;
      this.AllCheckBox.Click += new System.EventHandler(this.AllCheckBox_Click);
      // 
      // LookupDataGridViewForm
      // 
      this.AcceptButton = this.OKButtonX;
      this.CancelButton = this.CancelButtonX;
      this.Controls.Add(this.AllCheckBox);
      this.Controls.Add(this.listView);
      this.Controls.Add(this.SearchButton);
      this.Controls.Add(this.SearchTextBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.CancelButtonX);
      this.Controls.Add(this.OKButtonX);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Size = new System.Drawing.Size(350, 460);
      this.Text = "Lookup Dialog";
      this.Load += new System.EventHandler(this.LookupListForm_Load);
      this.ResumeLayout(false);

		}

		#endregion

    private Gizmox.WebGUI.Forms.Button OKButtonX;
    private Gizmox.WebGUI.Forms.Button CancelButtonX;
    private Gizmox.WebGUI.Forms.Label label1;
    private Gizmox.WebGUI.Forms.TextBox SearchTextBox;
    private Gizmox.WebGUI.Forms.Button SearchButton;    
    private Gizmox.WebGUI.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private Gizmox.WebGUI.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private Gizmox.WebGUI.Forms.ListView listView;
    private Gizmox.WebGUI.Forms.CheckBox AllCheckBox;
	}
}
