using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Controls.Lookups
{
	public partial class LookupListForm : Form
	{
    Savitar.Data.Interfaces.ISavItemList _Items = null;

    public Savitar.Data.Interfaces.ISavItem SelectedItem
    {
      get
      {        
        if (ItemsListBox.SelectedItem == null)
          return null;

        return (Savitar.Data.Interfaces.ISavItem)ItemsListBox.SelectedItem;
      }
    }

    public LookupListForm()
		{
			InitializeComponent();      
		}

    private void OKButtonX_Click(object sender, EventArgs e)
    {
      if (this.SelectedItem == null)
      {
        MessageBox.Show("Nothing has been selected");
        return;
      }

      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void CancelButtonX_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }

    public override void Show()
    {
      throw new Exception(this.GetType().ToString() + ".Show() cannot be used. Use .ShowDialog(items) instead");
    }

    public override DialogResult ShowDialog()
    {
      throw new Exception(this.GetType().ToString() + ".ShowDialog() cannot be used. Use .ShowDialog(items) instead");
    }

    public DialogResult ShowDialog(Savitar.Data.Interfaces.ISavItemList items, Savitar.Data.Interfaces.ISavItem selectedItem)
    {
      _Items = items;

      ItemsListBox.DisplayMember = _Items.GetDisplayMember();
      ItemsListBox.ValueMember = _Items.GetValueMember();
      BindItems(_Items, selectedItem);
      
      return base.ShowDialog();
    }

    private void BindItems(Savitar.Data.Interfaces.ISavItemList items, Savitar.Data.Interfaces.ISavItem defaultSelection)
    {
      ItemsListBox.DataSource = items;

      if (ItemsListBox.Items.Count > 0)
      {
        if (defaultSelection != null)
          ItemsListBox.SelectedItem = defaultSelection;
        else
          ItemsListBox.SelectedIndex = 0;
      }

      OKButtonX.Enabled = ItemsListBox.Items.Count > 0;
    }

    private void SearchButton_Click(object sender, EventArgs e)
    {
      if (String.IsNullOrEmpty(SearchTextBox.Text))
        ItemsListBox.DataSource = _Items;        
      else
      {
        string searchText = SearchTextBox.Text.ToLower();

        var qry = (
          from i in _Items.Cast<Savitar.Data.Interfaces.ISavItem>()
          where i.Text.ToLower().Contains(searchText)
          select i);
        
        Savitar.Data.SavItemList<Savitar.Data.SavItem> items = new Savitar.Data.SavItemList<Savitar.Data.SavItem>();
        foreach (var item in qry.ToList())
        {
          items.Add(new Savitar.Data.SavItem()
          {
            Text = item.Text,
            ID = item.ID
          });
        }

        BindItems(items, null);
      }
    }

    private void LookupListForm_Load(object sender, EventArgs e)
    {
      SearchTextBox.Focus();
    }

    private void ItemsListBox_SelectedValueChanged(object sender, EventArgs e)
    {
      OKButtonX.Enabled = this.SelectedItem != null;
    }
	}
}
