using System.Drawing;

namespace Savitar.VWG.Controls.Lookups
{
	partial class LookupListForm
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.OKButtonX = new Gizmox.WebGUI.Forms.Button();
      this.CancelButtonX = new Gizmox.WebGUI.Forms.Button();
      this.label1 = new Gizmox.WebGUI.Forms.Label();
      this.SearchTextBox = new Gizmox.WebGUI.Forms.TextBox();
      this.SearchButton = new Gizmox.WebGUI.Forms.Button();
      this.ItemsListBox = new Gizmox.WebGUI.Forms.ListBox();
      this.SuspendLayout();
      // 
      // OKButtonX
      // 
      this.OKButtonX.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.OKButtonX.Enabled = false;
      this.OKButtonX.Location = new System.Drawing.Point(182, 369);
      this.OKButtonX.Name = "OKButtonX";
      this.OKButtonX.Size = new System.Drawing.Size(75, 23);
      this.OKButtonX.TabIndex = 1;
      this.OKButtonX.Text = "OK";
      this.OKButtonX.UseVisualStyleBackColor = true;
      this.OKButtonX.Click += new System.EventHandler(this.OKButtonX_Click);
      // 
      // CancelButtonX
      // 
      this.CancelButtonX.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.CancelButtonX.Location = new System.Drawing.Point(263, 369);
      this.CancelButtonX.Name = "CancelButtonX";
      this.CancelButtonX.Size = new System.Drawing.Size(75, 23);
      this.CancelButtonX.TabIndex = 2;
      this.CancelButtonX.Text = "Cancel";
      this.CancelButtonX.UseVisualStyleBackColor = true;
      this.CancelButtonX.Click += new System.EventHandler(this.CancelButtonX_Click);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(75, 23);
      this.label1.TabIndex = 4;
      this.label1.Text = "Search";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // SearchTextBox
      // 
      this.SearchTextBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)(((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.SearchTextBox.Location = new System.Drawing.Point(93, 12);
      this.SearchTextBox.Name = "SearchTextBox";
      this.SearchTextBox.Size = new System.Drawing.Size(210, 20);
      this.SearchTextBox.TabIndex = 5;
      // 
      // SearchButton
      // 
      this.SearchButton.Location = new System.Drawing.Point(309, 12);
      this.SearchButton.Name = "SearchButton";
      this.SearchButton.Size = new System.Drawing.Size(29, 20);
      this.SearchButton.TabIndex = 7;
      this.SearchButton.Text = "...";
      this.SearchButton.UseVisualStyleBackColor = true;
      this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
      // 
      // ItemsListBox
      // 
      this.ItemsListBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.ItemsListBox.Location = new System.Drawing.Point(12, 43);
      this.ItemsListBox.Name = "ItemsListBox";
      this.ItemsListBox.SelectionMode = Gizmox.WebGUI.Forms.SelectionMode.One;
      this.ItemsListBox.Size = new System.Drawing.Size(326, 316);
      this.ItemsListBox.TabIndex = 8;
      this.ItemsListBox.SelectedValueChanged += new System.EventHandler(this.ItemsListBox_SelectedValueChanged);
      // 
      // LookupListForm
      // 
      this.AcceptButton = this.OKButtonX;
      this.CancelButton = this.CancelButtonX;
      this.Controls.Add(this.ItemsListBox);
      this.Controls.Add(this.SearchButton);
      this.Controls.Add(this.SearchTextBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.CancelButtonX);
      this.Controls.Add(this.OKButtonX);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Size = new System.Drawing.Size(350, 399);
      this.Text = "Lookup Form";
      this.Load += new System.EventHandler(this.LookupListForm_Load);
      this.ResumeLayout(false);

		}

		#endregion

    private Gizmox.WebGUI.Forms.Button OKButtonX;
    private Gizmox.WebGUI.Forms.Button CancelButtonX;
    private Gizmox.WebGUI.Forms.Label label1;
    private Gizmox.WebGUI.Forms.TextBox SearchTextBox;
    private Gizmox.WebGUI.Forms.Button SearchButton;
    private Gizmox.WebGUI.Forms.ListBox ItemsListBox;
	}
}
