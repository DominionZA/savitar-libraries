using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Controls.Lookups
{
	public partial class BaseLookup : UserControl
	{
    private Savitar.Data.Interfaces.ISavItemList _Items = null;

    public delegate void SelectedItemChangedDelegate(object sender, Savitar.Data.Interfaces.ISavItem item);
    public event SelectedItemChangedDelegate SelectedItemChanged;
    public virtual bool ShowAllOption
    {
      get { return _ShowAllOption; }
      set { _ShowAllOption = value; }
    } bool _ShowAllOption = true;

    [Browsable(false)]
    public Savitar.Data.Interfaces.ISavItem SelectedItem
    {
      get { return _SelectedItem; }
      set
      {        
        //if (_SelectedItem == value)
        //  return;

        _SelectedItem = value;

        if (_SelectedItem == null)
        {
          if (ShowAllOption)
            TextLabel.Text = ">> All <<";
          else
            TextLabel.Text = "Please select";
        }
        else
          TextLabel.Text = _SelectedItem.Text;

        if (SelectedItemChanged != null)
          SelectedItemChanged(this, _SelectedItem);
      }
    } Savitar.Data.Interfaces.ISavItem _SelectedItem;

    [Browsable(false)]    
    public int SelectedID
    {
      get
      {
        if (this.SelectedItem == null)
          return 0;

        return this.SelectedItem.ID;
      }
      set
      {
        if (_Items == null)
        {
          this.SelectedItem = null;
          return;
        }

        foreach (Savitar.Data.Interfaces.ISavItem picItem in _Items)
        {
          if (picItem.ID == value)
          {
            this.SelectedItem = picItem;
            return;
          }
        }

        this.SelectedItem = null;
      }
    }

    public BaseLookup()
		{
			InitializeComponent();
           
      LookupButton.Image = Savitar.VWG.Image.Get("Buttons.Search.png");

      this.Load += new EventHandler(BaseLookup_Load);
		}

    void BaseLookup_Load(object sender, EventArgs e)
    {
      if (Savitar.ComponentModel.Component.IsInDesignMode)
        return;

      BindData();
    }

    protected virtual Savitar.Data.Interfaces.ISavItemList GetItems()
    {
      return null;
    }

    public void BindData()
    {
      if (Savitar.ComponentModel.Component.IsInDesignMode)
        return;

      BindData(GetItems());
    }

    public void BindData(Savitar.Data.Interfaces.ISavItemList items)
    {
      _Items = items;
      if ((_Items == null) || (_Items.Count == 0))
        this.SelectedItem = null;
      else
      {
        if (ShowAllOption)
          this.SelectedItem = null;
        else
        {
          System.Collections.IEnumerator enumerator = _Items.GetEnumerator();
          enumerator.MoveNext();
          this.SelectedItem = (Savitar.Data.Interfaces.ISavItem)enumerator.Current;
        }
      }
    }

    private void LookupButton_Click(object sender, EventArgs e)
    {
      LookupDataGridViewForm frm = new LookupDataGridViewForm();
      frm.FormClosed += new Form.FormClosedEventHandler(frm_FormClosed);
      frm.ShowDialog(_Items, this.Text, ShowAllOption);      
    }

    void frm_FormClosed(object sender, FormClosedEventArgs e)
    {
      LookupDataGridViewForm frm = (LookupDataGridViewForm)sender;      
      if (frm.DialogResult != DialogResult.OK)
        return;

      this.SelectedItem = frm.SelectedItem;
    }    
	}
}
