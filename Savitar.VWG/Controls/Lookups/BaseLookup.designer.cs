using System.Drawing;

namespace Savitar.VWG.Controls.Lookups
{
	partial class BaseLookup
	{
	        /// <summary>
	        /// Required designer variable.
	        /// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
	        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Visual WebGui Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.LookupButton = new Gizmox.WebGUI.Forms.Button();
      this.TextLabel = new Gizmox.WebGUI.Forms.Label();
      this.SuspendLayout();
      // 
      // LookupButton
      // 
      this.LookupButton.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.LookupButton.Location = new System.Drawing.Point(194, 0);
      this.LookupButton.Name = "button1";
      this.LookupButton.Size = new System.Drawing.Size(35, 21);
      this.LookupButton.TabIndex = 1;
      this.LookupButton.Text = "...";
      this.LookupButton.UseVisualStyleBackColor = true;
      this.LookupButton.Click += new System.EventHandler(this.LookupButton_Click);
      // 
      // TextLabel
      // 
      this.TextLabel.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.TextLabel.Location = new System.Drawing.Point(0, 0);
      this.TextLabel.Name = "TextLabel";
      this.TextLabel.Size = new System.Drawing.Size(188, 21);
      this.TextLabel.TabIndex = 2;
      this.TextLabel.Text = "<Text Here>";
      this.TextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // BaseLookup
      // 
      this.Controls.Add(this.TextLabel);
      this.Controls.Add(this.LookupButton);
      this.Size = new System.Drawing.Size(229, 21);
      this.ResumeLayout(false);

		}

		#endregion

    private Gizmox.WebGUI.Forms.Button LookupButton;
    private Gizmox.WebGUI.Forms.Label TextLabel;
	}
}
