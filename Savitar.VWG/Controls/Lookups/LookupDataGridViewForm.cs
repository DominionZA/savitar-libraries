using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Controls.Lookups
{
	public partial class LookupDataGridViewForm : Form
	{
    Savitar.Data.Interfaces.ISavItemList _Items = null;

    public Savitar.Data.Interfaces.ISavItem SelectedItem
    {
      get
      {
        if (AllCheckBox.Checked)
          return null;

        if (listView.SelectedItem == null)
          return null;
        if (listView.SelectedItem.Tag == null)
          throw new Exception(this.GetType().ToString() + ".listView.SelectedItem.Tag is null. This should not be");

        if (listView.DataSource == _Items)
          return (Savitar.Data.Interfaces.ISavItem)listView.SelectedItem.Tag;
        else // The user has done a search. Search items are a customer query so cannot just return the selected. Need to look it up in _Items.
        {
          Savitar.Data.Interfaces.ISavItem selectedItem = (Savitar.Data.Interfaces.ISavItem)listView.SelectedItem.Tag;
          foreach (Savitar.Data.Interfaces.ISavItem item in _Items)
          {
            if (item.ID == selectedItem.ID)
              return item;
          }

          return null;
        }
      }
    }

    public LookupDataGridViewForm()
		{
			InitializeComponent();

      listView.UseInternalPaging = true;
      listView.ItemsPerPage = 20;
 		}

    private void OKButtonX_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void CancelButtonX_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }

    public override void Show()
    {
      throw new Exception(this.GetType().ToString() + ".Show() cannot be used. Use .ShowDialog(items) instead");
    }

    public override DialogResult ShowDialog()
    {
      throw new Exception(this.GetType().ToString() + ".ShowDialog() cannot be used. Use .ShowDialog(items) instead");
    }

    public DialogResult ShowDialog(Savitar.Data.Interfaces.ISavItemList items, string title, bool showAll)
    {
      _Items = items;
      BindItems(_Items);
      this.Text = title;
      AllCheckBox.Visible = showAll;

      return base.ShowDialog();
    }

    private void BindItems(Savitar.Data.Interfaces.ISavItemList items)
    {
      listView.DataSource = items;      
    }

    private void SearchButton_Click(object sender, EventArgs e)
    {
      if (String.IsNullOrEmpty(SearchTextBox.Text))
        BindItems(_Items);
      else
      {
        string searchText = SearchTextBox.Text.ToLower();

        var qry = (
          from i in _Items.Cast<Savitar.Data.Interfaces.ISavItem>()
          where i.Text.ToLower().Contains(searchText)
          select i);
        
        Savitar.Data.SavItemList<Savitar.Data.SavItem> items = new Savitar.Data.SavItemList<Savitar.Data.SavItem>();
        foreach (var item in qry.ToList())
        {
          items.Add(new Savitar.Data.SavItem()
          {
            Text = item.Text,
            ID = item.ID
          });
        }

        BindItems(items);
      }
    }

    private void LookupListForm_Load(object sender, EventArgs e)
    {
      SearchTextBox.Focus();
    }

    private void listView_SelectedIndexChanged(object sender, EventArgs e)
    {
      OKButtonX.Enabled = this.SelectedItem != null;
      if (this.SelectedItem != null)
        AllCheckBox.Checked = false;
    }

    private void AllCheckBox_Click(object sender, EventArgs e)
    {
      if (AllCheckBox.Checked)
        OKButtonX.Enabled = true;
      else
        OKButtonX.Enabled = this.SelectedItem != null;
    }

    private void listView_DoubleClick(object sender, EventArgs e)
    {
      // Check if an item has been selected. If not, then do nothing
      if (this.SelectedItem == null)
        return;

      this.DialogResult = DialogResult.OK;
      this.Close();
    }
	}
}
