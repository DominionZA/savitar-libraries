﻿using System;
using System.Collections.Generic;
using Gizmox.WebGUI.Forms;
using Savitar.Data.Interfaces;
using Savitar.Model;

namespace Savitar.VWG.Controls
{
    public class Utils
    {
        public static MenuItem AddMenuItem(ContextMenu contextMenu, string menuItemText, object tag,
                                           EventHandler clickHandler)
        {
            MenuItem item = new MenuItem(menuItemText) {Tag = tag};
            item.Click += clickHandler;
            contextMenu.MenuItems.Add(item);
            return item;
        }

        public static MenuItem AddMenuItem(MenuItem parentMenuItem, string menuItemText, object tag,
                                           EventHandler clickHandler)
        {
            MenuItem item = new MenuItem(menuItemText) {Tag = tag};
            item.Click += clickHandler;
            parentMenuItem.MenuItems.Add(item);

            return item;
        }

        public static void Uncheck(MenuItem menuItem)
        {
            if (menuItem.Checked)
                menuItem.Checked = false;

            foreach (MenuItem item in menuItem.MenuItems)
                Uncheck(item);
        }

        public static void CheckItem(ContextMenu contextMenu, MenuItem menuItem)
        {
            foreach (MenuItem item in contextMenu.MenuItems)
                Uncheck(item);

            menuItem.Checked = true;
        }

        public static void CreateContextMenu<TEntity>(ContextMenu contextMenu, IList<TEntity> data,
                                             EventHandler clickHandler)
            where TEntity : class, IEntity
        {
            contextMenu.MenuItems.Clear();

            // Add the parent menu item.
            foreach (ISelfRefLookupEntity item in data)
            {
                AddMenuItem(contextMenu, item.Text, item, clickHandler);
            }

            foreach (MenuItem item in contextMenu.MenuItems)
                CreateChildren(item, data, clickHandler);
        }

        public static void CreateChildren<TEntity>(MenuItem root, IList<TEntity> data, EventHandler clickHandler)
            where TEntity : class, IEntity
        {
            TEntity rootItem = root.Tag as TEntity;
            if (rootItem == null)
                return;

            // Look for children
            foreach (TEntity item in data)
            {
                ISelfRefLookupEntity selfRefItem;
                if (item is ISelfRefLookupEntity)
                {
                    selfRefItem = item as ISelfRefLookupEntity;

                    if ((selfRefItem.fkId.HasValue) && (selfRefItem.fkId.Value == rootItem.Id))
                    {
                        MenuItem menuItem = AddMenuItem(root, item.Text, item, clickHandler);
                        CreateChildren(menuItem, data, clickHandler);
                    }
                }
            }
        }
    }
}