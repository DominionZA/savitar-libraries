using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Savitar.VWG.Forms;
using Gizmox.WebGUI.Forms;
using System.Web;
using System.Web.Security;

namespace Savitar.VWG
{
  public partial class RibbonBase : Form
  {
    protected string FooterText
    {
      get { return FooterLabel.Text; }
      set
      {
        FooterLabel.Text = value;
        bool isVisible = !String.IsNullOrEmpty(FooterLabel.Text);

        if (isVisible != FooterLabel.Visible)
          FooterLabel.Visible = isVisible;
      }
    }
    protected RibbonBar Ribbon
    {
      get { return this.MainRibbonBar; }
    }

    protected RibbonBarPage Ribbon_Homepage
    {
      get { return this.HomePage; }
    }

    public RibbonBase()
    {
      InitializeComponent();             
    }

    private void Main_Load(object sender, EventArgs e)
    {      
      ShowSplashView();
      UpdateUI();
    }

    protected virtual Views.BaseView GetSplashView()
    {
      throw new NotImplementedException();
    }

    protected virtual Views.BaseView GetMyDetailsView()
    {
      throw new NotImplementedException();
    }

    protected virtual string GetAppName()
    {
      throw new NotImplementedException();
    }

    protected virtual Version GetAppVersion()
    {
      throw new NotImplementedException();
    }

    public virtual void LoadPage(Savitar.VWG.Views.BaseView view)
    {
      try
      {
        this.Cursor = Cursors.WaitCursor;
        
        Savitar.VWG.Views.BaseTabPage page = new Savitar.VWG.Views.BaseTabPage(view);
        view.Dock = DockStyle.Fill;
        view.Margin = new Padding(0);
        page.Text = view.Text;
        page.OnViewCanInvoke += new Savitar.VWG.Views.BaseView.ActionDelegate(page_OnViewCanInvoke);
        page.OnShowView += new Savitar.VWG.Views.BaseView.ShowViewDelegate(page_OnShowView);
        page.OnCloseTab += new Savitar.VWG.Views.BaseTabPage.CloseTabDelegate(page_OnCloseTab);
        tabControl.TabPages.Add(page);        
        view.TextChanged += new EventHandler(view_TextChanged);

        ModuleNameLabel.Text = GetAppName() + " - " + view.Text;

        tabControl.SelectedIndex = tabControl.TabPages.Count - 1;
      }
      catch (Exception ex)
      {
        Utils.ShowMessage(ex, "Error Showing View " + view.ToString());
      }
      finally
      {
        this.Cursor = Cursors.Default;

        UpdateUI();
      }
    }

    void page_OnCloseTab(Savitar.VWG.Views.BaseTabPage sender)
    {
      tabControl_CloseClick(null, null);
    }

    void page_OnShowView(Savitar.VWG.Views.BaseView sender, Savitar.VWG.Views.BaseView view)
    {
      this.LoadPage(view);
    }

    void page_OnControlError(Gizmox.WebGUI.Forms.Control owner, string message)
    {
      errorProvider.SetError(owner, message);           
    }

    void page_OnViewCanInvoke(Savitar.VWG.Views.BaseView sender, Savitar.VWG.Views.BaseView.ViewAction viewAction, bool value)
    {
      switch (viewAction)
      {
        case Savitar.VWG.Views.BaseView.ViewAction.Add:
          EnableButton(AddButton, value);
          break;
        case Savitar.VWG.Views.BaseView.ViewAction.Edit:
          EnableButton(EditButton, value);
          break;
        case Savitar.VWG.Views.BaseView.ViewAction.Delete:
          EnableButton(DeleteButton, value);
          break;
        case Savitar.VWG.Views.BaseView.ViewAction.Save:
          EnableButton(SaveButton, value);
          break;
      }
    }

    void view_TextChanged(object sender, EventArgs e)
    {
      Savitar.VWG.Views.BaseView view = (Savitar.VWG.Views.BaseView)sender;
      ModuleNameLabel.Text = GetAppName() + " - " + view.Text;
      CurrentPage.Text = view.Text;
    }


    protected void EnableButton(Button button, bool value)
    {
      if (button.Enabled == value)
        return;

      button.Enabled = value;
      Utils.SetButtonImage(button);
      button.Invalidate();

      FooterPanel.Visible = AddButton.Enabled || DeleteButton.Enabled || EditButton.Enabled || SaveButton.Enabled;
    }

    protected void ShowSplashView()
    {
      Views.BaseView splashView = GetSplashView();
      splashView.Text = "Version " + GetAppVersion().ToString();
      LoadPage(splashView);
    }

    private void UpdateUI()
    {
      MainRibbonBar.Enabled = Context.Session.IsLoggedOn;

      if (Context.Session.IsLoggedOn)
        UserLabel.Text = HttpContext.Current.User.Identity.Name; // Membership.GetUser().UserName;
      else
        UserLabel.Text = "Not Logged In";
    }

    private void MyDetailsButton_Click(object sender, EventArgs e)
    {
      Views.BaseView view = GetMyDetailsView();
      view.Text = "My Details";
      LoadPage(view);
    }

    private void LogoutButton_Click(object sender, EventArgs e)
    {
      Context.Cookies["UserGUID"] = null;
      Context.Session["User"] = null;
      Context.Session.IsLoggedOn = false;

      UpdateUI();
    }

    private Views.BaseTabPage CurrentPage
    {
      get
      {
        return (Views.BaseTabPage)tabControl.SelectedItem;
      }
    }

    private void AddButton_Click(object sender, EventArgs e)
    {
      CurrentPage.InvokeAction(Savitar.VWG.Views.BaseView.ViewAction.Add);
    }

    private void EditButton_Click(object sender, EventArgs e)
    {
      CurrentPage.InvokeAction(Savitar.VWG.Views.BaseView.ViewAction.Edit);
    }

    private void DeleteButton_Click(object sender, EventArgs e)
    {
      CurrentPage.InvokeAction(Savitar.VWG.Views.BaseView.ViewAction.Delete);
    }

    private void SaveButton_Click(object sender, EventArgs e)
    {            
      CurrentPage.InvokeAction(Savitar.VWG.Views.BaseView.ViewAction.Save);
    }
    
    protected virtual Gizmox.WebGUI.Forms.RibbonBarPage AddPage(string tabName)
    {
      Gizmox.WebGUI.Forms.RibbonBarPage page = new RibbonBarPage();
      page.Text = tabName;
      MainRibbonBar.Pages.Add(page);
      return page;
    }

    protected virtual Gizmox.WebGUI.Forms.RibbonBarGroup AddGroup(Gizmox.WebGUI.Forms.RibbonBarPage page, string groupName)
    {
      Gizmox.WebGUI.Forms.RibbonBarGroup group = new RibbonBarGroup();
      group.Text = groupName;
      page.Groups.Add(group);
      return group;
    }

    protected virtual Gizmox.WebGUI.Forms.RibbonBarButtonItem AddButtonItem(Gizmox.WebGUI.Forms.RibbonBarGroup group, string text, string imageName, Type module)
    {
      if (!module.IsSubclassOf(typeof(Views.BaseView)))
        throw new Exception("All modules must inherit from Views.BaseView.");

      Gizmox.WebGUI.Forms.RibbonBarButtonItem button = new RibbonBarButtonItem();
      button.Text = text;
      button.Image = Utils.GetImage(imageName);
      button.Tag = module;
      button.Click += new EventHandler(button_Click);
      group.Items.Add(button);
      return button;
    }

    protected virtual Gizmox.WebGUI.Forms.RibbonBarFlowItem AddFlowItem(Gizmox.WebGUI.Forms.RibbonBarGroup group, string text)
    {
      Gizmox.WebGUI.Forms.RibbonBarFlowItem item = new RibbonBarFlowItem();      
      item.Text = text;
      group.Items.Add(item);
      return item;
    }
    
    protected virtual Gizmox.WebGUI.Forms.RibbonBarComboBoxItem AddComboBox(Gizmox.WebGUI.Forms.RibbonBarFlowItem flowItem)
    {      
      Gizmox.WebGUI.Forms.RibbonBarComboBoxItem item = new RibbonBarComboBoxItem();
      flowItem.Items.Add(item);
      return item;
    }

    protected virtual Gizmox.WebGUI.Forms.RibbonBarDropDownButtonItem AddDropDownButtonItem(Gizmox.WebGUI.Forms.RibbonBarGroup group, string buttonText, object tag, string imageName)
    {
      Gizmox.WebGUI.Forms.RibbonBarDropDownButtonItem item = new RibbonBarDropDownButtonItem();
      item.Text = buttonText;
      item.Tag = tag;

      if (!String.IsNullOrEmpty(imageName ))
        item.Image = Utils.GetImage(imageName);
      group.Items.Add(item);

      item.DropDownMenu = new ContextMenu();
      
      return item;
    }

    protected MenuItem AddDropDownButtonMenuItem(Gizmox.WebGUI.Forms.RibbonBarDropDownButtonItem dropDownButtonItem, string menuItemText, object tag)
    {
      MenuItem item = new MenuItem(menuItemText);
      item.Tag = tag;
      dropDownButtonItem.DropDownMenu.MenuItems.Add(item);
      return item;
    }

    void button_Click(object sender, EventArgs e)
    {
      RibbonBarButtonItem button = (RibbonBarButtonItem)sender;
      Type viewType = (Type)button.Tag;
      Views.BaseView view = (Views.BaseView)Activator.CreateInstance(viewType);

      LoadPage(view);
    }

    private void tabControl_CloseClick(object sender, EventArgs e)
    {
      foreach (WorkspaceTab tab in tabControl.TabPages)
      {
        if (tab == this.CurrentPage)
        {
          tab.Dispose();
          break;
        }
      }

      if (tabControl.TabPages.Count == 0)
        ShowSplashView();

      tabControl.SelectedIndex = tabControl.TabPages.Count - 1;
    }

    protected void CleanRibbon()
    {
      for (int pageIndex = MainRibbonBar.Pages.Count - 1; pageIndex >= 0; pageIndex--)
      {
        Gizmox.WebGUI.Forms.RibbonBarPage page = MainRibbonBar.Pages[pageIndex];
        for (int grpIndex = page.Groups.Count - 1; grpIndex >= 0; grpIndex--)
        {
          Gizmox.WebGUI.Forms.RibbonBarGroup grp = page.Groups[grpIndex];
          if (grp.Items.Count == 0)
            page.Groups.Remove(grp);
        }

        if (page.Groups.Count == 0)
          MainRibbonBar.Pages.Remove(page);
      }
    }

    private void tabControl_SelectedIndexChanged_1(object sender, EventArgs e)
    {
      if (CurrentPage == null)
        return;

      ModuleNameLabel.Text = GetAppName() + " - " + CurrentPage.Text;
      EnableButton(AddButton, CurrentPage.CanAdd);
      EnableButton(DeleteButton, CurrentPage.CanDelete);
      EnableButton(EditButton, CurrentPage.CanEdit);
      EnableButton(SaveButton, CurrentPage.CanSave);
    }    
  }
}