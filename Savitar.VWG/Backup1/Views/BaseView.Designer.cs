namespace Savitar.VWG.Views
{
  partial class BaseView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Visual WebGui UserControl Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.errorProvider = new Gizmox.WebGUI.Forms.ErrorProvider(this.components);
      this.SuspendLayout();
      // 
      // errorProvider
      // 
      this.errorProvider.BlinkRate = 3;
      this.errorProvider.BlinkStyle = Gizmox.WebGUI.Forms.ErrorBlinkStyle.BlinkIfDifferentError;
      this.errorProvider.DataMember = "";
      this.errorProvider.DataSource = "";
      this.errorProvider.Icon = null;
      // 
      // BaseView
      // 
      this.Size = new System.Drawing.Size(391, 306);
      this.Text = "BaseUserControl";
      this.Load += new System.EventHandler(this.BaseView_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.ErrorProvider errorProvider;


  }
}