#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.Views
{
  public partial class MultiAssign : UserControl
  {
    public delegate void OnAssignDelegate(System.Object sender, List<Savitar.SubSonic.ISavRecord> items);
    public delegate void OnRemoveDelegate(System.Object sender, List<Savitar.SubSonic.ISavRecord> items);

    public event OnAssignDelegate OnAssign;
    public event OnRemoveDelegate OnRemove;

    public MultiAssign()
    {
      InitializeComponent();      
    }

    public ListBox Available
    {
      get { return AvailableListBox; }
    }

    public ListBox Assigned
    {
      get { return AssignedListBox; }
    }

    private List<Savitar.SubSonic.ISavRecord> GetAvailableSelected()
    {
      List<Savitar.SubSonic.ISavRecord> items = new List<Savitar.SubSonic.ISavRecord>();
      foreach (Savitar.SubSonic.ISavRecord item in AvailableListBox.SelectedItems)
        items.Add(item);

      return items;
    }

    private List<Savitar.SubSonic.ISavRecord> GetAssignedSelected()
    {
      List<Savitar.SubSonic.ISavRecord> items = new List<Savitar.SubSonic.ISavRecord>();
      foreach (Savitar.SubSonic.ISavRecord item in AssignedListBox.SelectedItems)
        items.Add(item);

      return items;
    }

    private void MoveRightButton_Click(System.Object sender, EventArgs e)
    {
      if (OnAssign != null)      
        OnAssign(this, GetAvailableSelected());
    }

    private void MoveLeftButton_Click(System.Object sender, EventArgs e)
    {
      if (OnRemove != null)
        OnRemove(this, GetAssignedSelected());
    }

    private void AvailableListBox_DoubleClick(System.Object sender, EventArgs e)
    {
      if (OnAssign != null)
        OnAssign(this, GetAvailableSelected());
    }

    private void AssignedListBox_DoubleClick(System.Object sender, EventArgs e)
    {
      if (OnRemove != null)
        OnRemove(this, GetAssignedSelected());
    }
  }
}