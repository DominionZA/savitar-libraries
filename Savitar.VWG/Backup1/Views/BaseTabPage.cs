using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG.Views
{
	public partial class BaseTabPage : WorkspaceTab
	{
    public delegate void CloseTabDelegate(BaseTabPage sender);
    
    public event Views.BaseView.ActionDelegate OnViewCanInvoke;
    public event Views.BaseView.ShowViewDelegate OnShowView;
    public event CloseTabDelegate OnCloseTab;
    
    public bool CanAdd
    {
      get { return baseView.CanAdd; }
      set { baseView.CanAdd = value; }
    }

    public bool CanEdit
    {
      get { return baseView.CanEdit; }
      set { baseView.CanEdit = value; }
    }

    public bool CanDelete
    {
      get { return baseView.CanDelete; }
      set { baseView.CanDelete = value; }
    }

    public bool CanSave
    {
      get { return baseView.CanSave; }
      set { baseView.CanSave = value; }
    }

    private BaseView baseView = null;

		public BaseTabPage()
		{
			InitializeComponent();
		}

    public BaseTabPage(BaseView view) : this()
    {
      baseView = view;
      this.Controls.Add(view);
      //view.AutoScroll = true;
      view.Dock = DockStyle.Fill;
      view.OnViewCanInvoke += new BaseView.ActionDelegate(view_OnViewCanInvoke);
      view.OnShowView += new BaseView.ShowViewDelegate(view_OnShowView);
      view.OnCloseView += new BaseView.CloseViewDelegate(view_OnCloseView);
      view.OnShowMessage += new BaseView.ShowMessageDelegate(view_OnShowMessage);
      view.OnShowException += new BaseView.ShowExceptionDelegate(view_OnShowException);      
    }

    void view_OnShowView(BaseView sender, BaseView view)
    {
      if (OnShowView != null)
        OnShowView(sender, view);
    }

    void view_OnCloseView(BaseView sender)
    {
      if (OnCloseTab != null)
        OnCloseTab(this);
    }

    void view_OnShowException(BaseView sender, Exception ex, string caption)
    {
      Savitar.VWG.Utils.ShowError(ex, caption);
    }

    void view_OnShowMessage(BaseView sender, string message, string caption)
    {
      Savitar.VWG.Utils.ShowMessage(message, caption);
    }

    void view_OnViewCanInvoke(BaseView sender, BaseView.ViewAction viewAction, bool value)
    {
      if (OnViewCanInvoke != null)
        OnViewCanInvoke(sender, viewAction, value);
    }

    public void InvokeAction(Savitar.VWG.Views.BaseView.ViewAction action)
    {
      switch (action)
      {
        case BaseView.ViewAction.Add:
          baseView.Add();
          break;
        case BaseView.ViewAction.Delete:
          baseView.Delete();
          break;
        case BaseView.ViewAction.Edit:
          baseView.Edit();
          break;
        case BaseView.ViewAction.Save:
          baseView.Save();
          break;
        default:
          throw new Exception("Unknown ViewAction " + action.ToString());
      }
    }

    public void UpdateUI()
    {
      baseView.UpdateUI();
    }
	}
}
