﻿using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
  public class Msg
  {
    public delegate void MethodDelegate();

    public static MethodDelegate AbortMethod;
    public static MethodDelegate CancelMethod;
    public static MethodDelegate IgnoreMethod;
    public static MethodDelegate NoMethod;
    public static MethodDelegate OKMethod;
    public static MethodDelegate RetryMethod;
    public static MethodDelegate YesMethod;

    public static void ResetMethodPointers()
    {
      AbortMethod = CancelMethod = IgnoreMethod = NoMethod = OKMethod = RetryMethod = YesMethod = null;
    }

    public static void ShowAbortRetryIgnore(string message, string caption, MethodDelegate abort, MethodDelegate retry, MethodDelegate cancel)
    {
      ResetMethodPointers();
      AbortMethod = abort;
      RetryMethod = retry;
      CancelMethod = cancel;
      Gizmox.WebGUI.Forms.MessageBox.Show(message, caption, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information, ResultHandler);
    }

    public static void ShowOK(string message, string caption, MessageBoxIcon icon)
    {
      ResetMethodPointers();
      Gizmox.WebGUI.Forms.MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    public static void ShowOKCancel(string message, string caption, MethodDelegate ok, MethodDelegate cancel)
    {
      ResetMethodPointers();
      OKMethod = ok;
      CancelMethod = cancel;
      Gizmox.WebGUI.Forms.MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information, ResultHandler);
    }

    public static void ShowRetryCancel(string message, string caption, MethodDelegate retry, MethodDelegate cancel)
    {
      ResetMethodPointers();
      RetryMethod = retry;
      CancelMethod = cancel;
      Gizmox.WebGUI.Forms.MessageBox.Show(message, caption, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question, ResultHandler);
    }

    public static void ShowYesNo(string message, string caption, MethodDelegate yes, MethodDelegate no)
    {
      ResetMethodPointers();
      YesMethod = yes;
      NoMethod = no;
      Gizmox.WebGUI.Forms.MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, ResultHandler);
    }

    public static void ShowYesNoCancel(string message, string caption, MethodDelegate yes, MethodDelegate no, MethodDelegate cancel)
    {
      ResetMethodPointers();
      YesMethod = yes;
      NoMethod = no;
      CancelMethod = cancel;
      Gizmox.WebGUI.Forms.MessageBox.Show(message, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, ResultHandler);
    }

    public static void ResultHandler(object sender, EventArgs e)
    {
      Gizmox.WebGUI.Forms.MessageBoxWindow msgBox = (Gizmox.WebGUI.Forms.MessageBoxWindow)sender;
      InvokeMethod(DialogResultToMethod(msgBox.DialogResult));
    }

    protected static MethodDelegate DialogResultToMethod(DialogResult dialogResult)
    {
      switch (dialogResult)
      {
        case DialogResult.Abort:
          return AbortMethod;
        case DialogResult.Cancel:
          return CancelMethod;
        case DialogResult.Ignore:
          return IgnoreMethod;
        case DialogResult.No:
          return NoMethod;
        case DialogResult.OK:
          return OKMethod;
        case DialogResult.Retry:
          return RetryMethod;
        case DialogResult.Yes:
          return YesMethod;
      }

      return null;
    }

    protected static void InvokeMethod(MethodDelegate method)
    {
      if (method != null)
        method();
    }
  }
}
