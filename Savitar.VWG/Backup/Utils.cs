﻿using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
  public class Utils
  {
    public class Data
    {
      public static void Bind(ComboBox comboBox, Savitar.SubSonic.ISavRecords dataList)
      {
        comboBox.DisplayMember = dataList.DisplayMember;
        comboBox.ValueMember = dataList.ValueMember;
        comboBox.DataSource = dataList;
      }

      public static void Bind(ListBox listBox, Savitar.SubSonic.ISavRecords dataList)
      {
        listBox.DisplayMember = dataList.DisplayMember;
        listBox.ValueMember = dataList.ValueMember;
        listBox.DataSource = dataList;
      }
    }

    public static void ShowError(Exception ex, string title)
    {
      ShowMessage(ex, title);
    }

    public static Gizmox.WebGUI.Common.Resources.ResourceHandle GetImage(string imageName)
    {
      Gizmox.WebGUI.Common.Resources.ResourceHandle rh = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      rh.File = imageName;
      return rh;
    }

    public static void ShowMessage(string msg, string title)
    {
      Gizmox.WebGUI.Forms.MessageBox.Show(msg, title, Gizmox.WebGUI.Forms.MessageBoxButtons.OK, Gizmox.WebGUI.Forms.MessageBoxIcon.Information);
      //Views.MessageBox.ShowOK(parent, msg, title, Gizmox.WebGUI.Forms.MessageBoxIcon.Information);
    }

    public static void ShowMessage(Exception ex, string title)
    {
      string message = ex.Message;
      if ((ex.InnerException != null) && (!String.IsNullOrEmpty(ex.InnerException.Message)))
        message += "\r\n\r\n" + ex.InnerException.Message;

      Gizmox.WebGUI.Forms.MessageBox.Show(message, title, Gizmox.WebGUI.Forms.MessageBoxButtons.OK, Gizmox.WebGUI.Forms.MessageBoxIcon.Error);      
    }    

    public static void SetButtonImage(Gizmox.WebGUI.Forms.Button button)
    {
      string imageName = button.Enabled ? button.Name : button.Name + "_Disabled";
      imageName = imageName.Replace("Button", "");
      imageName += ".png";

      button.Image = Utils.GetImage(imageName);
    }

    public static void ClearErrorProviderErrors(ErrorProvider errorProvider, Control control)
    {
      errorProvider.SetError(control, null);

      foreach (Control subControl in control.Controls)
        ClearErrorProviderErrors(errorProvider, subControl);
    }
  }
}
