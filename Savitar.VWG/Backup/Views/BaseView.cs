#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.Views
{
  public struct PermissionAttributes
  {
    public int PermissionCode;
    public bool CanView;
    public bool CanAdd;
    public bool CanEdit;
    public bool CanDelete;
  }

  public partial class BaseView : UserControl
  {
    private bool haveFormErrors = false;
    protected PermissionAttributes Permission;
    public bool IsLoading;
    public new bool IsDirty
    {
      get { return _IsDirty; }
      set
      {
        if (_IsDirty == value)
          return;

        if (IsLoading)
          return;

        _IsDirty = !IsLoading && value;
      }
    } bool _IsDirty;

    public enum ViewAction { Add, Edit, Delete, Save }

    public delegate void ShowViewDelegate(Savitar.VWG.Views.BaseView sender, Savitar.VWG.Views.BaseView view);
    public delegate void CloseViewDelegate(Savitar.VWG.Views.BaseView sender);
    public delegate void ClosedDelegate(BaseView sender, BaseView nextView, Object tag);
    public delegate void ActionDelegate(BaseView sender, ViewAction viewAction,  bool value);
    public delegate void ShowMessageDelegate(BaseView sender, string message, string caption);
    public delegate void ShowExceptionDelegate(BaseView sender, Exception ex, string caption);

    public event ShowViewDelegate OnShowView;
    public event CloseViewDelegate OnCloseView;
    public event ClosedDelegate OnViewClosed;
    public event ActionDelegate OnViewCanInvoke;
    public event ShowMessageDelegate OnShowMessage;
    public event ShowExceptionDelegate OnShowException;    

    public BaseView()
    {
      InitializeComponent();
      AutoScroll = true;
      IsDirty = false;
      errorProvider.ContainerControl = this;      
    }

    public bool Enabled
    {
      get { return base.Enabled; }
      set
      {
        if (base.Enabled == value)
          return;

        base.Enabled = value;
        if (this.Enabled)
          this.BackColor = Color.White;
        else
          this.BackColor = Color.Silver;
      }
    }

    /// <summary>
    /// Gets the permission attributes from the child views.
    /// </summary>
    /// <returns></returns>
    protected virtual PermissionAttributes GetPermissionAttributes()
    {
      throw new NotImplementedException(this.GetType().ToString() + ".GetPermissionAttributes");
    }

    protected void Close(Object tag)
    {
      Close(null, tag);
    }

    protected void ShowView(BaseView view)
    {
      if (OnShowView != null)
        OnShowView(this, view);
    }

    protected void CloseView(BaseView view)
    {
      if (OnCloseView != null)
        OnCloseView(this);
    }

    protected void Close(BaseView nextView, Object tag)
    {
      if (OnViewClosed != null)
        OnViewClosed(this, nextView, tag);      
    }

    protected void ShowMessage(string message, string caption)
    {
      if (OnShowMessage != null)
        OnShowMessage(this, message, caption);
    }

    protected void ShowException(Exception ex, string caption)
    {
      if (OnShowException != null)
        OnShowException(this, ex, caption);
    }

    protected void AddControlError(Control owner, string message)
    {
      haveFormErrors = true;
      errorProvider.SetError(owner, message);
    }

    #region Can... methods
    public virtual bool CanAdd
    {
      get { return _CanAdd && this.Permission.CanAdd; }
      set
      {
        _CanAdd = value;
        CanInvoke(ViewAction.Add, this.CanAdd);
      }
    } bool _CanAdd;

    public virtual bool CanEdit
    {
      get { return _CanEdit && this.Permission.CanEdit; }
      set
      {
        _CanEdit = value;
        CanInvoke(ViewAction.Edit, this.CanEdit);
      }
    } bool _CanEdit;

    public virtual bool CanDelete
    {
      get { return _CanDelete && this.Permission.CanDelete; }
      set
      {
        _CanDelete = value;
        CanInvoke(ViewAction.Delete, this.CanDelete);
      }
    } bool _CanDelete;

    public virtual bool CanSave
    {
      get { return _CanSave; }
      set
      {
        _CanSave = value;
        CanInvoke(ViewAction.Save, value);
      }
    } bool _CanSave;

    private void CanInvoke(ViewAction viewAction, bool value)
    {
      if (OnViewCanInvoke != null)
        OnViewCanInvoke(this, viewAction, value);
    }
    #endregion

    public virtual void Add() { }
    public virtual void Edit() { }
    public virtual void Delete() { }

    public void ClearErrorProviderErrors(Control control)
    {
      Savitar.VWG.Utils.ClearErrorProviderErrors(errorProvider, control);
      haveFormErrors = false;
    }

    protected virtual void ValidateViewData()
    {
      throw new NotImplementedException("ValidateViewData must be implemented in base classes");
    }

    protected virtual void SaveViewData()
    {
      throw new NotImplementedException("SaveViewData must be implemented in base classes");
    }

    public bool Save() 
    {
      ClearErrorProviderErrors(this);

      ValidateViewData();
      if (haveFormErrors)
        return false;

      try
      {
        try
        {
          this.Cursor = Cursors.WaitCursor;
          SaveViewData();
        }
        finally
        {
          this.Cursor = Cursors.Default;
        }

        this.IsDirty = false;
//        this.CanSave = false;

        ShowMessage("Data successfully saved", "Success");
        
        return true;
      }
      catch (Exception ex)
      {
        ShowException(ex, "Error Saving");
        return false;
      }
    }

    private void BaseView_Load(object sender, EventArgs e)
    {
      if (Savitar.ComponentModel.Component.IsInDesignMode)
        return;

      IsLoading = true;
      try
      {        
        this.Permission = GetPermissionAttributes();
        BindData();
        UpdateUI();
      }
      finally
      {
        this.Enabled = this.Permission.CanView;
        IsLoading = false;
        IsDirty = false;
      }
    }

    public virtual void BindData()
    {
      
    }

    public virtual void UpdateUI()
    {
      CanInvoke(ViewAction.Add, this.CanAdd);
      CanInvoke(ViewAction.Edit, this.CanEdit);
      CanInvoke(ViewAction.Delete, this.CanDelete);
      CanInvoke(ViewAction.Save, this.CanSave);
    }
  }
}