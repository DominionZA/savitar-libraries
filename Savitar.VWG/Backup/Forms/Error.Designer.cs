namespace Savitar.VWG.Forms
{
  partial class ErrorForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.MessageTextBox = new Gizmox.WebGUI.Forms.TextBox();
      this.OKButton = new Gizmox.WebGUI.Forms.Button();
      this.SubmitButton = new Gizmox.WebGUI.Forms.Button();
      this.SuspendLayout();
      // 
      // MessageTextBox
      // 
      this.MessageTextBox.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((((Gizmox.WebGUI.Forms.AnchorStyles.Top | Gizmox.WebGUI.Forms.AnchorStyles.Bottom)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Left)
                  | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.MessageTextBox.BorderColor = System.Drawing.Color.Red;
      this.MessageTextBox.Location = new System.Drawing.Point(12, 12);
      this.MessageTextBox.Multiline = true;
      this.MessageTextBox.Name = "MessageTextBox";
      this.MessageTextBox.Size = new System.Drawing.Size(365, 123);
      this.MessageTextBox.TabIndex = 0;
      // 
      // OKButton
      // 
      this.OKButton.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Right)));
      this.OKButton.Location = new System.Drawing.Point(302, 141);
      this.OKButton.Name = "OKButton";
      this.OKButton.Size = new System.Drawing.Size(75, 23);
      this.OKButton.TabIndex = 1;
      this.OKButton.Text = "OK";
      this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
      // 
      // SubmitButton
      // 
      this.SubmitButton.Anchor = ((Gizmox.WebGUI.Forms.AnchorStyles)((Gizmox.WebGUI.Forms.AnchorStyles.Bottom | Gizmox.WebGUI.Forms.AnchorStyles.Left)));
      this.SubmitButton.Location = new System.Drawing.Point(12, 141);
      this.SubmitButton.Name = "SubmitButton";
      this.SubmitButton.Size = new System.Drawing.Size(75, 23);
      this.SubmitButton.TabIndex = 2;
      this.SubmitButton.Text = "Submit";
      this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
      // 
      // ErrorForm
      // 
      this.AcceptButton = this.OKButton;
      this.Controls.Add(this.SubmitButton);
      this.Controls.Add(this.OKButton);
      this.Controls.Add(this.MessageTextBox);
      this.DialogResult = Gizmox.WebGUI.Forms.DialogResult.OK;
      this.FormBorderStyle = Gizmox.WebGUI.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Size = new System.Drawing.Size(389, 176);
      this.Text = "Error";
      this.Load += new System.EventHandler(this.ErrorForm_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.TextBox MessageTextBox;
    private Gizmox.WebGUI.Forms.Button OKButton;
    private Gizmox.WebGUI.Forms.Button SubmitButton;


  }
}