#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.VWG.Forms
{
  public partial class ErrorForm : Form
  {
    public string Message
    {
      get { return MessageTextBox.Text; }
      set { MessageTextBox.Text = value; }
    }

    public string Title
    {
      get { return this.Text; }
      set { this.Text = value; }
    }

    public ErrorForm()
    {
      InitializeComponent();
    }

    public static void Show(string message, string title)
    {
      ErrorForm frm = new ErrorForm();
      frm.Title = title;
      frm.Message = message;
      frm.ShowDialog();
    }

    public static void Show(Exception ex, string title)
    {
      Show(ex.Message, title);
    }

    private void OKButton_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void SubmitButton_Click(object sender, EventArgs e)
    {
      try
      {
        this.Enabled = false;

        string toAddress = System.Configuration.ConfigurationManager.AppSettings["SupportEmail"];

        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        msg.To.Add(toAddress);
        msg.IsBodyHtml = true;
        msg.Body = MessageTextBox.Text;

        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
        smtpClient.Send(msg);

        MessageBox.Show("Error Message sent to support.\n\nThank-you", "Message Sent", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Mail send failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        this.Enabled = true;
      }
    }

    private void ErrorForm_Load(object sender, EventArgs e)
    {
      SubmitButton.Enabled = System.Configuration.ConfigurationManager.AppSettings["SupportEmail"] != null;
    }
  }
}