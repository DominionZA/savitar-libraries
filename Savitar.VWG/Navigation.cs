﻿using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
  public class Navigation
  {
    public static void ShowForm(string form)
    {
      
      //if (VWGContext.Current == null)
      //  throw new Exception("VWGContext.Current is null. Cannot redirect");
      
      form = form.Replace(".wgx", ".ashx");
      System.Web.HttpContext.Current.Response.Redirect(form);
      //VWGContext.Current.Redirect(form);
    }    
  }
}
