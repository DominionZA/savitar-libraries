﻿using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
	public class Wizard
	{
		public enum Direction { Back, Next }

		public delegate void PageChangingDelegate(object sender, TabPage currentPage, TabPage newPage, Direction direction, ref bool cancel);
		public delegate void PageChangedDelegate(object sender, TabPage currentPage, TabPage oldPage, Direction direction);
		public delegate void FinshedDelegate(object sender);

		public event PageChangingDelegate OnPageChanging;
		public event PageChangedDelegate OnPageChanged;
		public event FinshedDelegate OnFinished;

		private TabControl tabControl = null;
		private Button backButton = null;
		private Button nextButton = null;
		private Label headerLabel = null;

		public bool BackEnabled
		{
			get { return _BackEnabled; }
			set 
			{ 
				_BackEnabled = value;
				CanGoBack = _BackEnabled;
			}
		} bool _BackEnabled = true;

		public bool NextEnabled
		{
			get { return _NextEnabled; }
			set
			{
				_NextEnabled = value;
				CanGoNext = _NextEnabled;
			}
		} bool _NextEnabled = true;

		protected bool CanGoBack
		{
			get { return backButton.Enabled; }
			set { backButton.Enabled = value && BackEnabled; }
		}

		protected bool CanGoNext
		{
			get { return nextButton.Enabled; }
			set { nextButton.Enabled = value && NextEnabled; }
		}

		public Wizard(TabControl tab, Button backButton, Button nextButton, Label headerLabel)
		{
			if (tab == null)
				throw new Exception("You cannot instantiate " + this.GetType().ToString() + " with a null tab value");
			if (backButton == null)
        throw new Exception("You cannot instantiate " + this.GetType().ToString() + " with a null backButton value");
			if (nextButton == null)
        throw new Exception("You cannot instantiate " + this.GetType().ToString() + " with a null nextButton value");

			tab.ShowCloseButton = false;
			tab.Multiline = false;
			tab.Appearance = TabAppearance.Logical;

			this.tabControl = tab;
			this.backButton = backButton;
			this.nextButton = nextButton;
			this.headerLabel = headerLabel;			

			this.nextButton.Click += new EventHandler(nextButton_Click);
			this.backButton.Click += new EventHandler(backButton_Click);

			GoFirst();
		}

		private void UpdateHeaderLabel()
		{
			if (headerLabel == null)
				return;

			headerLabel.Text = tabControl.SelectedItem.Text;
		}

		private void UpdateNextButton()
		{
			if (tabControl.SelectedIndex == tabControl.TabPages.Count - 1)
				nextButton.Text = "Finish";
			else
				nextButton.Text = ">>";
		}

		void backButton_Click(object sender, EventArgs e)
		{
			GoBack();	
		}

		void nextButton_Click(object sender, EventArgs e)
		{
			GoNext();	
		}

		public void GoFirst()
		{
			tabControl.SelectedIndex = 0;
			_BackEnabled = true;
			_NextEnabled = true;
			CanGoBack = false;
			CanGoNext = this.tabControl.TabPages.Count > 1;
			UpdateNextButton();
			UpdateHeaderLabel();
		}

		public void GoBack()
		{
			if (this.tabControl.SelectedIndex == 0)
				return;

			try
			{
				if (OnPageChanging != null)
				{
					bool cancel = false;
					OnPageChanging(this, tabControl.SelectedItem, tabControl.TabPages[tabControl.SelectedIndex - 1], Direction.Back, ref cancel);
					if (cancel)
						return;
				}

				this.tabControl.SelectedIndex--;

				if (OnPageChanged != null)
					OnPageChanged(this, tabControl.SelectedItem, tabControl.TabPages[tabControl.SelectedIndex + 1], Direction.Back);
			}
			finally
			{
				UpdateHeaderLabel();
				CanGoBack = (tabControl.SelectedIndex > 0) && BackEnabled;
				UpdateNextButton();
			}
		}

		public void GoNext()
		{
			if (this.tabControl.SelectedIndex == this.tabControl.TabPages.Count - 1) // On the last page. FINISH
			{
				if (OnFinished != null)
					OnFinished(this);

				return;
			}

			try
			{
				if (OnPageChanging != null)
				{
					bool cancel = false;
					OnPageChanging(this, tabControl.SelectedItem, tabControl.TabPages[tabControl.SelectedIndex + 1], Direction.Next, ref cancel);
					if (cancel)
						return;
				}

				this.tabControl.SelectedIndex++;

				if (OnPageChanged != null)
					OnPageChanged(this, tabControl.SelectedItem, tabControl.TabPages[tabControl.SelectedIndex - 1], Direction.Next);
			}
			finally
			{
				UpdateHeaderLabel();
				CanGoBack = true && BackEnabled;
				UpdateNextButton();
			}			
		}
	}
}
