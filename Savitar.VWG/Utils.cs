﻿using System;
using System.Collections.Generic;
using System.Text;
using Gizmox.WebGUI.Forms;

namespace Savitar.VWG
{
  public class Utils
  {
    public static void SetButtonImage(Gizmox.WebGUI.Forms.Button button)
    {
      string imageName = button.Enabled ? button.Name : button.Name + "_Disabled";
      imageName = imageName.Replace("Button", "");
      imageName += ".png";

      button.Image = Image.Get(imageName);
    }

    public static void ClearErrorProviderErrors(ErrorProvider errorProvider, Control control)
    {
      errorProvider.SetError(control, null);

      foreach (Control subControl in control.Controls)
        ClearErrorProviderErrors(errorProvider, subControl);
    }

    /*
    public static void ShowView(Savitar.VWG.Views.BaseView view)
    {
      if (Forms.RibbonBase.AppMainForm == null)
        throw new Exception("Forms.RibbonBase.AppMainForm is null. It needs to point to the RibbonBase main application form");

      Forms.RibbonBase.AppMainForm.LoadView(view);
    }
     */ 
  }
}
