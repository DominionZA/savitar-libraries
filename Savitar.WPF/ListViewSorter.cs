﻿using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections;
using System.Windows;

namespace Savitar.WPF
{
    public delegate void SortMethod(string sortBy, ListSortDirection direction, IEnumerable source);

    public class ListViewSorter
    {
        private ListSortDirection lastDirection = ListSortDirection.Ascending;
        private GridViewColumnHeader lastHeaderClicked;

        public DataTemplate DescendingTemplate { get; set; }

        public DataTemplate AscendingTemplate { get; set; }

        public SortMethod CustomSorter { get; set; }  = null;

        private void Sort(string sortBy, ListSortDirection direction, IEnumerable source)
        {
            if (!string.IsNullOrEmpty(sortBy) && source != null)
            {
                var dataView = CollectionViewSource.GetDefaultView(source);

                dataView.SortDescriptions.Clear();
                var sd = new SortDescription(sortBy, direction);
                dataView.SortDescriptions.Add(sd);
                dataView.Refresh();
            }
        }

        public void HeaderSort(GridViewColumnHeader headerClicked, IEnumerable itemSource)
        {
            ListSortDirection direction;

            if (headerClicked == null) 
                return;

            if (headerClicked.Role == GridViewColumnHeaderRole.Padding) 
                return;

            if (headerClicked != lastHeaderClicked)
                direction = ListSortDirection.Ascending;
            else
                direction = lastDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;

            string header = (headerClicked.Tag != null) ? headerClicked.Tag as string : headerClicked.Name;

            if (CustomSorter == null)
                Sort(header, direction, itemSource);
            else
                CustomSorter(header, direction, itemSource);

            headerClicked.Column.HeaderTemplate = direction == ListSortDirection.Ascending ? AscendingTemplate : DescendingTemplate;

            // Remove arrow from previously sorted header
            if (lastHeaderClicked != null && lastHeaderClicked != headerClicked)
                lastHeaderClicked.Column.HeaderTemplate = null;

            lastHeaderClicked = headerClicked;
            lastDirection = direction;
        }
    }
}
