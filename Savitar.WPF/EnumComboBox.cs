﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;

namespace Savitar.WPF
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:Savitar.WPF"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:Savitar.WPF;assembly=Savitar.WPF"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:EnumComboBox/>
    ///
    /// </summary>
    public class EnumComboBox : ComboBox
    {
        static EnumComboBox()
        {

            //This OverrideMetadata call tells the system that this element wants to provide a style that is different than its base class.  
            //This style is defined in themes\generic.xaml  
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(EnumComboBox), new FrameworkPropertyMetadata(typeof(EnumComboBox)));  
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            FillItems();
        }

        [Browsable(true), Category("Data"), Description("Sets the fully qualified type of the enum to display in the DropDownList")]
        public string EnumTypeName { get; set; }

        private List<KeyValuePair<int, string>> CachedValues
        {
            get
            {
                if (System.Windows.Application.Current != null)
                    return (List<KeyValuePair<int, string>>)System.Windows.Application.Current.Properties["EnumComboBox_" + EnumTypeName];

                return null;
            }
            set
            {
                if (System.Windows.Application.Current != null)
                    System.Windows.Application.Current.Properties["EnumComboBox_" + EnumTypeName] = value;
            }
        }

        private void FillItems()
        {
            var values = CachedValues;
            if (values == null)
            {
                values = new List<KeyValuePair<int, string>>();
                var enumType = Type.GetType(EnumTypeName);
                if (enumType == null)
                    throw new ArgumentException("Cannot load the type: " + EnumTypeName);

                var enumValues = Enum.GetValues(enumType);
                for (var index = 0; index < enumValues.Length; index++)
                {
                    var value = (int)enumValues.GetValue(index);
                    var enumValue = (Enum)enumValues.GetValue(index);
                    values.Add(new KeyValuePair<int, string>(value, EnumerateUtil.GetDescription(enumValue)));
                }
                CachedValues = values;
            }

            this.ItemsSource = values;
            this.DisplayMemberPath = "Value";
            this.SelectedValuePath = "Key";
        }
    }
}
