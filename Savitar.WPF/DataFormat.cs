﻿using System;
using System.Windows.Data;

namespace Savitar.WPF
{
    [ValueConversion(typeof(object), typeof(string))]
    public class DataFormat : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter is string formatString)
                return string.Format(culture, formatString, value);

            return value?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // we don't intend this to ever be called
            return null;
        }
    }
}