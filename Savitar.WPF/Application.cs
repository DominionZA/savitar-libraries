﻿using System.Threading;
using System.Windows.Threading;

namespace Savitar.WPF
{
    public class Application
    {
        private static readonly Windows.Wait WaitWindow = new Windows.Wait();

        public static void DoEvents()
        {
            System.Windows.Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));
        }

        public static void WaitStart(string msg)
        {
            WaitWindow.Show();
        }

        public static void WaitEnd()
        {
            WaitWindow.Hide();
        }
    }
}