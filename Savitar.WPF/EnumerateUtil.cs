﻿using System;
using System.EnterpriseServices;

namespace Savitar.WPF
{
    // This code thanks to http://roecode.wordpress.com/category/wpf/
    //public enum TestType
    //{
    //  [Description("Object One")]
    //  ObjectOne = 1,
    //  [Description("Object Two")]
    //  ObjectTwo = 2
    //}  

    public class EnumerateUtil
    {
        private EnumerateUtil()
        {
        }

        public static string GetDescription(Enum value)
        {
            DescriptionAttribute[] customAttributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return ((customAttributes.Length > 0) ? customAttributes[0].ToString() : value.ToString());
        }
    }
}