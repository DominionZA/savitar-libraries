using System.Web;

namespace Savitar.Web
{
    public class Application
    {
        public static string MachineName => HttpContext.Current.Server.MachineName;

        public static string PageUrlWithParams => HttpContext.Current.Request.RawUrl;

        public static string PagePhysicalPath => HttpContext.Current.Request.PhysicalPath;

        public static string ServerName => HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

        public static string RootUrl => $"http://{HttpContext.Current.Request.ServerVariables["SERVER_NAME"]}";

        public static string PhysicalPath()
        {
            return HttpContext.Current.Request.PhysicalApplicationPath;
        }
    }
}