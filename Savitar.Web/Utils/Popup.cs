namespace Savitar.Web.Utils
{
    public class Popup
    {
        public static void ShowDialog(System.Web.UI.Page page, string msg)
        {
            if (!page.ClientScript.IsClientScriptBlockRegistered("DialogMsg"))
                page.ClientScript.RegisterClientScriptBlock(page.GetType(), "DialogMsg",
                    $"alert(\"{msg.Replace("\r\n", "\\n").Replace("\"", "")}\")", true);
        }

        public static void ShowWindow(System.Web.UI.Page parentPage, string popupPage, bool scrollbars, bool resizable, int width, int height)
        {
            var scrollBarsText = scrollbars ? "yes" : "no";
            var resizableText = resizable ? "yes" : "no";

            parentPage.ClientScript.RegisterClientScriptBlock(parentPage.GetType(), "OpenPopupWindow",
                $"window.open('{parentPage.ResolveClientUrl(popupPage)}', '', 'scrollbars={scrollBarsText},resizable={resizableText},width={width},height={height}');", true);
        }

        public static void ShowReport(System.Web.UI.Page parentPage, string reportPage)
        {
            Popup.ShowWindow(parentPage, reportPage, false, false, 800, 570);
        }
    }
}