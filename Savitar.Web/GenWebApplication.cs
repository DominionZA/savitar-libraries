using System;
using System.Web;

namespace Savitar.Web
{
	public class GenWebApplication {
		public static string MachineName => HttpContext.Current.Server.MachineName;

        public static bool IsLive
		{
			get 
			{
				var s = GenWebApplication.MachineName;

				return (!s.ToUpper().Equals("MIKE")) &&
					(!s.ToUpper().Equals("DEV01")) &&
					(!s.ToUpper().Equals("WENDY")) &&
					(!s.ToUpper().Equals("LAPTOP")) &&
                    (!s.ToUpper().Equals("ROWAN")) &&
                    (!s.ToUpper().Equals("SMARTDEV01"));
			}
		}

		public static string PageUrlWithParams => HttpContext.Current.Request.RawUrl;

        public static string PagePhysicalPath => HttpContext.Current.Request.PhysicalPath;

        public static string PageVirtualUrl => HttpContext.Current.Request.Path;

        public static string ServerName => HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

        public static string RootUrl
		{
			get 
            {				
                var result = "http://" + GenWebApplication.ServerName + '/';

                if (GenWebApplication.IsLive) 
                    return result;

                var s = GenWebApplication.PageVirtualUrl;
                var i = s.IndexOf('/');

                if (i != 0) return 
                    result;

                i = s.IndexOf('/', 2);
                if (i > 0)						
                    result += s.Substring(1, i);

                return result;
			}			
		}

		public static string PhysicalPath()
		{
			return HttpContext.Current.Request.PhysicalApplicationPath;
		}				
	}

	public class GenWeb
	{
		public static int RequestToInt(System.Web.UI.Page page, string varName, int defaultValue)
		{
            var result = defaultValue;
			if (page.Request.QueryString[varName] == null)
			{
				return result;
			}

			result = Convert.ToInt32(page.Request.QueryString[varName]);
			return result;
		}

		public static bool IsDomainAvailable(string domainName)
		{
			try 
			{        
				var oRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(domainName);			
				oRequest.Method = "GET";
				oRequest.Timeout = 3000;		
	
				System.Net.HttpWebResponse oResponse = (System.Net.HttpWebResponse)oRequest.GetResponse();
			
				return (oResponse.StatusCode != System.Net.HttpStatusCode.OK);				
			}
			catch 
			{
				return true; // Should be available
			}
		}
	}
	
}
