using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;

namespace Generic.Web
{
	public class GenWebApplication {
		static public string MachineName
		{
			get { return HttpContext.Current.Server.MachineName; }
		}

		static public bool IsLive
		{
			get 
			{
				string s = GenWebApplication.MachineName;

				return (!s.ToUpper().Equals("MIKE")) &&
					(!s.ToUpper().Equals("DEV01")) &&
					(!s.ToUpper().Equals("WENDY")) &&
					(!s.ToUpper().Equals("LAPTOP")) &&
                    (!s.ToUpper().Equals("ROWAN")) &&
                    (!s.ToUpper().Equals("SMARTDEV01"));
			}
		}

		public static string PageURLWithParams
		{            
			get { return HttpContext.Current.Request.RawUrl; }
		}	

		public static string PagePhysicalPath
		{
			get { return HttpContext.Current.Request.PhysicalPath; }
		}

		public static string PageVirtualURL
		{
			get { return HttpContext.Current.Request.Path; }
		}
   
		public static string ServerName
		{
			get { return HttpContext.Current.Request.ServerVariables["SERVER_NAME"]; }
		}

		public static string RootURL
		{
			get {				
        string result = "http://" + GenWebApplication.ServerName + '/';				 				 
				
				if (!GenWebApplication.IsLive)
				{
					string s = GenWebApplication.PageVirtualURL;
					int i = s.IndexOf('/');

					if (i == 0)
					{
						i = s.IndexOf('/', 2);
						if (i > 0)						
							result = result + s.Substring(1, i);						
					}
				}

				return result;
			}			
		}

		public static string PhysicalPath()
		{
			return HttpContext.Current.Request.PhysicalApplicationPath;
		}				
	}

	public class GenWeb
	{
		public static int RequestToInt(System.Web.UI.Page APage, string AVarName, int ADefaultValue)
		{
			int result = 0;
			result = ADefaultValue;
			if (!(APage.Request.QueryString[AVarName] != null))
			{
				return result;
			}
			result = Convert.ToInt32(APage.Request.QueryString[AVarName]);
			return result;
		}

		public static bool IsDomainAvailable(string ADomainName)
		{
			try 
			{        
				System.Net.HttpWebRequest oRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(ADomainName);			
				oRequest.Method = "GET";
				oRequest.Timeout = 3000;		
	
				System.Net.HttpWebResponse oResponse = (System.Net.HttpWebResponse)oRequest.GetResponse();
			
				return (!(oResponse.StatusCode == System.Net.HttpStatusCode.OK));				
			}
			catch 
			{
				return true; // Should be available
			}
		}
	}
	
}
