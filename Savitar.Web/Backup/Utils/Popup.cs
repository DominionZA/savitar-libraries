using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.Web.Utils
{
  public class Popup
  {
    public static void ShowDialog(System.Web.UI.Page page, string msg)
    {
      if (!page.ClientScript.IsClientScriptBlockRegistered("DialogMsg"))
        page.ClientScript.RegisterClientScriptBlock(page.GetType(), "DialogMsg",
          String.Format("alert(\"{0}\")", msg.Replace("\r\n", "\\n").Replace("\"", "")), true);
    }

    public static void ShowWindow(System.Web.UI.Page parentPage, string popupPage, bool scrollbars, bool resizable, int width, int height)
    {
      string scrollBarsText = scrollbars == true ? "yes" : "no";
      string resizableText = resizable == true ? "yes" : "no";
      
      parentPage.ClientScript.RegisterClientScriptBlock(parentPage.GetType(), "OpenPopupWindow",
          String.Format("window.open('{0}', '', 'scrollbars={1},resizable={2},width={3},height={4}');",
          parentPage.ResolveClientUrl(popupPage), scrollBarsText, resizableText, width, height), true);
    }

    public static void ShowReport(System.Web.UI.Page parentPage, string reportPage)
    {
      Popup.ShowWindow(parentPage, reportPage, false, false, 800, 570);
    }
  }
}
