using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;

namespace Savitar.Web
{
  public class Application
  {
    static public string MachineName
    {
      get { return HttpContext.Current.Server.MachineName; }
    }    

    public static string PageURLWithParams
    {
      get { return HttpContext.Current.Request.RawUrl; }
    }

    public static string PagePhysicalPath
    {
      get { return HttpContext.Current.Request.PhysicalPath; }
    }

    public static string ServerName
    {
      get { return HttpContext.Current.Request.ServerVariables["SERVER_NAME"]; }
    }

    public static string RootURL
    {
      get
      {
        return String.Format("http://{0}", HttpContext.Current.Request.ServerVariables["SERVER_NAME"]);
      }
    }

    public static string PhysicalPath()
    {
      return HttpContext.Current.Request.PhysicalApplicationPath;
    }	
  }
}
