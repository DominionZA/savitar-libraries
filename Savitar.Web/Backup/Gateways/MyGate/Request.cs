using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Savitar.Web.Gateways.MyGate
{
  public enum RequestMode { Test = 0, Live = 1 }
  public enum CurrencyCode { ZAR }

  public class Request
  {
    public string URL = "https://www.mygate.co.za/virtual/4x0x0/dsp_details.cfm";

    /// <summary>
    /// Specifies a Live or Test transaction
    /// </summary>
    public RequestMode Mode = RequestMode.Test;
        
    /// <summary>
    /// The MerchantID allocated by MyGate
    /// </summary>
    public Guid MerchantID = Guid.Empty;

    /// <summary>
    /// The ApplicationID allocated by MyGate
    /// </summary>
    public Guid ApplicationID = Guid.Empty;

    /// <summary>
    /// This is any unique number that you refer to your transaction with. EG: Invoice No, Order No, Cart ID, etc...
    /// </summary>
    public string MerchantReference = "";

    /// <summary>
    /// The price of the transaction. Numeric with 2 decimal places.
    /// </summary>
    public Decimal Price = 0;

    /// <summary>
    /// 3 letters as specified in ISO4217. Eg: ZAR
    /// </summary>
    public CurrencyCode? CurrencyCode = null;

    /// <summary>
    /// Optional: This is used if you want to display a different price to your users and the price needs to show differently. The actual price and currency code will the user will be billed for.
    /// </summary>
    public Decimal? DisplayPrice = null;

    /// <summary>
    /// Optional: This is used if you ant to display a different currency to your users. The actual price and currency code is still billed to your users.
    /// </summary>
    public CurrencyCode? DisplayCurrencyCode = null;

    /// <summary>
    /// This is the URL the system will redirect to in the event of a completed successful transaction.
    /// </summary>
    public string RedirectSuccessfulURL = "";

    /// <summary>
    /// This is the URL the system will redirect to in the event of a completed failed transaction.
    /// </summary>
    public string RedirectFailedURL = "";

    /// <summary>
    /// Optional: The two letter country code of the country to which the item/s are being shipped.
    /// </summary>
    public string ShippingCountryCode = null;

    /// <summary>
    /// Optional: Unique Client Index.
    /// </summary>
    public string UCI = null;

    /// <summary>
    /// Optional: The IP Address of the online purchaser.
    /// </summary>
    public string IPAddress = null;

    /// <summary>
    /// The number of lineitems that were ordered.
    /// </summary>
    public Double Qty = 0;

    /// <summary>
    /// Optional: Item Reference number. Allows you to link back to an item reference number in the shopping cart.
    /// </summary>
    public string ItemReference = null;

    /// <summary>
    /// Item Description.
    /// </summary>
    public string ItemDescription = "";

    /// <summary>
    /// The price paid by the purchaser for the item.
    /// </summary>
    public Decimal ItemAmount = 0;

    /// <summary>
    /// Optional: Shipping costs for the order.
    /// </summary>
    public Decimal? ShippingCost = 0;

    /// <summary>
    /// Optional: Discount given on the order.
    /// </summary>
    public Decimal? Discount = 0;

    /// <summary>
    /// Optional: The name of the individual who has placed the order
    /// </summary>
    public string Recipient = null;

    /// <summary>
    /// Optional: The shipping address line 1
    /// </summary>
    public string ShippingAddress1 = null;

    /// <summary>
    /// Optional: The shipping address line 2
    /// </summary>
    public string ShippingAddress2 = null;

    /// <summary>
    /// Optional: The shipping address line 3
    /// </summary>
    public string ShippingAddress3 = null;

    /// <summary>
    /// Optional: The shipping address line 4
    /// </summary>
    public string ShippingAddress4 = null;

    /// <summary>
    /// Optional: The shipping address line 5
    /// </summary>
    public string ShippingAddress5 = null;

    public string Reference = "";

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine("<html>");
      sb.AppendLine("<title>MyGate Express</title>");
      sb.AppendLine("</head>");
      sb.AppendLine("<body onload=\"frm.submit();\">");
      sb.AppendLine("<form name=\"frm\" action=\"" + URL + "\" method=\"post\">");

      sb.Append(this.ToHiddenInputs());

      sb.AppendLine("</form>");
      sb.AppendLine("</body>");
      sb.AppendLine("</html>");

      return sb.ToString();
    }

    public string ToHiddenInputs()
    {
//      throw new Exception(this.MerchantID.ToString() + " : " + this.ApplicationID.ToString());
      StringBuilder sb = new StringBuilder();

      // Now slot in each of the properties.
      sb.AppendLine(Request.PropToHTMLString("Mode", ((int)this.Mode).ToString()));      
      sb.AppendLine(Request.PropToHTMLString("txtMerchantID", "{" + String.Format("{0}", this.MerchantID.ToString().ToUpper()) + "}"));
      sb.AppendLine(Request.PropToHTMLString("txtApplicationID", "{" + String.Format("{0}", this.ApplicationID.ToString().ToUpper()) + "}"));
      sb.AppendLine(Request.PropToHTMLString("txtMerchantReference", "MyMerchRef")); //this.MerchantReference));
      sb.AppendLine(Request.PropToHTMLString("txtPrice", this.Price.ToString()));
      if (this.CurrencyCode.HasValue)
        sb.AppendLine(Request.PropToHTMLString("txtCurrencyCode", this.CurrencyCode.ToString()));
      if (this.DisplayPrice.HasValue)
        sb.AppendLine(Request.PropToHTMLString("txtDisplayPrice", this.DisplayPrice.ToString()));
      if (this.DisplayCurrencyCode.HasValue)
        sb.AppendLine(Request.PropToHTMLString("txtDisplayCurrencyCode", this.DisplayCurrencyCode.ToString()));
      sb.AppendLine(Request.PropToHTMLString("txtRedirectSuccessfulURL", this.RedirectSuccessfulURL));
      sb.AppendLine(Request.PropToHTMLString("txtRedirectFailedURL", this.RedirectFailedURL));
      if (!String.IsNullOrEmpty(this.ShippingCountryCode))
        sb.AppendLine(Request.PropToHTMLString("ShippingCountryCode", this.ShippingCountryCode));
      if (!String.IsNullOrEmpty(this.UCI))
        sb.AppendLine(Request.PropToHTMLString("UCI", this.UCI));
      if (!String.IsNullOrEmpty(this.IPAddress))
        sb.AppendLine(Request.PropToHTMLString("IPAddress", this.IPAddress));
      sb.AppendLine(Request.PropToHTMLString("txtQty", this.Qty.ToString()));        
      if (!String.IsNullOrEmpty(this.ItemReference ))
        sb.AppendLine(Request.PropToHTMLString("txtItemRef", this.ItemReference));
      sb.AppendLine(Request.PropToHTMLString("txtItemDescr", this.ItemDescription));
      sb.AppendLine(Request.PropToHTMLString("txtItemAmount", this.ItemAmount.ToString()));
      if (this.ShippingCost.HasValue)
        sb.AppendLine(Request.PropToHTMLString("txtShippingCost", this.ShippingCost.ToString()));
      if (this.Discount.HasValue)
        sb.AppendLine(Request.PropToHTMLString("txtDiscount", this.Discount.ToString()));

      if (!String.IsNullOrEmpty(this.Recipient))
        sb.AppendLine(Request.PropToHTMLString("txtRecipient", this.Recipient));
      if (!String.IsNullOrEmpty(this.ShippingAddress1))
        sb.AppendLine(Request.PropToHTMLString("txtShippingAddress1", this.ShippingAddress1));
      if (!String.IsNullOrEmpty(this.ShippingAddress2))
        sb.AppendLine(Request.PropToHTMLString("txtShippingAddress2", this.ShippingAddress2));
      if (!String.IsNullOrEmpty(this.ShippingAddress3))
        sb.AppendLine(Request.PropToHTMLString("txtShippingAddress3", this.ShippingAddress3));
      if (!String.IsNullOrEmpty(this.ShippingAddress4))
        sb.AppendLine(Request.PropToHTMLString("txtShippingAddress4", this.ShippingAddress4));
      if (!String.IsNullOrEmpty(this.ShippingAddress5))
        sb.AppendLine(Request.PropToHTMLString("txtShippingAddress5", this.ShippingAddress5));

      if (String.IsNullOrEmpty(this.Reference))
        throw new Exception("Reference is a mandatory field");
      
      sb.AppendLine(Request.PropToHTMLString("Variable1", this.Reference));

      return sb.ToString();
    }

    protected static string PropToHTMLString(string propertyName, string propertyValue)
    {
      return String.Format("<INPUT TYPE=\"hidden\" NAME=\"{0}\" VALUE=\"{1}\"> ", propertyName, propertyValue);
    }

    public void Send()
    {
      //System.IO.File.WriteAllText("C:\\Text.htm", this.ToString());      
      
      HttpContext.Current.Response.ClearHeaders();
      HttpContext.Current.Response.Clear();
      HttpContext.Current.Response.Write(this.ToString());
      //HttpContext.Current.Response.Flush();
      //HttpContext.Current.Response.End();
    }
  }
}
