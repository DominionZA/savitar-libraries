using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.Web.Gateways.MyGate
{
  public class Response
  {
    /// <summary>
    /// This contains the transaction result code. < 0 : Failed, 0 : Success, > 0 Warning
    /// </summary>
    public int Result;

    /// <summary>
    /// This is the error code should one exist.
    /// </summary>
    public string ErrorCode;

    /// <summary>
    /// This is the source of the error which can be provided to us for tracking purposes.
    /// </summary>
    public string ErrorSource;

    /// <summary>
    /// This is the error message
    /// </summary>
    public string ErrorMessage;

    /// <summary>
    /// The is the detail of the error message.
    /// </summary>
    public string ErrorDetail;

    public string Reference;

    public Response(System.Collections.Specialized.NameValueCollection postedForm)
    {
      StringBuilder sb = new StringBuilder();
      foreach (string key in postedForm.AllKeys)
      {
        sb.AppendLine(key + " = " + postedForm.Get(key));
      }

//      System.IO.File.WriteAllText("C:\\Respose.txt", sb.ToString());

      if (String.IsNullOrEmpty(postedForm.Get("_Result")))
        throw new Exception("No result was returned from MyGate");

      this.Result =  Convert.ToInt32(postedForm.Get("_Result"));
      this.ErrorCode = String.IsNullOrEmpty(postedForm.Get("_ERROR_CODE")) ? "" : postedForm.Get("_ERROR_CODE").ToString();
      this.ErrorSource = String.IsNullOrEmpty(postedForm.Get("_ERROR_SOURCE")) ? "" : postedForm.Get("_ERROR_SOURCE").ToString();
      this.ErrorMessage= String.IsNullOrEmpty(postedForm.Get("_ERROR_MESSAGE")) ? "" : postedForm.Get("_ERROR_MESSAGE").ToString();
      this.ErrorDetail = String.IsNullOrEmpty(postedForm.Get("_ERROR_DETAIL")) ? "" : postedForm.Get("_ERROR_DETAIL").ToString();

      this.Reference = String.IsNullOrEmpty(postedForm.Get("Variable1")) ? "" : postedForm.Get("Variable1").ToString();      
    }
  }
}
