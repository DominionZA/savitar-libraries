using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.Web.Gateways.MWebSafeshop
{
  public class Response
  {
    public enum SafeshopTransactionResult { Unknown, Successful, Failed, StageOrder }
    public static SafeshopTransactionResult StrToTransactionResult(string transactionResult)
    {
      switch (transactionResult.ToLower())
      {
        case "successful": return SafeshopTransactionResult.Successful;
        case "failed": return SafeshopTransactionResult.Failed;
        case "stageorder": return SafeshopTransactionResult.StageOrder;
        default: return SafeshopTransactionResult.Unknown;
      }
    }

    public static SafeshopTransactionType StrToTransactionType(string transactionType)
    {
      switch (transactionType.ToLower())
      {
        case "auth": return SafeshopTransactionType.Auth;
        case "auth_settle": return SafeshopTransactionType.Auth_Settle;
        default: return SafeshopTransactionType.NotSet;
      }
    }

    public Response(System.Collections.Specialized.NameValueCollection postedForm)
    {
      this.LogRefNr = String.IsNullOrEmpty(postedForm.Get("LogRefNr")) ? 0 : Convert.ToInt32(postedForm.Get("LogRefNr"));
      this.MerchantReference = String.IsNullOrEmpty(postedForm.Get("MerchantReference")) ? "" : postedForm.Get("MerchantReference");
      this.TransactionAmount = String.IsNullOrEmpty(postedForm.Get("TransactionAmount")) ? 0 : Convert.ToDecimal(Convert.ToInt32(postedForm.Get("TransactionAmount")) / 100);
      this.TransactionType = String.IsNullOrEmpty(postedForm.Get("TransactionType")) ? SafeshopTransactionType.NotSet : StrToTransactionType(postedForm.Get("TransactionType"));
      this.SafePayRefNr = String.IsNullOrEmpty(postedForm.Get("SafePayRefNr")) ? "" : postedForm.Get("SafePayRefNr");
      this.BankRefNr = String.IsNullOrEmpty(postedForm.Get("BankRefNr")) ? "" : postedForm.Get("BankRefNr");
      this.TransactionErrorResponse = String.IsNullOrEmpty(postedForm.Get("TransactionErrorResponse")) ? "" : postedForm.Get("TransactionErrorResponse");
      this.TransactionResult = String.IsNullOrEmpty(postedForm.Get("TransactionResult")) ? SafeshopTransactionResult.Unknown : Response.StrToTransactionResult(postedForm.Get("TransactionResult"));
      this.LiveTransaction = String.IsNullOrEmpty(postedForm.Get("LiveTransaction")) ? true : Convert.ToBoolean(postedForm.Get("LiveTransaction"));
      this.SafeTrack = String.IsNullOrEmpty(postedForm.Get("SafeTrack")) ? Guid.Empty : new Guid(postedForm.Get("SafeTrack"));
      this.ReceiptURL = String.IsNullOrEmpty(postedForm.Get("ReceiptURL")) ? "" : postedForm.Get("ReceiptURL");
      this.BuyerCreditCardNr = String.IsNullOrEmpty(postedForm.Get("BuyerCreditCardNr")) ? "" : postedForm.Get("BuyerCreditCardNr");
      this.hidStoreKey = String.IsNullOrEmpty(postedForm.Get("hidStoreKey")) ? Guid.Empty : new Guid(postedForm.Get("hidStoreKey"));
    }

    # region Response Properties
    /// <summary>
    /// The transaction log reference number created by SafeShop
    /// </summary>
    public int LogRefNr = 0;
    /// <summary>
    /// A unique reference number per-transaction supplied by the merchant.
    /// </summary>
    public string MerchantReference = "";
    /// <summary>
    /// The total amount, in CENTS, that was charged, e.g. if the amount that was charged was R12.99, then SafeShop� 0 returns 1299 in cents as the amount charged.
    /// </summary>
    public Decimal TransactionAmount = 0;
    /// <summary>
    /// The type of transaction that was executed
    /// </summary>
    public SafeshopTransactionType TransactionType = SafeshopTransactionType.Auth_Settle;
    /// <summary>
    /// Unique reference number per transaction. If you have any queries against a transaction use this number for support.
    /// </summary>
    public string SafePayRefNr = "";
    /// <summary>
    /// Represents the bank reference number for the transaction issued by the Inquiring Institution.
    /// </summary>
    public string BankRefNr = "";
    /// <summary>
    /// Transaction failed reason explanation.
    /// </summary>
    public string TransactionErrorResponse = "";
    /// <summary>
    /// Return the status of the transaction. The values returned are:
    /// Successful - The transaction was successfully processed
    /// Failed - Transaction failed Check TransactionErrorResponse tag for more information
    /// StageOrder - This transaction was put in stage mode. (See Staged Orders)
    /// </summary>
    public SafeshopTransactionResult TransactionResult = SafeshopTransactionResult.Unknown;
    /// <summary>
    /// Indicates whether the transaction was executed as live (True) or was simulated (False) by SafeShop�.
    /// </summary>
    public Boolean LiveTransaction = false;
    /// <summary>
    /// SafeTrack represents a GUID that was passed from a portal. NOTE: You must also include the curly brackets.
    /// </summary>
    public Guid SafeTrack = Guid.Empty;
    /// <summary>
    /// The Receipt URL. The Merchant may use ReceiptURL to redirect the Shopper to view their receipt on SafeShop�.
    /// </summary>
    public string ReceiptURL = "";
    /// <summary>
    /// The full names that appears on the credit card, e.g.. John Doe
    /// </summary>
    public string BuyerCreditCardNr = "";
    /// <summary>
    /// A unique SafeKey is issued to each store. SafeKey resembles {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}. NOTE: You must also include the curly brackets around the SafeKey GUID.
    /// </summary>
    public Guid hidStoreKey = Guid.Empty;
    #endregion
  }
}
