using System;
using System.Collections.Specialized;

namespace Savitar.Web.Gateways.ClickATell
{
  public enum ErrorCode
  {
    None = 0, AuthenticationFailed = 1, UnknownUserNameOrPassword = 2,
    SessionIDExpired = 3, AccountFrozen = 4, MissingSessionID = 5,
    InvalidParams = 101, InvalidUDH = 102, UnknownAPIMsgID = 103,
    UnknownCLIMsgID = 104, InvalidDestination = 105,
    InvalidSource = 106, EmptyMsg = 107, InvalidAPI_ID = 108,
    MissingMsgID = 109, ErrorWithEmailMsg = 110, InvalidProtocol = 111,
    InvalidMsg_Type = 112, MaxMsgPartsExceeded = 113,
    CannotRouteMsg = 114, MsgExpired = 115, InvalidUnicodeData = 116,
    InvalidDeliveryTime = 120, InvalidBatchID = 201,
    NoBatchTemplate = 202, NoCreditLeft = 301, MaxAllowedCredit = 302
  };

  public enum MsgStatus
  {
    Unknown = 1, Queued = 2, Delivered = 3, Received = 4,
    ErrorWithMsg = 5, UserCancelled = 6, ErrorDelivering = 7,
    OK = 8, RoutingError = 9, Expired = 10, DelayedDelivery = 11,
    OutOfCredit = 12
  };

  public class ResponseItem
  {
    protected string FResponseStr = "";
    public ErrorCode ErrCode = ErrorCode.None;
    public NameValueCollection Values = new NameValueCollection();

    public void SetResponseStr(string AResponseStr)
    {
      FResponseStr = AResponseStr;

      // Now to parse the string.
      Values.Clear();

      string sType = "";
      string sValue = "";


      // "ID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003"

      for (int i = 0; i < FResponseStr.Length; i++)
      {
        if (sValue == "")
          sType += FResponseStr[i];
        else
          sValue += FResponseStr[i];

        if ((FResponseStr[i] == ' ') || (i == FResponseStr.Length - 1))
        {
          if (sValue == "")
            sValue = " ";
          else
          {
            sType = sType.Replace(':', ' ');
            this.Values.Add(sType.Trim(), sValue.Trim());
            //						System.Web.HttpContext.Current.Response.Write("2");
            //						System.Web.HttpContext.Current.Response.Write("Type: " + sType + ", Value: " + sValue + "<br/>");
            sType = "";
            sValue = "";
          }
        }

      }
    }

    public string ErrCodeToStr(ErrorCode AErrCode)
    {
      return "None";
    }

    public ResponseItem()
    {
      this.DoResetFields();
    }

    public void DoResetFields()
    {
      this.ErrCode = ErrorCode.None;
    }
  }

  public class Response : System.Collections.CollectionBase
  {
    public ResponseItem Add()
    {
      ResponseItem Item = new ResponseItem();
      this.List.Add(Item);
      return Item;
    }

    public ResponseItem this[int Index]
    {
      get { return (ResponseItem)this.List[Index]; }
      set { this.List[Index] = value; }
    }

    protected int GetNextTypePos(string AStr, int AStartPos)
    {
      // Look for the next colon
      int iNextColonPos = AStr.IndexOf(':', AStartPos);
      // If not found, then return -1 (No more types)
      if (iNextColonPos < 0)
        return -1;

      // We have a type. Extract the string up to the colon
      string s = AStr.Substring(0, iNextColonPos);

      // Look for the last space in the extracted string
      int iLastSpacePos = s.LastIndexOf(' ');
      // If not found, then the start pos of this type is 0 (Beginning of string)
      if (iLastSpacePos < 0)
        return 0;

      // If we found a space, then return the pos of that space + 1 (Start of the type)
      return (iLastSpacePos + 1);
    }

    protected string GetType(string AValue)
    {
      int iLastSpacePos = AValue.LastIndexOf(' ');
      if (iLastSpacePos < 0)
        return AValue;
      else
        return AValue.Substring(iLastSpacePos);
    }

    public void SetResponseStr(string AResponseStr)
    {
      // "ID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003\rID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003\r"
      this.Clear();

      string sResponseStr = "";

      // Prepare the string for splitting
      int LastSpaceIndex = -1;
      AResponseStr = AResponseStr.Replace("\n", " \n");
      for (int i = 0; i < AResponseStr.Length; i++)
      {
        if (AResponseStr[i] == ':') // We have a type trailer.
        {
          LastSpaceIndex = sResponseStr.LastIndexOf(' ');
          if (LastSpaceIndex >= 0)
            sResponseStr = sResponseStr.Insert(LastSpaceIndex + 1, ":");
        }

        sResponseStr += AResponseStr[i];
      }

      // Split the items into records
      string[] Records = sResponseStr.Split('\n');
      ResponseItem oItem = null;
      for (int iRecord = 0; iRecord < Records.Length; iRecord++)
      {
        oItem = this.Add();

        string[] Parts = Records[iRecord].Split(':');
        for (int i = 0; i < Parts.Length; i++)
        {
          if ((i % 2 != 0) && (Parts[i - 1].Trim().Length > 0) && (Parts[i].Trim().Length > 0))
          {
            oItem.Values.Add(Parts[i - 1].Trim(), Parts[i].Trim());
          }
        }
      }
    }
    /*
          this.Clear();
    // "ID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003, Authentication failed"

          string[] Parts = AResponseStr.Split(' ');

          string Type = "";
          string FirstType = "";
          string Value = "";
          ResponseItem oItem = this.Add();
			
          for (int iPart = 0; iPart < Parts.Length; iPart++)
          {
            if (Parts[iPart].IndexOf(':') == Parts[iPart].Length - 1) // Have a type
            {
              if ((Type != "") && (Value != "")) // Have a type and value. Add them
              {
                oItem.Values.Add(Type, Value);
                Type = "";
                Value = "";
              }

              Type = Parts[iPart];

              if (FirstType == "")
                FirstType = Parts[iPart];
              else if ((String.Compare(FirstType, Type) == 0) || (String.Compare(Type, "ERR:") == 0))
                oItem = this.Add();
            }
            else // Still adding value
            {
              if (Value != "")
                Value += " ";

              Value += Parts[iPart];
            }

            if (iPart == Parts.Length - 1)
            {
              if ((Type != "") && (Value != "")) // Have a type and value. Add them
              {
                oItem.Values.Add(Type, Value);
                Type = "";
                Value = "";
              }
            }
          }
    */
  }
}