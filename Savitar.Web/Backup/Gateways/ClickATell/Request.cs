using System;
using System.Net;
using System.IO;

namespace Savitar.Web.Gateways.ClickATell
{
  public class Request
  {
    public string SessionID = "";
    protected System.DateTime LastActionTime;
    protected int SessionTimeout = 15;
    protected string User = "KAI";
    protected string Password = "kai1111";
    protected string APIID = "1044778";

    public Request()
    {
    }

    protected void CheckSessionID()
    {
      if ((this.SessionID == null) || (this.LastActionTime.AddMinutes(this.SessionTimeout) < System.DateTime.Now))
        this.UpdateSessionID();
    }

    // Sends the message and returns the response from ClickATell
    protected string DoSend(WebClient AClient, string AURL)
    {
      AClient.Headers.Add("user-agent", "Mozilla/4.0(compatible;MSIE6.0;Windows NT 5.2; .NET CLR 1.0.3705;)");

      Stream data = AClient.OpenRead(AURL);
      try
      {
        StreamReader reader = new StreamReader(data);
        try
        {
          LastActionTime = System.DateTime.Now;

          string Response = reader.ReadToEnd();
          //					System.IO.StreamWriter oFile = System.IO.File.CreateText("c:\\test.txt");
          //					oFile.Write(Response);
          //					oFile.Close();

          return Response;
        }
        finally
        {
          reader.Close();
        }
      }
      finally
      {
        data.Close();
      }
    }

    protected string ParseResponse(string AResponse)
    {
      int i = AResponse.IndexOf(':');
      if (i < 0)
        throw new Exception("Invalid Response Code: " + AResponse);

      string sResponse = AResponse.Substring(0, i).ToUpper().Trim();
      string sValue = AResponse.Substring(i + 1).Trim();

      if (sResponse != "OK")
        throw new Exception("ClickATell responded with an error message of : " + sValue);

      return sValue;
    }

    public string Ping()
    {
      CheckSessionID();

      WebClient client = new WebClient();

      client.QueryString.Add("session_id", this.SessionID);

      Response oResponse = new Response();
      oResponse.SetResponseStr(this.DoSend(client, "http://api.clickatell.com/http/ping"));

      return oResponse.Count.ToString();
    }

    public Response QueryMessage(string AAPIMsgID)
    {
      CheckSessionID();

      WebClient client = new WebClient();

      client.QueryString.Add("session_id", this.SessionID);
      client.QueryString.Add("apimsgid", AAPIMsgID);

      Response oResponse = new Response();
      oResponse.SetResponseStr(this.DoSend(client, "http://api.clickatell.com/http/querymsg"));

      return oResponse;
    }

    public string SendMsg(string ACellNumber, string AMessage)
    {
      CheckSessionID();

      WebClient client = new WebClient();

      client.QueryString.Add("session_id", this.SessionID);
      client.QueryString.Add("to", ACellNumber);
      client.QueryString.Add("text", AMessage);

      return this.DoSend(client, "http://api.clickatell.com/http/sendmsg");
    }

    // Returns the batch ID
    public string StartBatch(string ATemplateMsg)
    {
      CheckSessionID();

      WebClient client = new WebClient();

      client.QueryString.Add("session_id", this.SessionID);
      client.QueryString.Add("template", ATemplateMsg);

      Response oResponse = new Response();
      oResponse.SetResponseStr(this.DoSend(client, "http://api.clickatell.com/http_batch/startbatch"));

      if (oResponse.Count == 0)
        throw new Exception("No response was returned when attempting to start a new batch");

      if (oResponse.Count > 1)
        throw new Exception("An invalid response was returned when attempting to start a new batch");

      if (String.Compare(oResponse[0].Values.GetKey(0), "ID") != 0)
        throw new Exception("An error was returned when attempting to start a new batch<br/>" +
          oResponse[0].Values.GetKey(0) + " = " + oResponse[0].Values[0]);

      // All should be kewl now. We have an ID
      return oResponse[0].Values[0];
    }

    public Response QuickSendBatch(string ABatchID, string ANumbers)
    {
      CheckSessionID();

      WebClient client = new WebClient();

      client.QueryString.Add("session_id", this.SessionID);
      client.QueryString.Add("batch_id", ABatchID);
      client.QueryString.Add("to", ANumbers);

      Response oResponse = new Response();
      oResponse.SetResponseStr(this.DoSend(client, "http://api.clickatell.com/http_batch/quicksend"));

      return oResponse;
    }

    public Response EndBatch(string ABatchID)
    {
      CheckSessionID();

      WebClient client = new WebClient();

      client.QueryString.Add("session_id", this.SessionID);
      client.QueryString.Add("batch_id", ABatchID);

      Response oResponse = new Response();
      oResponse.SetResponseStr(this.DoSend(client, "http://api.clickatell.com/http_batch/endbatch"));

      return oResponse;
    }

    public bool UpdateSessionID()
    {
      WebClient client = new WebClient();

      client.QueryString.Add("api_id", this.APIID);
      client.QueryString.Add("user", this.User);
      client.QueryString.Add("password", this.Password);

      SessionID = this.ParseResponse(this.DoSend(client, "http://api.clickatell.com/http/auth"));

      return true;
    }
  }
}