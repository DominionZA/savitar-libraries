using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Savitar.Web.UI
{
  public class Page
  {
    public static void InsertMetaRefresh(System.Web.UI.Page page, int refreshSeconds, string url)
    {
      System.Web.UI.HtmlControls.HtmlGenericControl metaRefresh = new System.Web.UI.HtmlControls.HtmlGenericControl("meta");
      metaRefresh.Attributes.Add("http-equiv", "refresh");
      metaRefresh.Attributes.Add("content", String.Format("{0};URL={1}", refreshSeconds, url));
      page.Header.Controls.Add(metaRefresh);      
    }
  }
}
