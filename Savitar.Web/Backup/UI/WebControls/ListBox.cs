using System;
using System.Data;
using System.ComponentModel;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:ListBox runat=server></{0}:ListBox>")]
  public partial class ListBox : System.Web.UI.WebControls.ListBox
  {
    public ListBox()
    {
      this.SelectionMode = ListSelectionMode.Multiple;
    }

    [Bindable(false), Category("MultiSelection"), DefaultValue("true"), System.ComponentModel.Description("Specifies whether all items must be auto selected when databound")]
    public bool SelectAll
    {
      get { return _SelectAll; }
      set 
      { 
        _SelectAll = value;

        if (_SelectAll)
          SelectAllItems();
      }
    } bool _SelectAll = true;

    [Bindable(false), Category("MultiSelection"), DefaultValue(","), System.ComponentModel.Description("The delimiter to use between items when calling SelectedValues")]
    public string ValuesDelimiter
    {
      get { return _ValuesDelimiter; }
      set { _ValuesDelimiter = value; }
    } string _ValuesDelimiter = ",";

    [Bindable(false), Category("MultiSelection"), DefaultValue(""), System.ComponentModel.Description("The prefix to use for each item when calling SelectedValues")]
    public string ValuesPrefix
    {
      get { return _ValuesPrefix; }
      set { _ValuesPrefix = value; }
    } string _ValuesPrefix = "";

    [Bindable(false), Category("MultiSelection"), DefaultValue(""), System.ComponentModel.Description("The suffix to use for each item when calling SelectedValues")]
    public string ValuesSuffix
    {
      get { return _ValuesSuffix; }
      set { _ValuesSuffix = value; }
    } string _ValuesSuffix = "";

    /// <summary>
    /// Returns a list of selected items in the listbox.
    /// </summary>
    public string SelectedValues
    {
      get
      {
        string result = "";

        foreach (ListItem item in this.Items)
        {
          if (!item.Selected)
            continue;
          
          if (!String.IsNullOrEmpty(result))
            result += this.ValuesDelimiter;

          result += String.Format("{0}{1}{2}", this.ValuesPrefix, item.Value, this.ValuesSuffix);
        }

        return result;
      }
    }

    protected override void OnDataBound(EventArgs e)
    {
      base.OnDataBound(e);

      if (this.SelectAll)
        this.SelectAllItems();
    }

    protected void SelectAllItems()
    {
      this.SelectionMode = ListSelectionMode.Multiple;

      foreach (ListItem item in this.Items)
        item.Selected = true;
    }

    public override ListSelectionMode SelectionMode
    {
      get
      {
        return base.SelectionMode;
      }
      set
      {
        base.SelectionMode = value;
        if (value != ListSelectionMode.Multiple)
          _SelectAll = false;
      }
    }

    [TypeConverter(typeof(aaaa))]
    public ListBox LinkedListBox
    {
      get { return _LinkedListBox; }
      set { _LinkedListBox = value; }
    } ListBox _LinkedListBox = null;

    protected override void OnPagePreLoad(object sender, EventArgs e)
    {
      base.OnPagePreLoad(sender, e);

      if (this.LinkedListBox != null)
      {
        if (!Page.ClientScript.IsClientScriptBlockRegistered("SAV_MoveListItem"))
        {
          System.Text.StringBuilder sb = new System.Text.StringBuilder();
          sb.AppendLine("function SAV_MoveListItem(strBox1, strBox2)");
          sb.AppendLine("{");
          sb.AppendLine("  var box1 = document.getElementById(strBox1);");
          sb.AppendLine("  var box2 = document.getElementById(strBox2);");
          sb.AppendLine("");
          sb.AppendLine("  for (var i=0; i < box1.options.length; i++)");
          sb.AppendLine("  {");
          sb.AppendLine("    if (box1.options[i].selected)");
          sb.AppendLine("    {");
          sb.AppendLine("      box2.options[box2.options.length] = new Option (box1.options[i].text, box1.options[i].value, false, false);");
          sb.AppendLine("    }");
          sb.AppendLine("  }");
          sb.AppendLine("  for (var i = box1.options.length - 1; i >= 0; i--)");
          sb.AppendLine("  {");
          sb.AppendLine("    if (box1.options[i].selected)");
          sb.AppendLine("      box1.options[i] = null;");
          sb.AppendLine("  }");
          sb.AppendLine("}");

          Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "SAV_MoveListItem", sb.ToString(), true);

          this.Attributes.Add("onclick", String.Format("SAV_MoveListItem('{0}', '{1}');", this.ClientID, this.LinkedListBox.ClientID));
        }
      }
    }    
  }
  
  public class aaaa : TypeConverter
  {
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
    {
      return true;      
    }    

    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (sourceType == typeof(string))
        return true;
      else
        return base.CanConvertFrom(context, sourceType);
    }

    public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
    {
      //if (value.GetType() == typeof(string))
      //  return this; // return (ListBox)value;
      //else
        return base.ConvertFrom(context, culture, value);
    }    
  }



  public class ListBoxConverter : ExpandableObjectConverter
  {
    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (sourceType == typeof(string))
        return true;

      return base.CanConvertFrom(context, sourceType);
    }

    public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
    {
      if (destinationType == typeof(string))
        return true;

      return base.CanConvertTo(context, destinationType);
    }

    public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
    {
      if (value == null)
        return new ListBox();

      if (value is string)
      {
        string s = (string)value;
        if (s.Length == 0)
          return new ListBox();
      }

      return base.ConvertFrom(context, culture, value);
    }

    public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
    {
      if (value != null)
      {
        if (!(value is ListBox))
        {
          throw new ArgumentException(
              "Invalid ListBox", "value");
        }
      }

      if (destinationType == typeof(string))
      {
        if (value == null)
          return String.Empty;

        return ((ListBox)value).ID;
      }

      return base.ConvertTo(context, culture, value,
          destinationType);
    }
  }
}
