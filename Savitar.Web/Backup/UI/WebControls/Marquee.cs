using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:Marquee runat=server></{0}:Marquee>")]
  public class Marquee : System.Web.UI.WebControls.DataList
  {
    public enum MarqueeScrollDirection { Up, Down, Left, Right }

    public MarqueeScrollDirection ScrollDirection
    {
      get { return _ScrollDirection; }
      set { _ScrollDirection = value; }
    } MarqueeScrollDirection _ScrollDirection = MarqueeScrollDirection.Left;

    public int ScrollAmount
    {
      get { return _ScrollAmount; }
      set { _ScrollAmount = value; }
    } int _ScrollAmount = 4;

    public int ScrollDelay
    {
      get { return _ScrollDelay; }
      set { _ScrollDelay = value; }
    } int _ScrollDelay = 8;

    public Marquee()
    {
      base.EditItemTemplate = null;
      this.Width = System.Web.UI.WebControls.Unit.Pixel(400);
      this.Height = System.Web.UI.WebControls.Unit.Pixel(100);           
    }    
   
    protected override void Render(HtmlTextWriter writer)
    {      
      if (!Savitar.ComponentModel.Component.IsInDesignMode)
      {        
//        writer.WriteLine(String.Format("<marquee onmouseover=this.stop() onmouseout=this.start() Loop=0 scrollamount={0} scrolldelay={1} direction={2} width={3} height={4}>", this.ScrollAmount, this.ScrollDelay, this.ScrollDirection, this.Width, this.Height));
        writer.WriteLine(String.Format("<marquee onmouseover=this.stop() onmouseout=this.start() Loop=0 scrollamount={0} scrolldelay={1} direction={2}>", this.ScrollAmount, this.ScrollDelay, this.ScrollDirection));
      }

      base.Render(writer);

      if (!Savitar.ComponentModel.Component.IsInDesignMode)
      {
        writer.Write("</marquee>");
      }
    }
  }
}
