using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Savitar.Web.UI.WebControls
{
  public class Utils
  {
    public static void SelectValue(System.Web.UI.WebControls.DropDownList dropDownList, int value)
    {
      if (value <= 0)
        return;

      ListItem item = dropDownList.Items.FindByValue(value.ToString());
      if (item != null)
      {
        dropDownList.ClearSelection();
        item.Selected = true;
      }
    }

    public static void SelectValue(Savitar.Web.UI.WebControls.DropDownList dropDownList, int value)
    {
      if (value <= 0)
        return;

      ListItem item = dropDownList.Items.FindByValue(value.ToString());
      if (item != null)
      {
        dropDownList.ClearSelection();
        item.Selected = true;
      }
    }
  }
}
