using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  /// <summary>
  /// Simple DropDownList that displays all the months in a year.
  /// </summary>
  [ToolboxData("<{0}:MonthDropDownList runat=server></{0}:MonthDropDownList>")]
  public class MonthDropDownList : Savitar.Web.UI.WebControls.DropDownList
  {
    public enum Month { Current = -2, BackOneFromCurrent, ForwardOneFromCurrent, January, February, March, April, May, June, July, August, September, October, November, December }

    public MonthDropDownList()
    {      
    }

    [Bindable(true), Category("Settings"), DefaultValue("Current"), System.ComponentModel.Description("The default month to select after binding.")]
    public Month DefaultMonth
    {
      get { return _Month; }
      set { _Month = value; }
    } Month _Month = Month.Current;

    protected override void OnLoad(EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        this.Items.Add(new ListItem("January", "1"));
        this.Items.Add(new ListItem("February", "2"));
        this.Items.Add(new ListItem("March", "3"));
        this.Items.Add(new ListItem("April", "4"));
        this.Items.Add(new ListItem("May", "5"));
        this.Items.Add(new ListItem("June", "6"));
        this.Items.Add(new ListItem("July", "7"));
        this.Items.Add(new ListItem("August", "8"));
        this.Items.Add(new ListItem("September", "9"));
        this.Items.Add(new ListItem("October", "10"));
        this.Items.Add(new ListItem("November", "11"));
        this.Items.Add(new ListItem("December", "12"));

        int monthIndex = System.DateTime.Now.Month;

        switch (this.DefaultMonth)
        {
          case Month.Current: monthIndex = System.DateTime.Now.Month; break;
          case Month.BackOneFromCurrent:
            {
              if (DateTime.Now.Month == 1)
                monthIndex = 12;
              else
                monthIndex = DateTime.Now.Month - 1;

              break;
            }
          case Month.ForwardOneFromCurrent:
            {
              if (DateTime.Now.Month == 12)
                monthIndex = 1;
              else
                monthIndex = DateTime.Now.Month + 1;

              break;
            }
          default: monthIndex = (int)this.DefaultMonth; break;
        }

        this.SelectedValue = monthIndex.ToString();
      }

      base.OnLoad(e);
    }
  }
}