using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:DropDownList runat=server></{0}:DropDownList>")]
  public class DropDownList : System.Web.UI.WebControls.DropDownList
  {    
    public enum ComboAction { None, Insert, Select, All, SelectAndAll, InsertAndAll }
    public delegate void AddNewSelectedDelegate();
    public delegate void ItemSelectedDelegate(string selectedValue);
    public delegate void AllSelectedDelegate(string selectedValue);

    public event AddNewSelectedDelegate AddNewSelected;
    public event ItemSelectedDelegate ItemSelected;
    public event AllSelectedDelegate AllSelected;

    public string DefaultSelectedValue
    {
      get 
      {
        if (ViewState["DefaultSelectedValue"] == null)
          return "";

        return ViewState["DefaultSelectedValue"].ToString();
      }
      set { ViewState["DefaultSelectedValue"] = value; }
    }

    public ComboAction Action
    {
      get 
      {
        if (ViewState["Action"] == null)
          return ComboAction.None;

        return ((ComboAction)Convert.ToInt32(ViewState["Action"]));
      }
      set { ViewState["Action"] = value; }
    }
    
    public override string SelectedValue
    {
      get
      {
        return base.SelectedValue;
      }
      set
      {
        try
        {                    
          //ListItem item = this.Items.FindByValue(this.DefaultSelectedValue);
          ListItem item = this.Items.FindByValue(value);
          if (item != null)
          {
            this.ClearSelection();
            base.SelectedValue = value;
          }
        }
        catch
        {
          throw new Exception(String.Format("The value '{0}' could not be set as the selected value", value));
        }
      }
    }

    protected override void OnDataBound(EventArgs e)
    {      
      base.OnDataBound(e);

      if ((this.Action == ComboAction.Insert) || (this.Action == ComboAction.InsertAndAll))
        this.Items.Insert(0, new ListItem(">> Add New <<", "0"));
      
      if ((this.Action == ComboAction.Select) || (this.Action == ComboAction.SelectAndAll))
        this.Items.Insert(0, new ListItem("Please Select", "0"));

      if (this.Action == ComboAction.All)
        this.Items.Insert(0, new ListItem(">> All <<", "0"));
      else if ((this.Action == ComboAction.InsertAndAll) || (this.Action == ComboAction.SelectAndAll))
        this.Items.Insert(1, new ListItem(">> All <<", "0"));

      if (this.DefaultSelectedValue != null)
      {
        try
        {
          ListItem item = this.Items.FindByValue(this.DefaultSelectedValue);
          if (item != null)
          {
            this.ClearSelection();
            item.Selected = true;
          }
        }
        catch
        {
          if (this.Items.Count > 0)
            this.SelectedIndex = 0;
        }
      }
      else if (this.Items.Count > 0)
        this.SelectedIndex = 0;

      ProcessSelectedItem();
    }

    protected void ProcessSelectedItem()
    {
      if (this.SelectedIndex < 0)
        return;

      if (this.SelectedValue == null)
        return;

      int selectedInt;
      if ((int.TryParse(this.SelectedValue, out selectedInt)) && (selectedInt == 0))
      {
        if (this.Action == ComboAction.Insert)
          this.FireAddNewSelected();
        else if (this.Action == ComboAction.Select)
          this.FireItemSelected("");
        else if (this.Action == ComboAction.All)
          this.FireAllSelected("");
        else if (this.Action == ComboAction.InsertAndAll)
        {
          if (this.SelectedIndex == 0)
            this.FireAddNewSelected();
          else if (this.SelectedIndex == 1)
            this.FireAllSelected("");
        }
        else if (this.Action == ComboAction.SelectAndAll)
        {
          if (this.SelectedIndex == 0)
            this.FireItemSelected("");
          else if (this.SelectedIndex == 1)
            this.FireAllSelected("");
        }
        else
          this.FireItemSelected(this.SelectedValue);
      }
      else
        this.FireItemSelected(this.SelectedValue);          
    }

    protected override void OnSelectedIndexChanged(EventArgs e)
    {
      ProcessSelectedItem();

      base.OnSelectedIndexChanged(e);
    }

    protected void FireAddNewSelected()
    {
      if (this.AddNewSelected == null)
        return;

      this.AddNewSelected();
    }

    protected void FireItemSelected(string selectedValue)
    {
      if (this.ItemSelected == null)
        return;

      this.ItemSelected(selectedValue);
    }

    protected void FireAllSelected(string selectedValue)
    {
      if (this.AllSelected == null)
        return;

      this.AllSelected(selectedValue);
    }
  }
}
