using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  /// <summary>
  /// Simple DropDownList that displays years from a min to max year.
  /// </summary>
  [ToolboxData("<{0}:YearDropDownList runat=server></{0}:YearDropDownList>")]
  public class YearDropDownList : Savitar.Web.UI.WebControls.DropDownList
  {
    public YearDropDownList()    
    {      
    }

    [Bindable(true), Category("Settings"), DefaultValue("0"), System.ComponentModel.Description("The year to select after binding is complete. Set to zero for the last year.")]
    public int DefaultYear
    {
      get { return _DefaultYear; }
      set { _DefaultYear = value; }
    } int _DefaultYear = 0;

    [Bindable(true), Category("Settings"), DefaultValue("2000"), System.ComponentModel.Description("The minimum year to render from.")]
    public int MinYear
    {
      get { return _MinYear; }
      set
      {
        _MinYear = value;
        BindYears();
      }
    } int _MinYear = 2000;

    [Bindable(true), Category("Settings"), DefaultValue("0"), System.ComponentModel.Description("The maximum year to render to. Leave as zero to render the current year.")]
    public int MaxYear
    {
      get { return _MaxYear; }
      set
      {
        _MaxYear = value;
        BindYears();
      }
    } int _MaxYear = 0;

    protected int GetMaxYear()
    {
      if (this.MaxYear == 0)
        return DateTime.Now.Year;
      else
        return this.MaxYear;
    }

    protected void BindYears()
    {
      this.ClearSelection();
      this.Items.Clear();

      for (int year = this.MinYear; year <= this.GetMaxYear(); year++)
        this.Items.Add(new ListItem(year.ToString(), year.ToString()));

      if (this.Items.Count > 0)
        this.SelectedIndex = this.Items.Count - 1;
    }

    protected override void OnLoad(EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        BindYears();

        int defYear;
        if (this.DefaultYear == 0)
          defYear = this.GetMaxYear();
        else
          defYear = this.DefaultYear;

        if (System.DateTime.Now.Year >= this.MaxYear)
          this.SelectedValue = defYear.ToString();
      }

      base.OnLoad(e);
    }
  }
}