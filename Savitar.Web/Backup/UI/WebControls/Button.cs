using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:Button runat=server></{0}:Button>")]
  public class Button : System.Web.UI.WebControls.Button
  {
    public string CssClass_Over
    {
      get { return _CssClass_Over; }
      set { _CssClass_Over = value; }
    } string _CssClass_Over = "btn btnhov";

    public Button()
    {
      this.CssClass = "btn";
      this.Text = this.ID;

      if (!String.IsNullOrEmpty(this.CssClass_Over))
        this.Attributes.Add("onmouseover", String.Format("this.className='{0}'", this.CssClass_Over));
      if (!String.IsNullOrEmpty(this.CssClass))
        this.Attributes.Add("onmouseout", String.Format("this.className='{0}'", this.CssClass));
    }    
  }
}