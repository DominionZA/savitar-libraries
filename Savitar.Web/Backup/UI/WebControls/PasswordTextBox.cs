using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:PasswordTextBox runat=server></{0}:PasswordTextBox>")]
  public class PasswordTextBox : System.Web.UI.WebControls.TextBox
  {
    public PasswordTextBox()
    {
      TextMode = TextBoxMode.Password;
    }

    public override string Text
    {
      get
      {
        return base.Text;
      }
      set
      {
        base.Text = value;

        Attributes["value"] = value;
      }
    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      Attributes["value"] = Text;
    }
  }
}