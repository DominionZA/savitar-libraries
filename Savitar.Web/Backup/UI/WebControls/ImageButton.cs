using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:ImageButton runat=server></{0}:ImageButton>")]
  public class ImageButton : System.Web.UI.WebControls.ImageButton
  {
    public string AppSetting
    {
      get { return _NoImageConfigName; }
      set { _NoImageConfigName = value; }
    } string _NoImageConfigName = "NoImageURL";

    public ImageButton()
    {
    }

    protected override void OnPreRender(EventArgs e)
    {
      if (!String.IsNullOrEmpty(AppSetting))
      {
        if (WebConfigurationManager.AppSettings[this.AppSetting] != null)
        {
          string filePath = WebConfigurationManager.AppSettings[this.AppSetting];
          if ((String.IsNullOrEmpty(this.ImageUrl)) && (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(filePath))))          
            this.ImageUrl = filePath;
        }
      }

      base.OnPreRender(e);
    }
  }
}
