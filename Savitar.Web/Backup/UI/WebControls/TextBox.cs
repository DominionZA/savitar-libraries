using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:TextBox runat=server></{0}:TextBox>")]
  public class TextBox : System.Web.UI.WebControls.TextBox
  {
    public enum TextBoxModeEx { Normal, Ordinal, Decimal, UpperCaseText, LowerCaseText }

    public bool StartFocused
    {
      get { return _StartFocused; }
      set { _StartFocused = value; }
    } bool _StartFocused = false;

    public TextBoxModeEx TextModeEx
    {
      get { return _TextModeEx; }
      set { _TextModeEx = value; }
    } TextBoxModeEx _TextModeEx = TextBoxModeEx.Normal;

    public int DecimalPlaces
    {
      get { return _DecimalPlaces; }
      set
      {
        _DecimalPlaces = value;
        
        if (_DecimalPlaces > 0)
          this.TextModeEx = TextBoxModeEx.Decimal;
      }
    } int _DecimalPlaces = 0;

    public TextBox()
    {
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      if (this.StartFocused)
      {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine("var canisterSerialTextBox = document.getElementById('" + this.ClientID + "');");
        sb.AppendLine("if (canisterSerialTextBox != null)");
        sb.AppendLine("{");
        sb.AppendLine("  canisterSerialTextBox.focus();");        
//        sb.AppendLine("  document.getElementById('" + this.ClientID + "').focus();");
//        sb.AppendLine("  alert('aaaaaaaa');");
        sb.AppendLine("}");
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "FocusCanisterTextbox", sb.ToString(), true);

        //this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "SetFocus", "document.getElementById('" + this.ClientID + "').focus();", true);
      }

      if ((this.TextModeEx == TextBoxModeEx.Decimal) || (this.TextModeEx == TextBoxModeEx.Ordinal))
      {
        if (!this.Page.ClientScript.IsClientScriptBlockRegistered("TextBoxNumericFilter"))
        {
          System.Text.StringBuilder sb = new System.Text.StringBuilder();
          sb.AppendLine("function TextBoxNumericFilter(e)");
          sb.AppendLine("{");
          sb.AppendLine("  if (e.keyCode > 46 && e.keyCode < 58)");
          sb.AppendLine("    return true;");
          sb.AppendLine("  else");
          sb.AppendLine("    return false; ");
          sb.AppendLine("}");

          this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "TextBoxNumericFilter", sb.ToString(), true);
        }        
      }
      else if (this.TextModeEx == TextBoxModeEx.UpperCaseText)
      {      
        this.Style.Add("text-transform", "uppercase");
      }
      else if (this.TextModeEx == TextBoxModeEx.LowerCaseText)
        this.Style.Add("text-transform", "lowercase");
    }
  }
}