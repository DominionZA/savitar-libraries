using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:Panel runat=server></{0}:Panel>")]
  public class Panel : System.Web.UI.WebControls.Panel, INamingContainer
  {
  }
}