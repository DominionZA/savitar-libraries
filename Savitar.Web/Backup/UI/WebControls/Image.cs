using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:Image runat=server></{0}:Image>")]
  public class Image : System.Web.UI.WebControls.Image
  {
    public string AppSetting
    {
      get { return _NoImageConfigName; }
      set { _NoImageConfigName = value; }
    } string _NoImageConfigName = "NoImageURL";

    protected override void OnPreRender(EventArgs e)
    {
      if (!String.IsNullOrEmpty(AppSetting))
      {
        if (WebConfigurationManager.AppSettings[this.AppSetting] != null)
        {
          string filePath = WebConfigurationManager.AppSettings[this.AppSetting];
          if ((String.IsNullOrEmpty(this.ImageUrl)) && (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(filePath))))
            this.ImageUrl = filePath;
        }
      }

      base.OnPreRender(e);
    }
  }
}
