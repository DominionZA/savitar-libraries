using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Savitar.Web.UI.WebControls
{
  [ToolboxData("<{0}:GridView runat=server></{0}:GridView>")]
  public class GridView : System.Web.UI.WebControls.GridView
  {
    public bool AutoRowSelection
    {
      get { return _AutoRowSelection; }
      set { _AutoRowSelection = value; }
    } bool _AutoRowSelection = true;

    public FormView LinkedFormView
    {
      get { return _LinkedFormView; }
      set { _LinkedFormView = value; }
    } FormView _LinkedFormView = null;

    public GridView()
    {
      this.Width = Unit.Percentage(100);            
      this.AutoGenerateColumns = false;
      this.CssClass = "grid";
      //this.AlternatingRowStyle.CssClass = "gridAltRow";
      //this.RowStyle.CssClass = "gridRow";
      this.SelectedRowStyle.CssClass = "gridSelectedRow";
      this.HeaderStyle.CssClass = "gridHeader";
      this.FooterStyle.CssClass = "gridFooter";
    }

    protected void SetRowSelectScript(GridViewRow row, bool removeScript)
    {
      if (removeScript)
      {
        row.Attributes.Remove("onmouseover");
        row.Attributes.Remove("onmouseout");
        row.Attributes.Remove("onclick");      
      }
      else
      {
        row.Attributes["onmouseover"] = "this.style.cursor='hand';this.style.textDecoration='underline';";
        row.Attributes["onmouseout"] = "this.style.textDecoration='none';";          
      }
    }

    protected void SetRowSelectScript(GridViewRow row)
    {
      SetRowSelectScript(row, false);
    }

    protected override void OnRowDataBound(GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        // Build in the "Delete" prompt
        Control DeleteButton = e.Row.FindControl("DeleteButton");
        if (DeleteButton != null)
        {
          string deletePrompt = "Are you sure you want to delete this record?";

          if (DeleteButton.GetType() == typeof(LinkButton))
            ((LinkButton)DeleteButton).Attributes.Add("onclick", String.Format("return confirm('{0}');", deletePrompt));
          else if (DeleteButton.GetType() == typeof(Button))
            ((Button)DeleteButton).Attributes.Add("onclick", String.Format("return confirm('{0}');", deletePrompt));
          else if (DeleteButton.GetType() == typeof(ImageButton))
            ((ImageButton)DeleteButton).Attributes.Add("onclick", String.Format("return confirm('{0}');", deletePrompt));
        }

        if (_AutoRowSelection)
        {
          // Don't want to set the auto select row script if the row is in edit mode.
//          if ((e.Row.RowIndex != this.SelectedIndex) && (e.Row.RowIndex != this.EditIndex))
            SetRowSelectScript(e.Row, false);
        }                
      }            

      base.OnRowDataBound(e);
    }    

    protected override void OnPageIndexChanged(EventArgs e)
    {
      base.OnPageIndexChanged(e);
    }

    protected override void OnPageIndexChanging(GridViewPageEventArgs e)
    {
      this.SelectedIndex = -1;
      base.OnPageIndexChanging(e);
    }

    protected override void  OnRowDeleting(GridViewDeleteEventArgs e)
    {
      this.SelectedIndex = -1;
 	    base.OnRowDeleting(e);
    }

    protected override void OnRowDeleted(GridViewDeletedEventArgs e)
    {
      base.OnRowDeleted(e);

      e.ExceptionHandled = DoError(String.Format("Deleting item"), e.Exception);
    }

    protected override void OnRowEditing(GridViewEditEventArgs e)
    {      
      this.SelectedIndex = -1;
      SetRowSelectScript(this.Rows[e.NewEditIndex], true);

      base.OnRowEditing(e);
    }    

    protected override void OnRowUpdated(GridViewUpdatedEventArgs e)
    {      
      base.OnRowUpdated(e);
    }

    protected override void OnRowCancelingEdit(GridViewCancelEditEventArgs e)
    {
      SetRowSelectScript(this.Rows[e.RowIndex], false);

      base.OnRowCancelingEdit(e);
    }

    protected override void OnSelectedIndexChanged(EventArgs e)
    {
      this.EditIndex = -1;
      base.OnSelectedIndexChanged(e);      
    }

    protected override void OnDataBound(EventArgs e)
    {      
      base.OnDataBound(e);      
    }

    protected override void OnDataBinding(EventArgs e)
    {
      int selectedIndex = this.SelectedIndex;

      base.OnDataBinding(e);

      this.SelectedIndex = selectedIndex;
    }

    protected bool DoError(string action, Exception ex)
    {
      if (ex == null)
        return false;

      Savitar.Web.Utils.Popup.ShowDialog(this.Page, ex.Message);      

      return true;
    }

    public void ExportToExcel()
    {      
      HttpContext.Current.Response.Clear();
      HttpContext.Current.Response.AddHeader(String.Format("content-disposition", "attachment;filename={0}.xls"), this.Page.Title);
      HttpContext.Current.Response.Charset = "";
      HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
      HttpContext.Current.Response.ContentType = "application/vnd.xls";
      System.IO.StringWriter stringWrite = new System.IO.StringWriter();
      System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
      this.RenderControl(htmlWrite);
      HttpContext.Current.Response.Write(stringWrite.ToString());
      HttpContext.Current.Response.End();
    }

    public void Export()
    {
      HttpContext.Current.Response.Clear();
      HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.xls", this.Page.Title));
      HttpContext.Current.Response.Charset = "";
      HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
      HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

      using (StringWriter sw = new StringWriter())
      {
        using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        {
          //  Create a table to contain the grid
          Table table = new Table();

          //  include the gridline settings
          table.GridLines = this.GridLines;

          //  add the header row to the table
          if (this.HeaderRow != null)
          {
            this.PrepareControlForExport(this.HeaderRow);
            table.Rows.Add(this.HeaderRow);
          }

          //  add each of the data rows to the table
          foreach (GridViewRow row in this.Rows)
          {
            this.PrepareControlForExport(row);
            table.Rows.Add(row);
          }

          //  add the footer row to the table
          if (this.FooterRow != null)
          {
            this.PrepareControlForExport(this.FooterRow);
            table.Rows.Add(this.FooterRow);
          }

          //  render the table into the htmlwriter
          table.RenderControl(htw);

          //  render the htmlwriter into the response
          HttpContext.Current.Response.Write(sw.ToString());
          HttpContext.Current.Response.End();
        }
      }
    }

    /// <summary>
    /// Replace any of the contained controls with literals
    /// </summary>
    /// <param name="control"></param>
    private void PrepareControlForExport(Control control)
    {
      for (int i = 0; i < control.Controls.Count; i++)
      {
        Control current = control.Controls[i];
        if (current is LinkButton)
        {
          control.Controls.Remove(current);
          control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
        }
        else if (current is ImageButton)
        {
          control.Controls.Remove(current);
          control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
        }
        else if (current is HyperLink)
        {
          control.Controls.Remove(current);
          control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
        }
        else if (current is DropDownList)
        {
          control.Controls.Remove(current);
          control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
        }
        else if (current is CheckBox)
        {
          control.Controls.Remove(current);
          control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
        }

        if (current.HasControls())
        {
          this.PrepareControlForExport(current);
        }
      }
    }
  }
}
