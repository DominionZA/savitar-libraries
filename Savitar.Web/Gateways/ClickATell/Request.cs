using System;
using System.Net;
using System.IO;

namespace Savitar.Web.Gateways.ClickATell
{
    public class Request
    {
        public string SessionID = "";
        protected DateTime LastActionTime;
        protected int SessionTimeout = 15;
        protected string User = "KAI";
        protected string Password = "kai1111";
        protected string APIID = "1044778";

        public Request()
        {
        }

        protected void CheckSessionID()
        {
            if ((SessionID == null) || (LastActionTime.AddMinutes(SessionTimeout) < DateTime.Now))
                UpdateSessionID();
        }

        // Sends the message and returns the response from ClickATell
        protected string DoSend(WebClient client, string url)
        {
            client.Headers.Add("user-agent", "Mozilla/4.0(compatible;MSIE6.0;Windows NT 5.2; .NET CLR 1.0.3705;)");

            var data = client.OpenRead(url);
            try
            {
                var reader = new StreamReader(data);
                try
                {
                    LastActionTime = DateTime.Now;

                    return reader.ReadToEnd();
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                data?.Close();
            }
        }

        protected string ParseResponse(string response)
        {
            var i = response.IndexOf(':');
            if (i < 0)
                throw new Exception("Invalid Response Code: " + response);

            var result = response.Substring(0, i).ToUpper().Trim();
            var value = response.Substring(i + 1).Trim();

            if (result != "OK")
                throw new Exception("ClickATell responded with an error message of : " + value);

            return value;
        }

        public string Ping()
        {
            CheckSessionID();

            var client = new WebClient();

            client.QueryString.Add("session_id", SessionID);

            var response = new Response();
            response.SetResponseStr(DoSend(client, "http://api.clickatell.com/http/ping"));

            return response.Count.ToString();
        }

        public Response QueryMessage(string apiMsgId)
        {
            CheckSessionID();

            var client = new WebClient();

            client.QueryString.Add("session_id", SessionID);
            client.QueryString.Add("apimsgid", apiMsgId);

            var result = new Response();
            result.SetResponseStr(DoSend(client, "http://api.clickatell.com/http/querymsg"));

            return result;
        }

        public string SendMsg(string cellNumber, string message)
        {
            CheckSessionID();

            WebClient client = new WebClient();

            client.QueryString.Add("session_id", SessionID);
            client.QueryString.Add("to", cellNumber);
            client.QueryString.Add("text", message);

            return DoSend(client, "http://api.clickatell.com/http/sendmsg");
        }

        // Returns the batch ID
        public string StartBatch(string templateMsg)
        {
            CheckSessionID();

            var client = new WebClient();

            client.QueryString.Add("session_id", SessionID);
            client.QueryString.Add("template", templateMsg);

            var response = new Response();
            response.SetResponseStr(DoSend(client, "http://api.clickatell.com/http_batch/startbatch"));

            if (response.Count == 0)
                throw new Exception("No response was returned when attempting to start a new batch");

            if (response.Count > 1)
                throw new Exception("An invalid response was returned when attempting to start a new batch");

            if (string.CompareOrdinal(response[0].Values.GetKey(0), "ID") != 0)
                throw new Exception("An error was returned when attempting to start a new batch<br/>" +
                  response[0].Values.GetKey(0) + " = " + response[0].Values[0]);

            // All should be kewl now. We have an ID
            return response[0].Values[0];
        }

        public Response QuickSendBatch(string batchId, string numbers)
        {
            CheckSessionID();

            var client = new WebClient();

            client.QueryString.Add("session_id", SessionID);
            client.QueryString.Add("batch_id", batchId);
            client.QueryString.Add("to", numbers);

            var response = new Response();
            response.SetResponseStr(DoSend(client, "http://api.clickatell.com/http_batch/quicksend"));

            return response;
        }

        public Response EndBatch(string batchId)
        {
            CheckSessionID();

            WebClient client = new WebClient();

            client.QueryString.Add("session_id", SessionID);
            client.QueryString.Add("batch_id", batchId);

            Response oResponse = new Response();
            oResponse.SetResponseStr(DoSend(client, "http://api.clickatell.com/http_batch/endbatch"));

            return oResponse;
        }

        public bool UpdateSessionID()
        {
            var client = new WebClient();

            client.QueryString.Add("api_id", APIID);
            client.QueryString.Add("user", User);
            client.QueryString.Add("password", Password);

            SessionID = ParseResponse(DoSend(client, "http://api.clickatell.com/http/auth"));

            return true;
        }
    }
}