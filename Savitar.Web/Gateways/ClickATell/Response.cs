using System.Collections.Specialized;

namespace Savitar.Web.Gateways.ClickATell
{
    public enum ErrorCode
    {
        None = 0, AuthenticationFailed = 1, UnknownUserNameOrPassword = 2,
        SessionIDExpired = 3, AccountFrozen = 4, MissingSessionID = 5,
        InvalidParams = 101, InvalidUDH = 102, UnknownAPIMsgID = 103,
        UnknownCLIMsgID = 104, InvalidDestination = 105,
        InvalidSource = 106, EmptyMsg = 107, InvalidAPI_ID = 108,
        MissingMsgID = 109, ErrorWithEmailMsg = 110, InvalidProtocol = 111,
        InvalidMsg_Type = 112, MaxMsgPartsExceeded = 113,
        CannotRouteMsg = 114, MsgExpired = 115, InvalidUnicodeData = 116,
        InvalidDeliveryTime = 120, InvalidBatchID = 201,
        NoBatchTemplate = 202, NoCreditLeft = 301, MaxAllowedCredit = 302
    };

    public enum MsgStatus
    {
        Unknown = 1, Queued = 2, Delivered = 3, Received = 4,
        ErrorWithMsg = 5, UserCancelled = 6, ErrorDelivering = 7,
        OK = 8, RoutingError = 9, Expired = 10, DelayedDelivery = 11,
        OutOfCredit = 12
    };

    public class ResponseItem
    {
        protected string FResponseStr = "";
        public ErrorCode ErrCode = ErrorCode.None;
        public NameValueCollection Values = new NameValueCollection();

        public void SetResponseStr(string responseStr)
        {
            FResponseStr = responseStr;

            // Now to parse the string.
            Values.Clear();

            var sType = "";
            var sValue = "";


            // "ID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003"

            for (var i = 0; i < FResponseStr.Length; i++)
            {
                if (sValue == "")
                    sType += FResponseStr[i];
                else
                    sValue += FResponseStr[i];

                if (FResponseStr[i] == ' ' || i == FResponseStr.Length - 1)
                {
                    if (sValue == "")
                        sValue = " ";
                    else
                    {
                        sType = sType.Replace(':', ' ');
                        Values.Add(sType.Trim(), sValue.Trim());
                        //						System.Web.HttpContext.Current.Response.Write("2");
                        //						System.Web.HttpContext.Current.Response.Write("Type: " + sType + ", Value: " + sValue + "<br/>");
                        sType = "";
                        sValue = "";
                    }
                }

            }
        }

        public string ErrCodeToStr(ErrorCode errorCode)
        {
            return "None";
        }

        public ResponseItem()
        {
            DoResetFields();
        }

        public void DoResetFields()
        {
            ErrCode = ErrorCode.None;
        }
    }

    public class Response : System.Collections.CollectionBase
    {
        public ResponseItem Add()
        {
            var item = new ResponseItem();
            List.Add(item);
            return item;
        }

        public ResponseItem this[int index]
        {
            get => (ResponseItem)List[index];
            set => List[index] = value;
        }

        protected int GetNextTypePos(string str, int startPos)
        {
            // Look for the next colon
            var iNextColonPos = str.IndexOf(':', startPos);
            // If not found, then return -1 (No more types)
            if (iNextColonPos < 0)
                return -1;

            // We have a type. Extract the string up to the colon
            var s = str.Substring(0, iNextColonPos);

            // Look for the last space in the extracted string
            var iLastSpacePos = s.LastIndexOf(' ');
            // If not found, then the start pos of this type is 0 (Beginning of string)
            if (iLastSpacePos < 0)
                return 0;

            // If we found a space, then return the pos of that space + 1 (Start of the type)
            return (iLastSpacePos + 1);
        }

        protected string GetType(string value)
        {
            var iLastSpacePos = value.LastIndexOf(' ');
            return iLastSpacePos < 0 ? value : value.Substring(iLastSpacePos);
        }

        public void SetResponseStr(string responseStr)
        {
            // "ID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003\rID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003\r"
            Clear();

            string sResponseStr = "";

            // Prepare the string for splitting
            responseStr = responseStr.Replace("\n", " \n");
            for (var i = 0; i < responseStr.Length; i++)
            {
                if (responseStr[i] == ':') // We have a type trailer.
                {
                    var lastSpaceIndex = sResponseStr.LastIndexOf(' ');
                    if (lastSpaceIndex >= 0)
                        sResponseStr = sResponseStr.Insert(lastSpaceIndex + 1, ":");
                }

                sResponseStr += responseStr[i];
            }

            // Split the items into records
            var records = sResponseStr.Split('\n');
            for (int record = 0; record < records.Length; record++)
            {
                var item = Add();

                var parts = records[record].Split(':');
                for (var i = 0; i < parts.Length; i++)
                {
                    if ((i % 2 != 0) && (parts[i - 1].Trim().Length > 0) && (parts[i].Trim().Length > 0))
                    {
                        item.Values.Add(parts[i - 1].Trim(), parts[i].Trim());
                    }
                }
            }
        }
        /*
              Clear();
        // "ID: 32c506028dd873eeb88cdd5a8576c4ef Status: 003, Authentication failed"

              string[] Parts = responseStr.Split(' ');

              string Type = "";
              string FirstType = "";
              string Value = "";
              ResponseItem oItem = Add();

              for (int iPart = 0; iPart < Parts.Length; iPart++)
              {
                if (Parts[iPart].IndexOf(':') == Parts[iPart].Length - 1) // Have a type
                {
                  if ((Type != "") && (Value != "")) // Have a type and value. Add them
                  {
                    oItem.Values.Add(Type, Value);
                    Type = "";
                    Value = "";
                  }

                  Type = Parts[iPart];

                  if (FirstType == "")
                    FirstType = Parts[iPart];
                  else if ((String.Compare(FirstType, Type) == 0) || (String.Compare(Type, "ERR:") == 0))
                    oItem = Add();
                }
                else // Still adding value
                {
                  if (Value != "")
                    Value += " ";

                  Value += Parts[iPart];
                }

                if (iPart == Parts.Length - 1)
                {
                  if ((Type != "") && (Value != "")) // Have a type and value. Add them
                  {
                    oItem.Values.Add(Type, Value);
                    Type = "";
                    Value = "";
                  }
                }
              }
        */
    }
}