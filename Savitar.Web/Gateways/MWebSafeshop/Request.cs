using System;
using System.Text;
using System.Web;

namespace Savitar.Web.Gateways.MWebSafeshop
{
    public enum SafeshopTransactionType { NotSet, Auth, AuthSettle }
    public enum SafeshopCurrencyCode { NotSet, ZAR }

    public class Request
    {
        #region Request Properties
        /// <summary>
        /// The URL to post the data to to initiate the payment.
        /// </summary>
        public string Url = "https://secure.SafeShop.co.za/SafePay/Lite/Index.asp";
        /// <summary>
        /// A unique SafeKey is issued to each store. This SafeKey will be used to authenticate the Store. SafeKey resembles {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}. NOTE: You must also include the curly brackets around the SafeKey GUID.
        /// </summary>
        public Guid SafeKey = Guid.Empty;
        /// <summary>
        /// Unique reference number per-transaction for their own record, e.g. this can be achieved by concatenating merchant website name and the current date and time of the purchase like �Merchantwebiste_01-March-2001 2:220:22 PM�. If no reference is supplied, SafeShop� will issue "No_Merchant_RefNr_+ Current DateTime".
        /// </summary>
        public string MerchantReferenceNumber = "";
        /// <summary>
        /// The total amount in CENTS to be reserved against the buyer�s account. e.g.. the amount R12.99 must be written in cents as in 1299. Minimum amount is 1 cents and maximum amount is 99999999 cents. If the amount is omitted, non-numeric or negative amount, SafeShop� will issue an error.
        /// </summary>
        public Decimal TransactionAmount = 0;
        /// <summary>
        /// This option only applies to World Pay transactions. This indicates the currency for the transaction. The default value is ZAR (South African Rand).
        /// </summary>
        public SafeshopCurrencyCode CurrencyCode = SafeshopCurrencyCode.ZAR;
        /// <summary>
        /// SafeTrack represents a GUID that was passed from a portal. NOTE: You must also include the curly brackets.
        /// </summary>
        public Guid SafeTrack = Guid.Empty;
        /// <summary>
        /// The ReceiptURL allows SafeShop� Payment Manager to redirect the shopper when a transaction has been
        /// completed successfully. e.g. http://strawberry.safeshop.co.za/thankyou.asp. If no URL is supplied, SafeShop� Payment Manager will issue an error.
        /// </summary>
        public string ReceiptUrl = "";
        /// <summary>
        /// The FailURL allows SafeShop� Payment Manager to redirect the shopper when transaction has failed. e.g. http://strawberry.safeshop.co.za/failed.asp
        /// </summary>
        public string FailUrl = "";
        /// <summary>
        /// This indicates the type of transaction to execute on SafeShop� Payment Manager. In this instance the transaction type must be set to Auth. If the transaction type is omitted or an unknown transaction type is entered, SafeShop� Payment Manager will issue an error. 
        /// Transaction types include: 
        /// Auth (Funds is reserved) 
        /// Auth_Settle (Funds is transferred to the merchant account)
        /// (Only applicable to credit card payments)
        /// </summary>
        public SafeshopTransactionType TransactionType = SafeshopTransactionType.AuthSettle;
        #endregion

        protected string PropToHTMLString(string propertyName, string propertyValue)
        {
            return $"<INPUT TYPE=\"hidden\" NAME=\"{propertyName}\" VALUE=\"{propertyValue}\"> ";
        }

        public void Send()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write(GetHTMLForm());
            //HttpContext.Current.Response.Flush();      
        }

        private static int RandsToCents(decimal randValue)
        {
            return (int)(randValue * 100);
        }

        private decimal CentsToRands(int cents)
        {
            return cents / (decimal)100;
        }

        public string GetHiddenInputs()
        {
            StringBuilder sb = new StringBuilder();

            // Now slot in each of the properties.
            sb.AppendLine(PropToHTMLString("SafeKey", "{" + $"{SafeKey.ToString().ToUpper()}" + "}"));
            sb.AppendLine(PropToHTMLString("MerchantReferenceNumber", MerchantReferenceNumber));
            sb.AppendLine(PropToHTMLString("TransactionAmount", RandsToCents(TransactionAmount).ToString()));
            sb.AppendLine(PropToHTMLString("CurrencyCode", CurrencyCode.ToString()));
            sb.AppendLine(PropToHTMLString("SafeTrack", "{0}"));
            sb.AppendLine(PropToHTMLString("ReceiptURL", ReceiptUrl));
            sb.AppendLine(PropToHTMLString("FailURL", FailUrl));
            sb.AppendLine(PropToHTMLString("TransactionType", TransactionType.ToString()));

            //throw new Exception(Safeshop.PropToHTMLString("TransactionAmount", RandsToCents(TransactionAmount).ToString()));

            return sb.ToString();
        }

        private string GetHTMLForm()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<html>");
            sb.AppendLine("<title>Safeshop Gateway</title>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body onload=\"frmPay.submit();\">");
            sb.AppendLine("<FORM name=frmPay ACTION=\"" + Url + "\" METHOD=\"POST\">");

            sb.Append(GetHiddenInputs());

            sb.AppendLine("</FORM>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");

            return sb.ToString();
        }
    }
}
