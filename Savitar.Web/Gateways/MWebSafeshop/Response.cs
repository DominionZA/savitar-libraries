using System;

namespace Savitar.Web.Gateways.MWebSafeshop
{
    public class Response
    {
        public enum SafeshopTransactionResult { Unknown, Successful, Failed, StageOrder }
        public static SafeshopTransactionResult StrToTransactionResult(string transactionResult)
        {
            switch (transactionResult.ToLower())
            {
                case "successful": return SafeshopTransactionResult.Successful;
                case "failed": return SafeshopTransactionResult.Failed;
                case "stageorder": return SafeshopTransactionResult.StageOrder;
                default: return SafeshopTransactionResult.Unknown;
            }
        }

        public static SafeshopTransactionType StrToTransactionType(string transactionType)
        {
            switch (transactionType.ToLower())
            {
                case "auth": return SafeshopTransactionType.Auth;
                case "auth_settle": return SafeshopTransactionType.AuthSettle;
                default: return SafeshopTransactionType.NotSet;
            }
        }

        public Response(System.Collections.Specialized.NameValueCollection postedForm)
        {
            LogRefNr = string.IsNullOrEmpty(postedForm.Get("LogRefNr")) ? 0 : Convert.ToInt32(postedForm.Get("LogRefNr"));
            MerchantReference = string.IsNullOrEmpty(postedForm.Get("MerchantReference")) ? "" : postedForm.Get("MerchantReference");
            TransactionAmount = string.IsNullOrEmpty(postedForm.Get("TransactionAmount")) ? 0 : Convert.ToDecimal(Convert.ToInt32(postedForm.Get("TransactionAmount")) / 100);
            TransactionType = string.IsNullOrEmpty(postedForm.Get("TransactionType")) ? SafeshopTransactionType.NotSet : StrToTransactionType(postedForm.Get("TransactionType"));
            SafePayRefNr = string.IsNullOrEmpty(postedForm.Get("SafePayRefNr")) ? "" : postedForm.Get("SafePayRefNr");
            BankRefNr = string.IsNullOrEmpty(postedForm.Get("BankRefNr")) ? "" : postedForm.Get("BankRefNr");
            TransactionErrorResponse = string.IsNullOrEmpty(postedForm.Get("TransactionErrorResponse")) ? "" : postedForm.Get("TransactionErrorResponse");
            TransactionResult = string.IsNullOrEmpty(postedForm.Get("TransactionResult")) ? SafeshopTransactionResult.Unknown : Response.StrToTransactionResult(postedForm.Get("TransactionResult"));
            LiveTransaction = string.IsNullOrEmpty(postedForm.Get("LiveTransaction")) || Convert.ToBoolean(postedForm.Get("LiveTransaction"));
            SafeTrack = string.IsNullOrEmpty(postedForm.Get("SafeTrack")) ? Guid.Empty : new Guid(postedForm.Get("SafeTrack"));
            ReceiptUrl = string.IsNullOrEmpty(postedForm.Get("ReceiptURL")) ? "" : postedForm.Get("ReceiptURL");
            BuyerCreditCardNr = string.IsNullOrEmpty(postedForm.Get("BuyerCreditCardNr")) ? "" : postedForm.Get("BuyerCreditCardNr");
            HidStoreKey = string.IsNullOrEmpty(postedForm.Get("hidStoreKey")) ? Guid.Empty : new Guid(postedForm.Get("hidStoreKey"));
        }

        #region Response Properties
        /// <summary>
        /// The transaction log reference number created by SafeShop
        /// </summary>
        public int LogRefNr;
        /// <summary>
        /// A unique reference number per-transaction supplied by the merchant.
        /// </summary>
        public string MerchantReference;
        /// <summary>
        /// The total amount, in CENTS, that was charged, e.g. if the amount that was charged was R12.99, then SafeShop� 0 returns 1299 in cents as the amount charged.
        /// </summary>
        public Decimal TransactionAmount;
        /// <summary>
        /// The type of transaction that was executed
        /// </summary>
        public SafeshopTransactionType TransactionType;
        /// <summary>
        /// Unique reference number per transaction. If you have any queries against a transaction use this number for support.
        /// </summary>
        public string SafePayRefNr;
        /// <summary>
        /// Represents the bank reference number for the transaction issued by the Inquiring Institution.
        /// </summary>
        public string BankRefNr;
        /// <summary>
        /// Transaction failed reason explanation.
        /// </summary>
        public string TransactionErrorResponse;
        /// <summary>
        /// Return the status of the transaction. The values returned are:
        /// Successful - The transaction was successfully processed
        /// Failed - Transaction failed Check TransactionErrorResponse tag for more information
        /// StageOrder - This transaction was put in stage mode. (See Staged Orders)
        /// </summary>
        public SafeshopTransactionResult TransactionResult;
        /// <summary>
        /// Indicates whether the transaction was executed as live (True) or was simulated (False) by SafeShop�.
        /// </summary>
        public Boolean LiveTransaction;
        /// <summary>
        /// SafeTrack represents a GUID that was passed from a portal. NOTE: You must also include the curly brackets.
        /// </summary>
        public Guid SafeTrack;
        /// <summary>
        /// The Receipt URL. The Merchant may use ReceiptURL to redirect the Shopper to view their receipt on SafeShop�.
        /// </summary>
        public string ReceiptUrl;
        /// <summary>
        /// The full names that appears on the credit card, e.g.. John Doe
        /// </summary>
        public string BuyerCreditCardNr;
        /// <summary>
        /// A unique SafeKey is issued to each store. SafeKey resembles {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}. NOTE: You must also include the curly brackets around the SafeKey GUID.
        /// </summary>
        public Guid HidStoreKey;
        #endregion
    }
}
