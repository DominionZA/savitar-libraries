namespace Savitar.Web.UI
{
    public class Page
    {
        public static void InsertMetaRefresh(System.Web.UI.Page page, int refreshSeconds, string url)
        {
            var metaRefresh = new System.Web.UI.HtmlControls.HtmlGenericControl("meta");
            metaRefresh.Attributes.Add("http-equiv", "refresh");
            metaRefresh.Attributes.Add("content", $"{refreshSeconds};URL={url}");
            page.Header.Controls.Add(metaRefresh);
        }
    }
}