using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Savitar.Web.UI.WebControls
{
    /// <summary>
    /// Simple DropDownList that displays years from a min to max year.
    /// </summary>
    [ToolboxData("<{0}:YearDropDownList runat=server></{0}:YearDropDownList>")]
    public class YearDropDownList : DropDownList
    {
        [Bindable(true), Category("Settings"), DefaultValue("0"), Description("The year to select after binding is complete. Set to zero for the last year.")]
        public int DefaultYear { get; set; } = 0;

        [Bindable(true), Category("Settings"), DefaultValue("2000"), Description("The minimum year to render from.")]
        public int MinYear
        {
            get => minYear;
            set
            {
                minYear = value;
                BindYears();
            }
        }

        private int minYear = 2000;

        [Bindable(true), Category("Settings"), DefaultValue("0"), Description("The maximum year to render to. Leave as zero to render the current year.")]
        public int MaxYear
        {
            get => maxYear;
            set
            {
                maxYear = value;
                BindYears();
            }
        }

        private int maxYear;

        protected int GetMaxYear()
        {
            if (MaxYear == 0)
                return DateTime.Now.Year;
            else
                return MaxYear;
        }

        protected void BindYears()
        {
            ClearSelection();
            Items.Clear();

            for (var year = MinYear; year <= GetMaxYear(); year++)
                Items.Add(new ListItem(year.ToString(), year.ToString()));

            if (Items.Count > 0)
                SelectedIndex = Items.Count - 1;
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindYears();

                int defYear;
                if (DefaultYear == 0)
                    defYear = GetMaxYear();
                else
                    defYear = DefaultYear;

                if (DateTime.Now.Year >= MaxYear)
                    SelectedValue = defYear.ToString();
            }

            base.OnLoad(e);
        }
    }
}