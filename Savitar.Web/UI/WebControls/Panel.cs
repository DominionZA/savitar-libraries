using System.Web.UI;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:Panel runat=server></{0}:Panel>")]
    public class Panel : System.Web.UI.WebControls.Panel, INamingContainer
    {
    }
}