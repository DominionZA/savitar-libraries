using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:ListBox runat=server></{0}:ListBox>")]
    public partial class ListBox : System.Web.UI.WebControls.ListBox
    {
        public ListBox()
        {
            SelectionMode = ListSelectionMode.Multiple;
        }

        [Bindable(false), Category("MultiSelection"), DefaultValue("true"), Description("Specifies whether all items must be auto selected when databound")]
        public bool SelectAll
        {
            get => selectAll;
            set
            {
                selectAll = value;

                if (selectAll)
                    SelectAllItems();
            }
        }

        private bool selectAll = true;

        [Bindable(false), Category("MultiSelection"), DefaultValue(","), Description("The delimiter to use between items when calling SelectedValues")]
        public string ValuesDelimiter { get; set; } = ",";

        [Bindable(false), Category("MultiSelection"), DefaultValue(""), Description("The prefix to use for each item when calling SelectedValues")]
        public string ValuesPrefix { get; set; } = "";

        [Bindable(false), Category("MultiSelection"), DefaultValue(""), Description("The suffix to use for each item when calling SelectedValues")]
        public string ValuesSuffix { get; set; } = "";

        /// <summary>
        /// Returns a list of selected items in the listbox.
        /// </summary>
        public string SelectedValues
        {
            get
            {
                var result = "";

                foreach (ListItem item in Items)
                {
                    if (!item.Selected)
                        continue;

                    if (!string.IsNullOrEmpty(result))
                        result += ValuesDelimiter;

                    result += $"{ValuesPrefix}{item.Value}{ValuesSuffix}";
                }

                return result;
            }
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);

            if (SelectAll)
                SelectAllItems();
        }

        protected void SelectAllItems()
        {
            SelectionMode = ListSelectionMode.Multiple;

            foreach (ListItem item in Items)
                item.Selected = true;
        }

        public sealed override ListSelectionMode SelectionMode
        {
            get => base.SelectionMode;
            set
            {
                base.SelectionMode = value;
                if (value != ListSelectionMode.Multiple)
                    selectAll = false;
            }
        }

        [TypeConverter(typeof(aaaa))]
        public ListBox LinkedListBox { get; set; } = null;

        protected override void OnPagePreLoad(object sender, EventArgs e)
        {
            base.OnPagePreLoad(sender, e);

            if (LinkedListBox == null) 
                return;

            if (!Page.ClientScript.IsClientScriptBlockRegistered("SAV_MoveListItem"))
            {
                var sb = new System.Text.StringBuilder();
                sb.AppendLine("function SAV_MoveListItem(strBox1, strBox2)");
                sb.AppendLine("{");
                sb.AppendLine("  var box1 = document.getElementById(strBox1);");
                sb.AppendLine("  var box2 = document.getElementById(strBox2);");
                sb.AppendLine("");
                sb.AppendLine("  for (var i=0; i < box1.options.length; i++)");
                sb.AppendLine("  {");
                sb.AppendLine("    if (box1.options[i].selected)");
                sb.AppendLine("    {");
                sb.AppendLine("      box2.options[box2.options.length] = new Option (box1.options[i].text, box1.options[i].value, false, false);");
                sb.AppendLine("    }");
                sb.AppendLine("  }");
                sb.AppendLine("  for (var i = box1.options.length - 1; i >= 0; i--)");
                sb.AppendLine("  {");
                sb.AppendLine("    if (box1.options[i].selected)");
                sb.AppendLine("      box1.options[i] = null;");
                sb.AppendLine("  }");
                sb.AppendLine("}");

                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "SAV_MoveListItem", sb.ToString(), true);

                Attributes.Add("onclick", $"SAV_MoveListItem('{ClientID}', '{LinkedListBox.ClientID}');");
            }
        }
    }

    public class aaaa : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }
    }



    public class ListBoxConverter : ExpandableObjectConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value == null)
                return new ListBox();

            if (!(value is string s)) 
                return base.ConvertFrom(context, culture, value);
            
            return s.Length == 0 ? new ListBox() : base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value != null)
            {
                if (!(value is ListBox))
                {
                    throw new ArgumentException(
                        @"Invalid ListBox", "value");
                }
            }

            if (destinationType != typeof(string))
                return base.ConvertTo(context, culture, value, destinationType);

            return value == null ? string.Empty : ((ListBox)value).ID;
        }
    }
}
