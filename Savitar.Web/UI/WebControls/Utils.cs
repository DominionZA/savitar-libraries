namespace Savitar.Web.UI.WebControls
{
    public class Utils
    {
        public static void SelectValue(System.Web.UI.WebControls.DropDownList dropDownList, int value)
        {
            if (value <= 0)
                return;

            var item = dropDownList.Items.FindByValue(value.ToString());
            if (item == null) 
                return;

            dropDownList.ClearSelection();
            item.Selected = true;
        }

        public static void SelectValue(DropDownList dropDownList, int value)
        {
            if (value <= 0)
                return;

            var item = dropDownList.Items.FindByValue(value.ToString());
            if (item == null) 
                return;

            dropDownList.ClearSelection();
            item.Selected = true;
        }
    }
}