using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:PasswordTextBox runat=server></{0}:PasswordTextBox>")]
    public sealed class PasswordTextBox : System.Web.UI.WebControls.TextBox
    {
        public PasswordTextBox()
        {
            TextMode = TextBoxMode.Password;
        }

        public override string Text
        {
            get => base.Text;
            set
            {
                base.Text = value;

                Attributes[@"value"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            Attributes["value"] = Text;
        }
    }
}