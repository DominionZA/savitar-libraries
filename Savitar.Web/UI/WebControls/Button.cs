using System.Web.UI;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:Button runat=server></{0}:Button>")]
    public sealed class Button : System.Web.UI.WebControls.Button
    {
        public string CssClassOver { get; set; } = "btn btnhov";

        public Button()
        {
            CssClass = "btn";
            Text = ID;

            if (!string.IsNullOrEmpty(CssClassOver))
                Attributes.Add("onmouseover", $"className='{CssClassOver}'");
            if (!string.IsNullOrEmpty(CssClass))
                Attributes.Add("onmouseout", $"className='{CssClass}'");
        }
    }
}