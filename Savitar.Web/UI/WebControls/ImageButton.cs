using System;
using System.Web.Configuration;
using System.Web.UI;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:ImageButton runat=server></{0}:ImageButton>")]
    public class ImageButton : System.Web.UI.WebControls.ImageButton
    {
        public string AppSetting { get; set; } = "NoImageURL";

        protected override void OnPreRender(EventArgs e)
        {
            if (!string.IsNullOrEmpty(AppSetting))
            {
                if (WebConfigurationManager.AppSettings[AppSetting] != null)
                {
                    string filePath = WebConfigurationManager.AppSettings[AppSetting];
                    if ((string.IsNullOrEmpty(ImageUrl)) && (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(filePath))))
                        ImageUrl = filePath;
                }
            }

            base.OnPreRender(e);
        }
    }
}