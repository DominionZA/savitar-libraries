using System;
using System.Web.UI;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:TextBox runat=server></{0}:TextBox>")]
    public class TextBox : System.Web.UI.WebControls.TextBox
    {
        public enum TextBoxModeEx { Normal, Ordinal, Decimal, UpperCaseText, LowerCaseText }

        public bool StartFocused { get; set; } = false;

        public TextBoxModeEx TextModeEx { get; set; } = TextBoxModeEx.Normal;

        public int DecimalPlaces
        {
            get => decimalPlaces;
            set
            {
                decimalPlaces = value;

                if (decimalPlaces > 0)
                    TextModeEx = TextBoxModeEx.Decimal;
            }
        }

        private int decimalPlaces;

        public TextBox()
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (StartFocused)
            {
                var sb = new System.Text.StringBuilder();
                sb.AppendLine("var canisterSerialTextBox = document.getElementById('" + ClientID + "');");
                sb.AppendLine("if (canisterSerialTextBox != null)");
                sb.AppendLine("{");
                sb.AppendLine("  canisterSerialTextBox.focus();");
                //        sb.AppendLine("  document.getElementById('" + ClientID + "').focus();");
                //        sb.AppendLine("  alert('aaaaaaaa');");
                sb.AppendLine("}");
                Page.ClientScript.RegisterStartupScript(GetType(), "FocusCanisterTextbox", sb.ToString(), true);

                //Page.ClientScript.RegisterStartupScript(Page.GetType(), "SetFocus", "document.getElementById('" + ClientID + "').focus();", true);
            }

            if ((TextModeEx == TextBoxModeEx.Decimal) || (TextModeEx == TextBoxModeEx.Ordinal))
            {
                if (!Page.ClientScript.IsClientScriptBlockRegistered("TextBoxNumericFilter"))
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.AppendLine("function TextBoxNumericFilter(e)");
                    sb.AppendLine("{");
                    sb.AppendLine("  if (e.keyCode > 46 && e.keyCode < 58)");
                    sb.AppendLine("    return true;");
                    sb.AppendLine("  else");
                    sb.AppendLine("    return false; ");
                    sb.AppendLine("}");

                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "TextBoxNumericFilter", sb.ToString(), true);
                }
            }
            else if (TextModeEx == TextBoxModeEx.UpperCaseText)
            {
                Style.Add("text-transform", "uppercase");
            }
            else if (TextModeEx == TextBoxModeEx.LowerCaseText)
                Style.Add("text-transform", "lowercase");
        }
    }
}