using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Collections;

namespace Savitar.Web.UI.WebControls
{
  //  CompositeDataBoundControl
  [ToolboxData("<{0}:Marquee runat=server></{0}:Marquee>")]
  public class Marquee3 : System.Web.UI.WebControls.CompositeDataBoundControl, INamingContainer
  {        
    public MarqueeDirection Direction
    {
      get { return _Direction; }
      set { _Direction = value; }
    } MarqueeDirection _Direction = MarqueeDirection.Up;

    public int ScrollAmount
    {
      get { return _ScrollAmount; }
      set { _ScrollAmount = value; }
    } int _ScrollAmount = 1;

    public int ScrollDelay
    {
      get { return _ScrollDelay; }
      set { _ScrollDelay = value; }
    } int _ScrollDelay = 10;

    public enum MarqueeDirection { Up, Down, Left, Right }

    #region Databinding methods
    protected override void OnLoad(EventArgs e)
    {
      if (!Page.IsPostBack)        
        this.DataBind();

      base.OnLoad(e);
    }

    #endregion

    #region Viewstate Management    
    protected override object SaveViewState()
    {
      object baseState = base.SaveViewState();
      object itemState = null; // Topics.SaveViewState();

      if ((baseState == null) && (itemState == null))
        return null;

      return new Pair(baseState, itemState);
    }

    protected override void LoadViewState(object savedState)
    {
      if (savedState != null)
      {
        Pair p = (Pair)savedState;
        base.LoadViewState(p.First);
//        Topics.LoadViewState(p.Second);
      }
      else
        base.LoadViewState(savedState);
    }     
    #endregion    

    #region Methods
    public Marquee3()
    {
      this.Width = Unit.Pixel(100);
    }

    protected override void Render(HtmlTextWriter output)
    {
      output.WriteLine(String.Format("<marquee onmouseover=this.stop() onmouseout=this.start() scrollamount={0} scrolldelay={1} direction={2} width={3} height={4}>", this.ScrollAmount, this.ScrollDelay, this.Direction, this.Width, this.Height));
      base.Render(output);
      output.WriteLine("</marquee>");            
    }

    protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
    {
      if (dataSource == null)
      {
        if (_EmptyDataTemplate != null)
        {
          TemplateContainer container = new TemplateContainer(this);
          _EmptyDataTemplate.InstantiateIn(container);
          this.Controls.Add(container);
        }

        return 0;
      }

      int count = 0;

      if (_HeaderTemplate != null)
      {
        TemplateContainer container = new TemplateContainer(this);
        _HeaderTemplate.InstantiateIn(container);
        this.Controls.Add(container);
      }

      foreach (object dataItem in dataSource)
      {
        if (_ItemTemplate != null)
        {
          ItemContainer container = new ItemContainer(dataItem, count++);

          _ItemTemplate.InstantiateIn(container);
          this.Controls.Add(container);
          
          if (dataBinding)
            container.DataBind();
        }        
      }

      if (_FooterTemplate != null)
      {
        TemplateContainer container = new TemplateContainer(this);
        _FooterTemplate.InstantiateIn(container);
        this.Controls.Add(container);
      }

      return count;            
    }    
    #endregion

    #region Template Handlers

    private ITemplate _EmptyDataTemplate;
    private ITemplate _HeaderTemplate;
    private ITemplate _ItemTemplate;
    private ITemplate _FooterTemplate;

    [Browsable(false)]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateContainer(typeof(TemplateContainer))]
    public virtual ITemplate EmptyDataTemplate
    {
      get { return _EmptyDataTemplate; }
      set { _EmptyDataTemplate = value; }
    }

    [Browsable(false)]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateContainer(typeof(TemplateContainer))]
    public virtual ITemplate HeaderTemplate
    {
      get { return _HeaderTemplate; }
      set { _HeaderTemplate = value; }
    }

    [Browsable(false)]
    [PersistenceMode(PersistenceMode.InnerProperty)]    
    [TemplateContainer(typeof(ItemContainer))]
    public virtual ITemplate ItemTemplate
    {
      get { return _ItemTemplate; }
      set { _ItemTemplate = value; }
    }

    [Browsable(false)]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateContainer(typeof(TemplateContainer))]
    public virtual ITemplate FooterTemplate
    {
      get { return _FooterTemplate; }
      set { _FooterTemplate = value; }
    }
    #endregion

    public class TemplateContainer : WebControl, INamingContainer
    {
      public Marquee Marquee
      {
        get { return _Marquee; }
      } Marquee _Marquee;

      public TemplateContainer(Marquee marquee)
      {
        _Marquee = marquee;
      }
    }

    public class ItemContainer : Control, IDataItemContainer
    {
      public ItemContainer(object dataItem, int index)
      {
        _DataItem = dataItem;
        _DataItemIndex = _DisplayIndex = index;
      }

      public object DataItem
      {
        get { return _DataItem; }
      } object _DataItem;

      public int DataItemIndex
      {
        get { return _DataItemIndex; }
      } int _DataItemIndex;

      public int DisplayIndex
      {
        get { return _DisplayIndex; }
      } int _DisplayIndex;
    }
    
    public class ItemTemplateDesigner : System.Web.UI.Design.ControlDesigner
    {
      public override void Initialize(IComponent component)
      {
        base.Initialize(component);
        SetViewFlags(ViewFlags.TemplateEditing, true);
      }

      public override System.Web.UI.Design.TemplateGroupCollection TemplateGroups
      {
        get
        {
          TemplateGroupCollection collection = new TemplateGroupCollection();
          ItemContainer control = (ItemContainer)Component;
          TemplateGroup group = new TemplateGroup("Item");
          
          TemplateDefinition template = new TemplateDefinition(this, "ItemTemplate", control, "ItemTemplate", true);

          group.AddTemplateDefinition(template);
          collection.Add(group);
          return collection;
        }
      }
    }
  }  
}