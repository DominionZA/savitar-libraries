using System;
using System.ComponentModel;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;

namespace Savitar.Web.UI.WebControls
{
    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal), 
        AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal),
        Designer(typeof(VacationHomeDesigner)), DefaultProperty("Title"), 
        ToolboxData("<{0}:VacationHome runat=\"server\"> </{0}:VacationHome>")]
    public class VacationHome : CompositeControl
    {
        [Bindable(true), Category("Data"), DefaultValue(""), Description("Caption")]
        public virtual string Caption
        {
            get
            {
                var s = (string)ViewState["Caption"];
                return s ?? string.Empty;
            }
            set => ViewState["Caption"] = value;
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TemplateOwner Owner { get; private set; }

        [Browsable(false), PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(typeof(ITemplate), ""), Description("Control template"), TemplateContainer(typeof(VacationHome))]
        public virtual ITemplate Template { get; set; }

        [Bindable(true), Category("Data"), DefaultValue(""), Description("Title"), Localizable(true)]
        public virtual string Title
        {
            get
            {
                var s = (string)ViewState["Title"];
                return s ?? string.Empty;
            }
            set => ViewState["Title"] = value;
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();
            Owner = new TemplateOwner();

            var temp = Template ?? new DefaultTemplate();

            temp.InstantiateIn(Owner);
            Controls.Add(Owner);
        }

        public override void DataBind()
        {
            CreateChildControls();
            ChildControlsCreated = true;
            base.DataBind();
        }

    }

    [ToolboxItem(false)]
    public class TemplateOwner : WebControl
    {
    }

    #region DefaultTemplate
    sealed class DefaultTemplate : ITemplate
    {
        void ITemplate.InstantiateIn(Control owner)
        {
            var title = new Label();
            title.DataBinding += title_DataBinding;

            var lineBreak = new LiteralControl("<br/>");

            var caption = new Label();
            caption.DataBinding += caption_DataBinding;

            owner.Controls.Add(title);
            owner.Controls.Add(lineBreak);
            owner.Controls.Add(caption);

        }

        void caption_DataBinding(object sender, EventArgs e)
        {
            Label source = (Label)sender;
            VacationHome container =
                (VacationHome)(source.NamingContainer);
            source.Text = container.Caption;
        }

        void title_DataBinding(object sender, EventArgs e)
        {
            Label source = (Label)sender;
            VacationHome container =
                (VacationHome)(source.NamingContainer);
            source.Text = container.Title;
        }
    }
    #endregion


    public class VacationHomeDesigner : ControlDesigner
    {

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        public override string GetDesignTimeHtml()
        {
            return "<span>This is design-time HTML</span>";
        }

        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                var collection = new TemplateGroupCollection();

                var control = (VacationHome)Component;
                var group = new TemplateGroup("Item");
                var template = new TemplateDefinition(this, "Template", control, "Template", true);
                group.AddTemplateDefinition(template);
                collection.Add(group);
                return collection;
            }
        }
    }
}