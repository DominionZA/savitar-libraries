using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Savitar.Web.UI.WebControls
{
    /// <summary>
    /// Simple DropDownList that displays all the months in a year.
    /// </summary>
    [ToolboxData("<{0}:MonthDropDownList runat=server></{0}:MonthDropDownList>")]
    public class MonthDropDownList : DropDownList
    {
        public enum Month { Current = -2, BackOneFromCurrent, ForwardOneFromCurrent, January, February, March, April, May, June, July, August, September, October, November, December }

        public MonthDropDownList()
        {
        }

        [Bindable(true), Category("Settings"), DefaultValue("Current"), Description("The default month to select after binding.")]
        public Month DefaultMonth { get; set; } = Month.Current;

        protected override void OnLoad(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Items.Add(new ListItem("January", "1"));
                Items.Add(new ListItem("February", "2"));
                Items.Add(new ListItem("March", "3"));
                Items.Add(new ListItem("April", "4"));
                Items.Add(new ListItem("May", "5"));
                Items.Add(new ListItem("June", "6"));
                Items.Add(new ListItem("July", "7"));
                Items.Add(new ListItem("August", "8"));
                Items.Add(new ListItem("September", "9"));
                Items.Add(new ListItem("October", "10"));
                Items.Add(new ListItem("November", "11"));
                Items.Add(new ListItem("December", "12"));

                int monthIndex;

                switch (DefaultMonth)
                {
                    case Month.Current: monthIndex = DateTime.Now.Month; break;
                    case Month.BackOneFromCurrent:
                        {
                            if (DateTime.Now.Month == 1)
                                monthIndex = 12;
                            else
                                monthIndex = DateTime.Now.Month - 1;

                            break;
                        }
                    case Month.ForwardOneFromCurrent:
                        {
                            if (DateTime.Now.Month == 12)
                                monthIndex = 1;
                            else
                                monthIndex = DateTime.Now.Month + 1;

                            break;
                        }
                    default: monthIndex = (int)DefaultMonth; break;
                }

                SelectedValue = monthIndex.ToString();
            }

            base.OnLoad(e);
        }
    }
}