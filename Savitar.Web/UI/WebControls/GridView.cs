using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:GridView runat=server></{0}:GridView>")]
    public sealed class GridView : System.Web.UI.WebControls.GridView
    {
        public bool AutoRowSelection { get; set; } = true;

        public FormView LinkedFormView { get; set; } = null;

        public GridView()
        {
            Width = Unit.Percentage(100);
            AutoGenerateColumns = false;
            CssClass = "grid";
            SelectedRowStyle.CssClass = "gridSelectedRow";
            HeaderStyle.CssClass = "gridHeader";
            FooterStyle.CssClass = "gridFooter";
        }

        private void SetRowSelectScript(GridViewRow row, bool removeScript)
        {
            if (removeScript)
            {
                row.Attributes.Remove("onmouseover");
                row.Attributes.Remove("onmouseout");
                row.Attributes.Remove("onclick");
            }
            else
            {
                row.Attributes["onmouseover"] = "style.cursor='hand';style.textDecoration='underline';";
                row.Attributes["onmouseout"] = "style.textDecoration='none';";
            }
        }

        private void SetRowSelectScript(GridViewRow row)
        {
            SetRowSelectScript(row, false);
        }

        protected override void OnRowDataBound(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Build in the "Delete" prompt
                var deleteButton = e.Row.FindControl("DeleteButton");
                const string deletePrompt = "Are you sure you want to delete this record?";
                if (deleteButton != null)
                {
                    if (deleteButton.GetType() == typeof(LinkButton))
                        ((LinkButton)deleteButton).Attributes.Add("onclick", String.Format("return confirm('{0}');", deletePrompt));
                    else if (deleteButton.GetType() == typeof(Button))
                        ((Button)deleteButton).Attributes.Add("onclick", String.Format("return confirm('{0}');", deletePrompt));
                    else if (deleteButton.GetType() == typeof(ImageButton))
                        ((ImageButton)deleteButton).Attributes.Add("onclick", String.Format("return confirm('{0}');", deletePrompt));
                }

                if (AutoRowSelection)
                {
                    // Don't want to set the auto select row script if the row is in edit mode.
                    //          if ((e.Row.RowIndex != SelectedIndex) && (e.Row.RowIndex != EditIndex))
                    SetRowSelectScript(e.Row, false);
                }
            }

            base.OnRowDataBound(e);
        }

        protected override void OnPageIndexChanging(GridViewPageEventArgs e)
        {
            SelectedIndex = -1;
            base.OnPageIndexChanging(e);
        }

        protected override void OnRowDeleting(GridViewDeleteEventArgs e)
        {
            SelectedIndex = -1;
            base.OnRowDeleting(e);
        }

        protected override void OnRowDeleted(GridViewDeletedEventArgs e)
        {
            base.OnRowDeleted(e);

            e.ExceptionHandled = DoError("Deleting item", e.Exception);
        }

        protected override void OnRowEditing(GridViewEditEventArgs e)
        {
            SelectedIndex = -1;
            SetRowSelectScript(Rows[e.NewEditIndex], true);

            base.OnRowEditing(e);
        }

        protected override void OnRowCancelingEdit(GridViewCancelEditEventArgs e)
        {
            SetRowSelectScript(Rows[e.RowIndex], false);

            base.OnRowCancelingEdit(e);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            EditIndex = -1;
            base.OnSelectedIndexChanged(e);
        }

        protected override void OnDataBinding(EventArgs e)
        {
            int selectedIndex = SelectedIndex;

            base.OnDataBinding(e);

            SelectedIndex = selectedIndex;
        }

        private bool DoError(string action, Exception ex)
        {
            if (ex == null)
                return false;

            Web.Utils.Popup.ShowDialog(Page, ex.Message);

            return true;
        }

        public void ExportToExcel()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", $"attachment;filename={Page.Title}.xls");
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.ContentType = "application/vnd.xls";
            var stringWrite = new StringWriter();
            var htmlWrite = new HtmlTextWriter(stringWrite);
            RenderControl(htmlWrite);
            HttpContext.Current.Response.Write(stringWrite.ToString());
            HttpContext.Current.Response.End();
        }

        public void Export()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.xls", Page.Title));
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a table to contain the grid
                    Table table = new Table();

                    //  include the gridline settings
                    table.GridLines = GridLines;

                    //  add the header row to the table
                    if (HeaderRow != null)
                    {
                        PrepareControlForExport(HeaderRow);
                        table.Rows.Add(HeaderRow);
                    }

                    //  add each of the data rows to the table
                    foreach (GridViewRow row in Rows)
                    {
                        PrepareControlForExport(row);
                        table.Rows.Add(row);
                    }

                    //  add the footer row to the table
                    if (FooterRow != null)
                    {
                        PrepareControlForExport(FooterRow);
                        table.Rows.Add(FooterRow);
                    }

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);

                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        private void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                var current = control.Controls[i];
                if (current is LinkButton button)
                {
                    control.Controls.Remove(button);
                    control.Controls.AddAt(i, new LiteralControl(button.Text));
                }
                else if (current is ImageButton imageButton)
                {
                    control.Controls.Remove(imageButton);
                    control.Controls.AddAt(i, new LiteralControl(imageButton.AlternateText));
                }
                else if (current is HyperLink link)
                {
                    control.Controls.Remove(link);
                    control.Controls.AddAt(i, new LiteralControl(link.Text));
                }
                else if (current is DropDownList list)
                {
                    control.Controls.Remove(list);
                    control.Controls.AddAt(i, new LiteralControl(list.SelectedItem.Text));
                }
                else if (current is CheckBox box)
                {
                    control.Controls.Remove(box);
                    control.Controls.AddAt(i, new LiteralControl(box.Checked ? "True" : "False"));
                }

                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }
    }
}
