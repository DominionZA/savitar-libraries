using System;
using System.Web.UI;

namespace Savitar.Web.UI.WebControls
{
    [ToolboxData("<{0}:Marquee runat=server></{0}:Marquee>")]
    public sealed class Marquee : System.Web.UI.WebControls.DataList
    {
        public enum MarqueeScrollDirection { Up, Down, Left, Right }

        public MarqueeScrollDirection ScrollDirection { get; set; } = MarqueeScrollDirection.Left;

        public int ScrollAmount { get; set; } = 4;

        public int ScrollDelay { get; set; } = 8;

        public Marquee()
        {
            EditItemTemplate = null;

            Width = System.Web.UI.WebControls.Unit.Pixel(400);
            Height = System.Web.UI.WebControls.Unit.Pixel(100);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!ComponentModel.Component.IsInDesignMode)
            {
                //        writer.WriteLine(String.Format("<marquee onmouseover=stop() onmouseout=start() Loop=0 scrollamount={0} scrolldelay={1} direction={2} width={3} height={4}>", ScrollAmount, ScrollDelay, ScrollDirection, Width, Height));
                writer.WriteLine($"<marquee onmouseover=stop() onmouseout=start() Loop=0 scrollamount={ScrollAmount} scrolldelay={ScrollDelay} direction={ScrollDirection}>");
            }

            base.Render(writer);

            if (!ComponentModel.Component.IsInDesignMode)
            {
                writer.Write("</marquee>");
            }
        }
    }
}