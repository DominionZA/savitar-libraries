﻿using Microsoft.Reporting.WebForms;

namespace Savitar.Reporting
{
    public class ReportViewer
    {
        public static byte[] ToPdfBytes(Microsoft.Reporting.WebForms.ReportViewer rv)
        {
            string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>8.5in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>1in</MarginLeft>" +
                "  <MarginRight>1in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";

            //Render the report
            var renderedBytes = rv.LocalReport.Render(
                "PDF",
                deviceInfo,
                out _,
                out _,
                out _,
                out _,
                out _);

            return renderedBytes;
        }

        public static System.IO.MemoryStream ToPfdStream(Microsoft.Reporting.WebForms.ReportViewer rv)
        {
            var result = ToPdfBytes(rv);

            var memoryStream = new System.IO.MemoryStream(result);
            return memoryStream;
        }
    }
}