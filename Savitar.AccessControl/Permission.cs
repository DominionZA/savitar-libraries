﻿namespace Savitar.AccessControl
{
    public class Permission
    {
        public virtual int PermissionCode { get; set; }
        public virtual int PermissionID { get; set; }
        public virtual string PermissionName { get; set; }

        private bool canView;
        private bool canAdd;
        private bool canEdit;
        private bool canDelete;

        public virtual bool CanView
        {
            get => canView;
            set
            {
                if (canView)
                    return;
                canView = value;
            }
        }

        public virtual bool CanAdd
        {
            get => canAdd;
            set
            {
                if (canAdd)
                    return;
                canAdd = value;
            }
        }
        
        public virtual bool CanEdit
        {
            get => canEdit;
            set
            {
                if (canEdit)
                    return;
                canEdit = value;
            }
        }
        
        public virtual bool CanDelete
        {
            get => canDelete;
            set
            {
                if (canDelete)
                    return;
                canDelete = value;
            }
        }
        
        public void Reset()
        {
            canAdd = false;
            canEdit = false;
            canDelete = false;
            canView = false;
        }
    }
}