﻿using System;

namespace Savitar.AccessControl
{
    public static class Global
    {
        public static Guid LoggedInUserID { get; set; }
    }
}
