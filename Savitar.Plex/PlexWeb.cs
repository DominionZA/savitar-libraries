﻿using System.IO;
using System.Net;

namespace Savitar.Plex
{
    public class PlexWeb
    {
        private static string urlFormat = "http://{0}:{1}/system/players/{2}/{3}/{4}";

        public static HttpWebResponse GetResponse(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "text/xml";

            return (HttpWebResponse)request.GetResponse();
        }

        public static string GetData(string url)
        {
            HttpWebResponse response = GetResponse(url);
            if (response.StatusCode != HttpStatusCode.OK)
                return "";

            string result = "";
            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            {
                result = sr.ReadToEnd();                
                sr.Close();
            }

            return result;
        }
    }
}
