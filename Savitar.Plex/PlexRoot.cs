﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Savitar.Plex
{
    [Serializable, XmlType("MediaContainer")]    
    public class PlexRoot
    {
        [XmlAttribute("size")]
        public int Size { get; set; }
        [XmlAttribute("friendlyName")]
        public string FriendlyName { get; set; }
        [XmlAttribute("machineIdentifier")]
        public string MachineIdentifier { get; set; }
        [XmlAttribute("myPlex")]
        public int MyPlex { get; set; }
        [XmlAttribute("platform")]
        public string Platform { get; set; }
        [XmlAttribute("requestParametersInCookie")]
        public bool RequestParametersInCookie { get; set; }
        [XmlAttribute("transcoderActiveVideoSessions")]
        public int TranscoderActiveVideoSessions { get; set; }
        [XmlAttribute("transcoderVideoBitrates")]
        public string TranscoderVideoBitrates { get; set; }
        [XmlAttribute("transcoderVideoQualities")]
        public string TranscoderVideoQualities { get; set; }
        [XmlAttribute("transcoderVideoResolutions")]
        public string TranscoderVideoResolutions { get; set; }
        [XmlAttribute("updatedAt")]
        public string UpdatedAt { get; set; }
        [XmlAttribute("version")]
        public string Version { get; set; }
        [XmlAttribute("webkit")]
        public int Webkit { get; set; }             
    }    

    [Serializable, XmlType("MediaContainer")]    
    public class PlexAccount { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexChannel { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexClient { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexLibrary { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexLog { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexMusic { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexMyPlex { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexSearch { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexSecurity { }

    //[Serializable, XmlType("MediaContainer")]    
    //public class PlexServer { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexService { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexStatus { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexSystem { }

    [Serializable, XmlType("MediaContainer")]    
    public class PlexVideo { }
}
