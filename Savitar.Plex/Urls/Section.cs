﻿namespace Savitar.Plex.Urls
{
    public class Section
    {
        public SectionTypes Type { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}