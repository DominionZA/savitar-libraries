﻿using System.Collections.Generic;
using Savitar.Plex.Model;

namespace Savitar.Plex.Urls
{
    public enum SectionTypes { Unknown, Movie, TVShow, Music, Photo }

    public class Sections : UrlBase
    {
        public string SectionsUrl => $"{RootUrl}system/library/sections";

        public Sections(string host, int port) : base(host, port)
        {
            
        }

        public MediaContainer All
        {
            get
            {
                    var xml = "Web.PageContent.GetPageContent(SectionsUrl)";
                    // Now attempt to deserialize it into MediaContainer.
                    
                    var mediaContainer = (MediaContainer)Object.Deserialize(xml, typeof (MediaContainer));
                    
#if DEBUG
                var test = new MediaContainer
                {
                    Identifier = "com.plexapp.com",
                    NoHistory = 0,
                    ReplaceParent = 0,
                    Size = 5,
                    Directories = new List<Directory>
                    {
                        new Directory(), new Directory(), new Directory(), new Directory()
                    }
                };


                Object.Save(@"C:\Test.xml", test);
#endif

                    return mediaContainer;                
            }
        }
    }
}