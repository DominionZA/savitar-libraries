﻿using System;

namespace Savitar.Plex.Urls
{
    public abstract class UrlBase
    {
        public string Host { get; }
        public int Port { get; }

        public string RootUrl => $"http://{Host}:{Port}/";

        protected UrlBase(string host, int port)
        {
            if (string.IsNullOrEmpty(host))
                throw new ArgumentException(@"Host is mandatory", nameof(host));
            if (port <= 0)
                throw new ArgumentException(@"Port is mandatory", nameof(port));

            Host = host;
            Port = port;    
        }
    }
}
