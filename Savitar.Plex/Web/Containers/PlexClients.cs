using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Savitar.Plex.Web.Containers
{
    [XmlType("Server")]
    public class PlexClient
    {
        [XmlIgnore]
        public PlexServer Server { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public void ExecuteCommand(PlexTypes.Commands command)
        {
            if (Server == null)
                throw new Exception(GetType() + ".Server has not been set. Server is needed to execute commands");
            //Server.ExecuteCommand(this, command);
        }
    }

    [Serializable, XmlType("MediaContainer")]
    public class PlexClients : List<PlexClient>, IPlexMediaContainer
    {
        [XmlAttribute("size")]
        public int Size { get; set; }
    }
}