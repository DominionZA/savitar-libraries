namespace Savitar.Plex.Web.Containers
{
    public interface IPlexMediaContainer
    {
        int Size { get; set; }
    }
}