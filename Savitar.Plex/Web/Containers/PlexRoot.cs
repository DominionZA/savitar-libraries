using System;
using System.Xml.Serialization;

namespace Savitar.Plex.Web.Containers
{
    [Serializable, XmlType("MediaContainer")]
    public class PlexRoot : IPlexMediaContainer
    {
        [XmlAttribute("size")]
        public int Size { get; set; }

        [XmlAttribute("friendlyName")]
        public string FriendlyName { get; set; }
        [XmlAttribute("machineIdentifier")]
        public string MachineIdentifier { get; set; }
        [XmlAttribute("myPlex")]
        public int MyPlex { get; set; }
        [XmlAttribute("platform")]
        public string Platform { get; set; }
        [XmlAttribute("requestParametersInCookie")]
        public bool RequestParametersInCookie { get; set; }
        [XmlAttribute("transcoderActiveVideoSessions")]
        public int TranscoderActiveVideoSessions { get; set; }
        [XmlAttribute("transcoderVideoBitrates")]
        public string TranscoderVideoBitrates { get; set; }
        [XmlAttribute("transcoderVideoQualities")]
        public string TranscoderVideoQualities { get; set; }
        [XmlAttribute("transcoderVideoResolutions")]
        public string TranscoderVideoResolutions { get; set; }
        [XmlAttribute("updatedAt")]
        public string UpdatedAt { get; set; }
        [XmlAttribute("version")]
        public string Version { get; set; }
        [XmlAttribute("webkit")]
        public int Webkit { get; set; }        
    }
}