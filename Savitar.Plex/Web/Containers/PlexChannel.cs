using System;
using System.Xml.Serialization;

namespace Savitar.Plex.Web.Containers
{
    [Serializable, XmlType("MediaContainer")]    
    public class PlexChannel { }
}