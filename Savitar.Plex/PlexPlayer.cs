﻿namespace Savitar.Plex
{
    public class PlexPlayer
    {        
        public string Host { get; private set; }
        public string Port { get; private set; }
        public string Player { get; private set; }

        public PlexPlayer(string host, string port, string clientName)
        {
            Host = host;
            Port = port;
            Player = clientName;
        }

        public void ExecuteCommand(PlexTypes.Commands command)
        {
            //PlexCommand.ExecuteCommand(Host, Port, Player, command);
        }

        public void Play()
        {
            ExecuteCommand(PlexTypes.Commands.Play);
        }

        public void Pause()
        {
            ExecuteCommand(PlexTypes.Commands.Pause);
        }
    }
}
