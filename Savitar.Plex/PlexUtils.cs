﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Savitar.Plex.Web.Containers;

namespace Savitar.Plex
{
    public static class PlexUtils
    {
        public static T Deserialize<T>(string xml)
            where T : class 
        {
            XmlSerializer s = new XmlSerializer(typeof(T));

            byte[] buffer = Encoding.UTF8.GetBytes(xml);

            MemoryStream ms = new MemoryStream(buffer);
            XmlReader reader = new XmlTextReader(ms);

            try
            {
                object o = s.Deserialize(reader);
                return (T)o;
            }
            finally
            {
                reader.Close();
            }
        }

        public static string GetURL(string host)
        {
            return GetURL(host, "32400");
        }

        public static string GetURL(string host, string port)
        {
            return String.Format("http://{0}:{1}/", host, port);
        }

        public static TContainer GetContainer<TContainer>(string host)
            where TContainer : class, IPlexMediaContainer
        {
            return GetContainer<TContainer>(host, "32400");
        }

        public static TContainer GetContainer<TContainer>(string host, string port)
            where TContainer : class, IPlexMediaContainer
        {
            string url = GetURL(host, port);

            if (typeof(TContainer) == typeof(PlexClients))
            {
                url += "clients";
            }

            string data = PlexWeb.GetData(url);

            TContainer container = Deserialize<TContainer>(data);
            return container;
        }
    }
}
