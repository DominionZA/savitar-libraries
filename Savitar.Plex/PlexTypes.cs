﻿using System;
using System.Collections.Generic;

// http://wiki.plexapp.com/index.php/HTTP_Control_API
namespace Savitar.Plex
{
    public static class PlexTypes
    {
        public enum Controllers { Navigation, Playback }
        public enum Commands
        {
            // Navigation Commands
            MoveUp,
            MoveDown,
            MoveLeft,
            MoveRight,
            PageUp,
            PageDown,
            NextLetter,
            PreviousLetter,
            Select,
            Back,
            ContextMenu,
            ToggleOSD,
            // Playback Commands
            Play,
            Pause,
            Stop,
            Rewind,
            FastForward,
            StepForward,
            BigStepForward,
            StepBack,
            BigStepBack,
            SkipNext,
            SkipPrevious
        }

        public static List<Commands> GetCommands(Controllers controller)
        {
            List<Commands> result = new List<Commands>();
            foreach (Commands command in Enum.GetValues(typeof(Commands)))
            {
                if (controller == Controllers.Navigation && command >= Commands.MoveUp && command <= Commands.ToggleOSD)
                    result.Add(command);
                else if (controller == Controllers.Playback && command >= Commands.Play && command <= Commands.SkipPrevious)
                    result.Add(command);
            }

            return result;
        }
    }
}
