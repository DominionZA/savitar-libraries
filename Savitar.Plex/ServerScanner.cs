﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Savitar.Plex
{
    public class ServerScanner
    {
        public string ThisIP { get; private set; }

        public ServerScanner()
        {
            IPHostEntry ip = Dns.GetHostByName(Dns.GetHostName());
            ThisIP = ip.AddressList[0].ToString();
        }

        public void ScanNetwork()
        {
            Find();
        }

        protected void Find() // 
        {
            if (String.IsNullOrEmpty(ThisIP))
                throw new Exception(GetType() + ".ThisIP is empty");

            int lastDecimalIndex = ThisIP.LastIndexOf(".", StringComparison.Ordinal);
            string ipPrefix = ThisIP.Substring(0, lastDecimalIndex + 1);

            System.Diagnostics.Debug.WriteLine(ThisIP);
            for (int i = 1; i <= 255; i++)
            {
                try
                {
                    string address = ipPrefix + i;

                    var response = PlexWeb.GetResponse("http://" + address + ":32400");
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        //System.Diagnostics.Debug.WriteLine(ThisIP.Substring(0, lastT + 1) + i);
                        IPHostEntry he = Dns.GetHostByAddress(address);

                        string hostName = he.HostName;
                    }
                    
                }
                catch (SocketException)
                {
                    // in cazul unei erori
                }
                catch (Exception)
                {
                    // previne bloacarea programului
                }
            }
        }
    }
}