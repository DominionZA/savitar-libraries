﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Savitar.Plex.Model
{
    [Serializable]
    public class MediaContainer
    {
        [XmlAttribute("allowSync")]
        public int AllowSync { get; set; }
        [XmlAttribute("content")]
        public string Content { get; set; }
        [XmlAttribute("noHistory")]
        public int NoHistory { get; set; }
        [XmlAttribute("replaceParent")]
        public int ReplaceParent { get; set; }
        [XmlAttribute("size")]
        public int Size { get; set; }
        [XmlAttribute("identifier")]
        public string Identifier { get; set; }
        [XmlAttribute("mediaTagPrefix")]
        public string MediaTagPrefix { get; set; }
        [XmlAttribute("mediaTagVersion")]
        public int MediaTagVersion { get; set; }
        [XmlAttribute("thumb")]
        public string Thumb { get; set; }
        [XmlAttribute("title1")]
        public string Title1 { get; set; }
        [XmlAttribute("viewGroup")]
        public string ViewGroup { get; set; }
        [XmlAttribute("viewMode")]
        public string ViewMode { get; set; }


        [XmlElement("Directory")]
        public List<Directory> Directories { get; set; } 
        [XmlElement("Video")]
        public List<Video> Videos { get; set; } 
    }

    [Serializable]
    public class Video : IPlexItem
    {
        [XmlAttribute("ratingKey")]
        public long RatingKey { get; set; }
        [XmlAttribute("key")]
        public string Key { get; set; }
        [XmlAttribute("type")]
        public string Type { get; set; }
        [XmlAttribute("title")]
        public string Title { get; set; }
        [XmlAttribute("summary")]
        public string Summary { get; set; }
        [XmlAttribute("rating")]
        public float Rating { get; set; }
        [XmlAttribute("viewCount")]
        public long ViewCount { get; set; }
        [XmlAttribute("year")]
        public int Year { get; set; }
        [XmlAttribute("tagline")]
        public string Tagline { get; set; }
        [XmlAttribute("thumb")]
        public string Thumb { get; set; }
        [XmlAttribute("art")]
        public string Art { get; set; }
        [XmlAttribute("duration")]
        public long Duration { get; set; }
        [XmlAttribute("originallyAvailableAt")]
        public string OriginallyAvailableAt { get; set; }
        [XmlAttribute("addedAt")]
        public string AddedAt { get; set; }
        [XmlAttribute("updatedAt")]
        public string UpdatedAt { get; set; }
        [XmlElement("Media")]
        public Media Media { get; set; }

        public override string ToString()
        {
            var result = Title;

            if (Year != 0)
                result += " (" + Year + ")";
            return result;
        }
    }

    [Serializable]
    public class Media
    {
        [XmlAttribute("videoResolution")]
        public string VideoResolution { get; set; }

        [XmlAttribute("id")]
        public long Id { get; set; }

        [XmlAttribute("duration")]
        public long Duration { get; set; }

        [XmlAttribute("bitrate")]
        public long Bitrate { get; set; }

        [XmlAttribute("width")]
        public long Width { get; set; }

        [XmlAttribute("height")]
        public long Height { get; set; }

        [XmlAttribute("aspectRatio")]
        public string AspectRatio { get; set; }

        [XmlAttribute("audioChannels")]
        public long AudioChannels { get; set; }

        [XmlAttribute("audioCodec")]
        public string AudioCodec { get; set; }

        [XmlAttribute("videoCodec")]
        public string VideoCodec { get; set; }

        [XmlAttribute("container")]
        public string Container { get; set; }

        [XmlAttribute("videoFrameRate")]
        public string VideoFrameRate { get; set; }

        [XmlElement("Part")]
        public List<Part> Part { get; set; }
    }

    [Serializable]
    public class Part
    {
        [XmlAttribute("id")]
        public string Id { get; set; }
        [XmlAttribute("key")]
        public string Key { get; set; }
        [XmlAttribute("file")]
        public string File { get; set; }
        [XmlAttribute("size")]
        public string Size { get; set; }
    }

    [Serializable]
    public class Directory : IPlexItem
    {
        [XmlAttribute("key")]
        public string Key { get; set; }

        [XmlAttribute("art")]
        public string Art { get; set; }

        [XmlAttribute("unique")]
        public int Unique { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("title")]
        public string Title { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("serverVersion")]
        public string ServerVersion { get; set; }

        [XmlAttribute("uuid")]
        public Guid Uuid { get; set; }

        [XmlAttribute("host")]
        public string Host { get; set; }

        [XmlAttribute("serverName")]
        public string ServerName { get; set; }

        [XmlAttribute("path")]
        public string Path { get; set; }

        [XmlAttribute("machineIdentifier")]
        public string MachineIdentifier { get; set; }

        [XmlAttribute("local")]
        public int Local { get; set; }

        [XmlAttribute("port")]
        public int Port { get; set; }

        [XmlAttribute("thumb")]
        public string Thumb { get; set; }

        [XmlAttribute("summary")]
        public string Summary { get; set; }

        [XmlAttribute("banner")]
        public string Banner { get; set; }

        [XmlAttribute("theme")]
        public string Theme { get; set; }

        [XmlAttribute("duration")]
        public long Duration { get; set; }

        [XmlAttribute("originallyAvailableAt")]
        public string OriginallyAvailableAt { get; set; }

        [XmlAttribute("leafCount")]
        public int LeafCount { get; set; }

        [XmlAttribute("viewedLeafCount")]
        public int ViewedLeafCount { get; set; }

        [XmlAttribute("addedAt")]
        public string AddedAt { get; set; }

        [XmlAttribute("updatedAt")]
        public string UpdatedAt { get; set; }

        [XmlAttribute("secondary")]
        public int Secondary { get; set; }
        [XmlAttribute("prompt")]
        public string Prompt { get; set; }
        [XmlAttribute("search")]
        public int Search { get; set; }
        [XmlAttribute("year")]
        public int Year { get; set; }
        public override string ToString()
        {
            var result = Name;
            if (String.IsNullOrEmpty(result))
                result = Title;
            if (string.IsNullOrEmpty(result))
                result = base.ToString();

            if (Year != 0)
                result += " (" + Year + ")";

            return result;
        }
    }
}
