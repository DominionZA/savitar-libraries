﻿namespace Savitar.Plex.Model
{
    public interface IPlexItem
    {
        string Title { get; set; }
        string Summary { get; set; }
        int Year { get; set; }
    }
}