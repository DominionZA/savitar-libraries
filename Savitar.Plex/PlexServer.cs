﻿using System;
using System.Collections.Generic;
using System.Linq;
using Savitar.Plex.Model;
using Savitar.Plex.Urls;

namespace Savitar.Plex
{
    public class PlexServer
    {
        public string Host { get; }
        public int Port { get; }

        public string RootUrl => $"http://{Host}:{Port}";

        public PlexServer(string host) : this(host, 32400)
        { }
        public PlexServer(string host, int port)
        {
            if (string.IsNullOrEmpty(host))
                throw new ArgumentException(@"Host is mandatory", nameof(host));
            if (port <= 0)
                throw new ArgumentException(@"Port is mandatory", nameof(port));

            Host = host;
            Port = port;
        }

        public MediaContainer GetMediaContainer(string url)
        {
            var xml = "Web.PageContent.GetPageContent(url)";
            if (string.IsNullOrEmpty(xml))
                return null;

            var result = (MediaContainer)Object.Deserialize(xml, typeof (MediaContainer));

            return result;
        }

        public MediaContainer GetSections()
        {
            return GetMediaContainer($"{RootUrl}/system/library/sections");            
        }
        
        public MediaContainer GetSectionOptions(Directory section)
        {
            var url = $"{RootUrl}{section.Path}";
            return GetMediaContainer(url);
        }

        public MediaContainer GetSectionOptionList(Directory section, Directory sectionOption)
        {
            var url = $"{RootUrl}{section.Path}/{sectionOption.Key}";
            return GetMediaContainer(url);
        }

        public MediaContainer GetSectionOptionList(Section section, Directory sectionOption)
        {
            var url = $"{section.Path}/{sectionOption.Key}";
            return GetMediaContainer(url);
        }

        public IList<Section> GetLibrarySections()
        {
            var result = new List<Section>();

            var mediaContainer = GetSections();
            if (mediaContainer == null)
                return null;

            foreach (var directory in mediaContainer.Directories)
            {
                var section = new Section {Title = directory.Title, Path = RootUrl + directory.Path};

                switch (directory.Name)
                {
                    case "Movies" : section.Type = SectionTypes.Movie;                        
                        break;
                    case "TV Shows" : section.Type = SectionTypes.TVShow;
                        break;
                    case "Music" : section.Type = SectionTypes.Music;
                        break;
                    case "Photos" : section.Type = SectionTypes.Photo;
                        break;
                    default:
                        section.Type = SectionTypes.Unknown;
                        break;
                }

                result.Add(section);
            }

            return result.OrderBy(x => x.Title).ToList();
        }
    }
}
