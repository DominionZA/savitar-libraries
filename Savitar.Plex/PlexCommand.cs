﻿using System;
using System.Net;
using Savitar.Plex.Web.Containers;

namespace Savitar.Plex
{
    public static class PlexCommand
    {
        private static string urlFormat = "http://{0}:{1}/system/players/{2}/{3}/{4}";

        public static PlexTypes.Controllers GetController(PlexTypes.Commands command)
        {
            if (command >= PlexTypes.Commands.MoveUp && command <= PlexTypes.Commands.ToggleOSD)
                return PlexTypes.Controllers.Navigation;
            
            return PlexTypes.Controllers.Playback;            
        }

        public static string GetURL(string host, string port, PlexClient client, PlexTypes.Commands command)
        {
            var controller = GetController(command);
            
            return string.Format(urlFormat, host, port, "client.Name", controller, command);
        }

        public static bool ExecuteCommand(string url)
        {
            return PlexWeb.GetResponse(url).StatusCode == HttpStatusCode.OK;            
        }

        public static void ExecuteCommand(string host, string port, PlexClient client, PlexTypes.Commands command)
        {
            var url = GetURL(host, port, client, command);
            ExecuteCommand(url);
        }
    }
}
