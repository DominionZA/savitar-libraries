using System;
using System.Net;

namespace Savitar.Net.Proxy.Http
{

    ///<summary>Listens on a specific port on the proxy server and forwards all incoming HTTP traffic to the appropriate server.</summary>
    public sealed class HttpListener : Listener
    {
        ///<summary>Initializes a new instance of the HttpListener class.</summary>
        ///<param name="port">The port to listen on.</param>
        ///<remarks>The HttpListener will start listening on all installed network cards.</remarks>
        public HttpListener(int port) : this(IPAddress.Any, port) { }
        ///<summary>Initializes a new instance of the HttpListener class.</summary>
        ///<param name="port">The port to listen on.</param>
        ///<param name="address">The address to listen on. You can specify IPAddress.Any to listen on all installed network cards.</param>
        ///<remarks>For the security of your server, try to avoid to listen on every network card (IPAddress.Any). Listening on a local IP address is usually sufficient and much more secure.</remarks>
        public HttpListener(IPAddress address, int port) : base(port, address) { }
        ///<summary>Called when there's an incoming client connection waiting to be accepted.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        public override void OnAccept(IAsyncResult ar)
        {
            try
            {
                var newSocket = ListenSocket.EndAccept(ar);
                if (newSocket != null)
                {
                    var newClient = new HttpClient(newSocket, RemoveClient);
                    AddClient(newClient);
                    newClient.StartHandshake();
                }
            }
            catch
            {
                // ignored
            }

            try
            {
                //Restart Listening
                ListenSocket.BeginAccept(OnAccept, ListenSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Returns a string representation of this object.</summary>
        ///<returns>A string with information about this object.</returns>
        public override string ToString()
        {
            return "HTTP service on " + Address + ":" + Port;
        }
        ///<summary>Returns a string that holds all the construction information for this object.</summary>
        ///<value>A string that holds all the construction information for this object.</value>
        public override string ConstructString => "host:" + Address + ";int:" + Port;
    }
}