using System;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Collections.Specialized;
using System.Linq;

namespace Savitar.Net.Proxy.Http
{

    ///<summary>Relays HTTP data between a remote host and a local client.</summary>
    ///<remarks>This class supports both HTTP and HTTPS.</remarks>
    public sealed class HttpClient : Client
    {
        ///<summary>Initializes a new instance of the HttpClient class.</summary>
        ///<param name="clientSocket">The <see cref ="Socket">Socket</see> connection between this proxy server and the local client.</param>
        ///<param name="destroyer">The callback method to be called when this Client object disconnects from the local client and the remote server.</param>
        public HttpClient(Socket clientSocket, DestroyDelegate destroyer) : base(clientSocket, destroyer) { }
        ///<summary>Gets or sets a StringDictionary that stores the header fields.</summary>
        ///<value>A StringDictionary that stores the header fields.</value>
        private StringDictionary HeaderFields { get; set; }

        ///<summary>Gets or sets the HTTP version the client uses.</summary>
        ///<value>A string representing the requested HTTP version.</value>
        private string HttpVersion { get; set; } = "";

        ///<summary>Gets or sets the HTTP request type.</summary>
        ///<remarks>
        ///Usually, this string is set to one of the three following values:
        ///<list type="bullet">
        ///<item>GET</item>
        ///<item>POST</item>
        ///<item>CONNECT</item>
        ///</list>
        ///</remarks>
        ///<value>A string representing the HTTP request type.</value>
        private string HttpRequestType { get; set; } = "";

        ///<summary>Gets or sets the requested path.</summary>
        ///<value>A string representing the requested path.</value>
        public string RequestedPath { get; set; }

        ///<summary>Gets or sets the query string, received from the client.</summary>
        ///<value>A string representing the HTTP query string.</value>
        private string HttpQuery
        {
            get => httpQuery;
            set => httpQuery = value ?? throw new ArgumentNullException();
        }
        ///<summary>Starts receiving data from the client connection.</summary>
        public override void StartHandshake()
        {
            try
            {
                ClientSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, OnReceiveQuery, ClientSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Checks whether a specified string is a valid HTTP query string.</summary>
        ///<param name="query">The query to check.</param>
        ///<returns>True if the specified string is a valid HTTP query, false otherwise.</returns>
        private bool IsValidQuery(string query)
        {
            var index = query.IndexOf("\r\n\r\n", StringComparison.Ordinal);
            if (index == -1)
                return false;
            HeaderFields = ParseQuery(query);
            if (HttpRequestType.ToUpper().Equals("POST"))
            {
                try
                {
                    var length = int.Parse(HeaderFields["Content-Length"]);
                    return query.Length >= index + 6 + length;
                }
                catch
                {
                    SendBadRequest();
                    return true;
                }
            }
            
            return true;
        }
        ///<summary>Processes a specified query and connects to the requested HTTP web server.</summary>
        ///<param name="query">A string containing the query to process.</param>
        ///<remarks>If there's an error while processing the HTTP request or when connecting to the remote server, the Proxy sends a "400 - Bad Request" error to the client.</remarks>
        private void ProcessQuery(string query)
        {
            HeaderFields = ParseQuery(query);
            if (HeaderFields == null || !HeaderFields.ContainsKey("Host"))
            {
                SendBadRequest();
                return;
            }

            int port;
            string host;
            int ret;
            if (HttpRequestType.ToUpper().Equals("CONNECT"))
            { //HTTPS
                ret = RequestedPath.IndexOf(":", StringComparison.Ordinal);
                if (ret >= 0)
                {
                    host = RequestedPath.Substring(0, ret);
                    port = RequestedPath.Length > ret + 1 ? int.Parse(RequestedPath.Substring(ret + 1)) : 443;
                }
                else
                {
                    host = RequestedPath;
                    port = 443;
                }
            }
            else
            { //Normal HTTP
                ret = (HeaderFields["Host"]).IndexOf(":", StringComparison.Ordinal);
                if (ret > 0)
                {
                    host = (HeaderFields["Host"]).Substring(0, ret);
                    port = int.Parse((HeaderFields["Host"]).Substring(ret + 1));
                }
                else
                {
                    host = HeaderFields["Host"]; //???
                    port = 80;
                }
                if (HttpRequestType.ToUpper().Equals("POST"))
                {
                    var index = query.IndexOf("\r\n\r\n", StringComparison.Ordinal);
                    httpPost = query.Substring(index + 4);
                }
            }
            try
            {
                var destinationEndPoint = new IPEndPoint(Dns.Resolve(host).AddressList[0], port);
                DestinationSocket = new Socket(destinationEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                if (HeaderFields.ContainsKey("Proxy-Connection") && HeaderFields["Proxy-Connection"].ToLower().Equals("keep-alive"))
                    DestinationSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1);

                DestinationSocket.BeginConnect(destinationEndPoint, new AsyncCallback(this.OnConnected), DestinationSocket);
            }
            catch
            {
                SendBadRequest();
            }
        }
        ///<summary>Parses a specified HTTP query into its header fields.</summary>
        ///<param name="query">The HTTP query string to parse.</param>
        ///<returns>A StringDictionary object containing all the header fields with their data.</returns>
        ///<exception cref="ArgumentNullException">The specified query is null.</exception>
        private StringDictionary ParseQuery(string query)
        {
            var result = new StringDictionary();
            var lines = query.Replace("\r\n", "\n").Split('\n');
            int i, tmp;
            //Extract requested URL
            if (lines.Length > 0)
            {
                //Parse the Http Request Type
                tmp = lines[0].IndexOf(' ');
                if (tmp > 0)
                {
                    HttpRequestType = lines[0].Substring(0, tmp);
                    lines[0] = lines[0].Substring(tmp).Trim();
                }
                //Parse the Http Version and the Requested Path
                tmp = lines[0].LastIndexOf(' ');
                if (tmp > 0)
                {
                    HttpVersion = lines[0].Substring(tmp).Trim();
                    RequestedPath = lines[0].Substring(0, tmp);
                }
                else
                {
                    RequestedPath = lines[0];
                }

                //Remove http:// if present
                if (RequestedPath.Length >= 7 && RequestedPath.Substring(0, 7).ToLower().Equals("http://"))
                {
                    tmp = RequestedPath.IndexOf('/', 7);
                    RequestedPath = tmp == -1 ? "/" : RequestedPath.Substring(tmp);
                }
            }
            for (i = 1; i < lines.Length; i++)
            {
                tmp = lines[i].IndexOf(":", StringComparison.Ordinal);
                if (tmp <= 0 || tmp >= lines[i].Length - 1) 
                    continue;

                try
                {
                    result.Add(lines[i].Substring(0, tmp), lines[i].Substring(tmp + 1).Trim());
                }
                catch
                {
                    // ignored
                }
            }

            return result;
        }
        ///<summary>Sends a "400 - Bad Request" error to the client.</summary>
        private void SendBadRequest()
        {
            const string brs = "HTTP/1.1 400 Bad Request\r\nConnection: close\r\nContent-Type: text/html\r\n\r\n<html><head><title>400 Bad Request</title></head><body><div align=\"center\"><table border=\"0\" cellspacing=\"3\" cellpadding=\"3\" bgcolor=\"#C0C0C0\"><tr><td><table border=\"0\" width=\"500\" cellspacing=\"3\" cellpadding=\"3\"><tr><td bgcolor=\"#B2B2B2\"><p align=\"center\"><strong><font size=\"2\" face=\"Verdana\">400 Bad Request</font></strong></p></td></tr><tr><td bgcolor=\"#D1D1D1\"><font size=\"2\" face=\"Verdana\"> The proxy server could not understand the HTTP request!<br><br> Please contact your network administrator about this problem.</font></td></tr></table></center></td></tr></table></div></body></html>";
            try
            {
                ClientSocket.BeginSend(Encoding.ASCII.GetBytes(brs), 0, brs.Length, SocketFlags.None, this.OnErrorSent, ClientSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Rebuilds the HTTP query, starting from the HttpRequestType, RequestedPath, HttpVersion and HeaderFields properties.</summary>
        ///<returns>A string representing the rebuilt HTTP query string.</returns>
        private string RebuildQuery()
        {
            var result = HttpRequestType + " " + RequestedPath + " " + HttpVersion + "\r\n";
            if (HeaderFields == null) 
                return result;

            result = HeaderFields.Keys.Cast<string>()
                .Where(sc => sc.Length < 6 || !sc.Substring(0, 6).Equals("proxy-"))
                .Aggregate(result, (current, sc) => current + (System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(sc) + ": " + HeaderFields[sc] + "\r\n"));
            result += "\r\n";
            
            if (httpPost != null)
                result += httpPost;
            return result;
        }
        ///<summary>Returns text information about this HttpClient object.</summary>
        ///<returns>A string representing this HttpClient object.</returns>
        public override string ToString()
        {
            return ToString(false);
        }
        ///<summary>Returns text information about this HttpClient object.</summary>
        ///<returns>A string representing this HttpClient object.</returns>
        ///<param name="withUrl">Specifies whether or not to include information about the requested URL.</param>
        public string ToString(bool withUrl)
        {
            string result;
            try
            {
                if (DestinationSocket == null || DestinationSocket.RemoteEndPoint == null)
                    result = "Incoming HTTP connection from " + ((IPEndPoint)ClientSocket.RemoteEndPoint).Address;
                else
                    result = "HTTP connection from " + ((IPEndPoint)ClientSocket.RemoteEndPoint).Address + " to " + ((IPEndPoint)DestinationSocket.RemoteEndPoint).Address + " on port " + ((IPEndPoint)DestinationSocket.RemoteEndPoint).Port;
                if (HeaderFields != null && HeaderFields.ContainsKey("Host") && RequestedPath != null)
                    result += "\r\n" + " requested URL: http://" + HeaderFields["Host"] + RequestedPath;
            }
            catch
            {
                result = "HTTP Connection";
            }
            return result;
        }
        ///<summary>Called when we received some data from the client connection.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnReceiveQuery(IAsyncResult ar)
        {
            int result;
            try
            {
                result = ClientSocket.EndReceive(ar);
            }
            catch
            {
                result = -1;
            }
            if (result <= 0)
            { //Connection is dead :(
                Dispose();
                return;
            }

            HttpQuery += Encoding.ASCII.GetString(Buffer, 0, result);
            //if received data is valid HTTP request...
            if (IsValidQuery(HttpQuery))
            {
                ProcessQuery(HttpQuery);
            }
            else
            {
                try
                {
                    ClientSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, OnReceiveQuery, ClientSocket);
                }
                catch
                {
                    Dispose();
                }
            }
        }
        ///<summary>Called when the Bad Request error has been sent to the client.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnErrorSent(IAsyncResult ar)
        {
            try
            {
                ClientSocket.EndSend(ar);
            }
            catch
            {
                // ignored
            }

            Dispose();
        }
        ///<summary>Called when we're connected to the requested remote host.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnConnected(IAsyncResult ar)
        {
            try
            {
                DestinationSocket.EndConnect(ar);
                string rq;
                if (HttpRequestType.ToUpper().Equals("CONNECT"))
                { //HTTPS
                    rq = HttpVersion + " 200 Connection established\r\nProxy-Agent: Mentalis Proxy Server\r\n\r\n";
                    ClientSocket.BeginSend(Encoding.ASCII.GetBytes(rq), 0, rq.Length, SocketFlags.None, this.OnOkSent, ClientSocket);
                }
                else
                { //Normal HTTP
                    rq = RebuildQuery();
                    DestinationSocket.BeginSend(Encoding.ASCII.GetBytes(rq), 0, rq.Length, SocketFlags.None, this.OnQuerySent, DestinationSocket);
                }
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when the HTTP query has been sent to the remote host.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnQuerySent(IAsyncResult ar)
        {
            try
            {
                if (DestinationSocket.EndSend(ar) == -1)
                {
                    Dispose();
                    return;
                }
                StartRelay();
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when an OK reply has been sent to the local client.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnOkSent(IAsyncResult ar)
        {
            try
            {
                if (ClientSocket.EndSend(ar) == -1)
                {
                    Dispose();
                    return;
                }
                StartRelay();
            }
            catch
            {
                Dispose();
            }
        }
        // private variables
        /// <summary>Holds the value of the HttpQuery property.</summary>
        private string httpQuery = "";

        /// <summary>Holds the POST data</summary>
        private string httpPost;
    }
}