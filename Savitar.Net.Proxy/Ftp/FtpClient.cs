using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Savitar.Net.Proxy.Ftp
{

    ///<summary>Relays FTP commands between a remote host and a local client.</summary>
    ///<remarks>This class supports the 'OPEN' command, 'USER user@host:port' and 'USER user@host port'.</remarks>
    public sealed class FtpClient : Client
    {
        ///<summary>Initializes a new instance of the FtpClient class.</summary>
        ///<param name="clientSocket">The Socket connection between this proxy server and the local client.</param>
        ///<param name="destroyer">The callback method to be called when this Client object disconnects from the local client and the remote server.</param>
        public FtpClient(Socket clientSocket, DestroyDelegate destroyer) : base(clientSocket, destroyer) { }
        ///<summary>Sends a welcome message to the client.</summary>
        public override void StartHandshake()
        {
            try
            {
                var toSend = "220 savitar.co.za FTP proxy server ready.\r\n";
                ClientSocket.BeginSend(Encoding.ASCII.GetBytes(toSend), 0, toSend.Length, SocketFlags.None, OnHelloSent, ClientSocket);
            }
            catch
            {
                Dispose();
            }
        }

        ///<summary>Called when the welcome message has been sent to the client.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnHelloSent(IAsyncResult ar)
        {
            try
            {
                if (ClientSocket.EndSend(ar) <= 0)
                {
                    Dispose();
                    return;
                }

                ClientSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, OnReceiveCommand, ClientSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when we have received some bytes from the client.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnReceiveCommand(IAsyncResult ar)
        {
            try
            {
                var ret = ClientSocket.EndReceive(ar);
                if (ret <= 0)
                {
                    Dispose();
                    return;
                }
                FtpCommand += Encoding.ASCII.GetString(Buffer, 0, ret);
                if (FtpClient.IsValidCommand(FtpCommand))
                {
                    var ftpCommand = FtpCommand;
                    if (ProcessCommand(ftpCommand))
                        DestinationSocket.BeginSend(Encoding.ASCII.GetBytes(ftpCommand), 0, ftpCommand.Length, SocketFlags.None, OnCommandSent, DestinationSocket);

                    FtpCommand = "";
                }
                else
                {
                    ClientSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, this.OnReceiveCommand, ClientSocket);
                }
            }
            catch
            {
                Dispose();
            }
        }

        ///<summary>Processes an FTP command, sent from the client.</summary>
        ///<param name="command">The command to process.</param>
        ///<returns>True if the command may be sent to the server, false otherwise.</returns>
        private bool ProcessCommand(string command)
        {
            try
            {
                var ret = command.IndexOf(' ');
                if (ret < 0)
                    ret = command.Length;
                switch (command.Substring(0, ret).ToUpper().Trim())
                {
                    case "OPEN":
                        ConnectTo(ParseIPPort(command.Substring(ret + 1)));
                        break;
                    case "USER":
                        ret = command.IndexOf('@');
                        if (ret < 0)
                        {
                            return true;
                        }
                        else
                        {
                            User = command.Substring(0, ret).Trim() + "\r\n";
                            ConnectTo(ParseIPPort(command.Substring(ret + 1)));
                        }
                        break;
                    case "PORT":
                        ProcessPortCommand(command.Substring(5).Trim());
                        break;
                    case "PASV":
                        DataForward = new FtpDataConnection();
                        DataForward.ProcessPasv(this);
                        return true;
                    default:
                        return true;
                }
                return false;
            }
            catch
            {
                Dispose();
                return false;
            }
        }

        ///<summary>Processes a PORT command, sent from the client.</summary>
        ///<param name="input">The parameters of the PORT command.</param>
        private void ProcessPortCommand(string input)
        {
            try
            {
                var parts = input.Split(',');
                if (parts.Length != 6) 
                    return;

                DataForward = new FtpDataConnection();
                var processPort = DataForward.ProcessPort(new IPEndPoint(IPAddress.Parse(String.Join(".", parts, 0, 4)), int.Parse(parts[4]) * 256 + int.Parse(parts[5])));
                DestinationSocket.BeginSend(Encoding.ASCII.GetBytes(processPort), 0, processPort.Length, SocketFlags.None, this.OnCommandSent, DestinationSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Parses an IP address and port from a specified input string.</summary>
        ///<remarks>The input string is of the following form:<br>  <c>HOST:PORT</c></br><br><em>or</em></br><br>  <c>HOST PORT</c></br></remarks>
        ///<param name="input">The string to parse.</param>
        ///<returns>An instance of the IPEndPoint class if successful, null otherwise.</returns>
        private IPEndPoint ParseIPPort(string input)
        {
            input = input.Trim();
            var ret = input.IndexOf(':');
            if (ret < 0)
                ret = input.IndexOf(' ');
            try
            {
                return ret > 0 ? new IPEndPoint(Dns.Resolve(input.Substring(0, ret)).AddressList[0], int.Parse(input.Substring(ret + 1))) : new IPEndPoint(Dns.Resolve(input).AddressList[0], 21);
            }
            catch
            {
                return null;
            }
        }

        ///<summary>Connects to the specified endpoint.</summary>
        ///<param name="remoteServer">The IPEndPoint to connect to.</param>
        ///<exception cref="SocketException">There was an error connecting to the specified endpoint</exception>
        private void ConnectTo(IPEndPoint remoteServer)
        {
            if (DestinationSocket != null)
            {
                try
                {
                    DestinationSocket.Shutdown(SocketShutdown.Both);
                }
                catch
                {
                    // ignored
                }
                finally
                {
                    DestinationSocket.Close();
                }
            }
            try
            {
                DestinationSocket = new Socket(remoteServer.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                DestinationSocket.BeginConnect(remoteServer, OnRemoteConnected, DestinationSocket);
            }
            catch
            {
                throw new SocketException();
            }
        }

        ///<summary>Called when we're connected to the remote FTP server.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnRemoteConnected(IAsyncResult ar)
        {
            try
            {
                DestinationSocket.EndConnect(ar);
                ClientSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, OnReceiveCommand, ClientSocket);
                if (User.Equals(""))
                    DestinationSocket.BeginReceive(RemoteBuffer, 0, RemoteBuffer.Length, SocketFlags.None, OnReplyReceived, DestinationSocket);
                else
                    DestinationSocket.BeginReceive(RemoteBuffer, 0, RemoteBuffer.Length, SocketFlags.None, OnIgnoreReply, DestinationSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when we receive a reply from the FTP server that should be ignored.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnIgnoreReply(IAsyncResult ar)
        {
            try
            {
                var ret = DestinationSocket.EndReceive(ar);
                if (ret <= 0)
                {
                    Dispose();
                    return;
                }
                FtpReply += Encoding.ASCII.GetString(RemoteBuffer, 0, ret);
                if (FtpClient.IsValidReply(FtpReply))
                {
                    DestinationSocket.BeginReceive(RemoteBuffer, 0, RemoteBuffer.Length, SocketFlags.None, OnReplyReceived, DestinationSocket);
                    DestinationSocket.BeginSend(Encoding.ASCII.GetBytes(User), 0, User.Length, SocketFlags.None, OnCommandSent, DestinationSocket);
                }
                else
                {
                    DestinationSocket.BeginReceive(RemoteBuffer, 0, RemoteBuffer.Length, SocketFlags.None, OnIgnoreReply, DestinationSocket);
                }
            }
            catch
            {
                Dispose();
            }
        }

        ///<summary>Called when an FTP command has been successfully sent to the FTP server.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnCommandSent(IAsyncResult ar)
        {
            try
            {
                if (DestinationSocket.EndSend(ar) <= 0)
                {
                    Dispose();
                    return;
                }

                ClientSocket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, OnReceiveCommand, ClientSocket);
            }
            catch
            {
                Dispose();
            }
        }

        ///<summary>Called when we receive a reply from the FTP server.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnReplyReceived(IAsyncResult ar)
        {
            try
            {
                var ret = DestinationSocket.EndReceive(ar);
                if (ret <= 0)
                {
                    Dispose();
                    return;
                }
                if (DataForward != null && DataForward.ExpectsReply)
                {
                    if (!DataForward.ProcessPasvReplyRecv(Encoding.ASCII.GetString(RemoteBuffer, 0, ret)))
                        DestinationSocket.BeginReceive(RemoteBuffer, 0, RemoteBuffer.Length, SocketFlags.None, OnReplyReceived, DestinationSocket);
                }
                else
                {
                    ClientSocket.BeginSend(RemoteBuffer, 0, ret, SocketFlags.None, OnReplySent, ClientSocket);
                }
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when the reply from the FTP server has been sent to the local FTP client.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnReplySent(IAsyncResult ar)
        {
            try
            {
                var ret = ClientSocket.EndSend(ar);
                if (ret <= 0)
                {
                    Dispose();
                    return;
                }
                DestinationSocket.BeginReceive(RemoteBuffer, 0, RemoteBuffer.Length, SocketFlags.None, OnReplyReceived, DestinationSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Sends a string to the local FTP client.</summary>
        ///<param name="command">The result of the asynchronous operation.</param>
        internal void SendCommand(string command)
        {
            ClientSocket.BeginSend(Encoding.ASCII.GetBytes(command), 0, command.Length, SocketFlags.None, OnReplySent, ClientSocket);
        }

        ///<summary>Checks whether a specified command is a complete FTP command or not.</summary>
        ///<param name="command">A string containing the command to check.</param>
        ///<returns>True if the command is complete, false otherwise.</returns>
        internal static bool IsValidCommand(string command)
        {
            return (command.IndexOf("\r\n", StringComparison.Ordinal) >= 0);
        }

        ///<summary>Checks whether a specified reply is a complete FTP reply or not.</summary>
        ///<param name="input">A string containing the reply to check.</param>
        ///<returns>True is the reply is complete, false otherwise.</returns>
        internal static bool IsValidReply(string input)
        {
            var lines = input.Split('\n');
            try
            {
                if (lines[lines.Length - 2].Trim().Substring(3, 1).Equals(" "))
                    return true;
            }
            catch
            {
                // ignored
            }

            return false;
        }
        ///<summary>Gets or sets a property that can be used to store the received FTP command.</summary>
        ///<value>A string that can be used to store the received FTP command.</value>
        private string FtpCommand { get; set; } = "";

        ///<summary>Gets or sets a property that can be used to store the received FTP reply.</summary>
        ///<value>A string that can be used to store the received FTP reply.</value>
        private string FtpReply { get; set; } = "";

        ///<summary>Gets or sets a string containing the logged on username.</summary>
        ///<value>A string containing the logged on username.</value>
        private string User { get; set; } = "";

        ///<summary>Gets or sets the dataconnection object used to transmit files and other data from and to the FTP server.</summary>
        ///<value>An FtpDataConnection object that's used to transmit files and other data from and to the FTP server.</value>
        internal FtpDataConnection DataForward { get; set; }

        ///<summary>Returns text information about this FtpClient object.</summary>
        ///<returns>A string representing this FtpClient object.</returns>
        public override string ToString()
        {
            try
            {
                return "FTP connection from " + ((IPEndPoint)ClientSocket.RemoteEndPoint).Address + " to " + ((IPEndPoint)DestinationSocket.RemoteEndPoint).Address;
            }
            catch
            {
                return "Incoming FTP connection";
            }
        }
        // private variables
    }
}