using System;
using System.Net;
using System.Net.Sockets;

namespace Savitar.Net.Proxy.Ftp
{

    ///<summary>Relays FTP data between a remote host and a local client.</summary>
    internal sealed class FtpDataConnection : Client
    {
        ///<summary>Initializes a new instance of the FtpDataConnection class.</summary>
        ///<param name="remoteAddress">The address on the local FTP client to connect to.</param>
        ///<returns>The PORT command string to send to the FTP server.</returns>
        public string ProcessPort(IPEndPoint remoteAddress)
        {
            try
            {
                ListenSocket = new Socket(IPAddress.Any.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                ListenSocket.Bind(new IPEndPoint(IPAddress.Any, 0));
                ListenSocket.Listen(1);
                ListenSocket.BeginAccept(this.OnPortAccept, ListenSocket);
                ClientSocket = new Socket(remoteAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                ClientSocket.BeginConnect(remoteAddress, this.OnPortConnected, ClientSocket);
                return "PORT " + Listener.GetLocalExternalIP().ToString().Replace('.', ',') + "," + Math.Floor((decimal)(((IPEndPoint)ListenSocket.LocalEndPoint).Port / 256)) + "," + (((IPEndPoint)ListenSocket.LocalEndPoint).Port % 256) + "\r\n";
            }
            catch
            {
                Dispose();
                return "PORT 0,0,0,0,0,0\r\n";
            }
        }
        ///<summary>Called when we're connected to the data port on the local FTP client.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnPortConnected(IAsyncResult ar)
        {
            try
            {
                ClientSocket.EndConnect(ar);
                StartHandshake();
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when there's a connection from the remote FTP server waiting to be accepted.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnPortAccept(IAsyncResult ar)
        {
            try
            {
                DestinationSocket = ListenSocket.EndAccept(ar);
                ListenSocket.Close();
                ListenSocket = null;
                StartHandshake();
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Starts relaying data between the remote FTP server and the local FTP client.</summary>
        public override void StartHandshake()
        {
            if (DestinationSocket != null && ClientSocket != null && DestinationSocket.Connected && ClientSocket.Connected)
                StartRelay();
        }
        ///<summary>Gets or sets the Socket that's used to listen for incoming connections.</summary>
        ///<value>A Socket that's used to listen for incoming connections.</value>
        private Socket ListenSocket
        {
            get => listenSocket;
            set
            {
                listenSocket?.Close();
                listenSocket = value;
            }
        }
        ///<summary>Gets or sets the parent of this FtpDataConnection.</summary>
        ///<value>The FtpClient object that's the parent of this FtpDataConnection object.</value>
        private FtpClient Parent { get; set; }

        ///<summary>Gets or sets a string that stores the reply that has been sent from the remote FTP server.</summary>
        ///<value>A string that stores the reply that has been sent from the remote FTP server.</value>
        private string FtpReply { get; set; } = "";

        ///<summary>Gets or sets a boolean value that indicates whether the FtpDataConnection expects a reply from the remote FTP server or not.</summary>
        ///<value>A boolean value that indicates whether the FtpDataConnection expects a reply from the remote FTP server or not.</value>
        internal bool ExpectsReply { get; set; }

        ///<summary>Called when the proxy server processes a PASV command.</summary>
        ///<param name="parent">The parent FtpClient object.</param>
        public void ProcessPasv(FtpClient parent)
        {
            Parent = parent;
            ExpectsReply = true;
        }
        ///<summary>Called when the FtpClient receives a reply on the PASV command from the server.</summary>
        ///<param name="input">The received reply.</param>
        ///<returns>True if the input has been processed successfully, false otherwise.</returns>
        internal bool ProcessPasvReplyRecv(string input)
        {
            FtpReply += input;
            if (FtpClient.IsValidReply(FtpReply))
            {
                ExpectsReply = false;
                ProcessPasvReply(FtpReply);
                FtpReply = "";
                return true;
            }
            return false;
        }
        ///<summary>Processes a PASV reply from the server.</summary>
        ///<param name="reply">The reply to process.</param>
        private void ProcessPasvReply(string reply)
        {
            try
            {
                var connectTo = ParsePasvIP(reply);
                DestinationSocket = new Socket(connectTo.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                DestinationSocket.BeginConnect(connectTo, this.OnPasvConnected, DestinationSocket);
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Parses a PASV reply into an instance of the IPEndPoint class.</summary>
        ///<param name="reply">The reply to parse into an IPEndPoint.</param>
        ///<returns>An instance of the IPEndPoint class when successful, null otherwise.</returns>
        private IPEndPoint ParsePasvIP(string reply)
        {
            var startIndex = reply.IndexOf("(", StringComparison.Ordinal);
            if (startIndex == -1)
            {
                return null;
            }
            
            var stopIndex = reply.IndexOf(")", startIndex, StringComparison.Ordinal);
            if (stopIndex == -1)
                return null;
            
            var ipString = reply.Substring(startIndex + 1, stopIndex - startIndex - 1);
            
            var parts = ipString.Split(',');
            return parts.Length == 6
                ? new IPEndPoint(IPAddress.Parse(String.Join(".", parts, 0, 4)),
                    int.Parse(parts[4]) * 256 + int.Parse(parts[5]))
                : null;
        }
        ///<summary>Called when we're connected to the data port of the remote FTP server.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnPasvConnected(IAsyncResult ar)
        {
            try
            {
                DestinationSocket.EndConnect(ar);
                ListenSocket = new Socket(IPAddress.Any.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                ListenSocket.Bind(new IPEndPoint(IPAddress.Any, 0));
                ListenSocket.Listen(1);
                ListenSocket.BeginAccept(OnPasvAccept, ListenSocket);
                Parent.SendCommand("227 Entering Passive Mode (" +
                                   Listener.GetLocalInternalIP().ToString().Replace('.', ',') + "," +
                                   Math.Floor((decimal) (((IPEndPoint) ListenSocket.LocalEndPoint).Port / 256)) + "," +
                                   (((IPEndPoint) ListenSocket.LocalEndPoint).Port % 256).ToString() + ").\r\n");
            }
            catch
            {
                Dispose();
            }
        }
        ///<summary>Called when there's a connection from the local FTP client waiting to be accepted.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnPasvAccept(IAsyncResult ar)
        {
            try
            {
                ClientSocket = ListenSocket.EndAccept(ar);
                StartHandshake();
            }
            catch
            {
                Dispose();
            }
        }
        // private variables
        /// <summary>Holds the value of the ListenSocket property.</summary>
        private Socket listenSocket;
    }
}