using System;
using System.Net;
using System.Reflection;
using System.Collections;

namespace Savitar.Net.Proxy
{
    /// <summary>
    /// Defines the class that controls the settings and listener objects.
    /// </summary>
    public class Proxy
    {
        /// <summary>
        /// Initializes a new Proxy instance.
        /// </summary>
        /// <param name="file">The XML configuration file to use.</param>
        public Proxy(string file)
        {
            Config = new ProxyConfig(this, file);
        }
        /// <summary>
        /// Starts a new Proxy server by reading the data from the configuration file and start listening on the specified ports.
        /// </summary>
        public void Start()
        {
            // Initialize some objects
            StartTime = DateTime.Now;
            if (System.IO.File.Exists(Config.File))
                Config.LoadData();
        }
        /// <summary>
        /// Asks the user which listener to delete.
        /// </summary>
        protected void ShowDelListener()
        {
            Console.WriteLine("Please enter the ID of the listener you want to delete:\r\n (use the 'listlisteners' command to show all the listener IDs)");
            var id = Console.ReadLine();
            if (id == "")
                return;

            try
            {
                var le = new ListenEntry { Guid = new Guid(id ?? string.Empty) };
                if (!Listeners.Contains(le))
                {
                    Console.WriteLine("Specified ID not found in list!");
                    return;
                }

                this[Listeners.IndexOf(le)].Dispose();
                Listeners.Remove(le);
                Config.SaveData();
            }
            catch
            {
                Console.WriteLine("Invalid ID tag!");
                return;
            }

            Console.WriteLine("Listener removed from the list.");
        }
        /// <summary>
        /// Shows the Listeners list.
        /// </summary>
        protected void ShowListeners()
        {
            foreach (var listener in Listeners)
            {
                Console.WriteLine(((ListenEntry)listener).Listener.ToString());
                Console.WriteLine("  id: " + ((ListenEntry)listener).Guid.ToString("N"));
            }
        }
        /// <summary>
        /// Asks the user which listener to add.
        /// </summary>
        protected void ShowAddListener()
        {
            Console.WriteLine("Please enter the full class name of the Listener object you're trying to add:\r\n (ie. Savitar.Net.Proxy.Http.HttpListener)");
            var classType = Console.ReadLine();
            if (classType == "")
                return;

            if (Type.GetType(classType) == null)
            {
                Console.WriteLine("The specified class does not exist!");
                return;
            }

            Console.WriteLine("Please enter the construction parameters:");
            var construct = Console.ReadLine();
            var listenObject = CreateListener(classType, construct);
            if (listenObject == null)
            {
                Console.WriteLine("Invalid construction string.");
                return;
            }

            Listener listener;
            try
            {
                listener = listenObject;
            }
            catch
            {
                Console.WriteLine("The specified object is not a valid Listener object.");
                return;
            }
            try
            {
                listener.Start();
                AddListener(listener);
            }
            catch
            {
                Console.WriteLine("Error while staring the Listener.\r\n(Perhaps the specified port is already in use?)");
                return;
            }
            Config.SaveData();
        }
        /// <summary>
        /// Shows a list of commands in the console.
        /// </summary>
        protected void ShowHelp()
        {
            Console.WriteLine("          help - Shows this help message\r\n        uptime - Shows the uptime of the proxy server\r\n       version - Prints the version of this program\r\n     listusers - Lists all users\r\n       adduser - Adds a user to the user list\r\n       deluser - Deletes a user from the user list\r\n listlisteners - Lists all the listeners\r\n   addlistener - Adds a new listener\r\n   dellistener - Deletes a listener\r\n\r\n Read the readme.txt file for more help.");
        }
        /// <summary>
        /// Shows the uptime of this proxy server.
        /// </summary>
        protected void ShowUpTime()
        {
            var upTime = DateTime.Now.Subtract(StartTime);
            Console.WriteLine("Up " + upTime.ToString());
        }

        /// <summary>
        /// Shows the version number of this proxy server.
        /// </summary>
        protected void ShowVersion()
        {
            Console.WriteLine("This is version " + Assembly.GetCallingAssembly().GetName().Version.ToString(3) + " of the Mentalis.org proxy server.");
        }

        /// <summary>
        /// Asks the user which username to add.
        /// </summary>
        protected void ShowAddUser()
        {
            Console.Write("Please enter the username to add: ");
            var name = Console.ReadLine();
            if (Config.UserList.IsUserPresent(name))
            {
                Console.WriteLine("Username already exists in database.");
                return;
            }

            const string pass1 = "PASSWORD HERE";
            const string pass2 = "CONFIRM PASSWORD HERE";

            if (!pass1.Equals(pass2))
                throw new Exception("\r\nThe passwords do not match.");

            Config.SaveUserPass(name, pass1);
        }

        /// <summary>
        /// Asks the user which username to delete.
        /// </summary>
        protected void ShowDelUser()
        {
            Console.Write("Please enter the username to remove: ");
            var name = Console.ReadLine();
            if (!Config.UserList.IsUserPresent(name))
            {
                Console.WriteLine("Username not present in database.");
                return;
            }
            Config.RemoveUser(name);
            Console.WriteLine("User '" + name + "' successfully removed.");
        }
        /// <summary>
        /// Shows a list of usernames in the console.
        /// </summary>
        protected void ShowUsers()
        {
            if (Config.UserList == null || Config.UserList.Keys.Length == 0)
                throw new Exception("There are no users in the user list.");

            //???System.Windows.Forms.MessageBox.Show("The following " + Config.UserList.Keys.Length.ToString() + " users are allowed to use the SOCKS5 proxy:\n\n" +
            //???  String.Join(", ", Config.UserList.Keys));      
        }

        /// <summary>
        /// Stops the proxy server.
        /// </summary>
        /// <remarks>When this method is called, all listener and client objects will be disposed.</remarks>
        public void Stop()
        {
            // Stop listening and clear the Listener list
            for (var i = 0; i < ListenerCount; i++)
            {
                Console.WriteLine(this[i] + " stopped.");
                this[i].Dispose();
            }
            Listeners.Clear();
        }

        /// <summary>
        /// Adds a listener to the Listeners list.
        /// </summary>
        /// <param name="newItem">The new Listener to add.</param>
        public void AddListener(Listener newItem)
        {
            if (newItem == null)
                throw new ArgumentNullException();
            var le = new ListenEntry { Listener = newItem, Guid = Guid.NewGuid() };

            while (Listeners.Contains(le))
            {
                le.Guid = Guid.NewGuid();
            }
            Listeners.Add(le);
            Console.WriteLine(newItem + " started.");
        }

        /// <summary>
        /// Creates a new Listener obejct from a given listener name and a given listener parameter string.
        /// </summary>
        /// <param name="type">The type of object to instantiate.</param>
        /// <param name="cpars"></param>
        /// <returns></returns>
        public Listener CreateListener(string type, string cpars)
        {
            try
            {
                var parts = cpars.Split(';');
                var pars = new object[parts.Length];
                string oval = null;
                // Start instantiating the objects to give to the constructor
                for (int i = 0; i < parts.Length; i++)
                {
                    var ret = parts[i].IndexOf(':');
                    string oType;
                    if (ret >= 0)
                    {
                        oType = parts[i].Substring(0, ret);
                        oval = parts[i].Substring(ret + 1);
                    }
                    else
                        oType = parts[i];

                    switch (oType.ToLower())
                    {
                        case "int":
                            pars[i] = int.Parse(oval);
                            break;
                        case "host":
                            pars[i] = Dns.Resolve(oval).AddressList[0];
                            break;
                        case "authlist":
                            pars[i] = Config.UserList;
                            break;
                        case "null":
                            pars[i] = null;
                            break;
                        case "string":
                            pars[i] = oval;
                            break;
                        case "ip":
                            pars[i] = IPAddress.Parse(oval);
                            break;
                        default:
                            pars[i] = null;
                            break;
                    }
                }
                return (Listener)Activator.CreateInstance(Type.GetType(type), pars);
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the collection that contains all the Listener objects.
        /// </summary>
        /// <value>An ArrayList object that contains all the Listener objects.</value>
        protected ArrayList Listeners { get; } = new ArrayList();

        /// <summary>
        /// Gets the number of Listener objects.
        /// </summary>
        /// <value>An integer specifying the number of Listener objects.</value>
        internal int ListenerCount => Listeners.Count;

        /// <summary>
        /// Gets the Listener object at the specified position.
        /// </summary>
        /// <value>The Listener instance at position <c>index</c>.</value>
        internal virtual Listener this[int index] => ((ListenEntry)Listeners[index]).Listener;

        /// <summary>
        /// Gets or sets the date when this Proxy server was first started.
        /// </summary>
        /// <value>A DateTime structure that indicates when this Proxy server was first started.</value>
        protected DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the configuration object for this Proxy server.
        /// </summary>
        /// <value>A ProxyConfig instance that represents the configuration object for this Proxy server.</value>
        public ProxyConfig Config { get; set; }

        // private variables
    }
}