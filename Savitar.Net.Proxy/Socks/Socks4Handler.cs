using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Savitar.Net.Proxy.Socks
{

    ///<summary>Implements the SOCKS4 and SOCKS4a protocols.</summary>
    internal sealed class Socks4Handler : SocksHandler
    {
        ///<summary>Initializes a new instance of the Socks4Handler class.</summary>
        ///<param name="clientConnection">The connection with the client.</param>
        ///<param name="callback">The method to call when the SOCKS negotiation is complete.</param>
        ///<exception cref="ArgumentNullException"><c>Callback</c> is null.</exception>
        public Socks4Handler(Socket clientConnection, NegotiationCompleteDelegate callback) : base(clientConnection, callback) { }
        ///<summary>Checks whether a specific request is a valid SOCKS request or not.</summary>
        ///<param name="request">The request array to check.</param>
        ///<returns>True is the specified request is valid, false otherwise</returns>
        protected override bool IsValidRequest(byte[] request)
        {
            try
            {
                if (request[0] != 1 && request[0] != 2)
                { //CONNECT or BIND
                    Dispose(false);
                }
                else
                {
                    if (request[3] == 0 && request[4] == 0 && request[5] == 0 && request[6] != 0)
                    { //Use remote DNS
                        var result = Array.IndexOf(request, (byte)0, 7);
                        if (result > -1)
                            return Array.IndexOf(request, (byte)0, result + 1) != -1;
                    }
                    else
                    {
                        return Array.IndexOf(request, (byte)0, 7) != -1;
                    }
                }
            }
            catch
            {
                // ignored
            }

            return false;
        }
        ///<summary>Processes a SOCKS request from a client.</summary>
        ///<param name="request">The request to process.</param>
        protected override void ProcessRequest(byte[] request)
        {
            try
            {
                if (request[0] == 1)
                { // CONNECT
                    IPAddress remoteIp;
                    var remotePort = request[1] * 256 + request[2];
                    var ret = Array.IndexOf(request, (byte)0, 7);
                    Username = Encoding.ASCII.GetString(request, 7, ret - 7);
                    if (request[3] == 0 && request[4] == 0 && request[5] == 0 && request[6] != 0)
                    {// Use remote DNS
                        ret = Array.IndexOf(request, (byte)0, ret + 1);
                        remoteIp = Dns.GetHostEntry(Encoding.ASCII.GetString(request, Username.Length + 8, ret - Username.Length - 8)).AddressList[0];
                    }
                    else
                    { //Do not use remote DNS
                        remoteIp = IPAddress.Parse(request[3].ToString() + "." + request[4].ToString() + "." + request[5].ToString() + "." + request[6].ToString());
                    }
                    RemoteConnection = new Socket(remoteIp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    RemoteConnection.BeginConnect(new IPEndPoint(remoteIp, remotePort), OnConnected, RemoteConnection);
                }
                else if (request[0] == 2)
                { // BIND
                    var reply = new byte[8];
                    long localIp = Listener.GetLocalExternalIP().Address;
                    AcceptSocket = new Socket(IPAddress.Any.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    AcceptSocket.Bind(new IPEndPoint(IPAddress.Any, 0));
                    AcceptSocket.Listen(50);
                    RemoteBindIp = IPAddress.Parse(request[3].ToString() + "." + request[4].ToString() + "." + request[5].ToString() + "." + request[6].ToString());
                    reply[0] = 0;  //Reply version 0
                    reply[1] = 90;  //Everything is ok :)
                    reply[2] = (byte)(Math.Floor((decimal)(((IPEndPoint)AcceptSocket.LocalEndPoint).Port / 256)));  //Port/1
                    reply[3] = (byte)(((IPEndPoint)AcceptSocket.LocalEndPoint).Port % 256);  //Port/2
                    reply[4] = (byte)(Math.Floor((decimal)((localIp % 256))));  //IP Address/1
                    reply[5] = (byte)(Math.Floor((decimal)((localIp % 65536) / 256)));  //IP Address/2
                    reply[6] = (byte)(Math.Floor((decimal)((localIp % 16777216) / 65536)));  //IP Address/3
                    reply[7] = (byte)(Math.Floor((decimal)(localIp / 16777216)));  //IP Address/4
                    Connection.BeginSend(reply, 0, reply.Length, SocketFlags.None, OnStartAccept, Connection);
                }
            }
            catch
            {
                Dispose(91);
            }
        }
        ///<summary>Called when we're successfully connected to the remote host.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        private void OnConnected(IAsyncResult ar)
        {
            try
            {
                RemoteConnection.EndConnect(ar);
                Dispose(90);
            }
            catch
            {
                Dispose(91);
            }
        }
        ///<summary>Sends a reply to the client connection and disposes it afterwards.</summary>
        ///<param name="value">A byte that contains the reply code to send to the client.</param>
        protected override void Dispose(byte value)
        {
            byte[] toSend;
            try
            {
                toSend = new byte[]{0, value, (byte)(Math.Floor((decimal)(((IPEndPoint)RemoteConnection.RemoteEndPoint).Port / 256))),
                                           (byte)(((IPEndPoint)RemoteConnection.RemoteEndPoint).Port % 256),
                                           (byte)Math.Floor((decimal)(((IPEndPoint)RemoteConnection.RemoteEndPoint).Address.Address % 256)),
                                           (byte)Math.Floor((decimal)(((IPEndPoint)RemoteConnection.RemoteEndPoint).Address.Address % 65536 / 256)),
                                           (byte)Math.Floor((decimal)(((IPEndPoint)RemoteConnection.RemoteEndPoint).Address.Address % 16777216 / 65536)),
                                           (byte)Math.Floor((decimal)(((IPEndPoint)RemoteConnection.RemoteEndPoint).Address.Address / 16777216))};
            }
            catch
            {
                toSend = new byte[] { 0, 91, 0, 0, 0, 0, 0, 0 };
            }
            try
            {
                Connection.BeginSend(toSend, 0, toSend.Length, SocketFlags.None, toSend[1] == 90 ? OnDisposeGood : new AsyncCallback(OnDisposeBad), Connection);
            }
            catch
            {
                Dispose(false);
            }
        }
        ///<summary>Called when there's an incoming connection in the AcceptSocket queue.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        protected override void OnAccept(IAsyncResult ar)
        {
            try
            {
                RemoteConnection = AcceptSocket.EndAccept(ar);
                AcceptSocket.Close();
                AcceptSocket = null;
                Dispose(RemoteBindIp.Equals(((IPEndPoint)RemoteConnection.RemoteEndPoint).Address) ? (byte)90 : (byte)91);
            }
            catch
            {
                Dispose(91);
            }
        }
    }
}