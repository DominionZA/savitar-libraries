using System;
using System.Net;
using Savitar.Net.Proxy.Authentication;

namespace Savitar.Net.Proxy.Socks
{

    ///<remarks>This class also implements the SOCKS4a protocol.</remarks>
    public sealed class SocksListener : Listener
    {
        ///<summary>Initializes a new instance of the SocksListener class.</summary>
        ///<param name="port">The port to listen on.</param>
        ///<remarks>The SocksListener will listen on all available network cards and it will not use an AuthenticationList.</remarks>
        public SocksListener(int port) : this(IPAddress.Any, port, null) { }
        ///<summary>Initializes a new instance of the SocksListener class.</summary>
        ///<param name="port">The port to listen on.</param>
        ///<param name="address">The address to listen on. You can specify IPAddress.Any to listen on all installed network cards.</param>
        ///<remarks>For the security of your server, try to avoid to listen on every network card (IPAddress.Any). Listening on a local IP address is usually sufficient and much more secure.</remarks>
        ///<remarks>The SocksListener object will not use an AuthenticationList.</remarks>
        public SocksListener(IPAddress address, int port) : this(address, port, null) { }
        ///<summary>Initializes a new instance of the SocksListener class.</summary>
        ///<param name="port">The port to listen on.</param>
        ///<param name="authList">The list of valid login/password combinations. If you do not need password authentication, set this parameter to null.</param>
        ///<remarks>The SocksListener will listen on all available network cards.</remarks>
        public SocksListener(int port, AuthenticationList authList) : this(IPAddress.Any, port, authList) { }
        ///<summary>Initializes a new instance of the SocksListener class.</summary>
        ///<param name="port">The port to listen on.</param>
        ///<param name="address">The address to listen on. You can specify IPAddress.Any to listen on all installed network cards.</param>
        ///<param name="authList">The list of valid login/password combinations. If you do not need password authentication, set this parameter to null.</param>
        ///<remarks>For the security of your server, try to avoid to listen on every network card (IPAddress.Any). Listening on a local IP address is usually sufficient and much more secure.</remarks>
        public SocksListener(IPAddress address, int port, AuthenticationList authList)
          : base(port, address)
        {
            AuthList = authList;
        }

        ///<summary>Called when there's an incoming client connection waiting to be accepted.</summary>
        ///<param name="ar">The result of the asynchronous operation.</param>
        public override void OnAccept(IAsyncResult ar)
        {
            try
            {
                var newSocket = ListenSocket.EndAccept(ar);
                if (newSocket != null)
                {
                    var newClient = new SocksClient(newSocket, RemoveClient, AuthList);
                    AddClient(newClient);
                    newClient.StartHandshake();
                }
            }
            catch
            {
                // Ignored
            }

            try
            {
                //Restart Listening
                ListenSocket.BeginAccept(OnAccept, ListenSocket);
            }
            catch
            {
                Dispose();
            }
        }

        ///<summary>Gets or sets the AuthenticationList to be used when a SOCKS5 client connects.</summary>
        ///<value>An AuthenticationList that is to be used when a SOCKS5 client connects.</value>
        ///<remarks>This value can be null.</remarks>
        private AuthenticationList AuthList { get; }

        ///<summary>Returns a string representation of this object.</summary>
        ///<returns>A string with information about this object.</returns>
        public override string ToString()
        {
            return $"SOCKS service on {Address} : {Port}";
        }

        ///<summary>Returns a string that holds all the construction information for this object.</summary>
        ///<value>A string that holds all the construction information for this object.</value>
        public override string ConstructString
        {
            get
            {
                if (AuthList == null)
                    return "host:" + Address + ";int:" + Port + ";null";


                return "host:" + Address + ";int:" + Port + ";authlist";
            }
        }
    }
}