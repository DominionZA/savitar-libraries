using System;

namespace Savitar.Net.Proxy
{
    /// <summary>
    /// Represents an item in a Listeners collection.
    /// </summary>
    public struct ListenEntry
    {
        /// <summary>
        /// The Listener object.
        /// </summary>
        public Listener Listener;
        /// <summary>
        /// The Listener's ID. It must be unique trough out the Listeners collection.
        /// </summary>
        public Guid Guid;
        /// <summary>
        /// Determines whether the specified Object is equal to the current Object.
        /// </summary>
        /// <param name="obj">The Object to compare with the current Object.</param>
        /// <returns>True if the specified Object is equal to the current Object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return ((ListenEntry)obj).Guid.Equals(Guid);
        }
    }
}