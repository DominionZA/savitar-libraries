﻿using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Savitar.Infrastructure.Repository.EF
{
    public static class Migration
    {
        public static void Execute<TContext>(DbMigrationsConfiguration<TContext> configuration)
            where TContext : DbContext
        {
            var dbMigration = new DbMigrator(configuration);
            dbMigration.Update(null);
        }
    }
}
