﻿namespace Savitar.Domain.Contracts
{
    public interface IEntity : IBaseEntity
    {
        int Id { get; set; }
    }
}
