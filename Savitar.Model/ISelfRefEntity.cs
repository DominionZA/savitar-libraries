namespace Savitar.Model
{
    public interface ISelfRefEntity : IEntity
    {
        int? fkID { get; set; }
    }
}