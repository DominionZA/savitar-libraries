using System.Collections.Generic;
using System.Data.Objects.DataClasses;

namespace Savitar.Model
{
    public interface IEntities<T> : IList<T>
        where T : EntityObject
    { }        
}
