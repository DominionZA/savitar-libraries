using System;

namespace Savitar.Model
{
    public interface ILookupEntity : IEntity
    {       
        Guid ObjectId { get; set; }
    }
}