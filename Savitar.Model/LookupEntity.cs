﻿using System;

namespace Savitar.Model
{
    public class LookupEntity : ILookupEntity
    {
        public int Id { get; set; }
        public Guid ObjectId { get; set; }
        public string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
