namespace Savitar.Model
{
    public interface ISelfRefLookupEntity : ILookupEntity
    {
        int? fkId { get; set; }
    }
}