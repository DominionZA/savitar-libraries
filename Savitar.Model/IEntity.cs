namespace Savitar.Model
{
    public interface IEntity
    {
        int Id { get; set; }
        string Text { get; set; }
    }
}