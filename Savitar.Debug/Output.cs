﻿using System;
using System.Runtime.InteropServices;
using EnvDTE;

namespace Savitar.Debug
{
    public static class Output
    {
        // Only tested with Visual Studio 2012
        public static void Clear()
        {
            var dte = (DTE)Marshal.GetActiveObject("VisualStudio.DTE.11.0");
            System.Diagnostics.Debug.Assert(dte != null, "Unable to find active object 'VisualStudio.DTE.11.0'");
            dte.ExecuteCommand("Edit.ClearOutputWindow");
        }

        public static void WriteTime()
        {
            WriteTime("");
        }

        public static void WriteTime(string message)
        {
            System.Diagnostics.Debug.WriteLine(String.IsNullOrEmpty(message)
                                                   ? string.Format("{0:HH:mm:ss}", DateTime.Now)
                                                   : string.Format("{0:HH:mm:ss} : {1}", DateTime.Now, message));
        }

        public static void WriteTime(string message, params object[] args)
        {
            string msg = message;
            if (args != null)
                msg = String.Format(message, args);

            WriteTime(msg);
        }
    }
}
