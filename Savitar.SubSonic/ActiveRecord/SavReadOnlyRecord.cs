﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SubSonic;

namespace Savitar.SubSonic
{  
  /*
  public abstract class SavReadOnlyRecord<T> : ReadOnlyRecord<T>, ISavReadOnlyRecord where T : ReadOnlyRecord<T>, new()  
  {
    public void MarkDirty()
    {
      TableSchema.Table table = this.GetSchema();
      foreach (TableSchema.TableColumn col in table.Columns)
        base.DirtyColumns.Add(col);
    }

    [Bindable(false)]
    public virtual object ID
    {
      get { throw new NotImplementedException(this.GetType().ToString() + ".ID Getter has not been implemented"); }
    }

    [Bindable(true)]
    public virtual string Text
    {
      get { throw new NotImplementedException(this.GetType().ToString() + ".Text Getter has not been implemented"); }
      set { throw new NotImplementedException(this.GetType().ToString() + ".Text Setter has not been implemented"); }
    }

    [Bindable(true)]
    public virtual string Display
    {
      get { return this.ToString(); }
    }

    public virtual void Delete()
    {
      throw new NotImplementedException(this.GetType().ToString() + ".Delete() is not implemented. Please include an override in the Subsonic Template to create this method in the Generated class");
    }

    public virtual void Save()
    {
      throw new NotImplementedException(this.GetType().ToString() + ".Save() has not been implemented");
    }

    public virtual void Save(Guid userID)
    {
      this.Save(userID);
    }

    public virtual void Save(int userID)
    {
      this.Save();
    }

    public virtual void Save(string userName)
    {
      this.Save();
    }

    public new virtual object NewFromXML(string xml)
    {
      return base.NewFromXML(xml);
    }

    public override string ToString()
    {
      return "Override " + this.GetType().ToString() + ".ToString() to provide the default text for this class";
    }

		public new QueryCommand GetInsertCommand(string userName)
		{
			throw new NotImplementedException(this.GetType().ToString() + ".GetInsertCommand() has not been implemented");
		}

		public new void MarkNew()
		{
			base.MarkNew();
		}
  }
   */
}
