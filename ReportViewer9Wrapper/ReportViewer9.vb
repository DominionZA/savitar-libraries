Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports Gizmox.WebGUI.Forms

'''<summary>
'''Provides a Visual WebGui callable wrapper for Microsoft.Reporting.WebForms.ReportViewer.
''' 
''' Wrapped by Pall Bjornsson 30. jan 2009 - in VWG 6.2.3 NET3.5 VB.NET Wrapper
''' 
''' Changes needed in code:
''' - Reference project to Microsoft.ReportViewer.WebForms
''' - Public Sub Reset() is later called with parameter and gives error. Changed name to ResetShowParameterPrompts or just delete
''' - Definition of Dispose() must be changed to Overrides
''' - Declaration of the Wrapped class must be changed to public
''' 
'''</summary>
<System.ComponentModel.ToolboxItem(True)> _
Partial Public Class ReportViewer9
  Inherits Gizmox.WebGUI.Forms.Hosts.AspControlBoxBase
End Class
