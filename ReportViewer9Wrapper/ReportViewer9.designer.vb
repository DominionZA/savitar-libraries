Partial Public Class ReportViewer9

#Region "Fields"

    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

#End Region

#Region "Visaul WebGui Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    End Sub

#End Region

#Region "Properties"

    Protected Overrides ReadOnly Property IsFixedSize() As Boolean
        Get
      Return False
        End Get
    End Property
    ''' <summary>
    ''' Get the hosted control type
    ''' </summary>
    Protected Overrides ReadOnly Property HostedControlType() As System.Type
        Get
            Return GetType(Microsoft.Reporting.WebForms.ReportViewer)
        End Get
    End Property

    ''' <summary>
    ''' Get hosted control typed instance
    ''' </summary>
  Protected ReadOnly Property HostedReportViewer() As Microsoft.Reporting.WebForms.ReportViewer
    Get
      Return CType(Me.HostedControl, Microsoft.Reporting.WebForms.ReportViewer)
    End Get
  End Property


  ''' <summary>
  ''' Determines whether the document map is visible or collapsed.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines whether the document map is visible or collapsed.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property DocumentMapCollapsed() As System.Boolean
    Get
      Return CType(Me.GetProperty("DocumentMapCollapsed"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("DocumentMapCollapsed", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines whether the document map is visible or collapsed. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the DocumentMapCollapsed property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeDocumentMapCollapsed() As Boolean
    Return Me.ShouldSerialize("DocumentMapCollapsed")
  End Function


  ''' <summary>
  ''' Resets the Determines whether the document map is visible or collapsed. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the DocumentMapCollapsed property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetDocumentMapCollapsed()
    Me.Reset("DocumentMapCollapsed")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines the visibility of the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowToolBar() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowToolBar"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowToolBar", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowToolBar property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowToolBar() As Boolean
    Return Me.ShouldSerialize("ShowToolBar")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowToolBar property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowToolBar()
    Me.Reset("ShowToolBar")
  End Sub


  ''' <summary>
  ''' Determines whether parameter prompts should be displayed during remote processing.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines whether parameter prompts should be displayed during remote processing.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowParameterPrompts() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowParameterPrompts"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowParameterPrompts", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines whether parameter prompts should be displayed during remote processing. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowParameterPrompts property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowParameterPrompts() As Boolean
    Return Me.ShouldSerialize("ShowParameterPrompts")
  End Function


  ''' <summary>
  ''' Resets the Determines whether parameter prompts should be displayed during remote processing. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowParameterPrompts property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowParameterPrompts()
    Me.Reset("ShowParameterPrompts")
  End Sub


  ''' <summary>
  ''' Determines whether credential prompts should be displayed during remote processing.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines whether credential prompts should be displayed during remote processing.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowCredentialPrompts() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowCredentialPrompts"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowCredentialPrompts", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines whether credential prompts should be displayed during remote processing. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowCredentialPrompts property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowCredentialPrompts() As Boolean
    Return Me.ShouldSerialize("ShowCredentialPrompts")
  End Function


  ''' <summary>
  ''' Resets the Determines whether credential prompts should be displayed during remote processing. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowCredentialPrompts property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowCredentialPrompts()
    Me.Reset("ShowCredentialPrompts")
  End Sub


  ''' <summary>
  ''' Determines whether the prompt area is visible or collapsed.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines whether the prompt area is visible or collapsed.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property PromptAreaCollapsed() As System.Boolean
    Get
      Return CType(Me.GetProperty("PromptAreaCollapsed"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("PromptAreaCollapsed", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines whether the prompt area is visible or collapsed. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the PromptAreaCollapsed property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializePromptAreaCollapsed() As Boolean
    Return Me.ShouldSerialize("PromptAreaCollapsed")
  End Function


  ''' <summary>
  ''' Resets the Determines whether the prompt area is visible or collapsed. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the PromptAreaCollapsed property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetPromptAreaCollapsed()
    Me.Reset("PromptAreaCollapsed")
  End Sub


  ''' <summary>
  ''' Determines if the report content is rendered.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines if the report content is rendered.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowReportBody() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowReportBody"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowReportBody", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines if the report content is rendered. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowReportBody property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowReportBody() As Boolean
    Return Me.ShouldSerialize("ShowReportBody")
  End Function


  ''' <summary>
  ''' Resets the Determines if the report content is rendered. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowReportBody property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowReportBody()
    Me.Reset("ShowReportBody")
  End Sub


  ''' <summary>
  ''' Determines the width of the document map.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines the width of the document map.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property DocumentMapWidth() As System.Web.UI.WebControls.Unit
    Get
      Return CType(Me.GetProperty("DocumentMapWidth"), System.Web.UI.WebControls.Unit)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.Unit)
      Me.SetProperty("DocumentMapWidth", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the width of the document map. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the DocumentMapWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeDocumentMapWidth() As Boolean
    Return Me.ShouldSerialize("DocumentMapWidth")
  End Function


  ''' <summary>
  ''' Resets the Determines the width of the document map. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the DocumentMapWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetDocumentMapWidth()
    Me.Reset("DocumentMapWidth")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the document map button on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the document map button on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowDocumentMapButton() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowDocumentMapButton"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowDocumentMapButton", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the document map button on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowDocumentMapButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowDocumentMapButton() As Boolean
    Return Me.ShouldSerialize("ShowDocumentMapButton")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the document map button on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowDocumentMapButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowDocumentMapButton()
    Me.Reset("ShowDocumentMapButton")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the prompt area button on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the prompt area button on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowPromptAreaButton() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowPromptAreaButton"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowPromptAreaButton", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the prompt area button on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowPromptAreaButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowPromptAreaButton() As Boolean
    Return Me.ShouldSerialize("ShowPromptAreaButton")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the prompt area button on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowPromptAreaButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowPromptAreaButton()
    Me.Reset("ShowPromptAreaButton")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the page navigation controls on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the page navigation controls on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowPageNavigationControls() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowPageNavigationControls"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowPageNavigationControls", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the page navigation controls on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowPageNavigationControls property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowPageNavigationControls() As Boolean
    Return Me.ShouldSerialize("ShowPageNavigationControls")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the page navigation controls on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowPageNavigationControls property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowPageNavigationControls()
    Me.Reset("ShowPageNavigationControls")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the back button on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the back button on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowBackButton() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowBackButton"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowBackButton", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the back button on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowBackButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowBackButton() As Boolean
    Return Me.ShouldSerialize("ShowBackButton")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the back button on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowBackButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowBackButton()
    Me.Reset("ShowBackButton")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the refresh button on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the refresh button on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowRefreshButton() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowRefreshButton"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowRefreshButton", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the refresh button on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowRefreshButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowRefreshButton() As Boolean
    Return Me.ShouldSerialize("ShowRefreshButton")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the refresh button on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowRefreshButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowRefreshButton()
    Me.Reset("ShowRefreshButton")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the print, print layout, and page setup buttons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the print, print layout, and page setup buttons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowPrintButton() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowPrintButton"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowPrintButton", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the print, print layout, and page setup buttons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowPrintButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowPrintButton() As Boolean
    Return Me.ShouldSerialize("ShowPrintButton")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the print, print layout, and page setup buttons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowPrintButton property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowPrintButton()
    Me.Reset("ShowPrintButton")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the export button on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the export button on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowExportControls() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowExportControls"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowExportControls", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the export button on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowExportControls property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowExportControls() As Boolean
    Return Me.ShouldSerialize("ShowExportControls")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the export button on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowExportControls property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowExportControls()
    Me.Reset("ShowExportControls")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the zoom button on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the zoom button on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowZoomControl() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowZoomControl"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowZoomControl", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the zoom button on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowZoomControl property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowZoomControl() As Boolean
    Return Me.ShouldSerialize("ShowZoomControl")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the zoom button on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowZoomControl property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowZoomControl()
    Me.Reset("ShowZoomControl")
  End Sub


  ''' <summary>
  ''' Determines the visibility of the find controls on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("ToolBar")> _
  <System.ComponentModel.Description("Determines the visibility of the find controls on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ShowFindControls() As System.Boolean
    Get
      Return CType(Me.GetProperty("ShowFindControls"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("ShowFindControls", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the visibility of the find controls on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ShowFindControls property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeShowFindControls() As Boolean
    Return Me.ShouldSerialize("ShowFindControls")
  End Function


  ''' <summary>
  ''' Resets the Determines the visibility of the find controls on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ShowFindControls property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetShowFindControls()
    Me.Reset("ShowFindControls")
  End Sub


  ''' <summary>
  ''' Color of the background of the control.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Color of the background of the control.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Overrides Property BackColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("BackColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("BackColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Color of the background of the control. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the BackColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeBackColor() As Boolean
    Return Me.ShouldSerialize("BackColor")
  End Function


  ''' <summary>
  ''' Resets the Color of the background of the control. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the BackColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetBackColor()
    Me.Reset("BackColor")
  End Sub


  ''' <summary>
  ''' Determines the font of the wait message.
  ''' </summary>
  ''' <returns></returns>        
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines the font of the wait message.")> _
  <System.ComponentModel.Browsable(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public ReadOnly Property WaitMessageFont() As System.Web.UI.WebControls.FontInfo
    Get
      Return CType(Me.GetProperty("WaitMessageFont"), System.Web.UI.WebControls.FontInfo)
    End Get
  End Property


  ''' <summary>
  ''' The border style between major areas of the report viewer.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border style between major areas of the report viewer.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property InternalBorderStyle() As System.Web.UI.WebControls.BorderStyle
    Get
      Return CType(Me.GetProperty("InternalBorderStyle"), System.Web.UI.WebControls.BorderStyle)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
      Me.SetProperty("InternalBorderStyle", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border style between major areas of the report viewer. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the InternalBorderStyle property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeInternalBorderStyle() As Boolean
    Return Me.ShouldSerialize("InternalBorderStyle")
  End Function


  ''' <summary>
  ''' Resets the The border style between major areas of the report viewer. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the InternalBorderStyle property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetInternalBorderStyle()
    Me.Reset("InternalBorderStyle")
  End Sub


  ''' <summary>
  ''' The border color between major areas of the report viewer.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border color between major areas of the report viewer.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property InternalBorderColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("InternalBorderColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("InternalBorderColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border color between major areas of the report viewer. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the InternalBorderColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeInternalBorderColor() As Boolean
    Return Me.ShouldSerialize("InternalBorderColor")
  End Function


  ''' <summary>
  ''' Resets the The border color between major areas of the report viewer. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the InternalBorderColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetInternalBorderColor()
    Me.Reset("InternalBorderColor")
  End Sub


  ''' <summary>
  ''' The border width between major areas of the report viewer.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border width between major areas of the report viewer.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property InternalBorderWidth() As System.Web.UI.WebControls.Unit
    Get
      Return CType(Me.GetProperty("InternalBorderWidth"), System.Web.UI.WebControls.Unit)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.Unit)
      Me.SetProperty("InternalBorderWidth", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border width between major areas of the report viewer. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the InternalBorderWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeInternalBorderWidth() As Boolean
    Return Me.ShouldSerialize("InternalBorderWidth")
  End Function


  ''' <summary>
  ''' Resets the The border width between major areas of the report viewer. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the InternalBorderWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetInternalBorderWidth()
    Me.Reset("InternalBorderWidth")
  End Sub


  ''' <summary>
  ''' The border style of icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border style of icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemBorderStyle() As System.Web.UI.WebControls.BorderStyle
    Get
      Return CType(Me.GetProperty("ToolBarItemBorderStyle"), System.Web.UI.WebControls.BorderStyle)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
      Me.SetProperty("ToolBarItemBorderStyle", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border style of icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemBorderStyle property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemBorderStyle() As Boolean
    Return Me.ShouldSerialize("ToolBarItemBorderStyle")
  End Function


  ''' <summary>
  ''' Resets the The border style of icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemBorderStyle property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemBorderStyle()
    Me.Reset("ToolBarItemBorderStyle")
  End Sub


  ''' <summary>
  ''' The border color of icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border color of icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemBorderColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("ToolBarItemBorderColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("ToolBarItemBorderColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border color of icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemBorderColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemBorderColor() As Boolean
    Return Me.ShouldSerialize("ToolBarItemBorderColor")
  End Function


  ''' <summary>
  ''' Resets the The border color of icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemBorderColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemBorderColor()
    Me.Reset("ToolBarItemBorderColor")
  End Sub


  ''' <summary>
  ''' The border width of icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border width of icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemBorderWidth() As System.Web.UI.WebControls.Unit
    Get
      Return CType(Me.GetProperty("ToolBarItemBorderWidth"), System.Web.UI.WebControls.Unit)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.Unit)
      Me.SetProperty("ToolBarItemBorderWidth", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border width of icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemBorderWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemBorderWidth() As Boolean
    Return Me.ShouldSerialize("ToolBarItemBorderWidth")
  End Function


  ''' <summary>
  ''' Resets the The border width of icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemBorderWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemBorderWidth()
    Me.Reset("ToolBarItemBorderWidth")
  End Sub


  ''' <summary>
  ''' The border style of pressed icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border style of pressed icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemPressedBorderStyle() As System.Web.UI.WebControls.BorderStyle
    Get
      Return CType(Me.GetProperty("ToolBarItemPressedBorderStyle"), System.Web.UI.WebControls.BorderStyle)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
      Me.SetProperty("ToolBarItemPressedBorderStyle", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border style of pressed icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemPressedBorderStyle property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemPressedBorderStyle() As Boolean
    Return Me.ShouldSerialize("ToolBarItemPressedBorderStyle")
  End Function


  ''' <summary>
  ''' Resets the The border style of pressed icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemPressedBorderStyle property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemPressedBorderStyle()
    Me.Reset("ToolBarItemPressedBorderStyle")
  End Sub


  ''' <summary>
  ''' The border color of pressed icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border color of pressed icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemPressedBorderColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("ToolBarItemPressedBorderColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("ToolBarItemPressedBorderColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border color of pressed icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemPressedBorderColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemPressedBorderColor() As Boolean
    Return Me.ShouldSerialize("ToolBarItemPressedBorderColor")
  End Function


  ''' <summary>
  ''' Resets the The border color of pressed icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemPressedBorderColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemPressedBorderColor()
    Me.Reset("ToolBarItemPressedBorderColor")
  End Sub


  ''' <summary>
  ''' The border width of pressed icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The border width of pressed icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemPressedBorderWidth() As System.Web.UI.WebControls.Unit
    Get
      Return CType(Me.GetProperty("ToolBarItemPressedBorderWidth"), System.Web.UI.WebControls.Unit)
    End Get
    Set(ByVal value As System.Web.UI.WebControls.Unit)
      Me.SetProperty("ToolBarItemPressedBorderWidth", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The border width of pressed icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemPressedBorderWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemPressedBorderWidth() As Boolean
    Return Me.ShouldSerialize("ToolBarItemPressedBorderWidth")
  End Function


  ''' <summary>
  ''' Resets the The border width of pressed icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemPressedBorderWidth property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemPressedBorderWidth()
    Me.Reset("ToolBarItemPressedBorderWidth")
  End Sub


  ''' <summary>
  ''' The background color of icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The background color of icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemHoverBackColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("ToolBarItemHoverBackColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("ToolBarItemHoverBackColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The background color of icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemHoverBackColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemHoverBackColor() As Boolean
    Return Me.ShouldSerialize("ToolBarItemHoverBackColor")
  End Function


  ''' <summary>
  ''' Resets the The background color of icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemHoverBackColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemHoverBackColor()
    Me.Reset("ToolBarItemHoverBackColor")
  End Sub


  ''' <summary>
  ''' The background color of pressed icons on the toolbar.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The background color of pressed icons on the toolbar.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ToolBarItemPressedHoverBackColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("ToolBarItemPressedHoverBackColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("ToolBarItemPressedHoverBackColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The background color of pressed icons on the toolbar. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ToolBarItemPressedHoverBackColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeToolBarItemPressedHoverBackColor() As Boolean
    Return Me.ShouldSerialize("ToolBarItemPressedHoverBackColor")
  End Function


  ''' <summary>
  ''' Resets the The background color of pressed icons on the toolbar. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ToolBarItemPressedHoverBackColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetToolBarItemPressedHoverBackColor()
    Me.Reset("ToolBarItemPressedHoverBackColor")
  End Sub


  ''' <summary>
  ''' The color of disabled links in the toolbar and prompt areas.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The color of disabled links in the toolbar and prompt areas.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property LinkDisabledColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("LinkDisabledColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("LinkDisabledColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The color of disabled links in the toolbar and prompt areas. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the LinkDisabledColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeLinkDisabledColor() As Boolean
    Return Me.ShouldSerialize("LinkDisabledColor")
  End Function


  ''' <summary>
  ''' Resets the The color of disabled links in the toolbar and prompt areas. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the LinkDisabledColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetLinkDisabledColor()
    Me.Reset("LinkDisabledColor")
  End Sub


  ''' <summary>
  ''' The color of active links in the toolbar and prompt areas.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The color of active links in the toolbar and prompt areas.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property LinkActiveColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("LinkActiveColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("LinkActiveColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The color of active links in the toolbar and prompt areas. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the LinkActiveColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeLinkActiveColor() As Boolean
    Return Me.ShouldSerialize("LinkActiveColor")
  End Function


  ''' <summary>
  ''' Resets the The color of active links in the toolbar and prompt areas. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the LinkActiveColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetLinkActiveColor()
    Me.Reset("LinkActiveColor")
  End Sub


  ''' <summary>
  ''' The color of active links in the toolbar and prompt areas while the mouse is hovering over the text.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("The color of active links in the toolbar and prompt areas while the mouse is hovering over the text.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property LinkActiveHoverColor() As System.Drawing.Color
    Get
      Return CType(Me.GetProperty("LinkActiveHoverColor"), System.Drawing.Color)
    End Get
    Set(ByVal value As System.Drawing.Color)
      Me.SetProperty("LinkActiveHoverColor", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The color of active links in the toolbar and prompt areas while the mouse is hovering over the text. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the LinkActiveHoverColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeLinkActiveHoverColor() As Boolean
    Return Me.ShouldSerialize("LinkActiveHoverColor")
  End Function


  ''' <summary>
  ''' Resets the The color of active links in the toolbar and prompt areas while the mouse is hovering over the text. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the LinkActiveHoverColor property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetLinkActiveHoverColor()
    Me.Reset("LinkActiveHoverColor")
  End Sub


  ''' <summary>
  ''' Determines if the report area has a fixed size or is the size of the report content.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines if the report area has a fixed size or is the size of the report content.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property SizeToReportContent() As System.Boolean
    Get
      Return CType(Me.GetProperty("SizeToReportContent"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("SizeToReportContent", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines if the report area has a fixed size or is the size of the report content. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the SizeToReportContent property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeSizeToReportContent() As Boolean
    Return Me.ShouldSerialize("SizeToReportContent")
  End Function


  ''' <summary>
  ''' Resets the Determines if the report area has a fixed size or is the size of the report content. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the SizeToReportContent property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetSizeToReportContent()
    Me.Reset("SizeToReportContent")
  End Sub


  ''' <summary>
  ''' Determines whether report processing occurs locally or on a report server.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Determines whether report processing occurs locally or on a report server.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ProcessingMode() As Microsoft.Reporting.WebForms.ProcessingMode
    Get
      Return CType(Me.GetProperty("ProcessingMode"), Microsoft.Reporting.WebForms.ProcessingMode)
    End Get
    Set(ByVal value As Microsoft.Reporting.WebForms.ProcessingMode)
      Me.SetProperty("ProcessingMode", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines whether report processing occurs locally or on a report server. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ProcessingMode property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeProcessingMode() As Boolean
    Return Me.ShouldSerialize("ProcessingMode")
  End Function


  ''' <summary>
  ''' Resets the Determines whether report processing occurs locally or on a report server. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ProcessingMode property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetProcessingMode()
    Me.Reset("ProcessingMode")
  End Sub


  ''' <summary>
  ''' Remote processing properties.
  ''' </summary>
  ''' <returns></returns>        
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Remote processing properties.")> _
  <System.ComponentModel.Browsable(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public ReadOnly Property ServerReport() As Microsoft.Reporting.WebForms.ServerReport
    Get
      Return CType(Me.GetProperty("ServerReport"), Microsoft.Reporting.WebForms.ServerReport)
    End Get
  End Property


  ''' <summary>
  ''' Local processing properties.
  ''' </summary>
  ''' <returns></returns>        
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Local processing properties.")> _
  <System.ComponentModel.Browsable(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public ReadOnly Property LocalReport() As Microsoft.Reporting.WebForms.LocalReport
    Get
      Return CType(Me.GetProperty("LocalReport"), Microsoft.Reporting.WebForms.LocalReport)
    End Get
  End Property


  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("")> _
  <System.ComponentModel.Browsable(False)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)> _
  Public Property CurrentPage() As System.Int32
    Get
      Return CType(Me.GetProperty("CurrentPage"), System.Int32)
    End Get
    Set(ByVal value As System.Int32)
      Me.SetProperty("CurrentPage", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if  should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the CurrentPage property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeCurrentPage() As Boolean
    Return Me.ShouldSerialize("CurrentPage")
  End Function


  ''' <summary>
  ''' Resets the  property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the CurrentPage property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetCurrentPage()
    Me.Reset("CurrentPage")
  End Sub


  ''' <summary>
  ''' Determines the type of zoom applied to the report.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines the type of zoom applied to the report.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ZoomMode() As Microsoft.Reporting.WebForms.ZoomMode
    Get
      Return CType(Me.GetProperty("ZoomMode"), Microsoft.Reporting.WebForms.ZoomMode)
    End Get
    Set(ByVal value As Microsoft.Reporting.WebForms.ZoomMode)
      Me.SetProperty("ZoomMode", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the type of zoom applied to the report. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ZoomMode property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeZoomMode() As Boolean
    Return Me.ShouldSerialize("ZoomMode")
  End Function


  ''' <summary>
  ''' Resets the Determines the type of zoom applied to the report. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ZoomMode property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetZoomMode()
    Me.Reset("ZoomMode")
  End Sub


  ''' <summary>
  ''' Determines the percentage of zoom applied to the report when ZoomMode is set to Percent.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Appearance")> _
  <System.ComponentModel.Description("Determines the percentage of zoom applied to the report when ZoomMode is set to Percent.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ZoomPercent() As System.Int32
    Get
      Return CType(Me.GetProperty("ZoomPercent"), System.Int32)
    End Get
    Set(ByVal value As System.Int32)
      Me.SetProperty("ZoomPercent", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines the percentage of zoom applied to the report when ZoomMode is set to Percent. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ZoomPercent property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeZoomPercent() As Boolean
    Return Me.ShouldSerialize("ZoomPercent")
  End Function


  ''' <summary>
  ''' Resets the Determines the percentage of zoom applied to the report when ZoomMode is set to Percent. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ZoomPercent property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetZoomPercent()
    Me.Reset("ZoomPercent")
  End Sub


  ''' <summary>
  ''' Determines if the report is rendered asynchronously from the rest of the web page.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Determines if the report is rendered asynchronously from the rest of the web page.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property AsyncRendering() As System.Boolean
    Get
      Return CType(Me.GetProperty("AsyncRendering"), System.Boolean)
    End Get
    Set(ByVal value As System.Boolean)
      Me.SetProperty("AsyncRendering", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines if the report is rendered asynchronously from the rest of the web page. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the AsyncRendering property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeAsyncRendering() As Boolean
    Return Me.ShouldSerialize("AsyncRendering")
  End Function


  ''' <summary>
  ''' Resets the Determines if the report is rendered asynchronously from the rest of the web page. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the AsyncRendering property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetAsyncRendering()
    Me.Reset("AsyncRendering")
  End Sub


  ''' <summary>
  ''' The target window of the hyperlinks in the report.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("The target window of the hyperlinks in the report.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property HyperlinkTarget() As System.String
    Get
      Return CType(Me.GetProperty("HyperlinkTarget"), System.String)
    End Get
    Set(ByVal value As System.String)
      Me.SetProperty("HyperlinkTarget", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if The target window of the hyperlinks in the report. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the HyperlinkTarget property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeHyperlinkTarget() As Boolean
    Return Me.ShouldSerialize("HyperlinkTarget")
  End Function


  ''' <summary>
  ''' Resets the The target window of the hyperlinks in the report. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the HyperlinkTarget property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetHyperlinkTarget()
    Me.Reset("HyperlinkTarget")
  End Sub


  ''' <summary>
  ''' Determines which mime types are exported as attachments.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Determines which mime types are exported as attachments.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ExportContentDisposition() As Microsoft.Reporting.WebForms.ContentDisposition
    Get
      Return CType(Me.GetProperty("ExportContentDisposition"), Microsoft.Reporting.WebForms.ContentDisposition)
    End Get
    Set(ByVal value As Microsoft.Reporting.WebForms.ContentDisposition)
      Me.SetProperty("ExportContentDisposition", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Determines which mime types are exported as attachments. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ExportContentDisposition property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeExportContentDisposition() As Boolean
    Return Me.ShouldSerialize("ExportContentDisposition")
  End Function


  ''' <summary>
  ''' Resets the Determines which mime types are exported as attachments. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ExportContentDisposition property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetExportContentDisposition()
    Me.Reset("ExportContentDisposition")
  End Sub


  ''' <summary>
  ''' Programmatic name of the control.
  ''' </summary>
  ''' <returns></returns>    
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Programmatic name of the control.")> _
  <System.ComponentModel.Browsable(True)> _
  <System.ComponentModel.ReadOnly(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)> _
  Public Property ID() As System.String
    Get
      Return CType(Me.GetProperty("ID"), System.String)
    End Get
    Set(ByVal value As System.String)
      Me.SetProperty("ID", value)
    End Set
  End Property

  ''' <summary>
  ''' Returns a boolean indicating if Programmatic name of the control. should be serialized.
  ''' </summary>
  ''' <returns></returns> 
  ''' <remarks>This method is used in design time to determine which is to serialize the ID property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Function ShouldSerializeID() As Boolean
    Return Me.ShouldSerialize("ID")
  End Function


  ''' <summary>
  ''' Resets the Programmatic name of the control. property to its default value.
  ''' </summary>
  ''' <remarks>This method is used in design time to determine how to reset the ID property.</remarks> 
  <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
  Public Sub ResetID()
    Me.Reset("ID")
  End Sub


  ''' <summary>
  ''' The collection of child controls owned by the control.
  ''' </summary>
  ''' <returns></returns>        
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("The collection of child controls owned by the control.")> _
  <System.ComponentModel.Browsable(False)> _
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)> _
  Public ReadOnly Property Controls() As System.Web.UI.ControlCollection
    Get
      Return CType(Me.GetProperty("Controls"), System.Web.UI.ControlCollection)
    End Get
  End Property



#End Region

#Region "Methods"

  ''' <summary>
  ''' Clean up any resources being used.
  ''' </summary>
  ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)

    If disposing And Not (components Is Nothing) Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub



  ''' <summary>
  ''' 
  ''' </summary>
  Public Sub JumpToDocumentMapId(ByVal documentMapId As System.String)
    Me.HostedReportViewer.JumpToDocumentMapId(documentMapId)
  End Sub


  ''' <summary>
  ''' 
  ''' </summary>
  Public Sub PerformBack()
    Me.HostedReportViewer.PerformBack()
  End Sub


  ''' <summary>
  ''' 
  ''' </summary>
  Public Sub ResetHostedViewer()
    Me.HostedReportViewer.Reset()
  End Sub


  ''' <summary>
  ''' 
  ''' </summary>
  Public Overloads Sub Dispose()
    Me.HostedReportViewer.Dispose()
  End Sub


  ''' <summary>
  ''' 
  ''' </summary>
  Public Sub JumpToBookmark(ByVal bookmarkId As System.String)
    Me.HostedReportViewer.JumpToBookmark(bookmarkId)
  End Sub


  ''' <summary>
  ''' 
  ''' </summary>
  Public Sub DataBind()
    Me.HostedReportViewer.DataBind()
  End Sub


#End Region

#Region "Events"



  ''' <summary>
  ''' Occurs when the user changes the displayed page of the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user changes the displayed page of the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event PageNavigation As Microsoft.Reporting.WebForms.PageNavigationEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.PageNavigationEventHandler)
      Me.AddEventHandler("PageNavigation", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.PageNavigationEventHandler)
      Me.RemoveEventHandler("PageNavigation", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user navigates back to the parent report of a drillthrough report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user navigates back to the parent report of a drillthrough report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event Back As Microsoft.Reporting.WebForms.BackEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.BackEventHandler)
      Me.AddEventHandler("Back", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.BackEventHandler)
      Me.RemoveEventHandler("Back", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user navigates to a document map entry in the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user navigates to a document map entry in the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event DocumentMapNavigation As Microsoft.Reporting.WebForms.DocumentMapNavigationEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.DocumentMapNavigationEventHandler)
      Me.AddEventHandler("DocumentMapNavigation", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.DocumentMapNavigationEventHandler)
      Me.RemoveEventHandler("DocumentMapNavigation", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user navigates to a bookmark in the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user navigates to a bookmark in the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event BookmarkNavigation As Microsoft.Reporting.WebForms.BookmarkNavigationEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.BookmarkNavigationEventHandler)
      Me.AddEventHandler("BookmarkNavigation", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.BookmarkNavigationEventHandler)
      Me.RemoveEventHandler("BookmarkNavigation", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user toggles an item in the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user toggles an item in the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event Toggle As System.ComponentModel.CancelEventHandler

    AddHandler(ByVal value As System.ComponentModel.CancelEventHandler)
      Me.AddEventHandler("Toggle", value)
    End AddHandler

    RemoveHandler(ByVal value As System.ComponentModel.CancelEventHandler)
      Me.RemoveEventHandler("Toggle", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user performs a drillthrough to a new report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user performs a drillthrough to a new report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event Drillthrough As Microsoft.Reporting.WebForms.DrillthroughEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.DrillthroughEventHandler)
      Me.AddEventHandler("Drillthrough", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.DrillthroughEventHandler)
      Me.RemoveEventHandler("Drillthrough", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user performs a sort on the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user performs a sort on the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event Sort As Microsoft.Reporting.WebForms.SortEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.SortEventHandler)
      Me.AddEventHandler("Sort", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.SortEventHandler)
      Me.RemoveEventHandler("Sort", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user searches for text in the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user searches for text in the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event Search As Microsoft.Reporting.WebForms.SearchEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.SearchEventHandler)
      Me.AddEventHandler("Search", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.SearchEventHandler)
      Me.RemoveEventHandler("Search", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when the user refreshes the report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when the user refreshes the report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event ReportRefresh As System.ComponentModel.CancelEventHandler

    AddHandler(ByVal value As System.ComponentModel.CancelEventHandler)
      Me.AddEventHandler("ReportRefresh", value)
    End AddHandler

    RemoveHandler(ByVal value As System.ComponentModel.CancelEventHandler)
      Me.RemoveEventHandler("ReportRefresh", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



  ''' <summary>
  ''' Occurs when errors are encountered while rendering a report.
  ''' </summary>
  <System.ComponentModel.Category("Misc")> _
  <System.ComponentModel.Description("Occurs when errors are encountered while rendering a report.")> _
  <System.ComponentModel.Browsable(True)> _
  Public Custom Event ReportError As Microsoft.Reporting.WebForms.ReportErrorEventHandler

    AddHandler(ByVal value As Microsoft.Reporting.WebForms.ReportErrorEventHandler)
      Me.AddEventHandler("ReportError", value)
    End AddHandler

    RemoveHandler(ByVal value As Microsoft.Reporting.WebForms.ReportErrorEventHandler)
      Me.RemoveEventHandler("ReportError", value)
    End RemoveHandler

    RaiseEvent()
    End RaiseEvent

  End Event



#End Region

End Class
