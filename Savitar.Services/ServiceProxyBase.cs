﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

// Got this from http://philmunro.wordpress.com/2012/02/15/creating-a-wcf-service-proxy-with-channelfactory/
namespace Savitar.Services
{
    public abstract class ServiceProxyBase<T> : IDisposable where T : class
    {
        protected string ServiceEndpointUri { get; set; }
        private readonly object sync = new object();
        private IChannelFactory<T> channelFactory;
        private T channel;
        private bool disposed;

        protected ServiceProxyBase()
        {
        }

        protected ServiceProxyBase(string serviceEndPointUri)
        {
            ServiceEndpointUri = serviceEndPointUri;
        }

        protected virtual TimeSpan GetOpenTimeout()
        {
            return new TimeSpan(0, 1, 0);
        }

        protected virtual TimeSpan GetCloseTimeout()
        {
            return new TimeSpan(0, 1, 0);
        }

        protected virtual TimeSpan GetReceiveTimeout()
        {
            return new TimeSpan(0, 1, 0);
        }

        protected virtual TimeSpan GetSendTimeout()
        {
            return new TimeSpan(0, 1, 0);
        }

        protected T Channel
        {
            get
            {
                if (channel == null)
                {
                    lock (sync)
                    {
                        if (String.IsNullOrEmpty(ServiceEndpointUri))
                            throw new Exception(GetType() + ".ServiceEndPointUri is null");

                        var basicHttpBinding = new BasicHttpBinding();
                        basicHttpBinding.MaxBufferPoolSize = long.MaxValue;
                        basicHttpBinding.MaxBufferSize = int.MaxValue;
                        basicHttpBinding.MaxReceivedMessageSize = int.MaxValue;
                        basicHttpBinding.OpenTimeout = GetOpenTimeout();
                        basicHttpBinding.CloseTimeout = GetCloseTimeout();
                        basicHttpBinding.ReceiveTimeout = GetReceiveTimeout();
                        basicHttpBinding.SendTimeout = GetSendTimeout();

                        channelFactory = new ChannelFactory<T>(basicHttpBinding);
                        channel = channelFactory.CreateChannel(new EndpointAddress(ServiceEndpointUri));
                    }        
                }
                return channel;
            }
        }

        protected void CloseChannel()
        {
            if (channel != null)
            {
                ((ICommunicationObject)channel).Close();
            }
        }

        #region Disposable
        ~ServiceProxyBase()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposeManaged)
        {
            if (disposed)
                return;

            if (disposeManaged)
            {
                lock (sync)
                {
                    CloseChannel();

                    if (channelFactory != null)
                    {
                        ((IDisposable) channelFactory).Dispose();
                    }

                    channel = null;
                    channelFactory = null;
                }
            }

            disposed = true;
        }
        #endregion
    }
}
