﻿using System.ServiceModel;
using System.ServiceModel.Description;

namespace Savitar.Services
{
    public static class WCFClient
    {
        // Got info on manually setting up the client here http://social.msdn.microsoft.com/Forums/en/wcf/thread/c85f3ed2-0b55-4375-af79-5926b6cc527c (last post)
        // Also check the code for configuring the server.
        public static void Initialise<TChannel>(ClientBase<TChannel> service, string address)
            where TChannel : class
        {
            service.Endpoint.Address = new EndpointAddress(address);

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxBufferSize = int.MaxValue;
            binding.MaxBufferPoolSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            service.Endpoint.Binding = binding;

            foreach (OperationDescription operation in service.Endpoint.Contract.Operations)
            {
                foreach (var behavior in operation.Behaviors)
                {
                    try
                    {
                        ((dynamic)behavior).MaxItemsInObjectGraph = int.MaxValue;
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
        }
    }
}
