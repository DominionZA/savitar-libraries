using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Savitar.MWeb.PaymentGateways
{ 
  public class Safeshop
  {
    public enum SafeshopTransactionType { NotSet, Auth, Auth_Settle }
    public static SafeshopTransactionType StrToTransactionType(string transactionType)
    {
      switch (transactionType.ToLower())
      {
        case "auth": return SafeshopTransactionType.Auth;
        case "auth_settle": return SafeshopTransactionType.Auth_Settle;
        default: return SafeshopTransactionType.NotSet;
      }
    }

    protected static string PropToHTMLString(string propertyName, string propertyValue)
    {
      return String.Format("<INPUT TYPE=\"hidden\" NAME=\"{0}\" VALUE=\"{1}\"> ", propertyName, propertyValue);
    }

    public enum SafeshopCurrencyCode { NotSet, ZAR }

    public class Request
    {
      # region Request Properties
      /// <summary>
      /// The URL to post the data to to initiate the payment.
      /// </summary>
      public string URL = "https://secure.SafeShop.co.za/SafePay/Lite/Index.asp";
      /// <summary>
      /// A unique SafeKey is issued to each store. This SafeKey will be used to authenticate the Store. SafeKey resembles {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}. NOTE: You must also include the curly brackets around the SafeKey GUID.
      /// </summary>
      public Guid SafeKey = Guid.Empty;
      /// <summary>
      /// Unique reference number per-transaction for their own record, e.g. this can be achieved by concatenating merchant website name and the current date and time of the purchase like �Merchantwebiste_01-March-2001 2:220:22 PM�. If no reference is supplied, SafeShop� will issue "No_Merchant_RefNr_+ Current DateTime".
      /// </summary>
      public string MerchantReferenceNumber = "";
      /// <summary>
      /// The total amount in CENTS to be reserved against the buyer�s account. e.g.. the amount R12.99 must be written in cents as in 1299. Minimum amount is 1 cents and maximum amount is 99999999 cents. If the amount is omitted, non-numeric or negative amount, SafeShop� will issue an error.
      /// </summary>
      public Decimal TransactionAmount = 0;
      /// <summary>
      /// This option only applies to World Pay transactions. This indicates the currency for the transaction. The default value is ZAR (South African Rand).
      /// </summary>
      public SafeshopCurrencyCode CurrencyCode = SafeshopCurrencyCode.ZAR;
      /// <summary>
      /// SafeTrack represents a GUID that was passed from a portal. NOTE: You must also include the curly brackets.
      /// </summary>
      public Guid SafeTrack = Guid.Empty;
      /// <summary>
      /// The ReceiptURL allows SafeShop� Payment Manager to redirect the shopper when a transaction has been
      /// completed successfully. e.g. http://strawberry.safeshop.co.za/thankyou.asp. If no URL is supplied, SafeShop� Payment Manager will issue an error.
      /// </summary>
      public string ReceiptURL = "";
      /// <summary>
      /// The FailURL allows SafeShop� Payment Manager to redirect the shopper when transaction has failed. e.g. http://strawberry.safeshop.co.za/failed.asp
      /// </summary>
      public string FailURL = "";
      /// <summary>
      /// This indicates the type of transaction to execute on SafeShop� Payment Manager. In this instance the transaction type must be set to Auth. If the transaction type is omitted or an unknown transaction type is entered, SafeShop� Payment Manager will issue an error. 
      /// Transaction types include: 
      /// Auth (Funds is reserved) 
      /// Auth_Settle (Funds is transferred to the merchant account)
      /// (Only applicable to credit card payments)
      /// </summary>
      public SafeshopTransactionType TransactionType = SafeshopTransactionType.Auth_Settle;
      #endregion

      public void Send()
      {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Write(this.GetHTMLForm());
        HttpContext.Current.Response.Flush();
      }      

      private int RandsToCents(Decimal randValue)
      {
        return (int)(randValue * 100);
      }

      private Decimal CentsToRands(int cents)
      {
        return cents / 100;
      }

      public string GetHiddenInputs()
      {
        StringBuilder sb = new StringBuilder();

        // Now slot in each of the properties.
        sb.AppendLine(Safeshop.PropToHTMLString("SafeKey", "{" + String.Format("{0}", this.SafeKey.ToString().ToUpper()) + "}"));
        sb.AppendLine(Safeshop.PropToHTMLString("MerchantReferenceNumber", this.MerchantReferenceNumber));
        sb.AppendLine(Safeshop.PropToHTMLString("TransactionAmount", RandsToCents(this.TransactionAmount).ToString()));
        sb.AppendLine(Safeshop.PropToHTMLString("CurrencyCode", this.CurrencyCode.ToString()));
        sb.AppendLine(Safeshop.PropToHTMLString("SafeTrack", String.Format("{{0}}", this.SafeTrack.ToString().ToUpper())));
        sb.AppendLine(Safeshop.PropToHTMLString("ReceiptURL", this.ReceiptURL));
        sb.AppendLine(Safeshop.PropToHTMLString("FailURL", this.FailURL));
        sb.AppendLine(Safeshop.PropToHTMLString("TransactionType", this.TransactionType.ToString()));

        //throw new Exception(Safeshop.PropToHTMLString("TransactionAmount", RandsToCents(this.TransactionAmount).ToString()));

        return sb.ToString();
      }

      private string GetHTMLForm()
      { 
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<html>");
        sb.AppendLine("<title>Safeshop Gateway</title>");
        sb.AppendLine("</head>");
        sb.AppendLine("<body onload=\"frmPay.submit();\">");
        sb.AppendLine("<FORM name=frmPay ACTION=\"" + URL + "\" METHOD=\"POST\">");       
        
        sb.Append(this.GetHiddenInputs());        

        sb.AppendLine("</FORM>");
        sb.AppendLine("</body>");
        sb.AppendLine("</html>");

        return sb.ToString();
      }
    }

    public class Response
    {
      public enum SafeshopTransactionResult { Unknown, Successful, Failed, StageOrder }
      public static SafeshopTransactionResult StrToTransactionResult(string transactionResult)
      {
        switch (transactionResult.ToLower())
        {
          case "successful": return SafeshopTransactionResult.Successful;
          case "failed": return SafeshopTransactionResult.Failed;
          case "stageorder": return SafeshopTransactionResult.StageOrder;
          default: return SafeshopTransactionResult.Unknown;
        }
      }

      public Response(System.Collections.Specialized.NameValueCollection postedForm)
      {
        this.LogRefNr = String.IsNullOrEmpty(postedForm.Get("LogRefNr")) ? 0 : Convert.ToInt32(postedForm.Get("LogRefNr"));
        this.MerchantReference = String.IsNullOrEmpty(postedForm.Get("MerchantReference")) ? "" : postedForm.Get("MerchantReference");
        this.TransactionAmount = String.IsNullOrEmpty(postedForm.Get("TransactionAmount")) ? 0 : Convert.ToDecimal(Convert.ToInt32(postedForm.Get("TransactionAmount")) / 100);
        this.TransactionType = String.IsNullOrEmpty(postedForm.Get("TransactionType")) ? SafeshopTransactionType.NotSet : Safeshop.StrToTransactionType(postedForm.Get("TransactionType"));
        this.SafePayRefNr = String.IsNullOrEmpty(postedForm.Get("SafePayRefNr")) ? "" : postedForm.Get("SafePayRefNr");
        this.BankRefNr = String.IsNullOrEmpty(postedForm.Get("BankRefNr")) ? "" : postedForm.Get("BankRefNr");
        this.TransactionErrorResponse = String.IsNullOrEmpty(postedForm.Get("TransactionErrorResponse")) ? "" : postedForm.Get("TransactionErrorResponse");
        this.TransactionResult = String.IsNullOrEmpty(postedForm.Get("TransactionResult")) ? SafeshopTransactionResult.Unknown : Response.StrToTransactionResult(postedForm.Get("TransactionResult"));
        this.LiveTransaction = String.IsNullOrEmpty(postedForm.Get("LiveTransaction")) ? true : Convert.ToBoolean(postedForm.Get("LiveTransaction"));
        this.SafeTrack = String.IsNullOrEmpty(postedForm.Get("SafeTrack")) ? Guid.Empty : new Guid(postedForm.Get("SafeTrack"));
        this.ReceiptURL = String.IsNullOrEmpty(postedForm.Get("ReceiptURL")) ? "" : postedForm.Get("ReceiptURL");
        this.BuyerCreditCardNr = String.IsNullOrEmpty(postedForm.Get("BuyerCreditCardNr")) ? "" : postedForm.Get("BuyerCreditCardNr");
        this.hidStoreKey = String.IsNullOrEmpty(postedForm.Get("hidStoreKey")) ? Guid.Empty : new Guid(postedForm.Get("hidStoreKey"));
      }

      # region Response Properties
      /// <summary>
      /// The transaction log reference number created by SafeShop
      /// </summary>
      public int LogRefNr = 0;
      /// <summary>
      /// A unique reference number per-transaction supplied by the merchant.
      /// </summary>
      public string MerchantReference = "";
      /// <summary>
      /// The total amount, in CENTS, that was charged, e.g. if the amount that was charged was R12.99, then SafeShop� 0 returns 1299 in cents as the amount charged.
      /// </summary>
      public Decimal TransactionAmount = 0;
      /// <summary>
      /// The type of transaction that was executed
      /// </summary>
      public SafeshopTransactionType TransactionType = SafeshopTransactionType.Auth_Settle;
      /// <summary>
      /// Unique reference number per transaction. If you have any queries against a transaction use this number for support.
      /// </summary>
      public string SafePayRefNr = "";
      /// <summary>
      /// Represents the bank reference number for the transaction issued by the Inquiring Institution.
      /// </summary>
      public string BankRefNr = "";
      /// <summary>
      /// Transaction failed reason explanation.
      /// </summary>
      public string TransactionErrorResponse = "";
      /// <summary>
      /// Return the status of the transaction. The values returned are:
      /// Successful - The transaction was successfully processed
      /// Failed - Transaction failed Check TransactionErrorResponse tag for more information
      /// StageOrder - This transaction was put in stage mode. (See Staged Orders)
      /// </summary>
      public SafeshopTransactionResult TransactionResult = SafeshopTransactionResult.Unknown;
      /// <summary>
      /// Indicates whether the transaction was executed as live (True) or was simulated (False) by SafeShop�.
      /// </summary>
      public Boolean LiveTransaction = false;
      /// <summary>
      /// SafeTrack represents a GUID that was passed from a portal. NOTE: You must also include the curly brackets.
      /// </summary>
      public Guid SafeTrack = Guid.Empty;
      /// <summary>
      /// The Receipt URL. The Merchant may use ReceiptURL to redirect the Shopper to view their receipt on SafeShop�.
      /// </summary>
      public string ReceiptURL = "";
      /// <summary>
      /// The full names that appears on the credit card, e.g.. John Doe
      /// </summary>
      public string BuyerCreditCardNr = "";
      /// <summary>
      /// A unique SafeKey is issued to each store. SafeKey resembles {XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX}. NOTE: You must also include the curly brackets around the SafeKey GUID.
      /// </summary>
      public Guid hidStoreKey = Guid.Empty;
      #endregion
    }    
  }
}
