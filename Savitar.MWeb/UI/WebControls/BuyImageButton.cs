using System;
using System.Web.UI;

namespace Savitar.MWeb.UI.WebControls
{
    [ToolboxData("<{0}:BuyImageButton runat=server></{0}:BuyImageButton>")]
    public class BuyImageButton : System.Web.UI.WebControls.ImageButton
    {
        public Guid SafeKey { get; set; }

        public Guid SafeTrack { get; set; }

        public string ReceiptPage { get; set; } = "~/Safeshop/Receipt.aspx";

        public string FailedPage { get; set; } = "~/Safeshop/Failed.aspx";

        public string MerchantReference { get; set; }

        public decimal TransactionAmount { get; set; }

        public PaymentGateways.Safeshop.SafeshopCurrencyCode CurrencyCode { get; set; } = PaymentGateways.Safeshop.SafeshopCurrencyCode.ZAR;

        public PaymentGateways.Safeshop.SafeshopTransactionType TransactionType { get; set; } = PaymentGateways.Safeshop.SafeshopTransactionType.Auth_Settle;

        public BuyImageButton()
        {
            PostBackUrl = "https://secure.SafeShop.co.za/SafePay/Lite/Index.asp";
        }

        protected string GetHiddenInput(string propertyName, string propertyValue)
        {
            return $"<INPUT TYPE=\"hidden\" NAME=\"{propertyName}\" VALUE=\"{propertyValue}\"> ";
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!ComponentModel.Component.IsInDesignMode)
            {
                writer.WriteLine(GetHiddenInput("SafeKey", "{" + $"{SafeKey.ToString().ToUpper()}" + "}"));
                writer.WriteLine(GetHiddenInput("MerchantReferenceNumber", MerchantReference));
                writer.WriteLine(GetHiddenInput("TransactionAmount", $"{(int) (TransactionAmount * 100)}"));
                writer.WriteLine(GetHiddenInput("CurrencyCode", "ZAR"));
                writer.WriteLine(GetHiddenInput("SafeTrack", string.Format("{{0}}")));
                writer.WriteLine(GetHiddenInput("ReceiptURL", Web.Application.RootUrl + Page.ResolveUrl(ReceiptPage)));
                writer.WriteLine(GetHiddenInput("FailURL", Web.Application.RootUrl + Page.ResolveUrl(FailedPage)));
                writer.WriteLine(GetHiddenInput("TransactionType", TransactionType.ToString()));
            }

            base.Render(writer);
        }
    }
}
