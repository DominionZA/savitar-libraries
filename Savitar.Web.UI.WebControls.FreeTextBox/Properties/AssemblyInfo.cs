﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Savitar.Web.UI.WebControls.FreeTextBox")]
[assembly: AssemblyDescription("Savitar Web FreeTextBox Controls")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Savitar Software Development")]
[assembly: AssemblyProduct("Savitar.Web.UI.WebControls.FreeTextBox")]
[assembly: AssemblyCopyright("Copyright © Savitar Software Development 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: TagPrefix("Savitar.Web.UI.WebControls.FreeTextBox", "Savitar")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("49ab0d8c-6021-46f7-a8a5-87271c9a1ca8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
