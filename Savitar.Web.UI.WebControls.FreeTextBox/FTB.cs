using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

namespace Savitar.Web.UI.WebControls.FreeTextBox
{
    public partial class CMEdit : FreeTextBoxControls.FreeTextBox
    {
        public string DbConnectionStringName { get; set; } = "Live";

        public CMEdit()
        {
            InitializeComponent();

            EnableHtmlMode = false;
        }

        public CMEdit(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            EnableHtmlMode = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Page.IsPostBack)
            {
                if (!System.Diagnostics.Process.GetCurrentProcess().ProcessName.ToUpper().Equals("DEVENV"))
                    BindData();
            }
        }

        protected SqlConnection GetSqlConnection()
        {
            if (string.IsNullOrEmpty(DbConnectionStringName))
                throw new Exception($"{ID}.DBConnectionStringName has not been set");

            if (ConfigurationManager.ConnectionStrings[DbConnectionStringName] == null)
                throw new Exception(
                    $"{ID}.DBConnectionStringName '{DbConnectionStringName}' could not be found in web.config");

            string connectionString = ConfigurationManager.ConnectionStrings[DbConnectionStringName].ConnectionString;

            if (String.IsNullOrEmpty(connectionString))
                throw new Exception(
                    $"Connection string '{DbConnectionStringName}' exists in the web.config file, but is empty");

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            return con;
        }

        public void BindData()
        {
            try
            {
                ValidateTableSetup();
                LoadText();
            }
            catch (Exception ex)
            {
                throw new Exception($"{ID}: {ex.Message}");
            }
        }

        public void SaveText()
        {
            SqlConnection con = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "spUpdateSAVCMEditor";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@RequestPath", SqlDbType.VarChar, 300).Value = HttpContext.Current.Request.Path;
                cmd.Parameters.Add("@ControlName", SqlDbType.VarChar, 300).Value = ID;
                cmd.Parameters.Add("@ControlText", SqlDbType.Text).Value = Text;

                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public void LoadText()
        {
            Text = "";

            SqlConnection con = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "SELECT ControlText FROM SAVCMEditor WHERE RequestPath = @RequestPath AND ControlName = @ControlName";
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@RequestPath", SqlDbType.VarChar, 300).Value = HttpContext.Current.Request.Path;
                cmd.Parameters.Add("@ControlName", SqlDbType.VarChar, 300).Value = ID;

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (!reader.Read())
                        return;

                    Text = reader["ControlText"] == DBNull.Value ? "" : reader["ControlText"].ToString();
                }
                finally
                {
                    reader.Close();
                }
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public void ValidateTableSetup()
        {
            SqlConnection con = GetSqlConnection();

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;

                try
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("IF NOT EXISTS (SELECT NULL FROM sys.Tables WHERE [Name] = 'SAVCMEditor')");
                    sb.AppendLine("BEGIN");
                    sb.AppendLine("CREATE TABLE [dbo].[SAVCMEditor](");
                    sb.AppendLine("  [SAVCMEditorID] [int] IDENTITY(1,1) NOT NULL,");
                    sb.AppendLine("  [RequestPath] [varchar](300) NOT NULL,");
                    sb.AppendLine("	 [ControlName] [varchar](300) NOT NULL,");
                    sb.AppendLine("  [ControlText] [text] NOT NULL,");
                    sb.AppendLine("CONSTRAINT [PK_SAVCMEditor] PRIMARY KEY CLUSTERED ");
                    sb.AppendLine("(");
                    sb.AppendLine("  [SAVCMEditorID] ASC");
                    sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
                    sb.AppendLine(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]");
                    sb.AppendLine("END");

                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();

                    sb.Length = 0;

                    sb.AppendLine("IF NOT EXISTS(SELECT NULL FROM sys.Indexes WHERE [Name] = 'IX_SAVCMEditor')");
                    sb.AppendLine("BEGIN");
                    sb.AppendLine("CREATE UNIQUE NONCLUSTERED INDEX [IX_SAVCMEditor] ON [dbo].[SAVCMEditor]");
                    sb.AppendLine("(");
                    sb.AppendLine("	[RequestPath] ASC,");
                    sb.AppendLine("	[ControlName] ASC");
                    sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
                    sb.AppendLine("END");

                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();

                    sb.Length = 0;

                    sb.AppendLine("IF NOT EXISTS (SELECT NULL FROM sys.Procedures WHERE [Name] = 'spUpdateSAVCMEditor')");
                    sb.AppendLine("BEGIN");
                    sb.AppendLine("EXEC('");
                    sb.AppendLine("CREATE PROCEDURE dbo.spUpdateSAVCMEditor");
                    sb.AppendLine("(");
                    sb.AppendLine("  @RequestPath VARCHAR(300),");
                    sb.AppendLine("  @ControlName VARCHAR(300),");
                    sb.AppendLine("  @ControlText TEXT");
                    sb.AppendLine(")");
                    sb.AppendLine("AS");

                    sb.AppendLine("IF NOT EXISTS (SELECT NULL FROM SAVCMEditor WHERE RequestPath = @RequestPath AND ControlName = @ControlName)");
                    sb.AppendLine("BEGIN");
                    sb.AppendLine("  INSERT INTO SAVCMEditor (RequestPath, ControlName, ControlText)");
                    sb.AppendLine("  VALUES(@RequestPath, @ControlName, @ControlText)  ");
                    sb.AppendLine("END");
                    sb.AppendLine("ELSE");
                    sb.AppendLine("BEGIN");
                    sb.AppendLine("  UPDATE SAVCMEditor");
                    sb.AppendLine("    SET ControlText = @ControlText");
                    sb.AppendLine("  WHERE RequestPath = @RequestPath AND ControlName = @ControlName");
                    sb.AppendLine("END");
                    sb.AppendLine("')");
                    sb.AppendLine("END");

                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception(String.Format("{0} : {1}", ex.Message, cmd.CommandText));
                }
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
    }
}