namespace Savitar.Web.Subscriptions.Reports
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      Gizmox.WebGUI.Common.Resources.ImageResourceHandle imageResourceHandle1 = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle();
      this.SubscribePictureBox = new Gizmox.WebGUI.Forms.PictureBox();
      this.SuspendLayout();
      // 
      // SubscribePictureBox
      // 
      this.SubscribePictureBox.Anchor = Gizmox.WebGUI.Forms.AnchorStyles.None;
      this.SubscribePictureBox.Dock = Gizmox.WebGUI.Forms.DockStyle.Fill;
      imageResourceHandle1.File = "View";
      this.SubscribePictureBox.Image = imageResourceHandle1;
      this.SubscribePictureBox.Location = new System.Drawing.Point(0, 0);
      this.SubscribePictureBox.Name = "SubscribePictureBox";
      this.SubscribePictureBox.Size = new System.Drawing.Size(115, 76);
      this.SubscribePictureBox.SizeMode = Gizmox.WebGUI.Forms.PictureBoxSizeMode.Normal;
      this.SubscribePictureBox.TabIndex = 0;
      this.SubscribePictureBox.DoubleClick += new System.EventHandler(this.SubscribePictureBox_DoubleClick);      
      // 
      // MainForm
      // 
      this.Controls.Add(this.SubscribePictureBox);
      this.Size = new System.Drawing.Size(115, 76);
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.PictureBox SubscribePictureBox;
  }
}
