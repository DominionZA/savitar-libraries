namespace Savitar.Web.Subscriptions.Reports
{
  partial class ScheduleForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new Gizmox.WebGUI.Forms.Label();
      this.button2 = new Gizmox.WebGUI.Forms.Button();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(100, 23);
      this.label1.TabIndex = 0;
      this.label1.Text = "Report Subscription Schedule";
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(83, 125);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "button2";
      this.button2.TextImageRelation = Gizmox.WebGUI.Forms.TextImageRelation.Overlay;
      // 
      // ScheduleForm
      // 
      this.Controls.Add(this.button2);
      this.Controls.Add(this.label1);
      this.Size = new System.Drawing.Size(395, 205);
      this.Text = "ScheduleForm";
      this.ResumeLayout(false);

    }

    #endregion

    private Gizmox.WebGUI.Forms.Label label1;
    private Gizmox.WebGUI.Forms.Button button2;


  }
}