#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;

using Gizmox.WebGUI.Common;
using Gizmox.WebGUI.Forms;

#endregion

namespace Savitar.Web.Subscriptions.Reports
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();

      SubscribePictureBox.Image = new Gizmox.WebGUI.Common.Resources.ImageResourceHandle("ReportSubscribe.PNG");
      SubscribePictureBox.Width = 22;
      SubscribePictureBox.Height = 22;
    }

    private void SubscribePictureBox_DoubleClick(object sender, EventArgs e)
    {
      ScheduleForm frm = new ScheduleForm();
      frm.ShowDialog();
    }    
  }
}