using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EO.Web;

namespace Savitar.EO.Web
{
  [DefaultProperty("DisplayPanel"), ToolboxData("<{0}:EOTabStrip runat=server></{0}:EOTabStrip>")]
  public class EOTabStrip : TabStrip
  {  
    public EOTabStrip()
    {
      base.RaisesServerEvent = true;
      base.ItemClick += new NavigationItemEventHandler(EOItemClick);
    }

    protected void EOItemClick(object sender, NavigationItemEventArgs e)
    {
      if (this.DisplayPanel != null)
      {
        HttpContext.Current.Response.Write("<br/>We have a PlaceHolder");
        
        Button button = new Button();
        button.Text = "Click Me";
        button.Width = Unit.Pixel(75);
        button.Height = Unit.Pixel(25);
        button.Visible = true;

        this.PlaceHolder.Controls.Add(button);
      }
    }

    [Browsable(true), PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(typeof(Panel), ""), Description("Panel to render associated user controls in")]
    public Panel DisplayPanel
    {
      get { return _DisplayPanel; }
      set { _DisplayPanel = value; }
    } Panel _DisplayPanel;

    [Browsable(true), PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(typeof(PlaceHolder), ""), Description("PlaceHolder to render associated user controls in")]
    public PlaceHolder PlaceHolder
    {
      get { return _PlaceHolder; }
      set { _PlaceHolder = value; }
    } PlaceHolder _PlaceHolder;
  }
}
