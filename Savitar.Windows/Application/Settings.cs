using System;

namespace Savitar.Windows.Application
{
    [Serializable]
    public class Settings<T>
    {
        public string FileName
        {
            get => string.IsNullOrEmpty(fileName) ? "AppSettings.dat" : fileName;
            set => fileName = value;
        }

        private string fileName;

        public Settings()
        {
        }

        public Settings(string fileName)
        {
            FileName = fileName;
        }

        public static T Load()
        {
            return Load("AppSettings.dat");
        }

        public static T Load(string fileName)
        {
            var settings = (T)Object.LoadFromAppFolder(fileName, typeof(T));
            if (settings == null)
                settings = (T)Activator.CreateInstance(typeof(T));

            return settings;
        }

        public virtual void Save()
        {
            Object.SaveInAppFolder(FileName, this);
        }
    }
}