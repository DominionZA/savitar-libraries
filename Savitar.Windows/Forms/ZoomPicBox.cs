using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace Savitar.Windows.Forms
{
    public sealed partial class ZoomPicBox : ScrollableControl
    {
        private Image image;
        private float zoom = 1;
        private int x1;
        private int y1;
        private int rw;
        private int rh;

        public InterpolationMode InterpolationMode { get; set; } = InterpolationMode.High;

        public float Zoom
        {
            get => zoom;
            set
            {
                if (value < 0 || value < 1e-05F)
                    value = 1e-05F;

                if (float.IsInfinity(value))
                    value = 1;

                zoom = value;
                UpdateScaleFactor();
                Invalidate();
            }
        }

        public Image Image
        {
            get => image;
            set
            {
                image = value;
                this.zoom = BestFit();
                UpdateScaleFactor();
                Invalidate();
            }
        }

        private float BestFit()
        {
            float bf = 1;

            try
            {
                //try to fit the image on the control
                if ((image.Height > Height) || (image.Width > Width))
                {
                    var h = Height / (float)image.Height;
                    var w = Width / (float)image.Width;
                    bf = (h > w) ? w : h;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"BestFit, {ex}");
            }
            return bf;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                x1 = e.X;
                y1 = e.Y;
                rh = 0;
                rw = 0;
                Invalidate();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                rw = e.X - x1;
                rh = e.Y - y1;
                Invalidate();
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if ((rw == 0) || (rh == 0))
            {
                Zoom = BestFit();
            }
            else
            {
                Zoom = Image.Width * Image.Height / rh * rw;
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta > 0)
                Zoom += 0.25F;
            else
                Zoom -= 0.25F;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (image == null)
            {
                base.OnPaintBackground(e);
                return;
            }

            try
            {
                var mx = new Matrix(zoom, 0, 0, zoom, 0, 0);
                mx.Translate(this.AutoScrollPosition.X / zoom, this.AutoScrollPosition.Y / zoom);
                e.Graphics.Transform = mx;
                e.Graphics.InterpolationMode = this.InterpolationMode;
                e.Graphics.DrawImage(image, new Rectangle(0, 0, this.image.Width, this.image.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                e.Graphics.DrawRectangle(Pens.Black, new Rectangle(x1, y1, rw, rh));

            }
            catch (Exception ex)
            {
                Trace.TraceError($"OnPaint, {ex}");
            }

            base.OnPaint(e);
        }


        private void UpdateScaleFactor()
        {
            if (image == null)
                AutoScrollMargin = Size;
            else
            {
                try
                {
                    AutoScrollMinSize = new Size((int)(image.Width * zoom + 0.5F), (int)(image.Height * zoom + 0.5F));

                    if ((rh > 0) && (rw > 0))
                        AutoScrollPosition = new Point((int)(x1 * zoom), (int)(y1 * zoom));
                    else
                        AutoScrollPosition = new Point((AutoScrollMinSize.Width / 2), (AutoScrollMinSize.Height / 2));

                    rh = 0;
                    rw = 0;
                    Trace.TraceInformation($"{DateTime.Now.Millisecond}- auto {AutoScrollPosition}, zoom{zoom}");

                }
                catch (Exception ex)
                {
                    Trace.TraceError($"UpdateScaleFactor, {ex}");
                }
            }
        }

        public ZoomPicBox()
        {
            InitializeComponent();
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.ResizeRedraw
                | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);

            AutoScroll = true;
        }
    }
}
