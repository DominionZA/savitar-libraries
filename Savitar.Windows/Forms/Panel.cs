using System;

namespace Savitar.Windows.Forms
{
    public sealed class Panel : System.Windows.Forms.Panel
    {
        public Panel()
        {
            BackColor = System.Drawing.Color.Transparent;
        }

        // This is for the panel to refer the closed event of the UserControl to the form owner.
        public event UserControlClosedDelegate UserControlClosed;

        private void UCClosed(UserControl sender, System.Windows.Forms.DialogResult dialogResult)
        {
            UserControlClosed?.Invoke(sender, dialogResult);
        }

        public UserControl Show(UserControl userControl)
        {
            if (!CloseCurrentUserControl())
                return null;

            return userControl;
        }

        public System.Windows.Forms.UserControl Show(Type userControl)
        {
            if (!CloseCurrentUserControl())
                return null;

            System.Windows.Forms.UserControl ctrl = (System.Windows.Forms.UserControl)Activator.CreateInstance(userControl);

            if (ctrl is UserControl)
                ((UserControl)ctrl).CloseResult += UCClosed;

            ctrl.Dock = System.Windows.Forms.DockStyle.Fill;
            Controls.Add(ctrl);

            return ctrl;
        }

        private bool CloseCurrentUserControl()
        {
            // First close off any open user controls on the page.            
            if (Controls.Count <= 0) 
                return true;

            var ctrl = (System.Windows.Forms.UserControl)Controls[0];

            if (ctrl is UserControl)
            {
                if (!((UserControl)ctrl).CanClose())
                    return false;
            }

            Controls.Remove(ctrl);
            ctrl.Dispose();

            return true;
        }
    }
}