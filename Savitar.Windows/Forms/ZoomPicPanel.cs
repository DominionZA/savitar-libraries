using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace Savitar.Windows.Forms
{
    public partial class ZoomPicPanel : UserControl
    {
        private int x;
        private int y;
        private int posX;
        private int posY;

        public ZoomPicPanel()
        {
            InitializeComponent();

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public Image Image
        {
            get => pictureBox1.Image;
            set => pictureBox1.Image = value;
        }

        private void ZoomPicPanel_Load(object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(Height, Width);
            pictureBox1.Top = 0;
            pictureBox1.Left = 0;
            posY = pictureBox1.Top;
            posX = pictureBox1.Left;
        }


        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                pictureBox1.Height *= 2;
                pictureBox1.Width *= 2;
            }
            else
            {
                pictureBox1.Height /= 2;
                pictureBox1.Width /= 2;
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                posX = 0;
                posY = 0;

                x = e.X;
                y = e.Y;

                Trace.TraceInformation($"MD{e.Location}");
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                posX += e.X - x;
                posY += e.Y - y;

                pictureBox1.Left = posX;
                pictureBox1.Top = posY;

                AutoScrollMinSize = pictureBox1.Size;

                Trace.TraceInformation($"nx{posX},ny{posY}");
            }

        }
    }
}
