namespace Savitar.Windows.Forms
{
    /// <summary>
    /// Utility class for Application related functions
    /// </summary>
    public class Application
    {
        /// <summary>
        /// Returns the actual full filename (not path) of the executing application.
        /// </summary>
        public static string ExeName => System.IO.Path.GetFileName(Savitar.Windows.Forms.Application.FullPath);

        /// <summary>
        /// Returns the full filename and path of the executing application.
        /// </summary>
        public static string FullPath
        {
            get
            {
                System.Reflection.Module[] modules = System.Reflection.Assembly.GetExecutingAssembly().GetModules();
                return modules[0].FullyQualifiedName;
            }
        }

        /// <summary>
        /// Returns the folder that the application is executing from
        /// </summary>
        public static string Folder
        {
            get
            {
                var path = System.IO.Path.GetDirectoryName(Application.FullPath);
                if (path == null)
                    return null;

                if (path[path.Length - 1] != '\\')
                    path += '\\';

                return path;
            }
        }
    }
}