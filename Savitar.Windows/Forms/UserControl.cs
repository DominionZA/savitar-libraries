using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;

namespace Savitar.Windows.Forms
{
    public delegate void UserControlClosedDelegate(UserControl sender, DialogResult dialogResult);

    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class UserControl : System.Windows.Forms.UserControl
    {
        public event UserControlClosedDelegate CloseResult;

        public UserControl()
        {
            InitializeComponent();
        }

        public virtual bool CanClose()
        {
            return true;
        }

        public new bool DesignMode => System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv";

        public void Close(DialogResult dialogResult)
        {
            Parent.Controls.Remove(this);
            Dispose();

            CloseResult?.Invoke(this, dialogResult);
        }
    }
}