using System;

namespace Savitar.Windows
{
    public class Object
    {
        public static object LoadFromAppFolder(string fileName, Type type)
        {
            return Savitar.Object.Load(Forms.Application.Folder + fileName, type);
        }

        public static void SaveInAppFolder(string fileName, object objectInstance)
        {
            Savitar.Object.Save(Forms.Application.Folder + fileName, objectInstance);
        }
    }
}