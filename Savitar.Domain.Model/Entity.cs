﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Savitar.Domain.Contracts;

namespace Savitar.Domain.Model
{
    public class BaseEntity<TEntity> : IBaseEntity
        where TEntity : class, IBaseEntity, new()
    {
        
    }

    public class Entity<TEntity> : BaseEntity<TEntity>, IEntity
        where TEntity : class, IEntity, new()
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
