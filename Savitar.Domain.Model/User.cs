﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Savitar.Domain.Model.Savitar.Domain.Model.Entities.Stage;

namespace Savitar.Domain.Model
{    
    public class User : Entity<User>
    {
        [Required, MaxLength(50)]
        public string UserName { get; set; }

        //[Required(ErrorMessage = "Email Required")]
        [RegularExpression("^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$",
            ErrorMessage = @"Not a valid email")]
        [MaxLength(255)]
        public string EmailAddress { get; set; }

        public DateTime? LastLogin { get; set; }

        public virtual ICollection<Role> Roles { get; set; }        
    }
}
