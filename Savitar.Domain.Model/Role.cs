﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Savitar.Domain.Model
{
    namespace Savitar.Domain.Model.Entities.Stage
    {
        [Table("webpages_Roles")]
        public class Role : BaseEntity<Role>
        {
            [Key]
            public int RoleId { get; set; }
            [StringLength(256)]
            public string RoleName { get; set; }
            public virtual ICollection<User> UserProfiles
            {
                get { return _UserProfiles ?? (_UserProfiles = new Collection<User>()); }
            } ICollection<User> _UserProfiles;
        }
    }

}
