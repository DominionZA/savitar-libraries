﻿using System.Collections.Generic;
using System.Data;

namespace Savitar.Data
{
    public class MDFColumn
    {
        public string Name { get; set; }
    }

    public class MDFColumnList : List<MDFColumn>
    {
    }

    public class dBase
    {
        public DataTable MDFData { get; private set; }

        public MDFColumnList MDFColumns { get; } = new MDFColumnList();

        public dBase(string fileName)
        {
            Populate(fileName);
        }

        public static DataTable GetData(string fileName)
        {
            var item = new dBase(fileName);
            return item.MDFData;
        }

        protected void Populate(string fileName)
        {
            var path = System.IO.Path.GetDirectoryName(fileName);
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(fileName);
            var table = fileInfo.Name;

            //string conString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + fileName + ";Extended Properties=dBase III";
            var conString = @"Provider=Microsoft.Jet.OLEDB.4.0;" + @"Data Source=" + path + ";Extended Properties=dBase IV";

            System.Data.OleDb.OleDbConnection con = new System.Data.OleDb.OleDbConnection(conString);
            con.Open();

            // strip any extension
            table = System.IO.Path.GetFileNameWithoutExtension(table);
            
            var da = new System.Data.OleDb.OleDbDataAdapter("Select * from [" + table + "]", con);

            var ds = new DataSet("dBaseFile");
            da.Fill(ds);

            MDFData = ds.Tables[0];

            foreach (DataColumn col in MDFData.Columns)
                MDFColumns.Add(new MDFColumn() { Name = col.ColumnName });
        }
    }
}
