﻿using System;
using System.ComponentModel;

namespace Savitar.Data
{
    public class SavItemTextConverter : StringConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value != null))
                return ((Interfaces.ISavItem)value).Text;

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class DateConverter : System.ComponentModel.DateTimeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value != null))
                return $"{value:yyyy/MM/dd}";

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class DateTimeConverter : System.ComponentModel.DateTimeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value != null))
                return $"{value:yyyy/MM/dd HH:MM}";

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class DoubleConverter : System.ComponentModel.DoubleConverter
    {
        public static string Convert(object value)
        {
            return $"{value:###,###,##0.00}";
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value != null))
                return Convert(value);

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class Int32Converter : System.ComponentModel.Int32Converter
    {
        public static string Convert(object value)
        {
            return $"{value:###,###,##0}";
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value != null))
                return Convert(value);

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class DecimalConverter : System.ComponentModel.DecimalConverter
    {
        public static string Convert(object value)
        {
            return $"{value:###,###,##0.00}";
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value != null))
                return Convert(value);

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class BoolToYesNoConverter : BooleanConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value != null && value is bool b)
                return b ? "Yes" : "No";

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string s)
                return s == "Yes";

            return base.ConvertFrom(context, culture, value);
        }
    }
}
