﻿using System;
using System.ComponentModel;
using System.Data.Objects.DataClasses;

namespace Savitar.Data.ComponentModel
{
    public class EFComboBoxConverter : TypeConverter
    {
        public System.Data.Objects.ObjectContext DB
        {
            get
            {
                if (_DB == null)
                    throw new Savitar.SavException(this, "DB", "DB Has not been set to an ObjectContext. Call SetDB first");

                return _DB;
            }
        }
        System.Data.Objects.ObjectContext _DB;

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true; // true means show a combobox
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true; // true will limit to list. false will show the list, but allow free-form entry
        }

        public void SetDB(ITypeDescriptorContext context)
        {
            if (context.Instance is IEntityWithRelationships)
            {
                IEntityWithRelationships db = (IEntityWithRelationships)context.Instance;
                _DB = db.GetContext();
            }
            else
                throw new Exception("[" + this.GetType() + "] context.Instance is not of type IEntityWithRelationships");
        }
    }
}