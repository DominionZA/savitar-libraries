﻿using System;
using System.Linq;

namespace Savitar.Data
{
    public partial class Upgrade
    {
        public static void Execute(string scriptsPath)
        {
            if (!System.IO.Directory.Exists(scriptsPath))
                return;

            int version = GetDBVersion();
            string[] files = GetUpdateFiles(scriptsPath);

            foreach (string file in files)
            {
                string fileText = System.IO.File.ReadAllText(file);
                string[] scripts = fileText.Split(new string[1] { "GO\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string script in scripts)
                    Global.Execute(script);

                // If here, then all good. Update the version number.
                // Only update if not in DEBUG.
                //#if !DEBUG
                Entities.Setting.DBVersion = Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(file));
                //#endif
            }
        }

        // Only returns files newer than the current DB version.
        public static string[] GetUpdateFiles(string scriptsPath)
        {
            var dbVersion = GetDBVersion();

            return (from file in System.IO.Directory.GetFiles(scriptsPath, "*.sql")
                where Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(file)) > dbVersion
                orderby file
                select file).ToArray();
        }

        public static int GetDBVersion()
        {
            return Entities.Setting.DBVersion;
        }

        public static int GetNewDBVersion(string scriptsPath)
        {
            var files = GetUpdateFiles(scriptsPath);
            if ((files == null) || (files.Length == 0))
                return 0;

            return Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(files[files.Length - 1]));
        }

        public static bool HaveUpdate(string scriptsPath)
        {
            var files = GetUpdateFiles(scriptsPath);
            return (files != null) && (files.Length != 0);
        }
    }
}