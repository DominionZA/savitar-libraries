﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SubSonic.Schema;
using System.Runtime.Serialization;

namespace Savitar.Data
{
    [DefaultProperty("Text")]
    [DataContract]
    public partial class SavItem : Interfaces.ISavItem
    {
        [Browsable(false)]
        public virtual bool AuditEnabled { get; set; } = true;

        [Browsable(false)]
        [DataMember(Name = "ID")]
        public virtual int ID { get; set; }

        [Browsable(false)]
        public virtual string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }

        public virtual string GetValueMember()
        {
            return ((IActiveRecord)this).KeyName();
        }

        public virtual string GetDisplayMember()
        {
            return ((IActiveRecord)this).DescriptorColumn();
        }

        protected virtual void Saving(CancelEventArgs e) { }

        protected virtual void Saved(List<IColumn> dirtyColumns)
        {
            if (!AuditEnabled)
                return;

            return; // For now Audit is disabled. Logging too much information. Discuss with Claude how and what to audit then re-enable.

            if (GetType().ToString().Contains("Audit")) // Will cause a recursive Saved method call each time the Audit table it updated to don't audit the Audit tables themselves.
                return;
            if (GetType().ToString().EndsWith("Permission")) // Permissions table will get new permissions loaded at start. Don't want an audit.
                return;
            if (GetType().ToString().Contains(".x"))
                return;

            var item = (IActiveRecord)this;

            var settings = new Dictionary<string, object>();
            var props = item.GetType().GetProperties();
            foreach (var pi in props)
            {
                try
                {
                    settings.Add(pi.Name, pi.GetValue(item, null));
                }
                catch { }
            }

            // If have dirtyColumns then this is an Update otherwise an Insert.
            // Update: Use the passed dirtyColumns
            // Insert: Get the values of all columns
            var dateTimeValueChanged = DateTime.Now;
            if (dirtyColumns == null)
            {
                var tbl = SubSonic.DataProviders.ProviderFactory.GetProvider().FindOrCreateTable(this.GetType());

                foreach (var key in settings.Keys)
                {
                    if (key.Equals("Text") || key.StartsWith("__")) continue;
                    var col = tbl.GetColumn(key);
                    if (col != null)
                        HandleColumnChanged(col, settings[key], true, dateTimeValueChanged);
                }
            }
            else
            {
                foreach (var col in dirtyColumns.Where(col => !col.AutoIncrement && !col.IsReadOnly && !col.Name.Equals(item.KeyName())))
                {
                    HandleColumnChanged(col, settings[col.Name], false, dateTimeValueChanged);
                }
            }
        }

        protected virtual void HandleColumnChanged(IColumn col, object columnValue, bool isNewRecord, DateTime dateTimeChanged)
        {
            var item = (IActiveRecord)this;

            if (!col.AutoIncrement && !col.IsReadOnly && !col.Name.Equals(item.KeyName()))
                ColumnChanged(col.Table.Name, col.Name, columnValue == null ? "" : columnValue.ToString(), col.DataType.ToString(), item.KeyName(), item.KeyValue().ToString(), dateTimeChanged, isNewRecord);
        }

        protected virtual void ColumnChanged(string tableName, string columnName, string columnValue, string columnDataType, string recordKeyName, string recordKeyValue, DateTime dateTimeChanged, bool isNewRecord)
        {
            var rec = new Entities.Audit_ColumnValue
            {
                TableName = tableName,
                ColumnName = columnName,
                ColumnValue = columnValue,
                ColumnDataType = columnDataType,
                RecordKeyName = recordKeyName,
                RecordKeyValue = recordKeyValue,
                UserID = Global.LoggedInUserID,
                DateTimeValueChanged = dateTimeChanged,
                IsRecordNew = isNewRecord
            };

            rec.Save();
        }

        protected virtual void RecordDeleted(string tableName, string columnValue, string recordKeyName, string recordKeyValue, DateTime dateTimeDeleted)
        {
            var rec = new Entities.Audit_DeletedRecord
            {
                TableName = tableName,
                RecordKeyName = recordKeyName,
                RecordKeyValue = recordKeyValue,
                UserID = Global.LoggedInUserID,
                DateTimeDeleted = dateTimeDeleted,
                ColumnValue = columnValue
            };

            rec.Save();
        }

        protected virtual void Deleting(CancelEventArgs e)
        {
            var item = (IActiveRecord)this;

            var tbl = SubSonic.DataProviders.ProviderFactory.GetProvider().FindOrCreateTable(GetType());
            RecordDeleted(tbl.Name, this.ToString(), item.KeyName(), item.KeyValue().ToString(), DateTime.Now);
        }

        protected virtual void Deleted() { }

        public virtual void Save() { }

        public virtual void Delete() { }

        protected object GetColumnValue(string propertyName)
        {
            var info = GetType().GetProperty(propertyName);
            if (info == null)
                throw new Exception(this.GetType() + ".GetColumnValue was unable to locate a property with the name " + propertyName);

            return !info.CanRead ? null : info.GetValue(this, null).ToString();
        }

        public virtual void ResetForeignKeys() { }
    }

    [TypeConverterAttribute(typeof(Savitar.ComponentModel.ItemListStringConverter)), DescriptionAttribute("Click to view all entries")]
    public partial class SavItemList<T> : List<T>, Interfaces.ISavItemList where T : Interfaces.ISavItem, new()
    {
        public SavItemList()
        {

        }

        #region ISavItemList Members    

        public virtual int Populate(string searchText)
        {
            throw new NotImplementedException(this.GetType() + ".Populate(searchText) has not been implemented");
        }

        public virtual int Populate()
        {
            return Populate("");
        }

        public virtual void Save()
        {
            foreach (var item in this)
                item.Save();
        }

        public virtual void Delete()
        {
            foreach (var item in this)
                item.Delete();
        }

        public virtual void Delete(Interfaces.ISavItem item)
        {
            item.Delete();
            Remove((T)item);
        }

        public virtual Interfaces.ISavItem GetNewItem()
        {
            return new T();
        }

        public virtual void Add(Interfaces.ISavItem item)
        {
            base.Add((T)item);
        }

        public virtual Interfaces.ISavItem Add()
        {
            var item = new T();
            Add(item);
            return item;
        }

        public virtual string GetValueMember()
        {
            throw new NotImplementedException(GetType() + ".GetValueMember() has not been implemented");
        }

        public virtual string GetDisplayMember()
        {
            throw new NotImplementedException(GetType() + ".GetDisplayMember() has not been implemented");
        }

        #endregion
    }

    public partial class SimpleSavItem : SavItem
    {
        [Browsable(false)]
        public override int ID { get; set; }

        [Browsable(true), DataMember(Name = "Text"), ReadOnly(true), DisplayName("Description")]
        public override string Text { get; set; }

        public override string GetValueMember()
        {
            return "ID";
        }

        public override string GetDisplayMember()
        {
            return "Text";
        }

        public override void Save()
        {
            throw new NotImplementedException(GetType() + ".Save is not supported");
        }

        public override void Delete()
        {
            throw new NotImplementedException(GetType() + ".Save is not supported");
        }
    }
}
