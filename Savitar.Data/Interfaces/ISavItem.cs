﻿using System.Collections;
using System.ServiceModel;

namespace Savitar.Data.Interfaces
{
    [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
    public interface ISavItem
    {
        int ID { get; set; }
        string Text { get; set; }
        string GetValueMember();
        string GetDisplayMember();

        void Save();
        void Delete();
        void ResetForeignKeys();
    }

    public interface ISavItemSelfRef : ISavItem
    {
        int? fkID { get; set; }
    }

    public interface ISavItemList : IList
    {
        void Save();
        void Delete(ISavItem item);
        ISavItem Add();
        void Add(ISavItem item);

        ISavItem GetNewItem();
        string GetValueMember();
        string GetDisplayMember();

        int Populate(string searchText);
        int Populate();
    }
}