﻿using System;
using SubSonic.SqlGeneration.Schema;

namespace Savitar.Data.Entities
{
  public class Audit_DeletedRecord
  {
    [SubSonicPrimaryKey]
    public int Audit_DeletedRecordsID { get; set; }
    [SubSonicStringLength(200)]
    public string TableName { get; set; }
    [SubSonicStringLength(100)]
    public string RecordKeyName { get; set; }
    [SubSonicStringLength(100)]
    public string RecordKeyValue { get; set; }
    [SubSonicStringLength(500)]
    public string ColumnValue { get; set; }

    public Guid UserID { get; set; }
    public DateTime DateTimeDeleted { get; set; }

    public void Save()
    {
      Global.DB.Add(this);
    }
  }
}
