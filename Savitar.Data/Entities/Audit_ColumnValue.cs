﻿using System;
using SubSonic.SqlGeneration.Schema;

namespace Savitar.Data.Entities
{
  public class Audit_ColumnValue
  {
    [SubSonicPrimaryKey] // Test
    public int Audit_ColumnValuesID { get; set; }
    [SubSonicStringLength(200)]
    public string TableName { get; set; }
    [SubSonicStringLength(250)]
    public string ColumnName { get; set; }    
    public string ColumnValue { get; set; }
    [SubSonicStringLength(50)]
    public string ColumnDataType { get; set; }
    public bool IsRecordNew { get; set; }
    [SubSonicStringLength(100)]
    public string RecordKeyName { get; set; }
    [SubSonicStringLength(200)]
    public string RecordKeyValue { get; set; }
    public Guid UserID { get; set; }
    public DateTime DateTimeValueChanged { get; set; }

    public void Save()
    {
      Global.DB.Add(this);
    }
  }
}
