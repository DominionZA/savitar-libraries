﻿using System;
using SubSonic.SqlGeneration.Schema;

namespace Savitar.Data.Entities
{
    public partial class Setting
    {
        [SubSonicPrimaryKey]
        public int SettingsID { get; set; }
        [SubSonicStringLength(500)]
        public string SettingsGroup { get; set; }
        [SubSonicStringLength(500)]
        public string SettingsName { get; set; }
        [SubSonicStringLength(500)]
        public string SettingsValue { get; set; }

        public static Setting Get(string group, string name)
        {
            return Global.DB.Single<Setting>(x => x.SettingsGroup == group && x.SettingsName == name);
        }

        public static string Get(string group, string name, string defaultValue)
        {
            var item = Get(group, name);
            if (item == null)
                return defaultValue;

            return item.SettingsValue;
        }

        public static int Get(string group, string name, int defaultValue)
        {
            var item = Get(group, name);
            return item == null ? defaultValue : Convert.ToInt32(item.SettingsValue);
        }

        public static int Set(string group, string name, string value)
        {
            Setting item = Get(group, name);
            if (item == null)
            {
                item = new Setting {SettingsGroup = @group, SettingsName = name, SettingsValue = value};
                Global.DB.Add(item);
            }
            else
            {
                item.SettingsValue = value;
                Global.DB.Update(item);
            }

            return item.SettingsID;
        }

        [SubSonicIgnore]
        public static int DBVersion
        {
            get => Setting.Get("Database", "Version", 0);
            set => Setting.Set("Database", "Version", value.ToString());
        }
    }
}