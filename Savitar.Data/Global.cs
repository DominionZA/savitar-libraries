﻿using System;
using SubSonic.Repository;

namespace Savitar.Data
{
    public static class Global
    {
        public static Guid LoggedInUserID { get; set; }

        public static string ConnectionStringName
        {
            get => _connectionStringName;
            set
            {
                if (_connectionStringName == value)
                    return;

                _connectionStringName = value;
                _DB = new SimpleRepository(value, SimpleRepositoryOptions);
            }
        }
        static string _connectionStringName = "DB";

        public static SimpleRepositoryOptions SimpleRepositoryOptions
        {
            get => _simpleRepositoryOptions;
            set
            {
                if (_simpleRepositoryOptions == value)
                    return;

                _simpleRepositoryOptions = value;
                _DB = new SimpleRepository(Global.ConnectionStringName, value);
            }
        }
        static SimpleRepositoryOptions _simpleRepositoryOptions = SimpleRepositoryOptions.RunMigrations;

        public static SimpleRepository DB => _DB ?? (_DB = new SimpleRepository(Global.ConnectionStringName, Global.SimpleRepositoryOptions));
        static SimpleRepository _DB;

        public static void Execute(string sql)
        {
            var provider = SubSonic.DataProviders.ProviderFactory.GetProvider(Global.ConnectionStringName);
            var qry = new SubSonic.Query.CodingHorror(provider, sql);
            qry.Execute();
        }
    }
}