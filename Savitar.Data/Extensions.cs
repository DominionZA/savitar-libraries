﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Data.Objects.DataClasses;
using Savitar.Model;

namespace Savitar.Data
{
    public static class Extensions
    {
        public static ObjectContext GetContext(this IEntityWithRelationships entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var relationshipManager = entity.RelationshipManager;

            var relatedEnd = relationshipManager.GetAllRelatedEnds()
                .FirstOrDefault();

            if (relatedEnd == null)
                throw new Exception($"[{entity.GetType()}] No relationships found to locate on ObjectContext.");

            var query = relatedEnd.CreateSourceQuery() as ObjectQuery;

            if (query == null)
                throw new Exception($"[{entity.GetType()}] The Entity is Detached and no ObjectContext could therefore be found.");

            return query.Context;

        }

        public static IList<LookupEntity> ToLookupList<TEntity>(this IList<TEntity> @this)
            where TEntity : class, IEntity
        {
            return @this.Select(item => new LookupEntity {Id = item.Id, Text = item.Text}).ToList();
        }
    }
}