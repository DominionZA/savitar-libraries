﻿using System;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Savitar.Web
{
    public static class PageContent
    {
        public static string GetPageContent(string url)
        {
            try
            {
                var objWebClient = new WebClient();
                var html = objWebClient.DownloadData(url);
                var utf8 = new UTF8Encoding();
                var result = utf8.GetString(html);
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show($@"An exception occurred while communicating with your Plex server.\r\n\r\n{ex.Message}", @"Communication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
