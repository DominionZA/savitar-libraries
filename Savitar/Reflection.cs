﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Savitar
{
    public static class Reflection
    {
        public static bool TryConstruct<T>(Type[] argumentTypes,
                                        object[] argumentValues,
                                        out T constructedInstance)
            where T : class
        {
            ConstructorInfo constructor;

            if (HasPublicConstructor<T>(argumentTypes, out constructor))
            {
                constructedInstance = constructor.Invoke(argumentValues) as T;
                return true;
            }

            constructedInstance = null;
            return false;
        }

        public static bool HasPublicConstructor<T>(Type[] argumentTypes,
                                                   out ConstructorInfo constructor)
        {
            Type hostType = typeof(T);
            constructor = hostType.GetConstructor(BindingFlags.Instance | BindingFlags.Public,
                                                  null,
                                                  argumentTypes,
                                                  null);

            return constructor != null;
        }

        public static bool PropertyExists(Type hostType, string propertyName)
        {
            return hostType.GetProperty(propertyName) != null;
        }

        public static FieldInfo FindFieldInTypeHierarchy(Type leafType, Func<FieldInfo, bool> isMatch)
        {
            Type searchingIn = leafType;
            FieldInfo matchingField = null;

            while (searchingIn != null
                   && matchingField == null)
            {
                FieldInfo[] allFields = searchingIn.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                matchingField = allFields.FirstOrDefault(isMatch);

                searchingIn = searchingIn.BaseType;
            }

            return matchingField;
        }

        public static PropertyInfo ExtractProperty(Expression expression)
        {
            LambdaExpression lambda = expression as LambdaExpression;
            if (lambda == null)
                return null;

            MemberExpression memberExpression = null;
            if (lambda.Body is UnaryExpression)
            {
                UnaryExpression unaryExpression = lambda.Body as UnaryExpression;
                if (unaryExpression != null)
                    memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                memberExpression = lambda.Body as MemberExpression;
            }

            if (memberExpression == null)
                return null;

            return (memberExpression.Member as PropertyInfo);
        }

        //public static string ExtractPropertyName<T>(Expression<Func<T, object>> selector)
        //{
        //    string path = new PropertyPathVisitor().GetPropertyPath(selector);
        //    return path;
        //}

        public static string ExtractPropertyName(Expression expression)
        {
            PropertyInfo property = ExtractProperty(expression);
            return property == null
                    ? null
                    : property.Name;
        }

        [DebuggerStepThrough]
        public static PropertyInfo GetProperty<T>(string propertyName)
        {
            return GetProperty(typeof(T), propertyName);
        }

        public static PropertyInfo GetProperty(Type type, string propertyName)
        {
            // Method only used for syntactic sugar.
            return type.GetProperty(propertyName);
        }
    }
}
