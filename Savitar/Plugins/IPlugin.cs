﻿namespace Savitar.Plugins
{
    public interface IPlugin
    {
        string Name { get; }
        void PerformAction(IPluginContext context);
    }

    public interface IPluginContext
    {
        string CurrentDocumentText { get; set; }
    }
}