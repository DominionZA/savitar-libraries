﻿using System;
using System.Configuration;
using System.Linq;
using System.Xml;

namespace Savitar.Plugins
{
    public class PluginSectionHandler : IConfigurationSectionHandler
    {
        public PluginSectionHandler()
        {
        }

        public object Create(object parent, object configContext, XmlNode section)
        {
            var result = new Plugins();
            result.AddRange((from XmlNode node in section.ChildNodes
                select node.Attributes["type"].Value
                into pluginClass
                select Type.GetType(pluginClass, true)
                into pluginType
                select Activator.CreateInstance(pluginType)
                into plugObject
                select plugObject).Cast<IPlugin>());

            return result;
        }
    }
}