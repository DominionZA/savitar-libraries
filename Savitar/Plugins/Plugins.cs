﻿using System.Collections.Generic;

namespace Savitar.Plugins
{
    public class Plugins : List<IPlugin>
    {
        public static Plugins Get()
        {
            return (Plugins)System.Configuration.ConfigurationManager.GetSection("Plugins");
        }
    }
}