﻿using System;
using System.Linq;

namespace Savitar.IO
{
    public class Path
    {
        public static string GetWithSlash(string path)
        {
            if (String.IsNullOrEmpty(path))
                return "";

            if (path[path.Length - 1] != '\\')
                path += '\\';

            return path;
        }

        public static string GetFilePath(string path, string fileName)
        {
            return GetWithSlash(path) + fileName;
        }

        public static void FixPath(ref string path)
        {
            if (String.IsNullOrEmpty(path))
                return;

            // Remove multiple trailing backslashes
            while (path.Last() == '\\')
                path = path.Remove(path.Length - 1);

            //if (path[path.Length - 1] != '\\')
            path = path + '\\';
        }

        public static string ValidatePath(string path)
        {
            if (String.IsNullOrEmpty(path))
                return path;

            // Remove multiple trailing backslashes
            while (path.Last() == '\\')
                path = path.Remove(path.Length - 1);


            return path + '\\';
        }
    }
}