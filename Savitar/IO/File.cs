﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Savitar.IO
{
    public class File
    {
        public static void ShellExecute(string fileName)
        {
            ShellExecute(fileName, "");
        }

        public static void ShellExecute(string fileName, string args)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = "Explorer.exe";
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Arguments = args;
            proc.Start();
        }

        public static void FixExtension(ref string extension)
        {
            if (String.IsNullOrEmpty(extension))
                return;

            if (extension[0] != '.')
                extension = '.' + extension;
        }

        public static IEnumerable<string> Search(string root, string searchPattern, Func<FileInfo, bool> where = null)
        {
            var dirs = new Queue<string>();
            dirs.Enqueue(root);
            while (dirs.Count > 0)
            {
                string dir = dirs.Dequeue();

                // files
                var paths = new List<string>();
                var directoryInfo = new DirectoryInfo(dir);

                foreach (string fileMask in searchPattern.Split(';'))
                {
                    List<string> foundFiles;
                    if (where == null)
                        foundFiles = directoryInfo.GetFiles(fileMask).Select(x => x.FullName).ToList();
                    else
                        foundFiles = directoryInfo.GetFiles(fileMask).Where(where).Select(x => x.FullName).ToList();

                    paths.AddRange(foundFiles);
                }

                if (paths.Count > 0)
                {
                    foreach (string file in paths)
                        yield return file;
                }

                // sub-directories
                paths = new List<string>();
                paths.AddRange(System.IO.Directory.GetDirectories(dir));

                if (paths.Count > 0)
                {
                    foreach (string subDir in paths)
                        dirs.Enqueue(subDir);
                }
            }
        }

        //public static IEnumerable<string> Search(string root, string searchPattern)
        //{
        //    var dirs = new Queue<string>();
        //    dirs.Enqueue(root);
        //    while (dirs.Count > 0)
        //    {
        //        string dir = dirs.Dequeue();

        //        // files
        //        var paths = new List<string>();
        //        try
        //        {
        //            foreach (string fileMask in searchPattern.Split(';'))
        //            {
        //                paths.AddRange(System.IO.Directory.GetFiles(dir, fileMask));
        //            }
        //        }
        //        catch { } // swallow

        //        if (paths.Count > 0)
        //        {
        //            foreach (string file in paths)
        //                yield return file;
        //        }

        //        // sub-directories
        //        paths = new List<string>();
        //        try
        //        {
        //            paths.AddRange(System.IO.Directory.GetDirectories(dir));
        //        }
        //        catch { } // swallow

        //        if (paths.Count > 0)
        //        {
        //            foreach (string subDir in paths)
        //                dirs.Enqueue(subDir);
        //        }
        //    }
        //}
    }
}
