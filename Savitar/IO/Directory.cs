using System;

namespace Savitar.IO
{
    public class Directory
    {
        [Obsolete("System.IO.Directory.CreateDirectory will now do this for you by default.")]
        public static void ForceDirectories(string path)
        {
            var folders = path.Split('\\');

            var currentPath = "";
            foreach (var folder in folders)
            {
                if (!String.IsNullOrEmpty(currentPath))
                    currentPath += "\\";
                
                currentPath += folder;

                if (!System.IO.Directory.Exists(currentPath))
                    System.IO.Directory.CreateDirectory(currentPath);
            }
        }

        public static string ValidateFolder(string folder)
        {
            if (String.IsNullOrEmpty(folder))
                return folder;

            if (folder[folder.Length - 1] != '\\')
                folder += '\\';

            return folder;
        }        
    }
}
