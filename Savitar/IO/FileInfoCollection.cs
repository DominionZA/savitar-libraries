using System;
using System.Collections.Generic;
using System.IO;

namespace Savitar.IO
{
    public class FileInfoCollection : List<FileInfo>
    {
        public FileInfoCollection()
        {
        }

        public FileInfoCollection(IEnumerable<FileInfo> items)
        {
            AddRange(items);
        }

        public FileInfoCollection(string[] files)
        {
            this.Load(files);
        }

        public void Load(string[] files)
        {
            foreach (string file in files)
                this.Add(new FileInfo(file));
        }

        public new void Sort()
        {
            base.Sort(new FileInfoNameComparer());
        }
    }

    internal class FileInfoNameComparer : IComparer<FileInfo>
    {
        public int Compare(FileInfo x, FileInfo y)
        {
            if (x == null)
                throw new Exception($"{nameof(x)} cannot be null");
            if (y == null)
                throw new Exception($"{nameof(y)} cannot be null");

            return string.Compare(x.Name, y.Name, StringComparison.OrdinalIgnoreCase);
        }
    }
}