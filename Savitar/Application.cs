using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar
{
  public class Application
  {
    static public string ExeName
    {
      get { return System.IO.Path.GetFileName(Savitar.Application.FullPath); }
    }

    static public string FullPath
    {
      get
      {
        System.Reflection.Module[] modules = System.Reflection.Assembly.GetExecutingAssembly().GetModules();
        return modules[0].FullyQualifiedName;
      }
    }

    // Returns the folder that the application is executing from
    static public string Folder
    {
      get
      {
        string path = System.IO.Path.GetDirectoryName(Savitar.Application.FullPath);

        if ((path != "") && (path[path.Length - 1] != '\\'))
          path += '\\';

        return path;
      }
    }
  }
}
