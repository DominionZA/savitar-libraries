﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.Net
{
  public class HTTPParameter
  {
    public string Name { get; set; }
    public string Value { get; set; }
  }

  public class HTTPParameters : List<HTTPParameter>
  {
    public HTTPParameter Add(string name, string value)
    {
      HTTPParameter param = new HTTPParameter();
      param.Name = name;
      param.Value = value;

      this.Add(param);

      return param;
    }

    public override string ToString()
    {
      string result = "";

      foreach (HTTPParameter param in this)
      {
        if (!String.IsNullOrEmpty(result))
          result += "&";

        result += String.Format("{0}={1}", param.Name, param.Value);
      }

      return result;
    }
  }
}
