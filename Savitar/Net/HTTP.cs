﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.Net
{
  public enum HttpMethod { POST, GET }

  public class HTTP
  {
    private string URI { get; set; }
    private System.Net.CookieContainer CookieContainer { get; set; }
    private HttpMethod Method { get; set; }
 
    public HTTPParameters Parameters
    {
      get { return _Parameters; }
    } HTTPParameters _Parameters = new HTTPParameters();

    /// <summary>
    /// Performs post/get operations.
    /// </summary>
    /// <param name="uri">The URI to post/get to</param>
    /// <param name="cookieCollection">NULL if cookies are to be ignored</param>
    public HTTP(string uri, System.Net.CookieContainer cookieContainer, HttpMethod method)
    {
      this.URI = uri;
      this.CookieContainer = cookieContainer;
      this.Method = method;
    }

    public System.Net.HttpWebResponse GetResponse()
    {
      System.Net.ServicePointManager.Expect100Continue = false;

      string parameters = this.Parameters.ToString();

      string uri = this.URI;
      if ((this.Method == HttpMethod.GET) && (!String.IsNullOrEmpty(parameters)))
        uri += String.Format("?{0}", System.Web.HttpUtility.UrlEncode(parameters));
      
      System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(uri);
      request.ContentType = "application/x-www-form-urlencoded";
      request.CookieContainer = this.CookieContainer;

      request.KeepAlive = true;
      request.Timeout = 300000;
      request.AllowAutoRedirect = false;
      request.Method = this.Method.ToString();
      request.Referer = "http://www.rchq.co.za/login.php";
      request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3 (.NET CLR 3.5.30729)";
      request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

      if (this.Method == HttpMethod.POST)
      {
        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(parameters);
        request.ContentLength = bytes.Length;

        System.IO.Stream reqStream = request.GetRequestStream();
        reqStream.Write(bytes, 0, bytes.Length);
        reqStream.Close();
      }      

      System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();

      int i = this.CookieContainer.Count;

      return response;
    }

    public string GetString()
    {
      System.Net.WebResponse response = GetResponse();

      if (response == null)
        return null;

      System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream());

      return System.Web.HttpUtility.HtmlDecode(sr.ReadToEnd().Trim());
    }
  }
}