﻿namespace Savitar.Net
{
    public class Mail
    {
        public static string DefaultFrom { get; set; }
        public static string DefaultSubjectPrefix { get; set; }

        public static void Send(string from, string to, string subject, string body)
        {
            Send(from, to, subject, body, true);
        }

        public static void Send(string from, string to, string subject, string body, bool isBodyHtml)
        {
            var mail = new System.Net.Mail.MailMessage();
            mail.To.Add(to);
            mail.From = new System.Net.Mail.MailAddress(from);
            mail.Subject = string.IsNullOrEmpty(DefaultSubjectPrefix) ? subject : "[" + DefaultSubjectPrefix + "] " + subject;
            mail.Body = body;
            mail.IsBodyHtml = isBodyHtml;

            Send(mail);
        }

        public static void Send(string to, string subject, string body)
        {
            Send(to, subject, body, true);
        }

        public static void Send(string to, string subject, string body, bool isBodyHtml)
        {
            var mail = new System.Net.Mail.MailMessage();
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = isBodyHtml;

            Send(mail);
        }

        public static void Send(System.Net.Mail.MailMessage mailMessage)
        {
            var smtpClient = new System.Net.Mail.SmtpClient();
            smtpClient.Send(mailMessage);
        }
    }
}