﻿using System;

namespace Savitar
{
    public class SavDateTime
    {
        public static DateTime DDMMYYYYToDate(string value)
        {
            if (value == "")
                return DateTime.MinValue;

            var day = Convert.ToInt32(value.Substring(0, 2));
            var month = Convert.ToInt32(value.Substring(3, 2));
            var year = Convert.ToInt32(value.Substring(6, 4));

            return new DateTime(year, month, day);
        }

        public static DateTime YYYYMMDDToDate(string value)
        {
            if (value == "")
                return DateTime.MinValue;

            var day = Convert.ToInt16(value.Substring(6, 2));
            var month = Convert.ToInt16(value.Substring(4, 2));
            var year = Convert.ToInt16(value.Substring(0, 4));

            return new DateTime(year, month, day);
        }

        public static DateTime YYYYMMDDHHMMSSToDateTime(string value)
        {
            if (value == "")
                return DateTime.MinValue;

            var year = Convert.ToInt16(value.Substring(0, 4));
            var month = Convert.ToInt16(value.Substring(4, 2));
            var day = Convert.ToInt16(value.Substring(6, 2));

            var iHour = Convert.ToInt16(value.Substring(8, 2));
            var iMin = Convert.ToInt16(value.Substring(10, 2));
            var iSec = Convert.ToInt16(value.Substring(12, 2));

            return new DateTime(year, month, day, iHour, iMin, iSec);
        }

        public struct YearMonth
        {
            public int Year;
            public int Month;
        }

        public static DateTime NowMonthStart()
        {
            return DateTime.Now.AddDays((DateTime.Now.Day - 1) * -1);
        }

        public static DateTime NowMonthEnd()
        {
            return DateTime.Now.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day);
        }

        public static YearMonth DateTimeToYearMonth(DateTime value)
        {
            YearMonth result;
            result.Year = value.Year;
            result.Month = value.Month;
            return result;
        }

        public static YearMonth GetNextYearMonth(YearMonth yearMonth)
        {
            YearMonth result;

            result.Year = yearMonth.Year;
            result.Month = yearMonth.Month + 1;

            if (result.Month > 12)
            {
                result.Month = 1;
                result.Year += 1;
            }

            return result;
        }

        public static YearMonth GetPrevYearMonth(YearMonth yearMonth)
        {
            YearMonth result;
            result.Year = yearMonth.Year;
            result.Month = yearMonth.Month - 1;
            if (result.Month >= 1) 
                return result;
            
            result.Month = 12;
            result.Year -= 1;
            return result;
        }

        public static YearMonth NowToYearMonth()
        {
            return DateTimeToYearMonth(DateTime.Now);
        }
    }
}
