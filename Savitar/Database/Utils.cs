namespace Savitar.Database
{
    public class Utils
    {
        public static string GetConnectionString(string Host, string Username, string Password, string Database)
        {
            var s = "Data Source={0};Persist Security Info=True;User ID={1};Password={2};Initial Catalog={3};";
            return string.Format(s, Host, Username, Password, Database);
        }

        public static string GetConnectionString(string Host, string Database)
        {
            var s = "Data Source={0};Initial Catalog={1};Integrated Security=True";
            return string.Format(s, Host, Database);
        }
    }
}