namespace Savitar.Database
{
	public class Properties
	{
		public string Host;
		public string Username;
		public string Password;
		public string Database;

		public override string ToString()
		{
			return Utils.GetConnectionString(this.Host, this.Username, this.Password, this.Database);
		}
	}	
}
