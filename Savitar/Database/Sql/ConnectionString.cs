using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Savitar.Database.Sql
{
  public enum AuthenticationType { SQL, Windows }
  public class ConnectionString
  {
    public string ServerName
    {
      get => _ServerName;
        set => _ServerName = value;
    } string _ServerName = "";

    public AuthenticationType AuthType
    {
      get => _AuthType;
        set => _AuthType = value;
    } AuthenticationType _AuthType = AuthenticationType.Windows;

    public string Login
    {
      get => _Login;
        set => _Login = value;
    } string _Login = "";

    public string Password
    {
      get => _Password;
        set => _Password = value;
    } string _Password = "";

    public string Database
    {
      get => _Database;
        set => _Database = value;
    } string _Database = "";

    public override string ToString()
    {
      if (this.AuthType == AuthenticationType.SQL)
        return String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};", this.ServerName, this.Database, this.Login, this.Password);
      else
        return String.Format("Data Source={0};Initial Catalog={1};Integrated Security=True", this.ServerName, this.Database);
    }

    private string GetValue(System.Collections.Specialized.NameValueCollection nameValueCollection, string name)
    {
      if (nameValueCollection[name] == null)
        return "";

      return nameValueCollection[name];
    }

    public void FromString(string connectionString)
    {
      System.Collections.Specialized.NameValueCollection nameValues = new System.Collections.Specialized.NameValueCollection();
      string[] parts = connectionString.Split(';');
      foreach (string part in parts)
      {
        string[] nv = part.Split('=');
        if (nv.Length != 2)
          continue;

        nameValues.Add(nv[0], nv[1]);
      }

      this.ServerName = this.GetValue(nameValues, "Data Source");
      this.Database = this.GetValue(nameValues, "Initial Catalog");
      this.Login = this.GetValue(nameValues, "User ID");
      this.Password = this.GetValue(nameValues, "Password");
      this.AuthType = this.GetValue(nameValues, "Integrated Security").ToLower() == "true" ? AuthenticationType.Windows : AuthenticationType.SQL;
    }

    public static string Get(string sectionName, string defaultValue)
    {
      string result = defaultValue;

      EnsureConnectionStringsSection();

      // If there is no connection string as yet, then create one using the default value.
      if (System.Configuration.ConfigurationManager.ConnectionStrings[sectionName] == null)
        Savitar.Database.Sql.ConnectionString.Set(sectionName, defaultValue);      

      return System.Configuration.ConfigurationManager.ConnectionStrings[sectionName].ConnectionString;
    }

    /// <summary>
    /// Sets a connectionString in the config file. Application will need a restart if this is changed.
    /// </summary>
    /// <param name="sectionName">The connection string section name</param>
    /// <param name="connectionString">The connection string its-self</param>
    public static void Set(string sectionName, string connectionString)
    {
      EnsureConnectionStringsSection();

      System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
      ConnectionStringsSection connectionStringsSection = config.ConnectionStrings;

      if (connectionStringsSection.ConnectionStrings[sectionName] == null)
        connectionStringsSection.ConnectionStrings.Add(new ConnectionStringSettings(sectionName, connectionString));
      else
        connectionStringsSection.ConnectionStrings[sectionName].ConnectionString = connectionString;

      config.Save();

      System.Configuration.ConfigurationManager.RefreshSection("connectionStrings");
    }

    public static void EnsureConnectionStringsSection()
    {
      System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

      System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
      doc.Load(config.FilePath);
      System.Xml.XmlNode node = doc.DocumentElement["connectionStrings"];

      if (node == null)
        throw new Exception(String.Format("Cannot locate a 'connectionStrings' section in '{0}'", config.FilePath));
      
      // Check to see if we use an external file for the connectionStrings.
      if (node.Attributes["configSource"] != null)
      {
        // We do, get the filename
        string fileName = node.Attributes["configSource"].Value;
        fileName = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\DB.Config";
        // Check the file exists. If not, create it with the default tags.
        if (!System.IO.File.Exists(fileName))
        {
          System.Text.StringBuilder sb = new System.Text.StringBuilder();
          sb.AppendLine(@"<connectionStrings>");
          sb.AppendLine(@"</connectionStrings>");

          System.IO.File.WriteAllText(fileName, sb.ToString());

          System.Configuration.ConfigurationManager.RefreshSection("connectionStrings");
        }
      }
    }
  }
}
