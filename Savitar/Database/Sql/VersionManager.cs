using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Savitar.Database.Sql
{
  public class VersionManager : System.ComponentModel.Component
  {
    public Version ApplicationVersion => _AppVersion;
      Version _AppVersion = new Version(System.Windows.Forms.Application.ProductVersion.ToString());

    public Version DatabaseVersion => _DatabaseVersion;
      Version _DatabaseVersion = new Version("0.0.0.0");

    public Savitar.IO.FileInfoCollection AllFiles => _AllFiles;
      Savitar.IO.FileInfoCollection _AllFiles = new Savitar.IO.FileInfoCollection();

    public Savitar.IO.FileInfoCollection NewFiles => _NewFiles;
      Savitar.IO.FileInfoCollection _NewFiles = new Savitar.IO.FileInfoCollection();

    public bool IsUpgradeAvailable => this.NewFiles.Count > 0;

      public delegate void StatusEventHander(string comment, bool isError);
    public delegate void StartEventHandler();
    public delegate void FinishEventHandler(Version fileVersion);

    public event StatusEventHander Status;
    public event StartEventHandler Start;
    public event FinishEventHandler Finish;

    protected void DoStatusEvent(string comment)
    {
      this.DoStatusEvent(comment, false);
    }

    protected void DoStatusEvent(string comment, bool isError)
    {
      if (Status != null)
        Status(comment, isError);
    }

    public string ConnectionString
    {
      get => _ConnectionString;
        set => _ConnectionString = value;
    } string _ConnectionString = "";

    public string ScriptsPath
    {
      get => _ScriptsPath;
        set => _ScriptsPath = value;
    } string _ScriptsPath = "";

    public VersionManager()
    {

    }

    public VersionManager(string connectionString, string scriptsPath)
    {
      this.ConnectionString = connectionString;
      this.ScriptsPath = scriptsPath;

      Refresh();
    }

    protected void Reset()
    {
      _DatabaseVersion = new Version("0.0.0.0");
      _AllFiles.Clear();
      _NewFiles.Clear();
    }

    public Version ConvertFileNameToVersion(string fileName)
    {
      try
      {
        return new Version(Path.GetFileNameWithoutExtension(fileName));
      }
      catch
        {
          throw new InvalidOperationException(String.Format("Unable to convert the filename {0} to a Version object. Expected format is x.x.x.x.txt", fileName));
        }
    }

    public void Refresh()
    {
      Reset();

      SqlConnection con = new SqlConnection(this.ConnectionString);
      con.Open();

      _DatabaseVersion = this.GetDBVersion(con);
      if (_DatabaseVersion == null)
      {
        DoStatusEvent("Checking the database is prepared for upgrades");
        RunScript(con, Savitar.Properties.Resources.Settings_Table, null);
        _DatabaseVersion = this.GetDBVersion(con);
      }

      // Get all available files in the scripts folder.            
      if (!Directory.Exists(this.ScriptsPath))
        throw new Exception(this.ScriptsPath + " does not exist");


      this.AllFiles.Load(Directory.GetFiles(this.ScriptsPath, "*.txt"));
      _AllFiles.Sort(new VersionComparer());

      // Get all new files (need to be executed)
      SetNewFiles();
    }

    protected void SetNewFiles()
    {
      Version fileVersion = null;
      foreach (FileInfo fileInfo in _AllFiles)
      {
        fileVersion = ConvertFileNameToVersion(fileInfo.Name);

        if (fileVersion > _DatabaseVersion)
          _NewFiles.Add(fileInfo);
      }

      if (_NewFiles.Count > 1)
        _NewFiles.Sort(new VersionComparer());
    }


    public Version Execute()
    {
      string script = "";

      try
      {
        if (String.IsNullOrEmpty(this.ConnectionString))
          throw new InvalidOperationException("Cannot execute database upgrades as the connection string has not been set");

        if (this.Start != null)
          this.Start();

        try
        {
          DoStatusEvent("Opening database connection");
          SqlConnection sqlConnection = new SqlConnection();
          sqlConnection.ConnectionString = this.ConnectionString;
          sqlConnection.Open();

          if (this.NewFiles.Count == 0)
            return _DatabaseVersion;

          Version fileVersion = null;
          foreach (FileInfo fileInfo in this.NewFiles)
          {
            try
            {
              fileVersion = new Version(Path.GetFileNameWithoutExtension(fileInfo.Name));
            }
            catch
            {
              throw new InvalidOperationException(String.Format("Unable to convert the filename {0} to a Version object. Expected format is x.x.x.x", fileInfo.Name));
            }

            DoStatusEvent("Have upgrade script. Upgrading to version: " + fileVersion.ToString());
            StreamReader reader = fileInfo.OpenText();
            try
            {
              script = reader.ReadToEnd();
              RunScript(sqlConnection, script, fileVersion);
              script = "";

              this.UpdateDBVersion(sqlConnection, fileVersion);
              _DatabaseVersion = fileVersion;
            }
            finally
            {
              reader.Close();
            }
          }

          DoStatusEvent("Complete");
        }
        finally
        {
          if ((this.Finish != null) && (_DatabaseVersion != null))
            this.Finish(_DatabaseVersion);
        }
      }
      catch (Exception ex)
      {
        if (this.Status != null)
        {
          if (!String.IsNullOrEmpty(script))
            this.Status(ex.Message + "\r\n\r\n" + script, true);
          else
            this.Status(ex.Message, true);

          return _DatabaseVersion;
        }
        else
        {
          if (!String.IsNullOrEmpty(script))
            throw new Exception(ex.Message + "\r\n\r\n" + script);
        }
      }

      return _DatabaseVersion;
    }

    protected void UpdateDBVersion(SqlConnection sqlConnection, Version version)
    {
      SqlCommand sqlCommand = new SqlCommand(String.Format("EXEC spSetSetting 'Database', 'Version', '{0}'", version), sqlConnection);
      sqlCommand.ExecuteNonQuery();

      DoStatusEvent(String.Format("Database version updated to {0}", version));
    }

    public void RunScript(SqlConnection sqlConnection, string script, Version fileVersion)
    {
      //SqlTransaction sqlTransaction = sqlConnection.BeginTransaction("RunUpgradeScript");

      try
      {
        string[] commands = ParseScriptToCommands(script);
        if (fileVersion == null)
          DoStatusEvent("Executing script");
        else
          DoStatusEvent(String.Format("Executing script for version {0}", fileVersion));

        SqlCommand sqlCommand = new SqlCommand();
        sqlCommand.CommandTimeout = 500;
        //sqlCommand.Transaction = sqlTransaction;
        sqlCommand.Connection = sqlConnection;

        foreach (string command in commands)
        {
          if (String.IsNullOrEmpty(command))
            continue;

          string[] comments = ParseCommandToComments(command);
          foreach (string comment in comments)
            DoStatusEvent(String.Format("{0} : {1}", fileVersion, comment));

          sqlCommand.CommandText = command;
          try
          {
            sqlCommand.ExecuteNonQuery();
          }
          catch (Exception ex)
          {
#if (DEBUG)
            MessageBox.Show(String.Format("FileVersion: {0}\r\n\r\n{1}\r\n\r\n{2}", fileVersion, ex.Message, command));
#endif
            throw;
          }
        }

        //sqlTransaction.Commit();
      }
      catch
      {
        try
        {
          //sqlTransaction.Rollback("RunUpgradeScript");
        }
        catch { }

        throw;
      }
    }

    public Version GetDBVersion(SqlConnection sqlConnection)
    {
      SqlCommand sqlCommand = new SqlCommand("EXEC spGetSetting 'Database', 'Version', '0.0.0.0'", sqlConnection);

      string version = Convert.ToString(sqlCommand.ExecuteScalar());

      return String.IsNullOrEmpty(version) ? null : new Version(version);
    }

    public string[] ParseScriptToCommands(string strScript)
    {
      return Regex.Split(strScript, "GO\r\n", RegexOptions.IgnoreCase);
    }

    public string[] ParseCommandToComments(string command)
    {
      StringBuilder sb = new StringBuilder();

      //string[] result = Regex.Split(command, "(---[^\\.\\?\\!]*)[\r\n]", RegexOptions.IgnoreCase);
      string[] lines = command.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
      for (int i = 0; i < lines.Length; i++)
      {
        string line = lines[i].Trim();
        if (line.StartsWith("---"))
          sb.AppendLine(line.Substring(2).Trim());
      }

      return sb.ToString().Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
    }
  }

  internal class VersionComparer : System.Collections.Generic.IComparer<System.IO.FileInfo>
  {
    public int Compare(System.IO.FileInfo x, System.IO.FileInfo y)
    {
      try
      {
        Version versionX = new Version(Path.GetFileNameWithoutExtension(x.Name));
        Version versionY = new Version(Path.GetFileNameWithoutExtension(y.Name));

        if (versionX < versionY)
          return -1;
        else if (versionX > versionY)
          return 1;
        else
          return 0;
      }
      catch
      {
        return 0;
      }
    }
  }
}