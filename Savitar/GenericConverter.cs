﻿using System;

namespace Savitar
{
    public static class GenericConverter
    {
        public static string ToString<T>(T sourceValue)
        {
            return (string)Convert.ChangeType(sourceValue, typeof(string));
        }

        public static T Parse<T>(string sourceValue) where T : IConvertible
        {
            return (T)Convert.ChangeType(sourceValue, typeof(T));
        }

        /// <summary>
        /// Converts a string value to the T type using the passed provider
        /// </summary>
        /// <typeparam name="T">The type to convert to. Ex: DateTime</typeparam>
        /// <param name="sourceValue">The string value to convert. Ex: 22/02/2010</param>
        /// <param name="provider">The provider to use to permorm the conversion. Ex: IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);</param>
        /// <returns></returns>
        public static T Parse<T>(string sourceValue, IFormatProvider provider) where T : IConvertible
        {
            return (T)Convert.ChangeType(sourceValue, typeof(T), provider);
        }
    }
}