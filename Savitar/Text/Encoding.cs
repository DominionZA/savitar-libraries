using System;

namespace Savitar.Text
{
    public class Encoding<T>
    {
        public static string StringToBase64(string data)
        {
            try
            {
                var encDataByte = System.Text.Encoding.UTF8.GetBytes(data);
                var encodedData = Convert.ToBase64String(encDataByte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Error in base 64 encoding", ex);
            }
        }

        public static string ObjectToBase64(T obj)
        {
            var serializedStr = Object.SerializeToString(obj);

            return StringToBase64(serializedStr);
        }

        public static string Base64ToString(string base64Data)
        {
            try
            {
                var encoder = new System.Text.UTF8Encoding();
                var utf8Decode = encoder.GetDecoder();

                var toDecodeByte = Convert.FromBase64String(base64Data);
                var charCount = utf8Decode.GetCharCount(toDecodeByte, 0, toDecodeByte.Length);
                var decodedChar = new char[charCount];
                utf8Decode.GetChars(toDecodeByte, 0, toDecodeByte.Length, decodedChar, 0);
                var result = new string(decodedChar);
                return result;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Error in FromBase64(base64Data)", ex);
            }
        }

        public static T Base64ToObject(string base64Data)
        {
            // First decode to a normal string
            var decodedStr = Savitar.Text.Encoding<T>.Base64ToString(base64Data);

            // Now try deserialize the data
            return (T)Object.Deserialize(decodedStr, typeof(T));
        }
    }
}