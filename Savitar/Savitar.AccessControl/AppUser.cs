﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace Savitar.AccessControl
{
  public class AppUser : IAppUser
  {
    #region IAppUser Members
    private bool isEmailChanged = false;

    public int UsersID { get; set; }
    public Guid UserID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }    
    public string Email 
    {
      get { return _Email; }
      set
      {
        if (_Email == value)
          return;

        _Email = value;
        isEmailChanged = this.UsersID > 0;
      }
    } string _Email = "";
    public string Password { get; set; }

    public string DisplayName
    {
      get { return FirstName + " " + LastName; }
    }

    public bool RememberMe { get; set; }

    public void Clear()
    {
      this.UsersID = 0;
      this.UserID = Guid.Empty;
      this.FirstName = "";
      this.LastName = "";
      this.Email = "";
      this.RememberMe = false;
    }

    public void Load(DAL.AspnetMembership user)
    {
      this.Clear();

      if (user.IsNew)
        throw new Exception("Unable to Load user as the user is unknown");

      this.UsersID = user.UsersID;
      this.UserID = user.UserId;
      this.FirstName = user.FirstName;
      this.LastName = user.LastName;
      this.Email = user.Email;

      MembershipUser membershipUser = Membership.GetUser(user.UserId);
      this.Password = membershipUser.GetPassword();
    }

    public void Load(int userID)
    {
      this.Clear();

      if (userID == 0)
        return;

      DAL.AspnetMembership item = new Savitar.AccessControl.DAL.AspnetMembership();
      item.LoadByParam(DAL.AspnetMembership.Columns.UsersID, userID);
      Load(item);
    }

    public void Load(Guid userID)
    {
      this.Clear();

      if (userID == Guid.Empty)
        return;

      DAL.AspnetMembership item = new Savitar.AccessControl.DAL.AspnetMembership();
      item.LoadByParam(DAL.AspnetMembership.Columns.UserId, userID);
      Load(item);
    }

    #endregion

    public void Save()
    {
      bool isNew = false;

      DAL.AspnetUsers user = new Savitar.AccessControl.DAL.AspnetUsers();
      if (this.UserID != Guid.Empty)
        user.LoadByParam(DAL.AspnetUsers.Columns.UserId, this.UserID);

      if (user.IsNew) // Create the user if new.
      {
        isNew = true;
        AppUser appUser = AppUser.CreateUser(this.Email, this.Password);
        user.LoadByParam(DAL.AspnetUsers.Columns.UserId, appUser.UserID);
      }
      else
      {
        if (user.UserName != this.Email)
        {
          user.UserName = this.Email;
          user.LoweredUserName = this.Email.ToLower();
          user.Save();
        }
      }

      // Set the membership properties for the abover user.
      DAL.AspnetMembership membership = new Savitar.AccessControl.DAL.AspnetMembership();
      membership.LoadByParam(DAL.AspnetMembership.Columns.UserId, user.UserId);
      membership.FirstName = this.FirstName;
      membership.LastName = this.LastName;
      membership.Email = this.Email;
      membership.LoweredEmail = this.Email.ToLower();
      membership.Save();

      if (isNew)
        this.Load(user.UserId);
      else
      {
        // Now do the password.
        MembershipUser membershipUser = Membership.GetUser(user.UserId);
        membershipUser.ChangePassword(membershipUser.GetPassword(), this.Password);
      }
    }

    public static AppUser CreateUser(string email, string password)
    {
      MembershipUser user = Membership.CreateUser(email, password, email);
      
      AppUser appUser = new AppUser();
      appUser.Load(new Guid(user.ProviderUserKey.ToString()));
      return appUser;
    }

    public static void ChangePassword(Guid userID, string oldPassword, string newPassword)
    {
      MembershipUser user = Membership.GetUser(userID);
      
      user.ChangePassword(oldPassword, newPassword);
    }
  }
}
