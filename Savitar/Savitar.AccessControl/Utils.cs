﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace Savitar.AccessControl
{
  public class Utils
  {
    public static IAppUser GetUser(Gizmox.WebGUI.Common.Interfaces.IContext context)
    {
      if (context.Session["User"] == null)
        return null;

      return (IAppUser)context.Session["User"];
    }
    
    public static bool Login(string email, string password, out string errMsg, IAppUser user)
    {
      if (!Login(email, password, out errMsg))
        return false;

      MembershipUser membershipUser = Membership.GetUser(email, true);
      user.Load((Guid)membershipUser.ProviderUserKey);

      return true;
    }

    public static bool Login(string email, string password, out string errMsg)
    {
      errMsg = null;

      bool isAuthenticated = Membership.ValidateUser(email, password);
      MembershipUser membershipUser = Membership.GetUser(email, isAuthenticated);
      isAuthenticated &= (membershipUser != null);
      
      if (!isAuthenticated)
      {
        errMsg = "The user name or password is incorrect; check Caps Lock key.";

        if (membershipUser != null)
        {
          if (!membershipUser.IsApproved)
            errMsg = "Your account is disabled.";
          else if (membershipUser.IsLockedOut)
            errMsg = "You account is locked out due to the excessive number of failed logon attempts; contact your administrator to unlock your account.";
        }
      }

      return isAuthenticated;
    }
  }
}
