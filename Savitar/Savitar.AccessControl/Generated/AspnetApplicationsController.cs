using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Savitar.AccessControl.DAL
{
    /// <summary>
    /// Controller class for aspnet_Applications
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class AspnetApplicationsController
    {
        // Preload our schema..
        AspnetApplications thisSchemaLoad = new AspnetApplications();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public AspnetApplicationsCollection FetchAll()
        {
            AspnetApplicationsCollection coll = new AspnetApplicationsCollection();
            Query qry = new Query(AspnetApplications.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public AspnetApplicationsCollection FetchByID(object ApplicationId)
        {
            AspnetApplicationsCollection coll = new AspnetApplicationsCollection().Where("ApplicationId", ApplicationId).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public AspnetApplicationsCollection FetchByQuery(Query qry)
        {
            AspnetApplicationsCollection coll = new AspnetApplicationsCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object ApplicationId)
        {
            return (AspnetApplications.Delete(ApplicationId) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object ApplicationId)
        {
            return (AspnetApplications.Destroy(ApplicationId) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string ApplicationName,string LoweredApplicationName,Guid ApplicationId,string Description)
	    {
		    AspnetApplications item = new AspnetApplications();
		    
            item.ApplicationName = ApplicationName;
            
            item.LoweredApplicationName = LoweredApplicationName;
            
            item.ApplicationId = ApplicationId;
            
            item.Description = Description;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string ApplicationName,string LoweredApplicationName,Guid ApplicationId,string Description)
	    {
		    AspnetApplications item = new AspnetApplications();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.ApplicationName = ApplicationName;
				
			item.LoweredApplicationName = LoweredApplicationName;
				
			item.ApplicationId = ApplicationId;
				
			item.Description = Description;
				
	        item.Save(UserName);
	    }
    }
}
