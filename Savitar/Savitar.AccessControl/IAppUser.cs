﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savitar.AccessControl
{
  public delegate void UserLoggedInDelegate(IAppUser user);
  public delegate void UserLoggedOutDelegate();

  public interface IAppUser
  {
    int UsersID { get; set; }
    Guid UserID { get; set; }
    string FirstName { get; set; }
    string LastName { get; set; }
    string Email { get; set; }
    string DisplayName { get; }
    bool RememberMe { get; set; }    

    void Load(int userID);
    void Load(Guid userID);
  }
}
