﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar.Drawing
{
  public class Image
  {
    public static byte[] ToByteArray(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
    {
      System.IO.MemoryStream ms = new System.IO.MemoryStream();
      image.Save(ms, format);
      return ms.ToArray();
    }

    public static System.Drawing.Image ToImage(byte[] value)
    {
      System.IO.MemoryStream ms = new System.IO.MemoryStream(value);
      System.Drawing.Image result = System.Drawing.Image.FromStream(ms);
      return result;
    }
  }
}
