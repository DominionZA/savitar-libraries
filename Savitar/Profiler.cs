﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savitar
{
    public class Execution
    {
        public DateTime Started { get; set; }
        public DateTime Stopped { get; set; }

        public TimeSpan Difference => Stopped - Started;

        public double Seconds
        {
            get
            {
                if ((Started == DateTime.MinValue) || (Stopped == DateTime.MinValue))
                    return -1;

                return Difference.TotalSeconds;
            }
        }

        public double MilliSeconds
        {
            get
            {
                if ((Started == DateTime.MinValue) || (Stopped == DateTime.MinValue))
                    return -1;

                return Difference.TotalMilliseconds;
            }
        }
    }

    public class Method
    {
        public string MethodName { get; set; }
        public DateTime Added { get; set; }
        public List<Execution> Executions { get; set; }
        public Method()
        {
            Executions = new List<Execution>();
            Added = DateTime.Now;
        }

        public void Start()
        {
            Execution item = new Execution();
            item.Started = DateTime.Now;
            Executions.Add(item);
        }

        public void Stop()
        {
            // Find the latest Execution where Stopped = MinDateTime.
            Execution ex = (
              from item in Executions
              orderby item.Started descending
              select item).Take(1).SingleOrDefault();

            if (ex == null)
                throw new Exception("[Stop] Unable to locate an execution point to Stop");

            ex.Stopped = DateTime.Now;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("{0} [{1}] - {2}", Added, MethodName, Executions.Count));

            if (Executions.Count > 0)
            {

                string exStr = "";
                foreach (Execution ex in Executions)
                {
                    if (!String.IsNullOrEmpty(exStr))
                        exStr += ",";

                    if (ex.Stopped != DateTime.MinValue)
                        exStr += String.Format("{0:0.000}", ex.Seconds);
                }

                sb.AppendLine("\t" + exStr);
            }

            return sb.ToString();
        }

    }

    public class Methods : List<Method>
    {
        public Method Find(string methodName)
        {
            return (
              from item in this
              where item.MethodName.Equals(methodName, StringComparison.InvariantCultureIgnoreCase)
              select item).SingleOrDefault();
        }

        public void Start(string methodName)
        {
            // Check if it is in the list first. If so, increment Count, else add it.
            var method = Find(methodName);
            if (method == null)
            {
                method = new Method();
                method.MethodName = methodName;
                method.Start();
                
                Add(method);
            }
            else
                method.Start();
        }

        public void Stop(string methodName)
        {
            var method = Find(methodName);
            if (method == null)
                throw new Exception("[Stop] No MethodName with the name [" + methodName + "] is currently being profiled so cannot be stopped");

            method.Stop();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            var methods = (
              from item in this
              orderby item.Added
              select item).ToList();

            foreach (var method in methods)
                sb.AppendLine(method.ToString());

            return sb.ToString();
        }
    }

    public static class Profiler
    {
        public static Methods Methods => new Methods();

        public new static string ToString()
        {
            return Methods.ToString();
        }

        public static void Clear()
        {
            Methods.Clear();
        }
    }
}
