﻿using System;

namespace Savitar.Extensions
{
    public static class ObjectExtensions
    {
        public static object ToValue(this object value, Type type)
        {
            if (value == null || value == DBNull.Value)
                return null;

            return Convert.ChangeType(value, type);
        }

        public static T ToValue<T>(this object value)
        {
            Type t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (value == null || DBNull.Value.Equals(value)) ? default(T) : (T)Convert.ChangeType(value, t);
        }

        public static T ToValue<T>(this object value, T defaultValue)
        {
            try
            {
                return value.ToValue<T>();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static bool Between(this int @this, int value1, int value2)
        {
            return @this >= value1 && @this <= value2;
        }
    }
}