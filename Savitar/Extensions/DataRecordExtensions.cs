﻿using System;
using System.Data;

namespace Savitar.Extensions
{
    public static class DataRecordExtensions
    {
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (var i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public static T ToValue<T>(this IDataRecord reader, string columnName, T defaultValue)
        {
            return !reader.HasColumn(columnName) ? defaultValue : reader[columnName].ToValue<T>();
        }
    }
}
