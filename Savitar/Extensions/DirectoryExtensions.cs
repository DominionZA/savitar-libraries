﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Savitar.Extensions
{
    public static class DirectoryExtensions
    {
        public static IEnumerable<FileInfo> GetFilesByExtension(this DirectoryInfo dir, SearchOption searchOption, params string[] extensions)
        {
            if (extensions == null)
                throw new ArgumentNullException("extensions");

            var files = new List<FileInfo>();
            foreach (var extension in extensions)
                files.AddRange(dir.GetFiles(extension, searchOption));
            
            return files;
        }
    }
}
