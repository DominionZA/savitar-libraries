﻿using System.Collections.Generic;

namespace Savitar.Extensions
{
    public static class SavExtensions
    {
        public static TDerived ToList<TDerived, T>(this IEnumerable<T> seq) where TDerived : List<T>, new()
        {
            var result = new TDerived();
            result.AddRange(seq);
            return result;
        }
    }
}