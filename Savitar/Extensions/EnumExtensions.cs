﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Savitar.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
                return attributes[0].Description;

            return value.ToString();
        }

        public static TResult ConvertTo<TResult>(this string source)
            where TResult : struct, IConvertible
        {
            if (!typeof(TResult).IsEnum)
                throw new NotSupportedException("TResult must be an Enum");

            return (TResult)Enum.Parse(typeof(TResult), source);
        }

        public static T ConvertToEnum<T>(this string value)
        {
            if (typeof(T).BaseType != typeof(Enum))
                throw new InvalidCastException();
            if (Enum.IsDefined(typeof(T), value) == false)
                throw new InvalidCastException();

            return (T)Enum.Parse(typeof(T), value);
        }

        public static int GetValue<TEnum>(this TEnum element)
            where TEnum : struct
        {
            if (typeof(TEnum).BaseType != typeof(Enum))
                throw new InvalidCastException();
            if (Enum.IsDefined(typeof(TEnum), element) == false)
                throw new InvalidCastException();

            return (int)Enum.Parse(element.GetType(), element.ToString());
        }

        public static Enum ToEnum<TEnum>(this TEnum element)
            where TEnum : struct
        {
            if (typeof(TEnum).BaseType != typeof(Enum))
                throw new InvalidCastException();
            if (Enum.IsDefined(typeof(TEnum), element) == false)
                throw new InvalidCastException();

            return (Enum)Enum.Parse(element.GetType(), element.ToString());
        }

        public static int EnumToId<TEnum>(this TEnum element)
            where TEnum : struct
        {
            if (!element.GetType().IsEnum)
                throw new ArgumentException(@"Not an enum", nameof(element));

            TEnum[] values = (TEnum[])Enum.GetValues(typeof(TEnum));
            return Array.IndexOf(values, element);
        }
        
        public static string GetDescription<TEnum>(this TEnum element)            
        {
            if (!element.GetType().IsEnum)
                throw new ArgumentException(@"Not an enum", nameof(element));

            FieldInfo fi = element.GetType().GetField(element.ToString());
            if (fi == null)
                return "";

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
                return attributes[0].Description;

            return element.ToString();
        }

        public static IList<TEnum> ToSortedList<TEnum>(this Enum e)
            where TEnum : struct
        {
            return Enum.GetValues(e.GetType()).Cast<TEnum>().OrderBy(x => x.GetDescription()).ToList();
        }
    }
}
