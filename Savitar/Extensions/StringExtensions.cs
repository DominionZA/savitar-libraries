﻿using System;
using System.Globalization;

namespace Savitar.Extensions
{
    public static class StringExtensions
    {
        public static string Append(this string @this, string value, string delimeter)
        {
            var result = @this;

            if (!string.IsNullOrEmpty(delimeter) && !String.IsNullOrEmpty(result))
                result += delimeter;

            result += value;

            return result;
        }

        public static string PadLeft(this int @this, int totalWidth, char paddingChar = '0')
        {
            string value = @this.ToString(CultureInfo.InvariantCulture);
            return value.PadLeft(totalWidth, paddingChar);
        }

        public static string PadLeft(this float @this, int totalWidth, char paddingChar = '0')
        {
            string value = @this.ToString(CultureInfo.InvariantCulture);
            return value.PadLeft(totalWidth, paddingChar);
        }

        public static string PadRight(this int @this, int totalWidth, char paddingChar = '0')
        {
            string value = @this.ToString(CultureInfo.InvariantCulture);
            return value.PadRight(totalWidth, paddingChar);
        }

        public static string ToStringWithNoDashes(this Guid @this)
        {
            var result = @this.ToString();
            result = result.Replace("-", "");
            return result;
        }

        public static int ToInt(this string @this)
        {
            if (int.TryParse(@this, out var result))
                return result;
            return -1;
        }

        public static Guid ToGuid(this string @this)
        {
            return Guid.TryParse(@this, out var result) ? result : Guid.Empty;
        }

        public static T ToValue<T>(this string[] @this, int elementIndex, T defaultValue, IFormatProvider formatProvider = null)
        {
            if (elementIndex >= @this.Length)
                return defaultValue;
            if (@this[elementIndex] == "")
                return defaultValue;

            object result = @this[elementIndex];

            if (typeof(T) == typeof(char) || typeof(T) == typeof(char?))
                result = @this[elementIndex][0];

            //    return (T) Convert.ChangeType(@this[elementIndex][0], typeof (T), CultureInfo.InvariantCulture);
            var returnType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

            if (formatProvider == null)
                formatProvider = CultureInfo.InvariantCulture;

            return (T)Convert.ChangeType(result, returnType, formatProvider);
        }
    }
}