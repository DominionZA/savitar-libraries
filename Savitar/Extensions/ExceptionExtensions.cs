﻿using System;
using System.Text;

namespace Savitar.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetCompleteMessage(this Exception @this)
        {
            var sb = new StringBuilder();

            sb.AppendLine(@this.Message);

            var innerException = @this.InnerException;
            while (innerException != null)
            {
                sb.AppendLine();
                sb.AppendLine(innerException.Message);
                innerException = innerException.InnerException;
            }

            return sb.ToString();
        }
    }
}
