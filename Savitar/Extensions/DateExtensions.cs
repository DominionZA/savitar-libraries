﻿using System;

namespace Savitar.Extensions
{
    public static class DateExtensions
    {
        public static DateTime GetLastDateForDayOfWeek(this DateTime @this, DayOfWeek dayOfWeek)
        {
            // Set result to the last day of the month.
            var result = new DateTime(@this.Year, @this.Month, 1).AddMonths(1).AddDays(-1);

            // Work backwards from the month end looking for the first day that matches dayOfWeek
            while (result.DayOfWeek != dayOfWeek)
                result = result.AddDays(-1);

            return result;
        }

        public static DateTime ClosestDate(this DateTime @this, DayOfWeek dayOfWeek)
        {
            if (@this.DaysFrom(dayOfWeek) < @this.DaysTill(dayOfWeek))
                return @this.PreviousDate(dayOfWeek);

            return @this.NextDate(dayOfWeek);
        }

        public static int DaysFrom(this DateTime @this, DayOfWeek dayOfWeek)
        {
            DateTime dateTime = @this.PreviousDate(dayOfWeek);

            var result = @this - dateTime;

            return result.Days;
        }

        public static int DaysTill(this DateTime @this, DayOfWeek dayOfWeek)
        {
            DateTime dateTime = @this.NextDate(dayOfWeek);

            var result = dateTime - @this;

            return result.Days;
        }

        public static DateTime PreviousDate(this DateTime @this, DayOfWeek dayOfWeek)
        {
            var result = @this.AddDays(-1);
            while (result.DayOfWeek != dayOfWeek)
                result = result.AddDays(-1);

            return result;
        }

        public static DateTime NextDate(this DateTime @this, DayOfWeek dayOfWeek)
        {
            var result = @this.AddDays(1);
            while (result.DayOfWeek != dayOfWeek)
                result = result.AddDays(1);

            return result;
        }

        public static DateTime GetYearStartDate(this DayOfWeek @this, int year)
        {
            // This is a hardcoded part the years that give us trouble with the automatic calculation. This was added on 13/01/2016 to handle the 53 weeks returned for 2015 year. While this is correct according to the ISO8601 standard, Pie City do not use this.
            switch (year)
            {
                case 2015: return new DateTime(2014, 12, 29);
                case 2016: return new DateTime(2015, 12, 28);
                case 2017: return new DateTime(2016, 12, 26);
                case 2018: return new DateTime(2018, 1, 1);
            }

            var result = new DateTime(year, 1, 1);
            return result.DayOfWeek == @this ? result : result.ClosestDate(@this);
        }


        public static DateTime GetYearEndDate(this DayOfWeek @this, int year)
        {
            // Get next years start date.
            var startDate = @this.GetYearStartDate(year + 1);

            // return next years start date less one day.
            return startDate.AddDays(-1);
        }

        public static TimeSpan ToTime(this DateTime @this)
        {
            return new TimeSpan(0, @this.Hour, @this.Minute, @this.Second, @this.Millisecond);
        }

        public static TimeSpan ToTimeHHMMSS(this DateTime @this)
        {
            return new TimeSpan(0, @this.Hour, @this.Minute, @this.Second, 0);
        }

        public static DateTime ToDate(this DateTime @this)
        {
            return new DateTime(@this.Year, @this.Month, @this.Day);
        }
    }
}