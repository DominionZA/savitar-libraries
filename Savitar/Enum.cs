using System;
using System.Collections.Generic;
using System.Text;

namespace Savitar
{
  public class Description : Attribute
  {
    public string Text;
    public Description(string text)
    {
      Text = text;
    }
  }

  public class Enum
  {
    public static string GetDesription(System.Enum en)
    {
      Type type = en.GetType();

      System.Reflection.MemberInfo[] memInfo = type.GetMember(en.ToString());

      if (memInfo != null && memInfo.Length > 0)
      {
        object[] attrs = memInfo[0].GetCustomAttributes(typeof(Description), false);

        if (attrs != null && attrs.Length > 0)
          return ((Description)attrs[0]).Text;
      }

      return en.ToString();
    }
  }
}
