using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Savitar
{
    /// <summary>
    /// Object class for handling object related functions.
    /// </summary>
    public class Object : object
    {
        public static object Deserialize(XmlDocument xml, Type type)
        {
            return Deserialize(xml.OuterXml, type);
        }

        public static object Deserialize(string xml)
        {
            return null;
        }

        public static object Deserialize(string xml, Type type)
        {
            var s = new XmlSerializer(type);

            var buffer = Encoding.UTF8.GetBytes(xml);

            var ms = new MemoryStream(buffer);
            var reader = new XmlTextReader(ms);

            try
            {
                return s.Deserialize(reader);
            }
            finally
            {
                reader.Close();
            }
        }

        public static string SerializeToString(object o)
        {
            var s = new XmlSerializer(o.GetType());

            var ms = new MemoryStream();
            var writer = new Xml.XmlTextWriter(ms, new UTF8Encoding())
            {
                Formatting = Formatting.Indented, IndentChar = ' ', Indentation = 2
            };

            try
            {
                s.Serialize(writer, o);

                return Encoding.UTF8.GetString(ms.ToArray());
            }
            finally
            {
                writer.Close();
                ms.Close();
            }
        }

        public static XmlDocument SerializeToXml(object o)
        {
            var xml = new XmlDocument();
            xml.LoadXml(SerializeToString(o));

            return xml;
        }

        public static T Load<T>(string fileName)
        {
            return (T)Load(fileName, typeof (T));
        }

        public static object Load(string fileName, Type type)
        {
            if (!File.Exists(fileName))            
                return null; //System.Activator.CreateInstance(type);      

            try
            {
                var reader = new XmlSerializer(type);

                // Read the XML file.
                var file = new StreamReader(fileName);

                try
                {
                    return reader.Deserialize(file);
                }
                finally
                {
                    file.Close();
                }
            }
            catch(Exception ex)
            {
                var message = "Error deserializing " + Path.GetFileName(fileName) + " to " + type + "\r\n\r\n" + ex.Message;
                throw new Exception(message);
            }
        }

        public static object LoadCompressed(string filename, Type type)
        {
            if (!File.Exists(filename))
                return null;

            var fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            try
            {
                using (Stream input = new GZipStream(fs, CompressionMode.Decompress))
                {
                    var serializer = new XmlSerializer(type);
                    return serializer.Deserialize(input);
                }
            }
            finally
            {
                fs.Close();
            }
        }        

        public static void Save(string filename, object objectInstance)
        {
            if (File.Exists(filename))
                File.Delete(filename);

            var xns = new XmlSerializerNamespaces();
            xns.Add("", "");

            var xmlWriterSettings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };

            var stringWriter = new StringWriter();
            var xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings);
            var xmlSerializer = new XmlSerializer(objectInstance.GetType());
            xmlSerializer.Serialize(xmlWriter, objectInstance, xns);

            File.WriteAllText(filename, stringWriter.ToString());
        }

        public static void SaveCompressed(string fileName, object objectInstance)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);

            var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
            try
            {
                using (var gzStream = new GZipStream(fs, CompressionMode.Compress))
                {
                    var serializer = new XmlSerializer(objectInstance.GetType());
                    serializer.Serialize(gzStream, objectInstance);
                }
            }
            finally
            {
                fs.Close();
            }
        }
    }
}
