﻿using System;
using System.Collections;
using System.Globalization;

namespace Savitar
{
    [Obsolete("Most of the functionality in here is now default in the framework.")]
    public class SavString
    {
        public static bool SameText(string value1, string value2)
        {
            return value1.Equals(value2, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string GetNextTagValue(string str, string startTag, string endTag)
        {
            int iStart = str.IndexOf(startTag, 1, StringComparison.Ordinal);
            if (iStart <= 0)
                return "";

            // We have a tag... Now to find the end tag
            int iEnd = str.IndexOf(endTag, iStart, StringComparison.Ordinal);
            if (iEnd <= 0)
                return "";

            return str.Substring(iStart + startTag.Length, iEnd - (iStart + startTag.Length));
        }

        public static string ReplaceTagWithHTMLTag(string str)
        {
            var result = str;
            var s = GetNextTagValue(result, "[URL]", "[/URL]");
            while (s != "")
            {
                result = result.Replace("[URL]" + s + "[/URL]", "<a target=\"_blank\" href=\"" + s + "\">" + s + "</a>");
                s = GetNextTagValue(result, "[URL]", "[/URL]");
            }
            s = GetNextTagValue(result, "[EMAIL]", "[/EMAIL]");
            while (s != "")
            {
                result = result.Replace("[EMAIL]" + s + "[/EMAIL]", "<a href=\"mailto:" + s + "\">" + s + "</a>");
                s = GetNextTagValue(result, "[EMAIL]", "[/EMAIL]");
            }
            result = result.Replace("\r\n", "<br>");
            result = result.Replace("[BR]", "<br>");
            result = result.Replace("[B]", "<b>");
            result = result.Replace("[/B]", "</b>");
            return result;
        }

        public static string IncAlphaStr(string value)
        {
            int i;
            string remaining = null;
            string s;
            s = "";
            for (i = value.Length; i >= 1; i--)
            {
                var cChar = value[i];
                if (!(new ArrayList(new[] { 'a', 'A' }).Contains(cChar)))
                    throw new Exception("IncAlphaStr only handles incrementing A..Z letters");

                remaining = i > 1 ? value.Substring(1 - 1, i - 1) : "";

                if (cChar.ToString().ToUpper() == "Z")
                {
                    cChar = cChar == 'z' ? 'a' : 'A';

                    s = cChar + s;
                }
                else
                {
                    cChar = (char)(cChar + 1);
                    s = cChar + s;
                    break;
                }
            }

            return remaining + s;
        }

        public static string IncNumStr(string value)
        {
            int i;
            string sRemaining = null;
            var s = "";
            for (i = value.Length; i >= 1; i--)
            {
                var cChar = value[i];
                sRemaining = i > 1 ? value.Substring(1 - 1, i - 1) : "";
                if (!(cChar >= '0' && cChar <= '9'))
                {
                    throw new Exception("IncAlphaStr only handles incrementing numbers");
                }
                if (cChar == '9')
                {
                    cChar = '0';
                    s = cChar + s;
                }
                else
                {
                    cChar = (char)(cChar + 1);
                    s = cChar + s;
                    break;
                }
            }

            return sRemaining + s;
        }

        public static string IncStr(string value)
        {
            int i;
            string sRemaining = null;
            var s = "";
            for (i = value.Length; i >= 1; i--)
            {
                var c = value[i];
                sRemaining = i > 1 ? value.Substring(1 - 1, i - 1) : "";
                if (c >= '0' && c <= '9')
                {
                    if (c == '9')
                    {
                        c = '0';
                        s = c + s;
                    }
                    else
                    {
                        c = (char)(c + 1);
                        s = c + s;
                        break;
                    }
                }
                else
                {
                    if (c.ToString().ToUpper() == "Z")
                    {
                        c = c == 'z' ? 'a' : 'A';
                        s = c + s;
                    }
                    else
                    {
                        c = (char)(c + 1);
                        s = c + s;
                        break;
                    }
                }
            }
            return sRemaining + s;
        }

        public static string PadC(string value, int totalLength, char padChar)
        {
            var result = value;
            if (result.Length >= totalLength)
            {
                return result;
            }
            // Get the current length of the string
            var length = result.Length;
            // Pad the left side
            result = PadL(result, (totalLength - length) / 2, padChar);
            // Pad the right side
            result = PadR(result, totalLength, padChar);
            while (result.Length < totalLength)
            {
                result = padChar + result;
            }
            return result;
        }

        public static string PadL(string value, int totalLength, char padChar)
        {
            var result = value;
            if (result.Length >= totalLength)
            {
                return result;
            }
            while (result.Length < totalLength)
            {
                result = padChar + result;
            }
            return result;
        }

        public static string PadR(string value, int totalLength, char padChar)
        {
            var result = value;
            if (result.Length >= totalLength)
            {
                return result;
            }
            while (result.Length < totalLength)
            {
                result += padChar;
            }
            return result;
        }

        public static DateTime HHMMToTime(string value)
        {
            var result = DateTime.MinValue;

            if (value == "")
                return result;

            var hour = 0;
            var min = 0;
            var sec = 0;
            var milliSec = 0;

            if (value.Length >= 2)
                hour = Convert.ToInt16(value.Substring(1, 2));
            if (value.Length >= 4)
                min = Convert.ToInt16(value.Substring(3, 2));
            if (value.Length >= 6)
                sec = Convert.ToInt16(value.Substring(5, 2));
            if (value.Length >= 8)
                milliSec = Convert.ToInt16(value.Substring(7, 2));

            return new DateTime(DateTime.MinValue.Year, DateTime.MinValue.Month, DateTime.MinValue.Day, hour, min, sec, milliSec);
        }

        public static DateTime YYYYMMDDToDate(string value)
        {
            var result = DateTime.MinValue;
            if (value == "")
                return result;

            int year = Convert.ToInt16(value.Substring(1, 4));
            int month = Convert.ToInt16(value.Substring(5, 2));
            int day = Convert.ToInt16(value.Substring(7, 2));

            return new DateTime(year, month, day);
        }

        // This function can only take numeric values as the value. IE: Do not pass '23 Sep 2004'.
        public static DateTime StrToDate(string value, string format)
        {
            int i;
            var yearStr = "";
            var monthStr = "";
            var dayStr = "";

            for (i = 1; i <= value.Length; i++)
            {
                if ((new ArrayList(new[] { 'Y', 'y' }).Contains(value[i])))
                    yearStr = yearStr + value[i];
                else
                {
                    if ((new ArrayList(new[] { 'M', 'm' }).Contains(value[i])))
                        monthStr += value[i];
                    else
                    {
                        if ((new ArrayList(new[] { 'D', 'd' }).Contains(value[i])))
                            dayStr += value[i];
                    }
                }
            }

            int year = Convert.ToInt16(yearStr);
            int month = Convert.ToInt16(monthStr);
            int day = Convert.ToInt16(dayStr);

            return new DateTime(year, month, day);
        }

        public static bool InStr(string value, string[] items)
        {
            value = value.ToUpper();

            for (var i = items.GetLowerBound(0); i <= items.GetUpperBound(0); i++)
            {
                if ((value.IndexOf(items[i].ToUpper(), StringComparison.Ordinal)) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static string ReplaceChr(string src, char oldChar, char newChar)
        {
            int i;
            var result = "";
            for (i = 1; i <= src.Length; i++)
            {
                if (src[i] == oldChar)
                    result += newChar;
                else
                    result += src[i];
            }
            return result;
        }

        public static string ToStr(int value, int emptyStringValue)
        {
            if (value == emptyStringValue)
                return "";

            return value.ToString();
        }

        public static string ToStr(long value, long emptyStringValue)
        {
            if (value == emptyStringValue)
                return "";

            return value.ToString();
        }

        public static string ToStr(short value, short emptyStringValue)
        {
            if (value == emptyStringValue)
                return "";

            return value.ToString();
        }

        public static string ToStr(float value, float emptyStringValue)
        {
            if (value == emptyStringValue)
                return "";

            return value.ToString(CultureInfo.InvariantCulture);
        }

        public static string ToStr(decimal value, decimal emptyStringValue)
        {
            if (value == emptyStringValue)
                return "";

            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}
