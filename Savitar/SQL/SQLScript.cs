﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Savitar.SQL
{
    public class SqlScript
    {
        public static string[] ParseScriptToCommands(string strScript)
        {
            return Regex.Split(strScript, "GO\r\n", RegexOptions.IgnoreCase);
        }

        public static int ExecuteFile(string connectionString, string filename)
        {
            if (!File.Exists(filename))
                throw new Exception("Migration script execution failed as the script file cannot be found.\r\n\r\nScript File: " + filename);
            
            var fileInfo = new FileInfo(filename);
            var reader = fileInfo.OpenText();

            try
            {
                string script = reader.ReadToEnd();
                return ExecuteScript(connectionString, script);
            }
            finally
            {
                reader.Close();
            }
        }

        public static int ExecuteScript(string connectionString, string script)
        {
            var commands = ParseScriptToCommands(script);

            var sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

            var sqlCommand = new SqlCommand {CommandTimeout = 500, Connection = sqlConnection};

            var successCount = 0;
            foreach (var command in commands)
            {
                if (string.IsNullOrEmpty(command))
                    continue;

                sqlCommand.CommandText = command;
                try
                {
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine("An error has occurred");
                    sb.AppendLine(ex.Message);
                    sb.AppendLine();
                    sb.AppendLine(command);

                    throw new Exception(sb.ToString());
                }

                

                successCount++;
            }

            return successCount;
        }
    }
}