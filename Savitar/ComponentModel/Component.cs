namespace Savitar.ComponentModel
{
    public class Component
    {
        /// <summary>
        /// Static method to determine if the application is running in the context of the IDE (Design Mode).
        /// Use this over the internal this.DesignMode as the internal method is not accurate
        /// </summary>
        public static bool IsInDesignMode => System.Diagnostics.Process.GetCurrentProcess().ProcessName.ToUpper().Equals("DEVENV");
    }
}