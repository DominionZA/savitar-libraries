﻿using System.ComponentModel;
using System.Globalization;

namespace Savitar.ComponentModel
{
    public class ItemListStringConverter : StringConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, System.Type destinationType)
        {
            if (destinationType == typeof(System.String) && value is System.Collections.IList)
            {
                System.Collections.IList items = (System.Collections.IList)value;
                int itemCount = items.Count;

                if (itemCount == 0)
                    return "No items";
                if (itemCount == 1)
                    return "1 item";
                
                return $"{itemCount} items";
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, System.Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }
    }
}