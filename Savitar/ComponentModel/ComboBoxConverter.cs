﻿using System;
using System.ComponentModel;

namespace Savitar.ComponentModel
{
    public class ComboBoxConverter : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true; // true means show a combobox
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true; // true will limit to list. false will show the list, but allow free-form entry
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            throw new NotImplementedException(GetType() + " does not implemented GetStandardValues for context.Instance " + context.Instance);
        }
    }
}