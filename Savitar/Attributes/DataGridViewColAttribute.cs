﻿using System;

namespace Savitar.Attributes
{
    public enum PDContentAlignment { NotSet = 0, Left, Center, Right }

    [AttributeUsage(AttributeTargets.Property)]
    public class DataGridViewColAttribute : Attribute
    {
        private bool isVisible;
        private int width;

        public bool IsWidthSet { get; private set; } = false;

        public bool IsVisibleSet { get; private set; } = false;

        public bool IsAlignmentSet { get; private set; } = false;

        public bool IsReadOnlySet { get; private set; } = false;

        public bool IsVisible
        {
            get => isVisible;
            set { IsVisibleSet = true; isVisible = value; }
        }

        public int Width
        {
            get => width;
            set { IsWidthSet = true; width = value; }
        }

        public PDContentAlignment Alignment
        {
            get => alignment;
            set { IsAlignmentSet = true; alignment = value; }
        }

        private PDContentAlignment alignment = PDContentAlignment.NotSet;

        public bool ReadOnly
        {
            get => _ReadOnly;
            set { IsReadOnlySet = true; _ReadOnly = value; }
        }
        bool _ReadOnly = false;

        public DataGridViewColAttribute() { }
    }
}