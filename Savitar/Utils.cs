﻿using System;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Linq;

namespace Savitar
{
    public class Utils
    {
        public static int GetValue(int? value, int defaultValue)
        {
            return value ?? defaultValue;
        }

        public static double GetValue(double? value, double defaultValue)
        {
            return value ?? defaultValue;
        }

        public static decimal GetValue(decimal? value, decimal defaultValue)
        {
            return value ?? defaultValue;
        }

        public static bool GetValue(bool? value, bool defaultValue)
        {
            return value ?? defaultValue;
        }

        public static Version GetDotNetVersion()
        {
            try
            {
                var items = new List<Version>();

                // (Note that lbInstVersions is a listbox placed on a WinForm or Web Form.)
                string componentsKeyName = "SOFTWARE\\Microsoft\\Active Setup\\Installed Components";
                // Find out in the registry anything under:
                //    HKLM\SOFTWARE\Microsoft\Active Setup\Installed Components
                // that has ".NET Framework" in the name
                var componentsKey = Registry.LocalMachine.OpenSubKey(componentsKeyName);
                if (componentsKey != null)
                {
                    var instComps = componentsKey.GetSubKeyNames();
                    items.AddRange(from instComp in instComps
                        select componentsKey.OpenSubKey(instComp)
                        into key
                        let friendlyName = (string) key?.GetValue(null)
                        where friendlyName != null && friendlyName.IndexOf(".NET Framework", StringComparison.Ordinal) >= 0
                        select (string) key.GetValue("Version")
                        into version
                        select version.Replace(",", ".")
                        into version
                        select new Version(version));
                }

                if (items.Count == 0)
                    return new Version();

                // Now order them and get the last. This will give us the latest version installed on the system.
                var latest = (
                  from item in items
                  orderby item descending
                  select item).Take(1).SingleOrDefault();

                return latest;
            }
            catch
            {
                return new Version();
            }
        }
    }
}