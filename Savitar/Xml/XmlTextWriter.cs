using System.Text;

namespace Savitar.Xml
{
    public class XmlTextWriter : System.Xml.XmlTextWriter
    {
        public XmlTextWriter(System.IO.TextWriter w) : base(w) { }
        public XmlTextWriter(System.IO.Stream w, Encoding encoding) : base(w, encoding) { }
        public XmlTextWriter(string filename, Encoding encoding) : base(filename, encoding) { }

        bool skip;

        // This is overridden to remove the <?xml crap inserted at the top of the document
        public override void WriteStartDocument()
        {
        }

        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            if (prefix == "xmlns" && (localName == "xsd" || localName == "xsi")) // Omits XSD and XSI declarations. 
            {
                skip = true;
                return;
            }

            base.WriteStartAttribute(prefix, localName, ns);
        }

        public override void WriteString(string text)
        {
            if (skip)
                return;

            base.WriteString(text);
        }

        public override void WriteEndAttribute()
        {
            if (skip)
            { // Reset the flag, so we keep writing. 
                skip = false;
                return;
            }

            base.WriteEndAttribute();
        }
    }
}