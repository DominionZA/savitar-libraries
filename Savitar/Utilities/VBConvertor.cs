﻿namespace Savitar.Utilities
{
    public class VBConvertor
    {
        public static bool FromVBBool(int? val)
        {
            return val.HasValue && (val.Value < 0);
        }

        public static int ToVBBool(bool value)
        {
            return value ? -1 : 0;

        }
    }
}
