namespace Savitar.Utilities
{
    /// <summary>
    /// Generic class for handling a list of objects that you may want to iterate through (like a browsers Back/Next buttons
    /// </summary>
    /// <typeparam name="T">The type of object you want to be maintained in the history list</typeparam>
    public abstract class UndoRedo<T>
    {
        private int currentIndex = -1;
        private readonly T defaultItem;
        private readonly System.Collections.ArrayList historyList = new System.Collections.ArrayList();

        /// <summary>
        /// Constructor for UndoRedo. Requires an instance of the object to maintain to be used as the default item. When clearing the list, the default item is place back in the list in first position. Pass null if you do not want a default item.
        /// </summary>
        /// <param name="defaultItem"></param>
        protected UndoRedo(T defaultItem)
        {
            this.defaultItem = defaultItem;
            this.Add(defaultItem);
        }

        /// <summary>
        /// Adds a new item to the list and sets the Current property to the newly added item.
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            if (item == null)
                return;

            while (historyList.Count - 1 > currentIndex)
                historyList.RemoveAt(historyList.Count - 1);

            historyList.Add(item);
            this.GoNext();
        }

        /// <summary>
        /// Moves to the next item in the list (.Current) if there is another item.
        /// </summary>
        /// <returns>True if moved, else False</returns>
        public bool GoNext()
        {
            if (this.EOF)
                return false;

            currentIndex++;
            return true;
        }

        /// <summary>
        /// Moves to the previous item in the list (.Current) if there is a previous item.
        /// </summary>
        /// <returns>True if moved, else False</returns>
        public bool GoPrevious()
        {
            if (this.BOF)
                return false;

            currentIndex--;
            return true;
        }

        /// <summary>
        /// The current item in the list. If you did not supply a default item in the constructor, then this will be null if there are no items in the list
        /// </summary>
        public T Current
        {
            get
            {
                if ((historyList.Count == 0) || (currentIndex < 0))
                    return default(T);

                return (T)historyList[currentIndex];
            }
        }

        public T First => defaultItem;

        /// <summary>
        /// Clears the history list. If a default item is supplied in the constructor, then this is inserted into the list after all items are cleared and Current is set to this item.
        /// </summary>
        public void Clear()
        {
            historyList.Clear();
            currentIndex = -1;
            this.Add(defaultItem);
        }

        /// <summary>
        /// Indicates that we are at the beginning of the list and cannot go back anymore.
        /// </summary>
        public bool BOF => (historyList.Count == 0) || (this.currentIndex <= 0);


        /// <summary>
        /// Indicates that we are at the end of the list and cannot go forward anymore.
        /// </summary>
        public bool EOF => (historyList.Count == 0) || (this.currentIndex >= historyList.Count - 1);
    }
}