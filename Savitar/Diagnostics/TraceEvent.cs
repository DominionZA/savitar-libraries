﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Savitar.Diagnostics
{
    public static class TraceEvent
    {
        public static void Write(TraceSeverity sev, string format, params object[] items)
        {

            try
            {
                if (String.IsNullOrEmpty(Thread.CurrentThread.Name))
                    Trace.WriteLine(String.Format(format, items), Enum.GetName(typeof(TraceSeverity), sev));
                else
                    Trace.WriteLine(String.Format("<TH:{0}> ", Thread.CurrentThread.Name)
                                        + String.Format(format, items), Enum.GetName(typeof(TraceSeverity), sev));

            }
            catch (Exception ex)
            {
                Trace.TraceError("Unable to trace, {0}", ex);
            }
        }
    }

    public enum TraceSeverity
    {
        Information,
        Warning,
        Error,
        Critical
    }
}
