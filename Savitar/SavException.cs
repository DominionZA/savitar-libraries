﻿using System;
using System.Runtime.Serialization;

namespace Savitar
{
    [Serializable]
    public class SavException : Exception
    {
        public object Host { get; set; }
        public string Method { get; set; }

        protected SavException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public SavException(object host, string method, string message)
            : base(message)
        {
            if (string.IsNullOrEmpty(method))
                throw new Exception("Savitar.SavException(): method arguement cannot be null or empty");

            Host = host ?? throw new Exception("Savitar.SavException(): host arguement cannot be null");
            Method = method;
        }

        public override string Message => base.Message + "\n\r\n\r" + "Occurred in " + Host.GetType() + "." + Method;
    }
}