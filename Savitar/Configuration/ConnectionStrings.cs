﻿using System;
using System.Configuration;
using System.Reflection;

namespace Savitar.Configuration
{
    public static class ConnectionStrings
    {
        private static FieldInfo _elementField;
        private static FieldInfo _collectionField;

        private static void EnsureFieldInfo()
        {
            if (_elementField != null)
                return;

            //new ReflectionPermission(ReflectionPermissionFlag.MemberAccess | ReflectionPermissionFlag.RestrictedMemberAccess).Assert();

            var flags = BindingFlags.Instance | BindingFlags.NonPublic;

            try
            {
                _elementField = typeof(ConfigurationElement).GetField("_bReadOnly", flags);
                _collectionField = typeof(ConfigurationElementCollection).GetField("bReadOnly", flags);
            }

            finally
            {
                // CodeAccessPermission.RevertAssert();
            }

            if (_elementField == null)
                throw new MemberAccessException(string.Format(null, "The operation to access field '{0}' failed.", "_bReadOnly"));

            if (_collectionField == null)
                throw new MemberAccessException(string.Format(null, "The operation to access field '{0}' failed.", "bReadOnly"));
        }

        private static void SetElementIsReadOnly(ConfigurationElement element, bool readOnly)
        {
            _elementField.SetValue(element, readOnly);
        }

        private static void SetCollectionIsReadOnly(ConfigurationElementCollection collection, bool readOnly)
        {
            SetElementIsReadOnly(collection, readOnly);
            _collectionField.SetValue(collection, readOnly);
        }

        public static void SetConnectionString(string name, string connectionString)
        {
            ConnectionStringSettings connectionStringSettings = null;

            foreach (ConnectionStringSettings item in ConfigurationManager.ConnectionStrings)
            {
                if (item.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    connectionStringSettings = item;
                    break;
                }
            }

            if (connectionStringSettings == null)
            {
                AddConnectionString(name, connectionString);
                return;
            }

            var field = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
            field.SetValue(connectionStringSettings, false);
            connectionStringSettings.ConnectionString = connectionString;
        }

        public static void AddConnectionString(string name, string connectionString)
        {
            ConnectionStringSettings settings = new ConnectionStringSettings(name, connectionString);

            if (!ConfigurationManager.ConnectionStrings.IsReadOnly())
            {
                ConfigurationManager.ConnectionStrings.Add(settings);
                return;
            }

            EnsureFieldInfo();

            //            new ReflectionPermission(ReflectionPermissionFlag.MemberAccess | ReflectionPermissionFlag.RestrictedMemberAccess).Assert();

            try
            {
                SetCollectionIsReadOnly(ConfigurationManager.ConnectionStrings, false);
                ConfigurationManager.ConnectionStrings.Add(settings);
                SetCollectionIsReadOnly(ConfigurationManager.ConnectionStrings, true);
            }
            finally
            {
                //CodeAccessPermission.RevertAssert();
            }
        }
    }
}
