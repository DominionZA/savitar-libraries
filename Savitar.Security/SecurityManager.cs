﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Diagnostics;

namespace Savitar.Security
{
    internal class SecurityManager : IUserSecurity
    {
        #region IUserSecurity Members

        public bool IsAuthenticated { get; private set; }

        public void Logout()
        {
            IsAuthenticated = false;
        }

        public bool AuthenticateUser(string userName, string password)
        {
            try
            {
                var credentials = new NamePasswordCredential(userName, password);

                IsAuthenticated = AuthProvider.Authenticate(credentials, out identity);
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("AuthenticateOperator failed, {0}", ex);
            }

            return IsAuthenticated;
        }

        public bool AuthorizeUser(string rule)
        {
            try
            {
                IPrincipal principal = new GenericPrincipal(identity, GetUserRoles());

                return RuleProvider.Authorize(principal, rule);
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("AuthorizeOperator failed, {0}", ex);
                return false;
            }
        }

        public string[] GetAllRoles()
        {
            try
            {
                return RoleProvider.GetAllRoles();
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("AuthorizeOperator failed, {0}", ex);
                throw;
            }
        }

        public string[] GetUserRoles()
        {
            return GetUserRoles(identity);
        }

        public string[] GetUserRoles(IIdentity ident)
        {
            try
            {
                string[] roles = GetAllRoles();
                List<string> opRoles = new List<string>();

                IPrincipal principal = RoleProvider.GetRoles(ident);

                foreach (string role in roles)
                {
                    if (principal.IsInRole(role))
                        opRoles.Add(role);
                }

                return opRoles.ToArray();
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("GetOperatorRoles failed, {0}", ex);
                throw;
            }
        }

        public void AddUser(string userName, string password)
        {
            try
            {
                Persistance.Save(new NamePasswordCredential(userName, password));
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("AddUser failed, {0}", ex);
                throw;
            }
        }

        public void AddUser(string userName, string password, object info)
        {
            try
            {
                Persistance.Save(new NamePasswordCredential(userName, password), info);
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("AddUser failed, {0}", ex);
                throw;
            }
        }

        public void DeleteUser(string userName)
        {
            try
            {
                Persistance.Delete(new GenericIdentity(userName));
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("DeleteUser failed, {0}", ex);
                throw;
            }
        }

        public void WriteProfile(IIdentity ident, object info)
        {
            try
            {
                ProfileProvider.SetProfile(ident, info);
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("WriteProfile failed, {0}", ex);
                throw;
            }
        }

        public void WriteProfile(object info)
        {
            WriteProfile(identity, info);
        }

        public object ReadProfile()
        {
            return ReadProfile(identity);
        }

        public object ReadProfile(IIdentity ident)
        {
            object info;
            try
            {
                info = ProfileProvider.GetProfile(ident);
            }
            catch (Exception ex)
            {
                Trace.TraceWarning("ReadProfile failed, {0}", ex);
                throw;
            }

            return info;
        }

        public object TryReadProfile()
        {
            return TryReadProfile(identity);
        }

        public object TryReadProfile(IIdentity ident)
        {
            object info = null;
            try
            {
                info = ReadProfile(ident);
            }
            catch
            {
                // Ignore
            }

            return info;
        }

        public IIdentity Identity => identity;

        #endregion

        #region Private members

        private IIdentity identity;

        #endregion

        #region ISecurityProvider Members

        public IAuthenticationProvider AuthProvider { get; set; }

        public IAuthorizationProvider RuleProvider { get; set; }

        public IRolesProvider RoleProvider { get; set; }

        public IProfileProvider ProfileProvider { get; set; }

        public ISecurityCacheProvider CacheProvider { get; set; }

        public IPersistanceProvider Persistance { get; set; }

        #endregion
    }
}
