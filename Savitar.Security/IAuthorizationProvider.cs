﻿namespace Savitar.Security
{
    public interface IAuthorizationProvider
    {
        bool Authorize(System.Security.Principal.IPrincipal principal, string rule);
    }
}