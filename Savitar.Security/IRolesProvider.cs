﻿namespace Savitar.Security
{
    public interface IRolesProvider
    {
        System.Security.Principal.IPrincipal GetRoles(System.Security.Principal.IIdentity ident);

        string[] GetAllRoles();
    }
}