﻿using System.Security.Principal;

namespace Savitar.Security
{
    public interface IAuthenticationProvider
    {
        bool Authenticate(NamePasswordCredential credentials, out IIdentity identity);
    }
}