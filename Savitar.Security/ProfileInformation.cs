﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savitar.Security
{
    public class ProfileInformation
    {
        private string telnumber;
        private string ext;

        public ProfileInformation(string telnumber, string ext)
        {
            this.telnumber = telnumber;
            this.ext = ext;
        }
    }
}
