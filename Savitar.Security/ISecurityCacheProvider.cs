﻿namespace Savitar.Security
{
    public interface ISecurityCacheProvider
    {
        object SaveIdentity(System.Security.Principal.IIdentity iIdentity);
    }
}