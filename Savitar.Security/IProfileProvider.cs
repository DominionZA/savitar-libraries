﻿namespace Savitar.Security
{
    public interface IProfileProvider
    {

        object GetProfile(System.Security.Principal.IIdentity ident);

        void SetProfile(System.Security.Principal.IIdentity iIdentity, object info);
    }
}