﻿namespace Savitar.Security
{
    public class NamePasswordCredential
    {
        public string Name { get; set; }

        public string Password { get; set; }

        public NamePasswordCredential(string name, string password)
        {
            Name = name;
            Password = password;
        }

    }
}
