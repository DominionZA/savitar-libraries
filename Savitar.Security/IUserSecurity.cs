﻿using System.Security.Principal;

namespace Savitar.Security
{
    public interface ISecurityProvider
    {
        IAuthenticationProvider AuthProvider { get; set; }
        IAuthorizationProvider RuleProvider { get; set; }
        IRolesProvider RoleProvider { get; set; }
        IProfileProvider ProfileProvider { get; set; }
        ISecurityCacheProvider CacheProvider { get; set; }
        IPersistanceProvider Persistance { get; set; }
    }

    public interface IUserSecurity : ISecurityProvider
    {
        bool IsAuthenticated { get; }
        bool AuthenticateUser(string userName, string password);
        bool AuthorizeUser(string rule);
        void Logout();

        void AddUser(string userName, string password);
        void AddUser(string userName, string password, object info);
        void DeleteUser(string userName);

        void WriteProfile(object info);
        void WriteProfile(IIdentity id, object info);

        object ReadProfile();
        object ReadProfile(IIdentity ident);
        object TryReadProfile();
        object TryReadProfile(IIdentity ident);

        string[] GetUserRoles();
        string[] GetUserRoles(IIdentity ident);
        string[] GetAllRoles();

        IIdentity Identity
        {
            get;
        }

        //TODO: implement this lot (when time allows)
        //void SaveIdentity();
        //void RestoreIdentity();

        //string[] GetAllUsers();
        //string[] GetAllRules();

        //int GetUserCount(string role);
        //bool IsInRole(string role);
        //void AddUser(string userName, string password, string role);
        //void AddUser(string userName, string password, string role, object info);
        //void AddRole(string userName, string role);
    }
}