﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

namespace Savitar.Security
{
    public interface IPersitanceProvider
    {
        void Save(NamePasswordCredential credential, object info);
        void Save(NamePasswordCredential credential);
        void Delete(GenericIdentity identity);
    }
}
