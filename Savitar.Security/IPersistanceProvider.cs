﻿using System.Security.Principal;

namespace Savitar.Security
{
    public interface IPersistanceProvider
    {
        void Save(NamePasswordCredential credential, object info);
        void Save(NamePasswordCredential credential);
        void Delete(GenericIdentity identity);
    }
}