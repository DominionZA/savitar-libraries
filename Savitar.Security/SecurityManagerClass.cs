﻿namespace Savitar.Security
{
    public class SecurityManagerClass
    {
        private SecurityManagerClass()
        { }

        public static IUserSecurity CreateSecurityManager()
        {
            return new SecurityManager();

            //TODO: assign default providers
        }

        public static IUserSecurity CreateSecurityManager(IAuthenticationProvider authProvider,
                                                        IAuthorizationProvider ruleProvider,
                                                        IRolesProvider roleProvider,
                                                        IProfileProvider profileProvider,
                                                        ISecurityCacheProvider cacheProvider,
                                                        IPersistanceProvider persistanceProvider)
        {
            var sm = new SecurityManager
            {
                AuthProvider = authProvider,
                CacheProvider = cacheProvider,
                ProfileProvider = profileProvider,
                RoleProvider = roleProvider,
                RuleProvider = ruleProvider,
                Persistance = persistanceProvider
            };
            return sm;
        }

        public static IUserSecurity GetSecurityManager()
        {
            return _secManager ?? (_secManager = new SecurityManager());
        }

        public static IUserSecurity GetSecurityManager(IAuthenticationProvider authProvider,
                                                        IAuthorizationProvider ruleProvider,
                                                        IRolesProvider roleProvider,
                                                        IProfileProvider profileProvider,
                                                        ISecurityCacheProvider cacheProvider,
                                                        IPersistanceProvider persistanceProvider)
        {
            if (_secManager == null)
            {
                _secManager = new SecurityManager();
                _secManager.AuthProvider = authProvider;
                _secManager.CacheProvider = cacheProvider;
                _secManager.ProfileProvider = profileProvider;
                _secManager.RoleProvider = roleProvider;
                _secManager.Persistance = persistanceProvider;
                _secManager.RuleProvider = ruleProvider;
            }

            return _secManager;
        }

        private static SecurityManager _secManager;
    }
}
